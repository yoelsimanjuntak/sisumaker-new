<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache,no-store,must-revalidate">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="expires" content="0">
        <title><?php echo $title; ?></title>
        <?php
			$base_url = base_url();
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.red.css");
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "jquery.ui.css");
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "responsive-tables.css");
			echo $this->lib_load_css_js->load_css($base_url, "assets/prettify/", "prettify.css");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-1.9.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.1.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.slimscroll.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "function.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.validate.min.js");
			?>
			<script type="text/javascript">base='<?php echo $base_url;?>';</script> 
			<!--[if lte IE 8]><?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "excanvas.min.js");?><![endif]-->
			<!--<?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "elements.js");?>-->
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery('body').removeClass('chatenabled');
				})
			</script>
	</head>
	<body class="depan">
		<div class="maincontentinner">
			<?php $this->load->view($this->view.$content);?>
		</div>
	</body>
</html>