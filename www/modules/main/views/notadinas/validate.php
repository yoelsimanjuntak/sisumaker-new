<script type="text/javascript">
	function dn_file()
	{
		jQuery('#v_dn_file').fadeOut()
		if(jQuery('#id_jnssurat').val() != '')
		{
			jQuery('#v_dn_file').load('<?php echo base_url().$this->page;?>download_file/' + jQuery('#id_jnssurat').val()).fadeIn();
		}
		return false;
	}
	
	fancybox();
	jQuery(document).ready(function(){
		jQuery('#id_compose').live('click', function(){
			n = jQuery(this).val();
			if(n == 1){
				jQuery('#no_surat').show();
				jQuery('#v_pegawaittd').hide();
			}else{
				jQuery('#v_pegawaittd').show();
				jQuery('#no_surat').hide();
			}
			jQuery('#compose_caption').html(jQuery(this).attr('tt'));
		});
		
		function check()
		{
			c_skpd = jQuery('#c_skpd').val();
			c_kecamatan = jQuery('#c_kecamatan').val();
			c_kelurahan = jQuery('#c_kelurahan').val();
			c_puskesmas = jQuery('#c_puskesmas').val();
			c_sekolah = jQuery('#c_sekolah').val();
			c_rs = jQuery('#c_rs').val();
			c_uptpendidikan = jQuery('#c_uptpendidikan').val();
			jQuery('#view_list_pegawai').load(base + '<?php echo $this->page;?>list_skpd/' + c_skpd + '/' + c_kecamatan + '/' + c_kelurahan + '/' + c_puskesmas + '/' + c_sekolah + '/' + c_rs + '/' + c_uptpendidikan);
		}
		
		jQuery('#check_skpd').on('click', function(){
			jQuery('#c_skpd').val('1-3');
			check();
		});
		
		jQuery('#check_kecamatan').on('click', function(){
			jQuery('#c_kecamatan').val('2-12');
			check();
		});
		
		jQuery('#check_kelurahan').on('click', function(){
			jQuery('#c_kelurahan').val('3-13');
			check();
		});
		
		jQuery('#check_puskesmas').on('click', function(){
			jQuery('#c_puskesmas').val('4-13');
			check();
		});
		
		jQuery('#check_sekolah').on('click', function(){
			jQuery('#c_sekolah').val('5-13');
			check();
		});
		
		jQuery('#check_rs').on('click', function(){
			jQuery('#c_rs').val('6-3');
			check();
		});
		
		jQuery('#check_uptpendidikan').on('click', function(){
			jQuery('#c_uptpendidikan').val('7-12');
			check();
		});
		
		jQuery("#check_paraf").live('click', function(){
			if(!jQuery("#check_paraf").hasClass('disabled'))
			{
				jQuery("#form1")
					.validate({
						rules: {
							id_jnssurat: "required",
							id_surat: "required",
							pegawai: "required",
							no_surat: "required",
							tgl_surat: "required",
							id_sifatsurat: "required",
							prihal: "required",
							isi_surat: "required",
							userfile: "required"
						},
						highlight: function(label) {
							jQuery(label).closest('.control-group').addClass('error');
						},
						success: function(label) {
							label
								.text('Ok!').addClass('valid')
								.closest('.control-group').addClass('success');
						}
					});
				if(jQuery("#form1").valid())
				{
					ret = false;
					jQuery('#pegawai option:selected').each(function(i, selected){ 
						ret = true; 
					});
					if(ret == false)
					{
						alert('Anda harus menyertakan penerima disposisi.');
						jQuery('#pegawai').focus();
					}
					else
					{
						jQuery.fancybox.open({
							href 			: jQuery(this).attr("to"),
							type 			: 'iframe',
							padding 		: 2,
							opacity			: true,
							titlePosition	: 'over',
							openEffect 		: 'elastic',
							openSpeed  		: 150,
							closeEffect 	: 'elastic',
							closeSpeed  	: 150,
							width			: 900,
							helpers : {
								title : {
									type : 'inside'
								},
								overlay : {
									css : {
										'background' : 'transparent',
										'background' : 'rgba(0, 0, 0, 0.6)'
									}
								}
							}
						});
					}
				}
			}
			return false;
		})
		jQuery('#send').live('click', function(){
			jQuery("#form1")
				.validate({
					rules: {
						id_jnssurat: "required",
						id_surat: "required",
						pegawai: "required",
						no_surat: "required",
						tgl_surat: "required",
						id_sifatsurat: "required",
						prihal: "required",
						isi_surat: "required",
						userfile: "required"
					},
					highlight: function(label) {
						jQuery(label).closest('.control-group').addClass('error');
					},
					success: function(label) {
						label
							.text('Ok!').addClass('valid')
							.closest('.control-group').addClass('success');
					}
				});
			if(jQuery("#form1").valid())
			{
				jQuery.fancybox.open({
					href 			: jQuery(this).attr("to"),
					type 			: 'iframe',
					padding 		: 2,
					opacity			: true,
					titlePosition	: 'over',
					openEffect 		: 'elastic',
					openSpeed  		: 150,
					closeEffect 	: 'elastic',
					closeSpeed  	: 150,
					width			: 900,
					helpers : {
						title : {
							type : 'inside'
						},
						overlay : {
							css : {
								'background' : 'transparent',
								'background' : 'rgba(0, 0, 0, 0.6)'
							}
						}
					}
				});
			}
		})
		jQuery("#tgl_surat").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_awal_kegiatan").datepicker("option", "minDate", selectedDate);
				if(jQuery("#d_awal_kegiatan").val() == ''){
					jQuery("#d_akhir_kegiatan").datepicker("option", "minDate", selectedDate);
				}
			}
		});
		jQuery("#d_awal_kegiatan").datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm:ss',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_akhir_kegiatan").datepicker("option", "minDate", selectedDate);
			}
		});
		jQuery("#d_akhir_kegiatan").datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm:ss',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_awal_kegiatan").datepicker("option", "maxDate", selectedDate);
			}
		});
		jQuery(".chzn-select").chosen();
		jQuery("#form1")
			.validate({
				rules: {
					id_jnssurat: "required",
					id_surat: "required",
					pegawai: "required",
					no_surat: "required",
					tgl_surat: "required",
					id_sifatsurat: "required",
					prihal: "required",
					isi_surat: "required",
					userfile: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				}
			});
		jQuery('#v_pegawaittd').hide();
	});
</script>