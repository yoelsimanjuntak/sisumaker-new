<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Main_inbox extends CI_Model
{

    var $table = 'tr_surat_penerima';
    var $key = 'id';

    function __construct()
	{
		parent::__construct();
		$this->load->database();
    }

//put your code here
	function newpesan($pegawai_id, $skpd_id)
	{
		return $this->db
				->select('id')
				->from('tr_surat_penerima')
				->where('id_pegawai_ke', $pegawai_id)
				//->where('id_skpd_dari', $skpd_id)
				->where('status', 0)
				->get()
				->num_rows();
	}

	function newsurat_masuk($pegawai_id, $skpd_id)
	{
		return $this->db
				->select('id')
				->from('tr_surat_penerima')
				->where('id_pegawai_ke', $pegawai_id)
				//->where('id_skpd_dari != ', $skpd_id)
				->where('status', 0)
				->get()
				->num_rows();
	}

	function auth_read($id, $pegawai_id)
	{
		return $this->db
			->select('id')
			->from('tr_surat_penerima')
			->where('id_persuratan', $id)
			->where('id_pegawai_ke', $pegawai_id)
			->get()
			->num_rows();
	}

	function read($id)
	{
		return $this->db
			->select('
				tmpersuratan.id,
				tr_surat_penerima.d_entry,
				tr_surat_penerima.catatan,
				tmpegawai.n_pegawai,
				tmpegawai.nip,
				tmuser.username,
				tmuser.photo,
				tmpersuratan.no_surat,
				tmpersuratan.dari,
				tmpersuratan.tgl_terima_surat,
				tmpersuratan.tgl_surat,
				tmpersuratan.no_agenda,
				tmpersuratan.isi_surat,
				tmpersuratan.dasar,
				tmpersuratan.status_setujui,
				tmpersuratan.id_skpd_in,
				tmpersuratan.id_skpd_out,
				tmpersuratan.status,
				tmpersuratan.d_awal_kegiatan,
				tmpersuratan.d_akhir_kegiatan,
				tmpersuratan.v_kegiatan,
				tmpersuratan.id_compose,
				tmsifat_surat.n_sifatsurat,
				tmpersuratan.prihal,
				tmpersuratan.file,
				tmpersuratan.file_lampiran,
				tmjenis_surat.n_jnissurat,
				tmpersuratan.id_pegawaittd,
				tmpersuratan.id_sifatsurat,
				tr_surat_penerima.c_absen,
				tr_surat_penerima.d_absen
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id', $id)
			->join('tmpegawai', 'tr_surat_penerima.id_pegawai_dari = tmpegawai.id')
			->join('tmuser', 'tmpegawai.tmuser_id = tmuser.id')
			->join('tmpersuratan', 'tr_surat_penerima.id_persuratan = tmpersuratan.id')
			->join('tmsifat_surat', 'tmpersuratan.id_sifatsurat = tmsifat_surat.id')
			->join('tmjenis_surat', 'tmpersuratan.id_jnssurat = tmjenis_surat.id')
			->get();
	}

	function surat_dari($id)
	{
		$return = NULL;

		$jabatan = $this->db
			->select('n_pegawai, n_jabatan, n_unitkerja, internal')
			->from('tmpersuratan')
			->where('tmpersuratan.id', $id)
			->join('tmpegawai', 'tmpersuratan.id_pegawai = tmpegawai.id')
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->get();

		foreach($jabatan->result() as $row)
		{
			$return .= $row->n_jabatan.' ';
			$return .= $row->n_unitkerja;
			if($row->internal == true)
			{
				$return .= " Badan Pelayanan Perijinan Terpadu(BP2T)";
			}
		}
		return $return;
	}

	function to($id, $cc_id = false)
	{
		$cc = 0;
		if($cc_id != false)
		{
			$cc = 1;
		}
		return $this->db
			->select('n_pegawai, nip, n_jabatan, n_unitkerja, n_golongan, internal')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_persuratan', $id)
			->where('tr_surat_penerima.cc', $cc)
			->join('tmpegawai', 'tr_surat_penerima.id_pegawai_ke = tmpegawai.id')
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->join('tmgolongan','tmpegawai.tmgolongan_id = tmgolongan.id')
			->get();
	}

	function history_all($id)
	{
		return $this->db
			->select('
				tr_surat_penerima.id_pegawai_dari,
				tr_surat_penerima.id_pegawai_ke,
				tr_surat_penerima.file_surat_koreksi,
				tr_surat_penerima.lampiran_file_koreksi,
				tr_surat_penerima.catatan,
				tr_surat_penerima.d_entry,
				ke.n_pegawai as ke_pegawai,
				dari.n_pegawai as dari_pegawai,
				rake.photo as photo,
				(SELECT count(a.id) from tr_surat_penerima as a where a.d_entry = tr_surat_penerima.d_entry) as num
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_persuratan', $id)
			->join('tmpegawai as ke', 'tr_surat_penerima.id_pegawai_ke = ke.id')
			->join('tmpegawai as dari', 'tr_surat_penerima.id_pegawai_dari = dari.id')
			->join('tmuser as rake', 'dari.tmuser_id = rake.id')
			->order_by('tr_surat_penerima.d_entry', 'asc')
			->group_by('tr_surat_penerima.d_entry')
			->get();
	}

	function read_to($id, $d_entry, $cc)
	{
		return $this->db
			->select('
				ke.n_pegawai as ke_pegawai
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_persuratan', $id)
			->where('tr_surat_penerima.d_entry', $d_entry)
			->where('tr_surat_penerima.cc', $cc)
			->join('tmpegawai as ke', 'tr_surat_penerima.id_pegawai_ke = ke.id')
			->get();
	}

	function disposisi($id)
	{
		return $this->db
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmskpd.id_kirim,
				tmjabatan.id as id_jabatan,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
			->from('truser_kirim')
			->where('truser_kirim.id_user ', $id)
			->join('tmpegawai','truser_kirim.id_kirim = tmpegawai.tmuser_id')
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->join('tmskpd', 'tmpegawai.tmskpd_id = tmskpd.id')
			->order_by('tmjabatan.id', 'asc')
			->get();
	}

	function disposisi_pendataan($skpd_id)
	{
		return $this->db
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmjabatan.n_jabatan,
				tmunitkerja.initial as n_unitkerja
			')
			->from('tmpegawai')
			->where('tmpegawai.tmskpd_id', $skpd_id)
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->order_by('tmpegawai.tmjabatan_id', 'asc')
			->get();
	}

	function penandatangan_sekda()
	{
		$jabatan_id = array(1, 10, 2, 8);
		return $this->db
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
			->from('tmpegawai')
			->where('tmpegawai.tmskpd_id', 17)
			->where_in('tmpegawai.tmjabatan_id', $jabatan_id)
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->order_by('tmjabatan.no_urut', 'asc')
			->get();
	}

	function penandatangan_skpd($skpd_id)
	{
		$jabatan_id = array(3, 4, 5, 12, 13);
		return $this->db
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
			->from('tmpegawai')
			->where('tmpegawai.tmskpd_id', $skpd_id)
			->where_in('tmpegawai.tmjabatan_id', $jabatan_id)
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->order_by('tmjabatan.no_urut', 'asc')
			->get();
	}
}

?>
