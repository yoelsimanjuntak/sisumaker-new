<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Menu_loader class
 * Use this class to applicated your module here.
 *
 * @author  Muhamad Yusuf
 * @since   2014
 *
 */
class Menu_loader {

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function set_menu($module_name = NULL, $title = NULL, $css_class = NULL) {
        $structure = NULL;
        if ($module_name !== NULL || $module_name !== '')
            if ($css_class === NULL) {
                $structure = "<li><a href='" . site_url($module_name) . "'>" . $title . "</a></li>";
            } else {
                $structure = "<li class='" . $css_class . "'><a href='" . site_url($module_name) . "'>" . $title . "</a></li>";
            }

        return $structure;
    }

    public function create_menu($role_id) {
        $menu = NULL;
        $beranda = FALSE;
        $nota_dinas = FALSE;
        $inbox = FALSE;
        $outbox = FALSE;
        $surat_masuk = FALSE;
        $surat_keluar = FALSE;
        $pendataan_surat = FALSE;
        $rekapitulasi = FALSE;
        $konfigurasi = FALSE;
        $agenda = FALSE;
        $jadwal_dinas = FALSE;
        $dashboard_surat = FALSE;

        $query = $this->ci->db
                ->select('userauth_id')
                ->from('tmuser_userauth')
                ->where('tmuser_id', $role_id)
                ->order_by('userauth_id', 'asc')
                ->get();

        if ($query->num_rows() != 0) {
            foreach ($query->result() as $row) {
                switch ($row->userauth_id) {
					/*case '13' :
						$menu .= $this->set_menu('search', "<span class='iconfa-search'></span>Cari Surat");
                        break;*/
                    case '1' :
                        $menu .= $this->set_menu('', "<span class='iconfa-laptop'></span>Beranda");
                        break;
                    case '2' :
                        $menu .= $this->set_menu('main/notadinas', "<span class='btn btn-primary'><span class='iconfa-edit'></span>Tulis Surat</span>", 'btn-compose');
                        break;
                    case '3' :
                        $menu .= $this->set_menu('main/inbox', "<span class='iconfa-inbox'></span>Surat Masuk <span id='count_new_inbox'></span><script type='text/javascript'>cek_inbox();</script>");
                        break;
                    case '4' :
                        $menu .= $this->set_menu('main/outbox', "<span class='iconfa-plane'></span>Surat Keluar");
                        break;
                    case '11' :
                        $menu .= $this->set_menu('main/surat_keluar', "<span class='iconfa-check'></span>Surat Keluar <span id='count_new_surat_keluar'></span></span><script type='text/javascript'>cek_surat_keluar();</script>");
                        break;
                    case '12' :
                        //$menu .= $this->set_menu('main/surat_masuk', "<span class='iconfa-envelope-alt'></span>Surat Masuk");
                        break;
                    case '5' :
                        $menu .= $this->set_menu('pendataan', "<span class='btn btn-primary'><span class='iconfa-pencil'></span>Surat Masuk <i id='count_new_pendataan'></i></span><script type='text/javascript'>cek_pendataan();</script>", 'btn-compose');
                        break;
                    case '6' :
                        $menu .= "<li class='dropdown'><a href='#'><span class='iconfa-group'></span>Rekapitulasi Surat </a>";
                        $menu .= "<ul>";
                        $menu .= $this->set_menu('rekapitulasi/masuk', "Masuk");
                        $menu .= $this->set_menu('rekapitulasi/keluar', "Keluar");
                        $menu .= "</ul>";
                        $menu .= "</li>";
                        break;
                    case '7' :
                        $menu .= $this->set_menu('pegawai', "<span class='iconfa-user'></span>Master Pegawai");
                        break;
                     case '9' :
                        $menu .= "<li class='dropdown'><a href='#'><span class='iconfa-cogs'></span>Konfigurasi</a>";
                        $menu .= "<ul>";
                        $menu .= "<li class='submenu'><a href='#'>UMUM</a>";
                        $menu .= "<ul>";
                        $menu .= $this->set_menu('pegawai/skpd', "SKPD");
                        $menu .= $this->set_menu('pegawai/unitkerja', "Unit Kerja");
                        $menu .= $this->set_menu('pegawai/jabatan', "Jabatan");
                        $menu .= "</ul>";
                        $menu .= "</li>";
                        $menu .= "<li class='submenu'><a href='#'>SURAT</a>";
                        $menu .= "<ul>";
                        $menu .= $this->set_menu('persuratan/sifat_surat', "Sifat Surat");
                        $menu .= $this->set_menu('persuratan/jenis_surat', "Jenis Surat");
                        $menu .= "</ul>";
                        $menu .= "</li>";
                        $menu .= $this->set_menu('user_setting/wallpaper', "Set Wallpaper");
                        $menu .= "</ul>";
                        $menu .= "</li>";
                        $menu .= $this->set_menu('sms/s_outbox', "<span class='iconfa-phone'></span>Sms Gateway");
                        break;
                    case '8' :
                        $menu .= $this->set_menu('sms/broadcast', "<span class='iconfa-bullhorn'></span>Broadcast");
                        break;
					case '10' :
						$menu .= $this->set_menu('pegawai/unitkerja', "<span class='iconfa-sitemap'></span>Unit Kerja");
                        break;
					case '13' :
						$menu .= $this->set_menu('agenda/agenda', "<span class='iconfa-calendar'></span>Agenda");
                        break;
					case '14' :
						$menu .= $this->set_menu('agenda/jadwal', "<span class='iconfa-briefcase'></span>Jadwal Dinas");
                        break;
					case '21' :
						$menu .= "<li><a href='".site_url('dashboard')."' target='_blank'><span class='iconfa-dashboard'></span>Dashboard SISTEM </a></li>";
                        break;
                }
            }
        }
        return $menu;
    }

}

//nu abdi
// This is the end of Menu_loader class
