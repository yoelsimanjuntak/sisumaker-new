<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Tmuser_userauth extends DataMapper 
{

    var $table = 'tmuser_userauth';
    var $key = 'id';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here
	function cek($user_id, $auth_id)
	{
		return $this->db
			->where('tmuser_id', $user_id)
			->where('userauth_id', $auth_id)
			->get($this->table)
			->num_rows();
	}
	
	function select($key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($this->table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($this->table);
		}
	}
	
    function insert($form) 
	{
        return $this->db
			->insert($this->table, $form);
    }

    function update($id, $form) 
	{
        return $this->db
			->where($this->key, $id)
			->update($this->table, $form);
    }

    function delete($id) 
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }

}

?>
