<div class="widgetbox box-inverse">
	<?php echo $this->load->view($this->view.'validate');?>
	<h4 class="widgettitle nopadding">
		<a href="<?php echo base_url().$this->page;?>" class="pull-right btn"><i class="icon-remove nopadding"></i></a>
		<div style="clear:both"></div>
	</h4>
	<div class="widgetcontent wc1">
		<?php if($result->num_rows() != 0){ foreach($result->result() as $row){ ?>
		<form id="form1" class="stdform" action="<?php echo site_url().$this->page.'disposisi_submit/'.$row->id;?>" method="post" enctype="multipart/form-data">
			<div class="control-group">
				<label class="control-label" for="nomor">Nomor Surat</label>
				<div class="controls">
					<input type="text" name="no_surat" value="<?php echo $row->no_surat;?>"/>
				</div>
			</div>
			<!--<div class="control-group">
				<label class="control-label" for="id_jnssurat">Jenis Surat</label>
				<div class="controls">
					<select id="id_jnssurat" name="id_jnssurat" class="span5">
						<?php foreach($result_jnssurat->result() as $row_jnssurat){?>
						<option value="<?php echo $row_jnssurat->id;?>"<?php if($row->id_jnssurat == $row_jnssurat->id){ ?> selected="selected"<?php }?>><?php echo $row_jnssurat->n_jnissurat;?></option>
						<?php }?>
					</select>
				</div>
			</div>-->
			<input type="hidden" id="c_skpd" name="c_skpd" value="0"/> 
			<input type="hidden" id="c_keluarahan" name="c_keluarahan" value="0"/> 
			<input type="hidden" id="c_kecamatan" name="c_kecamatan" value="0"/> 
			<div class="control-group">
				<label class="control-label" for="pegawai">Kepada</label>
				<div class="controls" id="view_list_pegawai">
					<select id="pegawai" name="pegawai[]" required="" data-placeholder="SKPD/ Kelurahan/ Kecamatan Tujuan" class="span7 chzn-select" multiple="multiple" style="z-index:99999;" tabindex="3">
						<?php foreach($result_disposisi->result() as $row_disposisi){?>
						<option value="<?php echo $row_disposisi->id;?>" id="id_kirim_<?php echo $row_disposisi->id_kirim;?>"><?php echo $row_disposisi->n_skpd;?></option>
						<?php }?>
					</select>
				</div>
				<small class="desc">
					<a style="cursor:pointer" id="check_skpd">Semua SKPD</a> | 
					<a style="cursor:pointer" id="check_kelurahan">Semua Kelurahan</a> | 
					<a style="cursor:pointer" id="check_kecamatan">Semua Kecamatan</a>
				</small>
			</div>
			<div class="control-group">
				<label class="control-label" for="pegawai">Ke Internal</label>
				<div class="controls">
					<select id="pegawai_internal" name="pegawai_internal[]" required="" data-placeholder="Jabatan - Nama Pegawai" class="span7 chzn-select" multiple="multiple" style="z-index:99999;" tabindex="3">
						<?php foreach($result_disposisi_internal->result() as $row_disposisi_internal){?>
						<option value="<?php echo $row_disposisi_internal->id;?>"><?php echo $row_disposisi_internal->n_jabatan.' '.$row_disposisi_internal->n_unitkerja.' - '.$row_disposisi_internal->n_pegawai;?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="tgl_surat">Tanggal Surat</label>
				<div class="controls"><input type="text" name="tgl_surat" id="tgl_surat" class="input-medium" value="<?php echo $row->tgl_surat;?>"></div>
			</div>
			<div class="control-group" id="cek_prihal">
				<label class="control-label" for="prihal">Perihal</label>
				<div class="controls"><input type="text" name="prihal" id="prihal" class="input-xxlarge" value="<?php echo $row->prihal;?>"></div>
			</div>
			<div class="control-group">
				<label class="control-label" for="id_sifatsurat">Sifat Surat</label>
				<div class="controls">
					<select name="id_sifatsurat" class="input-medium">
						<option value="">Pilih</option>
						<?php foreach($result_tmsifat_surat->result() as $row_tmsifat_surat){ ?>
						<option value="<?php echo $row_tmsifat_surat->id;?>"<?php if($row->id_sifatsurat == $row_tmsifat_surat->id){ ?> selected="selected"<?php }?>><?php echo $row_tmsifat_surat->n_sifatsurat;?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="isi_surat">Memo Surat</label>
				<div class="controls" style="margin-left:140px;">
					<textarea name="isi_surat" rows="5" cols="60" class="tinymce"></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="userfile">File Surat</label>
				<div class="controls">
					<input type="file" name="userfile" id="userfile" class="input-large">
					&nbsp; <sup class="text-warning label label-warning" style="font-weight:bold"> &nbsp; .pdf&nbsp;</sup>
				</div>
			</div>
			<span<?php if($row->d_awal_kegiatan == '' || $row->d_akhir_kegiatan == '' || $row->v_kegiatan == ''){ ?> style="display:none"<?php }?>>
				<b>Diisi jika perlu ada kegiatan:</b>
				<div class="control-group">
					<label class="control-label" for="d_awal_kegiatan">Tanggal Mulai</label>
					<div class="controls"><input type="text" name="d_awal_kegiatan" id="d_awal_kegiatan" class="input-medium datepicker_eng_time" placeholder="0000-00-00 00:00:00" value="<?php echo $row->d_awal_kegiatan;?>"></div>
				</div>
				<div class="control-group">
					<label class="control-label" for="d_akhir_kegiatan">Tanggal Akhir</label>
					<div class="controls"><input type="text" name="d_akhir_kegiatan" id="d_akhir_kegiatan" class="input-medium datepicker_eng_time" placeholder="0000-00-00 00:00:00" value="<?php echo $row->d_akhir_kegiatan;?>"></div>
				</div>
				<div class="control-group">
					<label class="control-label" for="v_kegiatan">Keterangan</label>
					<div class="controls"><textarea name="v_kegiatan" id="v_kegiatan" class="input-xlarge" placeholder="exp: Lokasi"><?php echo $row->v_kegiatan;?></textarea></div>
				</div>
			</span>
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action"><i class="iconfa-plane"></i> Kirim</button>
				<a href="<?php echo base_url().$this->page;?>" class="btn btn-default">Batal</a>
			</p>
		</form>
		<?php $id = $row->id;}}else{ ?>
		<p>Ada Kesalahan! Data Tidak Ditemukan.</p>
		<?php }?>
	</div>
</div>
<div class="widgetbox box-inverse">
	<h4 class="widgettitle">History Pengiriman Surat</h4>
	<div class="widgetcontent">
		<script type="text/javascript">
			var oTable;
			var oTable1;
			datatables_ps("<?php echo site_url().$this->page.'history_disposisi/'.$this->session->userdata('pegawai_id').'/'.$id;?>", 4);
			datatables_ps1("<?php echo site_url().$this->page.'history_disposisi_i/'.$this->session->userdata('pegawai_id').'/'.$id;?>", 3);
		</script>
		<div id="result"></div>
		<b>Luar</b> <?php echo $this->session->userdata('n_skpd');?>:
		<table id="dyntable" class="table table-bordered" style="margin-top:1px;">
			<colgroup>
				<col class="con0" style="align: center; width: 4%" />
				<col class="con1" />
				<col class="con0" />
				<col class="con1" />
				<col class="con0" />
			</colgroup>
			<thead>        
				<tr>
					<th width="15px">No</th>
					<th>SKPD</th>
					<th width="140px">File Surat</th>
					<th width="160px">Status Terima</th>
					<th width="180px">Tanggal Kirim</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td valign="top" colspan="5" class="dataTables_empty">Loading <img src="<?php echo base_url().'assets/images/loaders/loader19.gif';?>"/></td>
				</tr>
			</tbody>
		</table>
		
		<div style="padding-top:17px;">
			<b>Internal</b> <?php echo $this->session->userdata('n_skpd');?>:
		</div>
		<table id="dyntable1" class="table table-bordered" style="margin-top:1px;">
			<colgroup>
				<col class="con0" style="align: center; width: 4%" />
				<col class="con1" />
				<col class="con0" />
				<col class="con1" />
			</colgroup>
			<thead>        
				<tr>
					<th width="15px">No</th>
					<th>Nama</th>
					<th width="160px">Status</th>
					<th width="180px">Tanggal Kirim</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td valign="top" colspan="4" class="dataTables_empty">Loading <img src="<?php echo base_url().'assets/images/loaders/loader19.gif';?>"/></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>