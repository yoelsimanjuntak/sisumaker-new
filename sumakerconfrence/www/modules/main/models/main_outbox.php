<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Main_outbox extends DataMapper 
{

    var $table = 'tr_surat_penerima';
    var $key = 'id';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here	
	function newpesan($pegawai_id, $skpd_id)
	{
		return $this->db
				->select('id')
				->from('tr_surat_penerima')
				->where('id_pegawai_ke', $pegawai_id)
				//->where('id_skpd_dari', $skpd_id)
				->where('status', 0)
				->get()
				->num_rows();
	}
	
	function newsurat_masuk($pegawai_id, $skpd_id)
	{
		return $this->db
				->select('id')
				->from('tr_surat_penerima')
				->where('id_pegawai_ke', $pegawai_id)
				//->where('id_skpd_dari != ', $skpd_id)
				->where('status', 0)
				->get()
				->num_rows();
	}
	
	
	function auth_read($id, $pegawai_id)
	{
		return $this->db
			->select('id')
			->from('tr_surat_penerima')
			->where('id', $id)
			->where('id_pegawai_dari', $pegawai_id)
			->get()
			->num_rows();
	}
	
	function read($id)
	{
		return $this->db
			->select('
				tmpersuratan.id,
				tr_surat_penerima.d_entry,
				tr_surat_penerima.catatan,
				tmpegawai.n_pegawai,
				tmpegawai.nip,
				tmuser.username,
				tmuser.photo,
				tmpersuratan.no_surat,
				tmpersuratan.dari,
				tmpersuratan.tgl_terima_surat,
				tmpersuratan.tgl_surat,
				tmpersuratan.no_agenda,
				tmpersuratan.isi_surat,
				tmpersuratan.dasar,
				tmsifat_surat.n_sifatsurat,
				tmpersuratan.prihal,
				tmpersuratan.file,
				tmpersuratan.file_lampiran,
				tmjenis_surat.n_jnissurat
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id', $id)
			->join('tmpegawai', 'tr_surat_penerima.id_pegawai_dari = tmpegawai.id')
			->join('tmuser', 'tmpegawai.tmuser_id = tmuser.id')
			->join('tmpersuratan', 'tr_surat_penerima.id_persuratan = tmpersuratan.id')
			->join('tmsifat_surat', 'tmpersuratan.id_sifatsurat = tmsifat_surat.id')
			->join('tmjenis_surat', 'tmpersuratan.id_jnssurat = tmjenis_surat.id')
			->get();
	}
	
	function surat_dari($id)
	{
		$return = NULL;
		
		$jabatan = $this->db
			->select('n_pegawai, n_jabatan, n_unitkerja, internal')
			->from('tmpersuratan')
			->where('tmpersuratan.id', $id)
			->join('tmpegawai', 'tmpersuratan.id_pegawai = tmpegawai.id')
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->get();
		
		foreach($jabatan->result() as $row)
		{
			$return .= $row->n_jabatan.' ';
			$return .= $row->n_unitkerja;
			if($row->internal == true)
			{
				$return .= " Badan Pelayanan Perijinan Terpadu(BP2T)";
			}
		}
		return $return;
	}
	
	function to($id, $cc_id = false)
	{
		$cc = 0;
		if($cc_id != false)
		{
			$cc = 1;
		}
		return $this->db
			->select('n_pegawai, nip, n_jabatan, n_unitkerja, n_golongan, internal')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_persuratan', $id)
			->where('tr_surat_penerima.cc', $cc)
			->join('tmpegawai', 'tr_surat_penerima.id_pegawai_ke = tmpegawai.id')
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->join('tmgolongan','tmpegawai.tmgolongan_id = tmgolongan.id')
			->get();
	}
	
	function history($pegawai_id, $id)
	{
		return $this->db
			->select('
				tr_surat_penerima.catatan,
				tr_surat_penerima.file_surat_koreksi,
				tr_surat_penerima.d_entry,
				tmpegawai.n_pegawai
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_pegawai_dari', $pegawai_id)
			->where('tr_surat_penerima.id_persuratan', $id)
			->join('tmpegawai', 'tr_surat_penerima.id_pegawai_ke = tmpegawai.id')
			->get();
	}
	
	function disposisi($id)
	{
		return $this->db
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmjabatan.n_jabatan,
				tmunitkerja.initial as n_unitkerja
			')
			->from('tmpegawai')
			->where('tmpegawai.id <> ', $id)
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->order_by('tmunitkerja.initial', 'asc')
			->get();
	}
}

?>
