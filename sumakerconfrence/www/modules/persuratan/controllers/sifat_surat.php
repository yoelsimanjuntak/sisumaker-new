<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Sifat_surat extends ICA_AdminCont {

    var $page = "persuratan/sifat_surat/";
    var $view = "persuratan/sifatsurat/";
    var $icon = "cogs";

    function __construct() {
        parent::__construct();
		$this->restrict('9');
        $this->tmsifatsurat = new Tmsifat_surat();
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> Pegawai <span class='separator'></span> Jabatan",
            'title' => 'Master sifat',
            'content' => 'table',
            'sort' => 1
        );
        $this->load->view('template', $data);
    }

    function add() {
        $this->load->view($this->view . 'add');
    }

    function edit($id) {
        $data = array(
            'result' => $this->tmsifatsurat->select('id', $id)
        );
        $this->load->view($this->view . 'edit', $data);
    }

    function save($id = false) {
        if ($id == false) {
            if ($this->tmsifatsurat->select('n_sifatsurat', $this->input->post('n_sifatsurat'))->num_rows() == 0) {
                $data = array(
                    'n_sifatsurat' => $this->input->post('n_sifatsurat')
                );
                $this->tmsifatsurat->insert($data);
                $output = array(
                    'status' => 1
                );
                echo json_encode($output);
            } else {
                $output = array(
                    'status' => 11
                );
                echo json_encode($output);
            }
        } else {
            if ($this->input->post('n_sifatsurat_temp') == $this->input->post('n_sifatsurat')) {
                $data = array(
                    'n_sifatsurat' => $this->input->post('n_sifatsurat')
                );
                $this->tmsifatsurat->update($id, $data);
                $output = array(
                    'status' => 2
                );
                echo json_encode($output);
            } else {
                if ($this->tmsifatsurat->select('n_sifatsurat', $this->input->post('n_sifatsurat'))->num_rows() == 0) {
                    $data = array(
                        'n_sifatsurat' => $this->input->post('n_sifatsurat')
                    );
                    $this->tmsifatsurat->update($id, $data);
                    $output = array(
                        'status' => 2
                    );
                    echo json_encode($output);
                } else {
                    $output = array(
                        'status' => 22
                    );
                    echo json_encode($output);
                }
            }
        }
    }

    function delete($id) {
        $this->tmsifatsurat->delete($id);
    }

    function data() {
        $this->load->library('datatables');
        echo $this->datatables
                ->select('
				id,
				n_sifatsurat
			')
                ->from('tmsifat_surat')
                ->add_column('aksi', '<center>
					<a id="edit" href="#" to="' . base_url() . $this->page . 'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
					<a id="delete" href="#" to="' . base_url() . $this->page . 'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>', 'tmsifat_surat.id')
                ->generate();
    }

}