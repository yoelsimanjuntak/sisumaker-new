<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>SISUMAKER :: Login</title>
		<?php $base_url = base_url(); ?>
		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/images/favicon.png"/>
        <?php
			$base_url = base_url();
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.red.css");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-1.9.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.1.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.slimscroll.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.validate.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "chat.js");
			?>
		<script type="text/javascript">
			base='<?php echo $base_url; ?>';
			jQuery(document).ready(function(){
				alert('Mohon tunggu beberapa saat kode verifikasi akan dikirim ke nomor ponsel Anda.');
				jQuery('body').removeClass('chatenabled');
				jQuery.removeCookie('enable-chat', { path: '/' });
				jQuery("form")
				.validate({
					submitHandler:function(form){
						jQuery('#action').button('loading');
						jQuery('.login-alert').fadeOut('fast');
						jQuery.ajax({
							type:'post',
							url:jQuery(form).attr('action'),
							data:jQuery(form).serialize(),
							dataType:'json',
							success:function(data){
								if(data.status == 1)
								{
									jQuery('.login-alert').html("<div class='alert alert-success'>Login Berhasil...<a href='<?php echo $base_url.$this->page;?>' style='font-weight:bold;' title='Klik, jika tidak dipindahkan oleh browser.'>Klik <span class='iconfa-info-sign'></span></a></div>");
									document.location.href = "<?php echo $base_url.$this->page;?>";
								}
								else if(data.status == 0)
								{
									jQuery('.login-alert').html("<div class='alert alert-error'><b>Kode verifikasi salah.</b><br/>Masukkan kode verifikasi terakhir.</div>");
								}
								jQuery('#action').button('reset');
								jQuery('.login-alert').fadeIn('fast');
							}
						});
						return false;
					}
				});
			});
		</script>
	</head>
	<body class="loginpage" style="background:url(<?php echo $base_url;?>uploads/wallpaper/Blur_Violet150605.png);background-size:cover;">
		<div class="loginpanel">
			<div class="loginpanelinner" style="background-image:url('<?php echo $base_url;?>assets/images/bg_login.png');background-repeat: no-repeat;background-color:#070a11">
				<div class="logo animate0 bounceIn"><img src="<?php echo $base_url.'assets/images/logo.png';?>" alt="Logo" width="100%"/></div>
				<form id="form" action="<?php echo $base_url.'panel/submit_verifikasi';?>" method="post">
					<input type="hidden" name="secure" value="<?php echo $secure;?>"/>
					<input type="hidden" name="encrip" value="<?php echo $encrip;?>"/>
					<input type="hidden" name="microtime" value="<?php $microtime = microtime();echo $microtime;?>"/>
					<input type="hidden" name="token" value="<?php echo sha1($secure.$encrip.$microtime);?>"/>
					<div class="inputwrapper login-alert"></div>
					<b class="text-login">Verifikasi tahap 2</b>
					<p class="text-login" style="margin-bottom:7px;">Sebuah pesan text dengan kode verifikasi telah<br/>dikirim ke nomor <?php echo $dash_no_telp;?></p>
					<div class="inputwrapper animate2 bounceIn">
						<input type="password" name="verifikasi" id="verifikasi" placeholder="Masukkan kode verifikasi" />
					</div>
					<div class="inputwrapper animate3 bounceIn">
						<button name="submit" id="action">Sign In</button>
					</div>
				</form>
			</div>
		</div>
		<div class="loginfooter">
			<p>Copyright &copy; 2015. Aplikasi Persuratan.</p>
		</div>
	</body>
</html>