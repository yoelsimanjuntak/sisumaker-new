<script type="text/javascript">
	var oTable;
	datatables(<?php echo $sort;?>);
	fancybox();
</script>
<span id="addVar">
	<a href="<?php echo base_url().$this->page.'print_all/'.$d_dari.'/'.$d_sampai;?>" class="btn btn-primary"><i class="icon-print icon-white"></i> Print Rekapirulasi</a>
</span>
<table id="dyntable" class="table table-bordered">
    <colgroup>
        <col class="con0" style="align: center; width: 4%" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
    </colgroup>
    <thead>
		<tr>
			<th width="15px">No</th>
			<th width="120px">No Surat</th>
			<th>Dari</th>
			<th width="120px">Tanggal Surat</th>
			<th width="80px">No Agenda</th>
			<th width="100px">Perihal</th>
			<th width="60px">Aksi</th>
		</tr>
    </thead>
	<tbody>
		<?php foreach($result->result() as $row){ ?> 
		<tr>
			<td><?php echo $row->id;?></td>
			<td><?php echo $row->no_surat;?></td>
			<td><?php echo $row->dari;?></td>
			<td><?php echo indonesian_date($row->tgl_surat);?></td>
			<td><?php echo $row->no_agenda;?></td>
			<td><?php echo $row->prihal;?></td>
			<td>
				<center>
				<a id="view" href="#" to="<?php echo base_url().$this->page.'info/'.$row->id;?>"><i class="iconfa-info-sign"></i> Info</a> &nbsp;<br/>
				<a id="view" href="#" to="<?php echo base_url().$this->page.'read/'.$row->id;?>"><i class="iconfa-external-link"></i> Lihat</a>
				</center>
			</td>
		</tr>
		<?php }?>
	</tbody>
</table>