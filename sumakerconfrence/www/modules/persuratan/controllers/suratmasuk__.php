<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Jenis_surat extends ICA_AdminCont {

    var $page = "persuratan/suratmasuk/";
    var $view = "persuratan/suratmasuk/";
    var $icon = "cogs";

    function __construct() {
        parent::__construct();
        $this->tmpersuratan = new Tmpersuratan();
        $this->tmjenissurat = new Tmjenis_surat();
        $this->tmsifatsurat = new Tmsifat_surat();
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> Jenis Surat <span class='separator'></span> Jenis Surat",
            'title' => 'Master Jenis Surat',
            'content' => 'table',
            'sort' => 1,
            'result_jenis' => $this->tmjenissurat->select(),
            'result_sifat' => $this->tmsifatsurat->select(),
            'result_kecamatan' => $this->tmkecamatan->select()
        );
        $this->load->view('template', $data);
    }

    function add() {
        $this->load->view($this->view . 'add');
    }

    function edit($id) {
        $data = array(
            'result' => $this->tmpersuratan->select('id', $id)
        );
        $this->load->view($this->view . 'edit', $data);
    }

    function save($id = false) {
        if ($id == false) {
            if ($this->tmpersuratan->select('n_jnissurat', $this->input->post('n_jnissurat'))->num_rows() == 0) {
                $data = array(
                    'n_jnissturat' => $this->input->post('n_jnissurat')
                );
                $this->tmpersuratan->insert($data);
                $output = array(
                    'status' => 1
                );
                echo json_encode($output);
            } else {
                $output = array(
                    'status' => 11
                );
                echo json_encode($output);
            }
        } else {
            if ($this->input->post('n_jnissurat_temp') == $this->input->post('n_jnissurat')) {
                $data = array(
                    'n_jnissurat' => $this->input->post('n_jnissurat')
                );
                $this->tmpersuratan->update($id, $data);
                $output = array(
                    'status' => 2
                );
                echo json_encode($output);
            } else {
                if ($this->tmpersuratan->select('n_jnissurat', $this->input->post('n_jnissurat'))->num_rows() == 0) {
                    $data = array(
                        'n_jnissurat' => $this->input->post('n_jnissurat')
                    );
                    $this->tmpersuratan->update($id, $data);
                    $output = array(
                        'status' => 2
                    );
                    echo json_encode($output);
                } else {
                    $output = array(
                        'status' => 22
                    );
                    echo json_encode($output);
                }
            }
        }
    }

    function delete($id) {
        $this->tmpersuratan->delete($id);
    }

    function data() {
        $this->load->library('datatables');
        echo $this->datatables
                ->select('
				
			')
                ->from('tmpersuratan as a
                        LEFT JOIN tmsifat_surat as b ON a.id_sifatsurat= b.id
                        LEFT JOIN tmjenis_surat as c on a.id_jnssurat=c.id')
                ->add_column('aksi', '<center>
					<a id="edit" href="#" to="' . base_url() . $this->page . 'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
					<a id="delete" href="#" to="' . base_url() . $this->page . 'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>', 'tmjenis_surat.id')
                ->generate();
    }

}