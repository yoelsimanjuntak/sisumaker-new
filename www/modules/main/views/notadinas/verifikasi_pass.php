<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#password').focus();
		jQuery("#form1")
			.validate({
				rules: {
					password: "required",
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 1)
							{
								parent.jQuery.fancybox.close();
								parent.jQuery('#check_paraf').prop('disabled', true).addClass('disabled');
								parent.jQuery('#send').show();
							}
							else
							{
								alert('Maaf, Password tidak sama.');
								jQuery('#password').val('');
							}
						}
					});
					return false;
				}
			});
	});
</script>
<style>
	.maincontentinner{background:url('<?php echo base_url().$this->session->userdata('wallpaper');?>')}
</style>
<div class="box-lockform" style="">
	<div class="logwindow">
		<div class="logwindow-inner">
			<form id="form1" action="<?php echo base_url().$this->page.'submit_pass';?>" method="post">
				<center>
					<h3>Validasi Password</h3>
					<span class="userphoto"><img alt="Photo Profile" style="max-height:140px" src="<?php echo base_url().'uploads/photo/'.$this->session->userdata('photo'); ?>"></span>
					
					<h5>Logged In: <?php echo $this->session->userdata('realname'); ?></h5>
					<input type="password" name="password" id="password" class="span5" placeholder="Enter password and hit enter to paraf...">
				</center>
			</form>
		</div>
	</div>
</div>