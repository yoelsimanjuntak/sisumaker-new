<?php

$lang['required'] 			= "<li>Data <b> %s </b> wajib diisi.</li>";
$lang['isset']				= "<li>Data <b>%s</b> mesti diset nilainya.</li>";
$lang['valid_email']                    = "<li>Data <b>%s</b> email tidak valid.</li>";
$lang['valid_emails']                   = "Field %s harus memiliki email yang valid dan lengkap.";
$lang['valid_url'] 			= "Field %s harus memiliki URL yang valid.";
$lang['valid_ip'] 			= "Field %s harus memiliki IP Address yang valid.";
$lang['min_length']			= "Field %s harus diisi minimal %s karakter.";
$lang['max_length']			= "The %s field can not exceed %s characters in length.";
$lang['exact_length']                   = "The %s field must be exactly %s characters in length.";
$lang['alpha']				= "The %s field may only contain alphabetical characters.";
$lang['alpha_numeric']                  = "The %s field may only contain alpha-numeric characters.";
$lang['alpha_dash']			= "The %s field may only contain alpha-numeric characters, underscores, and dashes.";
$lang['numeric']			= "<li>Data <b>%s</b> Harus Angka.</li>";
$lang['is_numeric']			= "The %s field must contain only numeric characters.";
$lang['integer']			= "The %s field must contain an integer.";
$lang['matches']			= "The %s field does not match the %s field.";
$lang['is_natural']			= "The %s field must contain only positive numbers.";
$lang['is_natural_no_zero']             = "The %s field must contain a number greater than zero.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/english/form_validation_lang.php */