<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#calendar').fullCalendar({
		//theme: true,
		lang: 'id',
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		//defaultDate: '<?php echo date('Y-m-d');?>',
		editable: false,
		eventLimit: true, // allow "more" link when too many events
		events: {
			url: base + '<?php echo $this->page;?>get_data',
			error: function() {
				jQuery('#script-warning').show();
			}
		},
		loading: function(bool) {
			jQuery('#loading').toggle(bool);
		}
	});
	jQuery(".fc-h-event").live('click',function(event){
		event.preventDefault();
			jQuery.fancybox.open({
				href 			: jQuery(this).attr("href"),
				type 			: 'iframe',
				padding 		: 2,
				opacity			: true,
				titlePosition	: 'over',
				openEffect 		: 'elastic',
				openSpeed  		: 150,
				closeEffect 	: 'elastic',
				closeSpeed  	: 150,
				width			: 900,
				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'transparent',
							'background' : 'rgba(0, 0, 0, 0.6)'
						}
					}
				}
		});
	});
});
</script>
<div id='script-warning'>
	<code>php/get-events.php</code> must be running.
</div>
<div class="widgetbox box-inverse">	
	<h4 class="widgettitle nopadding"></h4>
	<div class="widgetcontent wc1">
		<div id='loading'>loading...</div>
		<div id='calendar'></div>
	</div>
	<style>
		#script-warning {
			display: none;
			background: #eee;
			border-bottom: 1px solid #ddd;
			padding: 0 5px;
			line-height: 40px;
			text-align: center;
			font-weight: bold;
			font-size: 12px;
			color: red;
		}
		#loading {
			display: none;
			position: absolute;
			top: 10px;
			right: 10px;
		}
		#calendar {
			max-width: 900px;
			margin: 20px auto;
			padding: 0 10px;
		}
		.fc-event-container a{
			color:#ccc;
		}
	</style>
</div>