<h2>Detect Location</h2>
<p><span class="label">Your Location:</span> <span id="location"></span></p>

<script type="text/JavaScript">
jQuery(document).ready(function(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showLocation);
    } else { 
        jQuery('#location').html('Geolocation is not supported by this browser.');
    }
});

function showLocation(position) {
	var latitude = position.coords.latitude;
	var longitude = position.coords.longitude;
	jQuery.ajax({
		type:'POST',
		url:'<?php echo base_url().$this->page;?>get_location',
		data:'latitude='+latitude+'&longitude='+longitude,
		success:function(msg){
            if(msg){
            	jQuery("#location").html(msg);
            }else{
                jQuery("#location").html('Not Available');
            }
		}
	});
}
</script>