<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Tr_chat extends DataMapper 
{

    var $table = 'tr_chat';
    var $key = 'id';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here
	function read_message($from, $to)
	{
		return $this->db
			->select('
				tr_chat.id,
				tr_chat.message,
				tr_chat.from,
				tr_chat.to,
				tr_chat.send,
				tr_chat.status
			')
			->from($this->table)
			->where('from', $from)
			->where('to', $to)
			->or_where('from', $to)
			->where('to', $from)
			->order_by('tr_chat.send', 'asc')
			->get();
	}
	
	function up_message($from, $to)
	{
		$form = array('status' => 1);
		return $this->db
			->where('from', $from)
			->where('to', $to)
			->or_where('from', $to)
			->where('to', $from)
			->update($this->table, $form);
	}
	
	function new_message($to)
	{
		return $this->db
			->select('
				tr_chat.from,
				tmpegawai.n_pegawai
			')
			->from($this->table)
			->join('tmpegawai', 'tmpegawai.tmuser_id = tr_chat.from')
			->where('to', $to)
			->where('status', 0)
			->group_by('tr_chat.from')
			->get();
	}

	function select($key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($this->table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($this->table);
		}
	}
	
    function insert($form) 
	{
        return $this->db
			->insert($this->table, $form);
    }

    function update($id, $form) 
	{
        return $this->db
			->where($this->key, $id)
			->update($this->table, $form);
    }

    function delete($id) 
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }

}

?>
