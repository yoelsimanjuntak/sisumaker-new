<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,no-store,must-revalidate"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="expires" content="0"/>
        <title>SISUMAKER | <?php echo $title; ?></title>
		<?php $base_url = base_url(); ?>
		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/images/favicon.png"/>
        <?php
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.red.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.mail.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "jquery.ui.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "responsive-tables.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/prettify/", "prettify.css");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-2.0.3.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.2.1.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.slimscroll.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "function.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "chat.js");
        foreach ($assets as $asset_js_css) {
            echo $asset_js_css;
        }
        ?>
        <script type="text/javascript">base='<?php echo $base_url; ?>';</script> 
        <!--[if lte IE 8]><?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "excanvas.min.js"); ?><![endif]-->
        <!--<?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "elements.js"); ?>-->
    </head>
    <body style="background-image:url('<?php echo $base_url.$this->session->userdata('wallpaper');?>')">
        <div class="mainwrapper">
            <div class="header">
				<a href="<?php echo $base_url;?>"><div class="logo"></div></a>
				<div class="headerinner">
					<ul class="headmenu">
						<li class="title-skpd" style="border:none">
                            <h2><?php echo $this->session->userdata('n_skpd');?></h2>
                            <h3>Kota Tangerang Selatan</h3>
                        </li>
						<li class="right">
                            <div class="userloggedinfo">
                                <span class="userphoto"><img alt="Photo Profile" width="80px" height="80px" src="<?php echo $base_url.'uploads/photo/'.$this->session->userdata('photo'); ?>" /></span>
                                <div class="userinfo">
                                    <h5><?php echo $this->session->userdata('realname'); ?></h5>
                                    NIP. <?php echo $this->session->userdata('nip'); ?>
                                    <ul>
                                        <li><a href="<?php echo $base_url.'panel/edit/'.$this->session->userdata('userid'); ?>">Edit Profile<small> - <?php echo $this->session->userdata('username'); ?></small></a></li>
                                        <li><a href="<?php echo $base_url.'panel/logout'; ?>">Sign Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
					</ul>
				</div>
			</div>
            <div class="leftpanel">
                <div class="leftmenu">        
                    <ul class="nav-o nav nav-tabs nav-stacked">
                        <li class="nav-header">Navigasi</li>
						<?php echo $this->menu_loader->create_menu($this->session->userdata('auth_id'));?>
						<!--<li class='dropdown'><a href='#'><span class='iconfa-credit-card'></span>Templates</a>
							<ul>
								<?php
								$this->tmjenissurat = new Tmjenis_surat();
								foreach($this->tmjenissurat->select("file_surat != ", '')->result() as $row){ ?>
								<li><a href="<?php echo $base_url.$row->file_surat;?>"><?php echo $row->n_jnissurat;?></a></li>
								<?php }?>
							<ul>
						</li>-->
						<li><a href="<?php echo $base_url.'chat/message';?>"><span class="iconfa-comments"></span>Pesan <i id='count_new_message'></i></span><script type='text/javascript'>cek_new_message();</script></a></li>
                    </ul>
					<div class="p-bottom">
						<ul class="nav nav-tabs nav-stacked">
							<li>
								<a href="<?php echo $base_url.'user_setting/main/wallpaper';?>" title="Setting Wallpaper"><span class='iconfa-picture'></span></a>
								<a href="" title="Send A Message" id="enablechat"><span class='iconfa-comments-alt'></span></a>
								<a href="https://docs.google.com/viewer?url=<?php echo $base_url;?>user_manual.pdf" title="Help?" target="_blank"><span class='iconfa-road'></span></a>
							</li>
						</ul>
					</div>
                </div>
            </div>
            <div class="rightpanel">	
                <ul class="breadcrumbs">
                    <li>
                        <a href="<?php echo base_url(); ?>">
                            <i class="iconfa-home"></i>
                        </a>
                        <span class="separator"></span>
                    </li>
                    <li><?php echo $breadcrubs; ?></li>
                    <li class="right">
                    </li>
                </ul>
                <div class="pageheader">
                    <div class="pageicon-mini" style="border-radius:none !important;"><span class='iconfa-<?php echo $this->icon; ?>'></span></div>
                    <div class="pagetitle">
                        <h1><?php echo $title; ?></h1>
                    </div>
                </div>
                <div class="maincontent">
                    <div class="maincontentinner">
                        <div class="row-fluid">
                            <?php echo $this->load->view($this->view.$content); ?>
                        </div>
                        <div class="footer">
                            <div class="footer-left">
                                <span>Copyright &copy; 2015. SISUMAKER (Sistem Informasi Surat Masuk dan Keluar)</span>
                            </div>
                            <div class="footer-right">
								<span>Copyright &copy; 2015. Kota Tangerang Selatan</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>