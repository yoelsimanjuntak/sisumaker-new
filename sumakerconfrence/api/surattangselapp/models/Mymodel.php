<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mymodel extends CI_Model{

	public function __construct()
  	{
    	parent::__construct();
    	$this->load->database();
  	}

	function select($table, $key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($table);
		}
	}

	public function surat_masuk($pegawai_id, $from_date, $to_date)
	{
		return $this->db
			->query('
				SELECT 
					tr_surat_penerima.id,
					tr_surat_penerima.d_entry,
					tr_surat_penerima.status,
					tmpegawai.n_pegawai,
					tr_surat_penerima.id_persuratan as id_persuratan,
					tmpersuratan.dari,
					tmpersuratan.tgl_surat,
					tmpersuratan.tgl_terima_surat,
					tmpersuratan.prihal,
					tmpersuratan.id_compose,
					tmsifat_surat.n_sifatsurat
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_dari = tmpegawai.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
				WHERE 
					tr_surat_penerima.id_pegawai_ke = '.$pegawai_id.' AND
					(tmpersuratan.tgl_surat BETWEEN "'.$from_date.'" AND "'.$to_date.'")
				ORDER BY tr_surat_penerima.d_entry DESC');
	}
	
	function read($id_persuratan)
	{
		return $this->db
			->select('
				tmpersuratan.id,
				tmpersuratan.no_surat,
				tmpersuratan.dari,
				tmpersuratan.tgl_terima_surat,
				tmpersuratan.tgl_surat,
				tmpersuratan.no_agenda,
				tmpersuratan.isi_surat,
				tmpersuratan.dasar,
				tmpersuratan.status_setujui,
				tmpersuratan.id_skpd_in,
				tmpersuratan.id_skpd_out,
				tmpersuratan.status,
				tmpersuratan.dasar,
				tmpersuratan.d_awal_kegiatan,
				tmpersuratan.d_akhir_kegiatan,
				tmpersuratan.v_kegiatan,
				tmpersuratan.id_compose,
				tmsifat_surat.n_sifatsurat,
				tmpersuratan.prihal,
				tmpersuratan.file,
				tmpersuratan.file_lampiran,
				tmpersuratan.c_lintas_skpd,
				tmjenis_surat.n_jnissurat,
				tmpersuratan.id_pegawai_setujui,
				tmpersuratan.id_pegawaittd
			')
			->from('tmpersuratan')
			->where('tmpersuratan.id', $id_persuratan)
			->join('tmsifat_surat', 'tmpersuratan.id_sifatsurat = tmsifat_surat.id')
			->join('tmjenis_surat', 'tmpersuratan.id_jnssurat = tmjenis_surat.id')
			->get();
	}
	
	public function history_all($id_persuratan)
	{
		return $this->db
			->select('
				tr_surat_penerima.id_pegawai_dari,
				tr_surat_penerima.id_pegawai_ke,
				tr_surat_penerima.file_surat_koreksi,
				tr_surat_penerima.lampiran_file_koreksi,
				tr_surat_penerima.catatan,
				tr_surat_penerima.d_entry,
				ke.n_pegawai as ke_pegawai,
				dari.n_pegawai as dari_pegawai,
				rake.photo as photo,
				(SELECT count(a.id) from tr_surat_penerima as a where a.d_entry = tr_surat_penerima.d_entry) as num
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_persuratan', $id_persuratan)
			->join('tmpegawai as ke', 'tr_surat_penerima.id_pegawai_ke = ke.id')
			->join('tmpegawai as dari', 'tr_surat_penerima.id_pegawai_dari = dari.id')
			->join('tmuser as rake', 'dari.tmuser_id = rake.id')
			->order_by('tr_surat_penerima.d_entry', 'asc')
			->group_by('tr_surat_penerima.d_entry')
			->get();
	}
	
	public function read_to($id_persuratan, $d_entry, $cc)
	{
		return $this->db
			->select('
				ke.n_pegawai as ke_pegawai
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_persuratan', $id_persuratan)
			->where('tr_surat_penerima.d_entry', $d_entry)
			->where('tr_surat_penerima.cc', $cc)
			->join('tmpegawai as ke', 'tr_surat_penerima.id_pegawai_ke = ke.id')
			->get();
	}
	
	function disposisi($id)
	{
		return $this->db
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmskpd.id_kirim,
				tmjabatan.id as id_jabatan,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
			->from('truser_kirim')
			->where('truser_kirim.id_user ', $id)
			->join('tmpegawai','truser_kirim.id_kirim = tmpegawai.tmuser_id')
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->join('tmskpd', 'tmpegawai.tmskpd_id = tmskpd.id')
			->order_by('tmjabatan.id', 'asc')
			->get();
	}
	
    function id_tu($skpd_id)
	{
		return $this->db
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmpegawai.telp,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
			->where('tmpegawai.tmskpd_id', $skpd_id)
			->where('tmuser_userauth.userauth_id', 5)
			->from('tmpegawai')
			->join('tmuser','tmpegawai.tmuser_id = tmuser.id')
			->join('tmuser_userauth', 'tmuser.id = tmuser_userauth.tmuser_id')
			->join('tmjabatan', 'tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja', 'tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->limit(1)
			->get();
	}
	
	function nomor_save($id_skpd, $id_jenissurat)
	{
		$thn = date('y');
		$query = $this->db
			->select('id, nomor')
			->from('penomoran')
			->where('id_jenissurat', $id_jenissurat)
			->where('id_skpd', $id_skpd)
			->where('tahun', $thn)
			->get();
		
		if($query->num_rows() != 0)
		{
			foreach($query->result() as $row)
			{
				$nomor = $row->nomor + 1;
				$id = $row->id;
			}
			$form = array('nomor' => $nomor);
			$this->db
				->where('id', $id)
				->update('penomoran', $form);
			$return = $nomor;
		}
		else
		{
			$form = array(
				'id_jenissurat' => $id_jenissurat,
				'id_skpd' => $id_skpd,
				'tahun' => $thn,
				'nomor' => 1
			);
			$this->db->insert('penomoran', $form);
			$return = 1;
		}
		$a = strlen($return);
		for ($i = 4; $i > $a; $i--) {
			$return = "0" . $return;
		}
		return $return;
	}
}
