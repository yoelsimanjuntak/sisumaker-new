<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>SISUMAKER :: Login</title>
		<?php $base_url = base_url(); ?>
		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/images/favicon.png"/>
        <?php
			$base_url = base_url();
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.login.css");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-1.9.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.1.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.slimscroll.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.validate.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "chat.js");
			?>
		<script type="text/javascript">
			base='<?php echo $base_url; ?>';
			jQuery(document).ready(function(){
				jQuery('body').removeClass('chatenabled');
				jQuery.removeCookie('enable-chat', { path: '/' });
				jQuery("form")
					.validate({
						rules: {
							username: "required",
							password: "required"
						},
						highlight: function(label) {
							jQuery(label).closest('.inputwrapper').addClass('error');
						},
						success: function(label) {
							label
								.closest('.inputwrapper').removeClass('error');
						},
						submitHandler:function(form){
							jQuery('#action').button('loading');
							jQuery('.login-alert').fadeOut('fast');
							jQuery.ajax({
								type:'post',
								url:jQuery(form).attr('action'),
								data:jQuery(form).serialize(),
								dataType:'json',
								success:function(data){
									if(data.status == 1)
									{
										jQuery('.login-alert').html("<div class='alert alert-success'>Berhasil, Sedang Dialihkan...</div>");
										document.location.href = data.link;
									}
									else if(data.status == 0)
									{
										jQuery('.login-alert').html("<div class='alert alert-error'><strong>Username atau Password belum terdaftar</strong></div>");
									}
									jQuery('#action').button('reset');
									jQuery('.login-alert').fadeIn('fast');
								}
							});
							return false;
						}
					});
				jQuery("#username").focus();
			});
		</script>
	</head>
	<body style="background-size:cover;background-attachment:fixed;">
		<div class="overlay">
			<div class="loginpage">
				<div class="loginpanel">
					<div class="loginpanelinner" style="background-color:#000;background-image:url('<?php echo $base_url.'assets/images/header_login.png';?>');background-size: contain;background-repeat: no-repeat;border-radius:7px;box-shadow: 0 1px 1px rgba(0,0,0,.05);">
						<div class="logo animate0 bounceIn">&nbsp;</div><br/><br/><br/><br/>
						<!-- <div class="underTitleLine gradientbg"></div> -->
						<form id="form" action="<?php echo $base_url.'panel/login_submit';?>" method="post" autocomplete="new-password">
							<div class="inputwrapper login-alert"></div>
							<div class="inputwrapper animate1 bounceIn">
								<input type="text" name="username" id="username" class="themeInputs" placeholder="Masukkan username" autocomplete="new-password" value=""/>
							</div>
							<div class="inputwrapper animate2 bounceIn">
								<input type="password" name="password" id="password" class="themeInputs" placeholder="Masukkan password" autocomplete="new-password" value=""/>
							</div>
							<div class="animate3 bounceIn" align="center">
								<button name="submit" id="action" class="gradientBtn">Sign In</button>
							</div>
						</form>
					</div>
				</div>
				<div class="loginfooter">
					<img src="<?php echo $base_url.'assets/images/logo-bsre.png';?>" alt="Logo BSRE" style="height: 50px" />
					<p>Copyright &copy; 2015. Aplikasi Persuratan.</p>
				</div>
				<div style="clear:both"></div>
			</div>
		</div>
	</body>
</html>
