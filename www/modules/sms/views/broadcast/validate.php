<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".chzn-select").chosen();
		jQuery("#form1")
			.validate({
				rules: {
					telp: "required",
					sms: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 0)
							{
								jQuery('#result').html('<p class="pesan gagal"><span class="iconfa-check"></span> Sms Gagal Terkirim! Error : ' + data.message + '<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							else
							{
								if(data.status == 1)
								{
									jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Sms Berhasil Terkirim<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
									jQuery('#form').trigger('reset');
									if(jQuery('#form .control-group').hasClass('success')) {
										jQuery('#form .control-group').removeClass('success').removeClass('error');
										jQuery('label.success').hide();
										jQuery('label.valid').hide();
									}
								}
								else
								{
									jQuery('#result').html('<p class="pesan gagal"><span class="iconfa-check"></span> Sms Gagal Terkirim! Error : ' + data.status + ' ' + data.message + '<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								}
							}
							jQuery('#action').button('reset');
							jQuery('#result').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
			
		jQuery("#form2")
			.validate({
				rules: {
					telp2: "required",
					sms2: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action2').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 1)
							{
								jQuery('#result2').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Sms Berhasil Terkirim<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								jQuery('#form2').trigger('reset');
								if(jQuery('#form2 .control-group').hasClass('success')) {
									jQuery('#form2 .control-group').removeClass('success').removeClass('error');
									jQuery('label.success').hide();
									jQuery('label.valid').hide();
								}
							}
							else
							{
								jQuery('#result2').html('<p class="pesan gagal"><span class="iconfa-check"></span> Sms Gagal Terkirim! Error : ' + data.status + ' ' + data.message + '<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							jQuery('#action2').button('reset');
							jQuery('#result2').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});

	});
</script>