/*
 * Additional function for chat.html
 *	Written by ThemePixels	
 *	http://themepixels.com/
 *
 *	Copyright (c) 2012 ThemePixels (http://themepixels.com)
 *	
 *	Built for Shamcey Premium Responsive Admin Template
 *  http://themeforest.net/category/site-templates/admin-templates
 */

jQuery(document).ready(function(){

	///// SEARCH USER FROM RIGHT SIDEBAR /////
	jQuery('.chatsearch input').bind('focusin focusout',function(e){
		if(e.type == 'focusin') {
			if(jQuery(this).val() == 'Search') jQuery(this).val('');	
		} else {
			if(jQuery(this).val() == '') jQuery(this).val('Search');	
		}
	});
	
	// this will enable facebook like chat in every page
	
	/*if(jQuery.cookie('enable-chat'))
		jQuery('#enablechat').text('Disable');*/
	
		
	jQuery('#enablechat').click(function(){
		
		if(jQuery.cookie('enable-chat')) {
				
			jQuery.removeCookie('enable-chat', { path: '/' });
			/*jQuery(this).text('Enable');*/
				
		} else {
				
			jQuery.cookie('enable-chat', true, { path: '/' });
			/*jQuery(this).text('Disable');*/
			
		}
		
		location.reload();
		return false;
	});
	
});
