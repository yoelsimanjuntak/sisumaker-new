<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,no-store,must-revalidate"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="expires" content="0"/>
        <title>SISUMAKER | QR-Code</title>
        <?php $base_url = base_url(); ?>
		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/images/favicon.png"/>
		<style>
			center, td{
				font-weight: 10px;
				font-family: 'RobotoRegular', 'Helvetica Neue', Helvetica, sans-serif;
			}
		</style>
	</head>
	<body>
		<br/><center>
			Sistem Informasi Surat Masuk Dan Keluar(SISUMAKER)<br/> Kota Tangerang Selatan
			<br/>menyatakan bahwa:
			<table>
				<tr>
					<td>Nomor Surat</td>
					<td>:</td>
					<td><?php echo $no_surat;?></td>
				</tr>
				<tr>
					<td>Dari</td>
					<td>:</td>
					<td><?php echo $dari;?></td>
				</tr>
				<tr>
					<td>Perihal</td>
					<td>:</td>
					<td><?php echo $prihal;?></td>
				</tr>
				<tr>
					<td>Tanggal Surat</td>
					<td>:</td>
					<td><?php echo $tgl_surat;?></td>
				</tr>
			</table>
			Adalah benar dan tercatat dalam database Kami.<br/>
			Untuk memastikan bahwa surat tersebut benar, pastikan bahwa URL pada browser Anda adalah <b>https://sisumaker.tangerangselatankota.go.id</b>
		</center>
	</body>
</html>