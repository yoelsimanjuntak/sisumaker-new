<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Outbox extends DataMapper 
{

    var $table = 'outbox';
    var $key = 'ID';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here
    function insert($form) 
	{
        return $this->db
			->insert($this->table, $form);
    }
	
	function not_send()
	{
		return $this->db
			->select('
				outbox.ID as id_outbox,
				outbox.DestinationNumber,
				outbox.TextDecoded,
				outbox.SendingDateTime,
				tmpegawai.n_pegawai
			')
			->join('tmpegawai','outbox.DestinationNumber = tmpegawai.telp')
			->order_by('outbox.ID', 'desc')
			->get($this->table);
	}
	
	function count_not_send()
	{
		return $this->db
			->select('SendingDateTime')
			->order_by('ID', 'desc')
			->get($this->table);
	}
	
	function delete($id) 
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }
	
}

?>
