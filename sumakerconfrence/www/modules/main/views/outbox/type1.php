<?php if ($result->num_rows() != 0) { if($c_read != 0){ foreach ($result->result() as $row) { ?>	
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page;?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
		</div>
	</div>
	<?php if($result_history->num_rows() != 0){
	$n_pegawai = NULL;
	foreach($result_history->result() as $row_history)
	{
		$n_pegawai .= $row_history->n_pegawai.', ';
		$d_entry = $row_history->d_entry;
		$file_surat_koreksi = $row_history->file_surat_koreksi;
	} ?>
	<div class="msgauthor">
		<div class="thumb"><img src="<?php echo base_url().'uploads/photo/'.$this->session->userdata('photo'); ?>" alt="" /></div>
		<div class="authorinfo">
			<span class="date pull-right"><?php echo indonesian_date($d_entry);?></span>
			<h5><strong><?php echo $this->session->userdata('realname');?></strong></h5>
			<span class="to">to <?php echo $n_pegawai;?></span>
		</div>
	</div>
	<?php } ?>
	<div class="msgbody">
		<table class="table table-bordered table-invoice">
			<colgroup>
				<col class="con1" />
				<col class="con0" />
				<col class="con1" />
				<col class="con0" />
			</colgroup>
			<tbody>
				<tr>
					<td width="120px">Surat Dari</td>
					<td><?php echo $row->dari;?></td>
					<td width="120px">Tanggal Surat</td>
					<td width="320px"><?php echo indonesian_date($row->tgl_surat);?></td>
				</tr>
				<tr>
					<td>Sifat</td>
					<td colspan="3"><?php echo $row->n_sifatsurat;?></td>
				</tr>
				<tr>
					<td>Perihal</td>
					<td colspan="3"><?php echo $row->prihal;?></td>
				</tr>
			</tbody>
		</table>
		<?php echo $row->isi_surat; ?>
		<div class="btn-group">
			<a href="https://docs.google.com/viewer?url=<?php echo base_url().$row->file;?>" class="btn btn-large text-link" target="_blank" title="View"><i class="iconfa-paper-clip"></i> File Surat</a>
			<a href="<?php echo base_url().$row->file;?>" class="btn btn-large text-link" target="_blank" title="Download"><i class="iconfa-download-alt"></i></a>
		</div>
		<?php if($row->file_lampiran != ''){ ?>
		<div class="btn-group">
			<a href="https://docs.google.com/viewer?url=<?php echo base_url().$row->file_lampiran;?>" class="btn btn-large text-link" target="_blank" title="View"><i class="iconfa-paper-clip"></i> File Lampiran</a>
			<a href="<?php echo base_url().$row->file_lampiran;?>" class="btn btn-large text-link" target="_blank" title="Download"><i class="iconfa-download-alt"></i></a>
		</div>
		<?php } ?>
		<!-- Jika Ada Catatan -->
		<?php if($row->catatan != ''){ ?>
		<blockquote>
			<p><?php echo $row->catatan;?></p>
			<small><?php echo $row->n_pegawai;?></small>
		</blockquote>
		<?php }?>
		
		<!-- Jika Ada File Surat Koreksi -->
		<?php if($file_surat_koreksi != ''){ ?>
		<a href="<?php echo base_url().$file_surat_koreksi;?>" class="btn"><i class="iconfa-paper-clip"></i> File Surat koreksi</a>
		<?php }?>
	</div>
</div>
<?php } }else { ?>
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page;?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
		</div>
	</div>
	<div class="msgauthor">
		<p><b>Terjadi Kesalahan!</b> Anda tidak mendisposisikan surat ini.</p>
	</div>
</div>
<?php }} else { ?>
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page;?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
		</div>
	</div>
	<div class="msgauthor">
		<p><b>Terjadi Kesalahan!</b> Data Tidak Ditemukan</p>
	</div>
</div>
<?php } ?>