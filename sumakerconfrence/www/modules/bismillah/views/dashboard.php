<?php
$total =  $count_masuk + $count_keluar;
if($count_masuk == 0)
{
$masuk = 0;
}
else
{
$masuk = round($count_masuk / $total * 100);
}
if($count_keluar== 0)
{
$keluar = 0;
}
else
{
$keluar = round($count_keluar / $total * 100);
}
?>


<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('#chartplace').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Perhitungan Jumlah Surat Masuk dan Keluar, 2014'
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Jumlah',
            data: [
                ['Surat Keluar',  <?php echo $keluar;?>],
                {
                    name: 'Surat Masuk',
                    y: <?php echo $masuk;?>,
                    sliced: true,
                    selected: true
                }
            ]
        }]
    });
});
</script>
<style>
rect{
	fill: rgba(255,255,255,0.4);
}
</style>
<div class="row-fluid">
	<div class="span7">
		<table class="table table-invoice">
			<thead>
				<tr><td colspan="2" style="text-align: center; background-color: rgba(255,255,255,0.4);">DRAF SURAT MASUK DAN KELUAR</td></tr>
			</thead>
			<tbody>
				<tr>
					<td class="width10" style="text-align: center;"> &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<div class="pageicon"><span class="iconfa-edit"></span></div></td>
					<td class="width70">
						1. Surat Masuk: <?php echo $count_masuk;?><br>
						2. Surat Keluar: <?php echo $count_keluar;?><br><br>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="span5">
		<div class="widgetbox">                        
			<div class="headtitle">
				<h4 class="widgettitle"></h4>
			</div>
			<div class="widgetcontent">
				<div style="width:100%">
					<div id="chartplace" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>	
		</div>
	</div>
</div>