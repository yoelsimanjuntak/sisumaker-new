<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Broadcast extends ICA_AdminCont
{
	var $page = "sms/broadcast/";
	var $view = "sms/broadcast/";
	var $icon = "bullhorn";

    function __construct() 
	{
        parent::__construct();
		$this->restrict('8');
		$this->tmpegawai = new Tmpegawai();
		$this->outbox = new Outbox();
		$this->load->library('api_sms_class_reguler_json');
    }

	function index()
	{
		$data = array(
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/css/", "jquery.chosen.css"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "chosen.jquery.min.js")
			),
			'breadcrubs' => " Broadcast",
			'title' => 'SMS Broadcast',
			'content' => 'form',
			'result' => $this->tmpegawai->select()
		);
		$this->load->view('template', $data);
	}
	
	function send_sms()
	{
		$valid = false;
		$true = false;
		$pegawai_list = $this->input->post('pegawai');
		$pegawai_list_len = count($pegawai_list);
		for($i=0; $i<$pegawai_list_len; $i++)
		{
			$valid = $pegawai_list[$i];
		}
		if($valid == false)
		{
			$data = array(
				'status' => 0,
				'message' => "Silahkan pilih no tujuan."
			);
			echo json_encode($data);
		}
		else
		{
			$senddata = array(
				'apikey' => SMSGATEWAY_APIKEY,
				'datapacket'=> array()
			);
			
			$message = $this->input->post('sms');
			for($i=0; $i<$pegawai_list_len; $i++)
			{
				$number = $pegawai_list[$i];
				if($number != "" && $number != "-")
				{
					$sendingdatetime = ""; 
					array_push($senddata['datapacket'],array(
						'number' => trim($number),
						'message' => urlencode(stripslashes(utf8_encode($message)))));
						
					$true = true;
				}
			}

			if($true == false)
			{
				$data = array(
					'status' => 0,
					'message' => "Nomor tujuan kosong."
				);
				echo json_encode($data);
			}
			else
			{
				$sms = new api_sms_class_reguler_json();
				$sms->setIp("103.219.112.9");
				$sms->setData($senddata);
				$responjson = $sms->send();
				//$this->saldo_sms->save($responjson);
				header('Content-Type: application/json');
				echo $responjson;
			}
		}
	}
	
	function send_sms_one()
	{
		$senddata = array(
			'apikey' => SMSGATEWAY_APIKEY, 
			'datapacket'=> array()
		);
		
		$number1 			= $this->input->post('telp2');
		$message1			= $this->input->post('sms2');
		$sendingdatetime1 	= ""; 
		array_push($senddata['datapacket'],array(
			'number' => trim($number1),
			'message' => urlencode(stripslashes(utf8_encode($message1)))));

		$sms = new api_sms_class_reguler_json();
		$sms->setIp("103.219.112.9");
		$sms->setData($senddata);
		$responjson = $sms->send();
		//$this->saldo_sms->save($responjson);
		header('Content-Type: application/json');
		echo $responjson;
	}
}