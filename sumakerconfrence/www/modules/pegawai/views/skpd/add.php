<div class="widgetbox box-inverse view_load">
	<?php echo $this->load->view($this->view.'validate');?>
	<h4 class="widgettitle"></h4>
	<div class="widgetcontent wc1">
		<form id="form1" class="stdform" method="post" action="<?php echo site_url().$this->page.'save';?>" novalidate="novalidate">
			<div class="par control-group">
				<label class="control-label" for="n_unitkerja">Nama SKPD</label>
				<div class="controls">
					<input type="text" name="n_skpd" id="n_skpd" class="input-large">
				</div>
			</div>
			<div class="par control-group">
				<label class="control-label" for="initial">Inisial</label>
				<div class="controls">
					<input type="text" name="initial" id="initial" class="input-large">
				</div>
			</div>
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action">Simpan</button>
				<a href="#" id="close_add" class="btn btn-default">Batal</a>
			</p>
		</form>
	</div>
</div>