<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class S_Outbox extends ICA_AdminCont
{
	var $page = "sms/s_outbox/";
	var $view = "sms/s_outbox/";
	var $icon = "phone";

    function __construct() 
	{
        parent::__construct();
		$this->outbox = new Outbox();
        $this->load->helper('function_helper');
    }
	
	function cek_min(){$result = $this->outbox->count_not_send();$error=0;$return=0;foreach($result->result() as $row){if($error == 0){$date_time = explode(' ', $row->SendingDateTime);$mulai=$date_time[1];$selesai=date('H:i:s');list($jam,$menit,$detik)=explode(':',$mulai);$buatWaktuMulai=mktime($jam,$menit,$detik,1,1,1);list($jam,$menit,$detik)=explode(':',$selesai);$buatWaktuSelesai=mktime($jam,$menit,$detik,1,1,1);$selisihDetik=$buatWaktuSelesai-$buatWaktuMulai;if($selisihDetik >= 300){$error = 1;}}if($error == 1){$return=$return+1;}}$output = array('count' => $return);echo json_encode($output);}
	
	function cek()
	{
		$result = $this->outbox->count_not_send();
		$error=0;
		$return=0;
		foreach($result->result() as $row)
		{ 
			if($error == 0)
			{
				$date_time = explode(' ', $row->SendingDateTime);
				$mulai=$date_time[1];
				$selesai=date('H:i:s');
				list($jam,$menit,$detik)=explode(':',$mulai);
				$buatWaktuMulai=mktime($jam,$menit,$detik,1,1,1);
				list($jam,$menit,$detik)=explode(':',$selesai);
				$buatWaktuSelesai=mktime($jam,$menit,$detik,1,1,1);
				$selisihDetik=$buatWaktuSelesai-$buatWaktuMulai;
				if($selisihDetik >= 300)
				{
					$error = 1;
				}
			}
			if($error == 1)
			{
				$return=$return+1;
			}
		}
		$output = array('count' => $return);
		echo json_encode($output);
	}

	function index()
	{
		$data = array(
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.dataTables.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
				),
			'breadcrubs' => " Send Outbox",
			'title' => 'Sms Gateway <h5>Error Panding</h5>',
			'content' => 'table',
			'sort' => 3,
			'result' => $this->outbox->not_send()
		);
		$this->load->view('template', $data);
	}
	
	function send($id)
	{
		$row = $this->outbox->where("ID = '".$id."'")->get();
		$data = array(
			'DestinationNumber' => $row->DestinationNumber,
			'TextDecoded' => $row->TextDecoded,
			'CreatorID' => 'Gammu',
			'DeliveryReport' => 'yes'
		);
		$this->outbox->insert($data);
		$this->outbox->delete($id);
		redirect(base_url().$this->page);
	}
}