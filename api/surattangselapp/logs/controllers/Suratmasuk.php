<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'controllers/Auth.php';

class Suratmasuk extends Auth
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credential: true');
        header('Access-Control-Max-Age: 86400');
        header('Access-Control-Allow-Methods:GET,POST,OPTIONS');
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $this->load->model('db_dml');
        $this->load->model('db_select');
        $this->load->model('mymodel');
        $this->load->model('tr_surat_penerima');
        $this->load->library('onesignal');
        $this->load->helper('function_helper');
        $this->load->library('onesignal');
        if ($this->cektoken() == false) {
            header('Content-type: application/json');
            $output = [
                'status' => 0,
                'message' => 'Wrong Token',
                'token' => $this->input->get_request_header('Authorization')
            ];
            exit(json_encode($output));
        }
    }

    public function index_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $per_page = $this->get('per_page', true);
        $page = $this->get('page', true);
        $output = [];

        if (!$per_page || !$page) {
            $output = [
                'status' => 0,
                'message' => 'Parameter request per_page & page tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            if ($page == 1) {
                $offset = 0;
            } else {
                $page = $page - 1;
                $offset = $page * $per_page;
            }

            $query = $this->mymodel->surat_masuk($pegawai_id, $per_page, $offset);
            if ($query->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Tidak ada surat.'
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                $pesan_baru = 0;
                foreach ($query->result() as $row) {
                    if ($row->status == 0) {
                        $pesan_baru++;
                    }
                    array_push($output, [
                        'asal_surat' => $row->dari,
                        'tgl_dibuat' => $row->tgl_surat,
                        'tgl_diterimaTU' => $row->tgl_terima_surat,
                        'tgl_dikirim' => simple_date($row->d_entry),
                        'pengirim' => $row->n_pegawai,
                        'perihal' => $row->prihal,
                        'n_sifatsurat' => $row->n_sifatsurat,
                        'status_dibaca' => $row->status,
                        'id_persuratan' => $row->id_persuratan,
                        'id_compose' => $row->id_compose,
                        'id_sifatsurat' => $row->id_sifatsurat,
                        'photo' => $row->photo
                        //'id_compose' => 2
                    ]);
                }

                $o = [
                    'status' => 1,
                    'page' => $page,
                    'pesan_baru' => $pesan_baru,
                    'total_page' => $this->mymodel->c_all_surat_masuk($pegawai_id) / $per_page,
                    'data' => $output
                ];
                $this->response($o, Auth::HTTP_OK);
            }
        }
    }

    public function filter_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $per_page = $this->get('per_page', true);
        $page = $this->get('page', true);
        $search = $this->get('search', true);
        $output = [];
        $filter_by = '';

        if (!$per_page || !$page || !$search) {
            $output = [
                'status' => 0,
                'message' => 'Parameter request per_page / page / search  tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            $exp = explode('-', $search);
            if ($exp[0] == 'b') {
                $where = ' AND tr_surat_penerima.status = ' . $exp[1];
                if ($exp[1] == 0) {
                    $filter_by = 'Belum dibaca';
                } elseif ($exp[1] == 1) {
                    $filter_by = 'Sudah dibaca';
                }
            } elseif ($exp[0] == 'c') {
                $where = ' AND tmpersuratan.id_sifatsurat = ' . $exp[1];
                if ($exp[1] == 1) {
                    $filter_by = 'Sangat Segera';
                } elseif ($exp[1] == 2) {
                    $filter_by = 'Segera';
                } elseif ($exp[1] == 3) {
                    $filter_by = 'Rahasia';
                } elseif ($exp[1] == 4) {
                    $filter_by = 'Biasa';
                }
            } elseif ($exp[0] == 's') {
                $v_search = substr($search, 2);
                $where = " 	AND (tmpersuratan.prihal LIKE '%" . $v_search . "%'
							OR tmpersuratan.dari LIKE '%" . $v_search . "%'
							OR tmpersuratan.no_surat LIKE '%" . $v_search . "%'
							OR tmpersuratan.tgl_surat LIKE '%" . $v_search . "%'
							OR tmpersuratan.d_awal_kegiatan LIKE '%" . $v_search . "%'
							OR tmpersuratan.d_akhir_kegiatan LIKE '%" . $v_search . "%'
							OR tmpersuratan.v_kegiatan LIKE '%" . $v_search . "%'
							OR tmpersuratan.isi_surat LIKE '%" . $v_search . "%'
							OR tmpegawai.n_pegawai LIKE '%" . $v_search . "%')";
            } elseif ($exp[0] == 't') {
                $where = ' AND tr_surat_penerima.cc = 1';
                if ($exp[1] == 1) {
                    $filter_by = 'cc';
                }
            } elseif ($exp[0] == 'ttd') {
                $where = ' AND tmpersuratan.id_pegawaittd = '. $pegawai_id ;
                
                if ($exp[1] == 1) {
                    $filter_by = 'ttd';
                }
            } elseif ($exp[0] == 'surat_keluar') {
                $where = ' AND tmpersuratan.id_compose = '. 2 ;
                $filter_by = 'Surat Keluar';
            } elseif ($exp[0] == 'skpd') {
                if ($exp[1] == 1) {
                    $filter_by = 'Surat OPD';
                    $where = ' AND tmpersuratan.id_skpd_in = tmpersuratan.id_skpd_out ' ;
                } elseif ($exp[1] == 2) {
                    $filter_by = 'Surat Luar OPD';
                    $where = ' AND tmpersuratan.id_skpd_in != tmpersuratan.id_skpd_out ' ;
                }
            }

            if ($page == 1) {
                $offset = 0;
            } else {
                $page = $page - 1;
                $offset = $page * $per_page;
            }

            $query = $this->mymodel->surat_masuk($pegawai_id, $per_page, $offset, $where);
            if ($query->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Tidak ada surat.'
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                foreach ($query->result() as $row) {
                    array_push($output, [
                        'asal_surat' => $row->dari,
                        'tgl_dibuat' => $row->tgl_surat,
                        'tgl_diterimaTU' => $row->tgl_terima_surat,
                        'tgl_dikirim' => simple_date($row->d_entry),
                        'pengirim' => $row->n_pegawai,
                        'perihal' => $row->prihal,
                        'n_sifatsurat' => $row->n_sifatsurat,
                        'status_dibaca' => $row->status,
                        'id_persuratan' => $row->id_persuratan,
                        'id_compose' => $row->id_compose,
                        'id_sifatsurat' => $row->id_sifatsurat,
                        'status_setujui' => $row->status_setujui,
                        'photo' => $row->photo,
                        'filter_by' => $filter_by
                        //'id_compose' => 2
                    ]);
                }

                $o = [
                    'status' => 1,
                    'page' => $page,
                    'total_page' => $this->mymodel->c_all_surat_masuk($pegawai_id) / $per_page,
                    'Filter by ' => $filter_by,
                    'Total Surat' => $query->num_rows(),
                    'data' => $output
                ];
                $this->response($o, Auth::HTTP_OK);
            }
        }
    }

    public function filterInfo_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $search = $this->get('search', true);
        $output = [];
        $filter_by = '';


        //Count surat belum di baca
        $where = ' AND tr_surat_penerima.status = 0' ;
        $filter_by = 'belum_dibaca';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                'total' => $query->row()->jumlah
            ];

        //Count surat sudah di baca
        $where = ' AND tr_surat_penerima.status = 1' ;
        $filter_by = 'sudah_dibaca';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                'total' => $query->row()->jumlah
            ];

        //Count surat sangat segera
        $where = ' AND tmpersuratan.id_sifatsurat = 1';
        $filter_by = 'sangat_segera';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                'total' => $query->row()->jumlah
            ];

        //Count surat sangat segera
        $where = ' AND tmpersuratan.id_sifatsurat = 2';
        $filter_by = 'segera';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                'total' => $query->row()->jumlah
            ];

        //Count surat rahasia
        $where = ' AND tmpersuratan.id_sifatsurat = 3';
        $filter_by = 'rahasia';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                'total' => $query->row()->jumlah
            ];

        //Count surat biasa
        $where = ' AND tmpersuratan.id_sifatsurat = 4';
        $filter_by = 'biasa';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                'total' => $query->row()->jumlah
            ];

        //Count surat cc
        $where = ' AND tr_surat_penerima.cc = 1';
        $filter_by = 'cc';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                 'total' => $query->row()->jumlah
             ];

        //Count surat keluar
        $where = ' AND tmpersuratan.id_compose = 2';
        $filter_by = 'surat_keluar';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                 'total' => $query->row()->jumlah
             ];

        //Count surat yg di ttd
        $where = ' AND tmpersuratan.id_pegawaittd = '. $pegawai_id;
        $filter_by = 'ttd_saya';
        $query = $this->mymodel->surat_masukGroupTtd($pegawai_id, $where);
        $output[$filter_by] = [
                'total' => $query->row()->jumlah
            ];

        $o = [
                'status' => 1,
                'data' => $output
            ];
        $this->response($o, Auth::HTTP_OK);

        // if ($exp[0] == 'b') {

            //     if ($exp[1] == 0) {
            //         $filter_by = 'Belum dibaca';
            //     } elseif ($exp[1] == 1) {
            //         $filter_by = 'Sudah dibaca';
            //     }
            // } elseif ($exp[0] == 'c') {
            //     $where = ' AND tmpersuratan.id_sifatsurat = ' . $exp[1];
            //     if ($exp[1] == 1) {
            //         $filter_by = 'Sangat Segera';
            //     } elseif ($exp[1] == 2) {
            //         $filter_by = 'Segera';
            //     } elseif ($exp[1] == 3) {
            //         $filter_by = 'Rahasia';
            //     } elseif ($exp[1] == 4) {
            //         $filter_by = 'Biasa';
            //     }
            // }

            // $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
            // if ($query->num_rows() == 0) {
            //     $output = [
            //         'status' => 0,
            //         'message' => 'Tidak ada surat.'
            //     ];
            //     $this->response($output, Auth::HTTP_OK);
            // } else {


            //     $o = [
            //         'status' => 1,
            //         'Filter_by ' => $filter_by,
            //         'Total_surat' => $query->row()->jumlah,
            //         // 'data' => $output
            //     ];
            //     $this->response($o, Auth::HTTP_OK);

            // }
    }

    public function cSurat0_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $search = $this->get('search', true);
        $output = [];
        $filter_by = '';


        //Count surat belum di baca
        $where = ' AND tr_surat_penerima.status = 0' ;
        $filter_by = 'belum_dibaca';
        $query = $this->mymodel->surat_masukGroup($pegawai_id, $where);
        $output[$filter_by] = [
                'total' => $query->row()->jumlah
            ];

        $o = [
                'status' => 1,
                'data' => $output
            ];
        $this->response($o, Auth::HTTP_OK);
    }


    public function count_all_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $o = [
            'status' => 1,
            'count' => $this->mymodel->c_all_surat_masuk($pegawai_id)
        ];
        $this->response($o, Auth::HTTP_OK);
    }

    public function count_new_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $o = [
            'status' => 1,
            'count' => $this->mymodel->c_new_surat_masuk($pegawai_id)
        ];
        $this->response($o, Auth::HTTP_OK);
    }

    public function read_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $id_persuratan = $this->get('id_persuratan', true);
        $output = [];

        if (!$id_persuratan) {
            $output = [
                'status' => 0,
                'message' => 'id_persuratan tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            if ($this->tr_surat_penerima->cek($pegawai_id, $id_persuratan)->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Anda tidak mendapatkan disposisi surat ini.',
                    'id_persuratan' => $id_persuratan,
                    'pegawai' => $pegawai_id
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                foreach ($this->tr_surat_penerima->read_all($pegawai_id, $id_persuratan)->result() as $row) {
                    $data = [
                        'status' => 1,
                        'd_read' => date('Y-m-d H:i:s')
                    ];
                    $this->tr_surat_penerima->update($row->id, $data);
                }

                $disposisi = [];
                $row = $this->mymodel->read($id_persuratan)->row();

                if ($row->id_pegawaittd == $pegawai_id) {
                    $canTtd = 1;
                } else {
                    $canTtd = 0;
                }

                foreach ($this->mymodel->history_all($id_persuratan, $pegawai_id)->result() as $row_history) {
                    $d_entry = $row_history->d_entry;
                    $ke_pegawai = $row_history->ke_pegawai . '; ';
                    $ke_pegawai_cc = '';
                    if ($row_history->num > 1) {	//Jika didisposisikan lebih dari satu pegawai
                        $ke_pegawai = '';
                        foreach ($this->mymodel->read_to($id_persuratan, $d_entry, 0)->result() as $row_to) {
                            $ke_pegawai .= $row_to->ke_pegawai . '; ';
                        }

                        $cc = $this->mymodel->read_to($id_persuratan, $d_entry, 1);
                        if ($cc->num_rows() != 0) {
                            foreach ($cc->result() as $row_cc) {
                                $ke_pegawai_cc .= $row_cc->ke_pegawai . '; ';
                            }
                        }
                    }
                    $date_d = date_create($row_history->d_entry);
                    $date_f = date_format($date_d, "Y-m-d");

                    array_push($disposisi, [
                        'dari_pegawai' => $row_history->dari_pegawai,
                        'dari_photo' => $row_history->photo,
                        'file_coretan' => $row_history->file_coretan,
                        'ke_pegawai' => $ke_pegawai,
                        'ke_pegawai_cc' => $ke_pegawai_cc,
                        'tgl' => $date_f,
                        'catatan' => $row_history->catatan,
                        'file_surat_koreksi' => $row_history->file_surat_koreksi,
                        'lampiran_surat_koreksi' => $row_history->lampiran_file_koreksi
                    ]);
                }

                $ttd = false;
                if ($pegawai_id == $row->id_pegawaittd) {
                    $ttd = 1;
                }
                $output = [
                    'status' => 1,
                    'no_surat' => $row->no_surat,
                    'dari' => $row->dari,
                    'tgl_surat' => $row->tgl_surat,
                    'd_awal_kegiatan' => $row->d_awal_kegiatan,
                    'd_akhir_kegiatan' => $row->d_akhir_kegiatan,
                    'v_kegiatan' => $row->v_kegiatan,
                    'perihal' => $row->prihal,
                    'n_sifatsurat' => $row->n_sifatsurat,
                    'status_setujui' => $row->status_setujui,
                    'id_pegawaittd' => $row->id_pegawaittd,
                    'canTtd' => $canTtd,
                    //'n_jenissurat' => $row->n_jnissurat,
                    'memo' => $row->isi_surat,
                    'file' => $row->file,
                    'file_lampiran' => $row->file_lampiran,
                    //'c_lintas_skpd' => $row->c_lintas_skpd,
                    'id_compose' => $row->id_compose,
                    //'id_compose' => 2,
                    'ttd' => $ttd,
                    //'ttd' => 1,
                    'status_ttd' => $row->status_setujui,
                    'disposisi' => $disposisi
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        }
    }

    public function readTtd_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $id_persuratan = $this->get('id_persuratan', true);
        $output = [];

        if (!$id_persuratan) {
            $output = [
                'status' => 0,
                'message' => 'id_persuratan tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            if ($this->tr_surat_penerima->cek($pegawai_id, $id_persuratan)->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Anda tidak mendapatkan disposisi surat ini.',
                    'id_persuratan' => $id_persuratan,
                    'pegawai' => $pegawai_id
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                foreach ($this->tr_surat_penerima->read_all($pegawai_id, $id_persuratan)->result() as $row) {
                    $data = [
                        'status' => 1,
                        'd_read' => date('Y-m-d H:i:s')
                    ];
                    $this->tr_surat_penerima->update($row->id, $data);
                }

                $disposisi = [];
                $row = $this->mymodel->read($id_persuratan)->row();
                foreach ($this->mymodel->history_all($id_persuratan, $pegawai_id)->result() as $row_history) {
                    $d_entry = $row_history->d_entry;
                    $ke_pegawai = $row_history->ke_pegawai . '; ';
                    $ke_pegawai_cc = '';
                    if ($row_history->num > 1) {	//Jika didisposisikan lebih dari satu pegawai
                        $ke_pegawai = '';
                        foreach ($this->mymodel->read_to($id_persuratan, $d_entry, 0)->result() as $row_to) {
                            $ke_pegawai .= $row_to->ke_pegawai . '; ';
                        }

                        $cc = $this->mymodel->read_to($id_persuratan, $d_entry, 1);
                        if ($cc->num_rows() != 0) {
                            foreach ($cc->result() as $row_cc) {
                                $ke_pegawai_cc .= $row_cc->ke_pegawai . '; ';
                            }
                        }
                    }
                    $date_d = date_create($row_history->d_entry);
                    $date_f = date_format($date_d, "Y-m-d");

                    array_push($disposisi, [
                        'dari_pegawai' => $row_history->dari_pegawai,
                        'dari_photo' => $row_history->photo,
                        'ke_pegawai' => $ke_pegawai,
                        'ke_pegawai_cc' => $ke_pegawai_cc,
                        'tgl' => $date_f,
                        'catatan' => $row_history->catatan,
                        'file_surat_koreksi' => $row_history->file_surat_koreksi,
                        'lampiran_surat_koreksi' => $row_history->lampiran_file_koreksi
                    ]);
                }

                $ttd = false;
                if ($pegawai_id == $row->id_pegawaittd) {
                    $ttd = 1;
                }
                $output = [
                    'status' => 1,
                    'no_surat' => $row->no_surat,
                    'dari' => $row->dari,
                    'tgl_surat' => $row->tgl_surat,
                    'd_awal_kegiatan' => $row->d_awal_kegiatan,
                    'd_akhir_kegiatan' => $row->d_akhir_kegiatan,
                    'v_kegiatan' => $row->v_kegiatan,
                    'perihal' => $row->prihal,
                    'n_sifatsurat' => $row->n_sifatsurat,
                    'status_setujui' => $row->status_setujui,
                    'id_pegawaittd' => $row->id_pegawaittd,
                    //'n_jenissurat' => $row->n_jnissurat,
                    'memo' => $row->isi_surat,
                    'file' => $row->file,
                    'file_lampiran' => $row->file_lampiran,
                    //'c_lintas_skpd' => $row->c_lintas_skpd,
                    'id_compose' => $row->id_compose,
                    //'id_compose' => 2,
                    'ttd' => $ttd,
                    //'ttd' => 1,
                    'status_ttd' => $row->status_setujui,
                    'disposisi' => $disposisi
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        }
    }

    // function info_get()
    // 	{
    // 		$decode = $this->viewtoken();
    // 		$pegawai_id = $decode->pegawai_id;
    // 		$id_persuratan = $this->get('id_persuratan',TRUE);
    // 		$output = array();
//
    // 		if(!$id_persuratan)
    // 		{
    // 			$output = array(
    // 				'status' => FALSE,
    // 				'message' => "id_persuratan tidak boleh kosong."
    // 			);
    // 			$this->response($output,Auth::HTTP_BAD_REQUEST);
    // 		}
    // 		else
    // 		{
    // 			$output = array();
    // 			$result = $this->tr_surat_penerima->info($id_persuratan);
    // 			foreach($result->result() as $row_info){
    // 				if($row_info->paraf != ''){
    // 					$paraf = base_url().$row_info->paraf;
    // 				}else{
    // 					$paraf = "*) paraf belum diunggah";
    // 				}
//
    // 				if($row_info->d_read != ''){
    // 					$tgl_baca = $row_info->d_read;
    // 				}else{
    // 					$tgl_baca = "<i class='text-warning'>* belum dibaca</i>";
    // 				}
//
    // 				array_push($output, array(){
    // 					'tgl_kirim' => $row_info->d_entry,
    // 					'dari' => $row_info->n_pegawai_dari.' ('.$row_info->n_jabatan_dari.' '.$row_info->n_unitkerja_dari.')',
    // 					'paraf' => $paraf,
    // 					'ke' => $row_info->n_pegawai_ke.' ('.$row_info->n_jabatan_ke.' '.$row_info->n_unitkerja_ke.')',
    // 					'tgl_baca' => $tgl_baca
    // 				});
    // 			}
    // 			$this->response($output,Auth::HTTP_OK);
    // 		}
    // 	}

    public function listdisposisi_get()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $id_persuratan = $this->get('id_persuratan', true);
        $output = [];

        $query = $this->mymodel->disposisi($auth_id);
        if ($query->num_rows() == 0) {
            $output = [
                'status' => false,
                'message' => 'List disposisi kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            foreach ($query->result() as $row) {
                array_push($output, [
                    'status' => 1,
                    'id_pegawai' => $row->id ,
                    'n_jabatan' => $row->n_jabatan,
                    'n_unitkerja' => $row->n_unitkerja,
                    'n_pegawai' => $row->n_pegawai  . ' | '. $row->n_jabatan,
                    'telp' => $row->telp
                ]);
            }
            $this->response($output, Auth::HTTP_OK);
        }
    }

    public function notif($data)
    {
        $notif = new onesignal();
        $notif->setData($data);
        $responjson1 = $notif->ios();
        $responjson2 = $notif->android();



        // return $responjson;
    }

    public function disposisi_post()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $dari = $decode->realname;
    
        $auth_id = $decode->auth_id;
        $skpd_id = $decode->skpd_id;
        $id_persuratan = $this->post('id_persuratan', true);
        $pegawai_list = $this->post('id_pegawai', true);
        $file_coretan = $this->post('file_coretan', true);
        $file_surat_koreksi = $this->post('file_surat_koreksi', true);
        $lampiran_file_koreksi = $this->post('lampiran_file_koreksi', true);
        $output = [];

        if (!$file_coretan) {
            $file_coretan = null;
        }

        if (!$id_persuratan) {
            $output = [
                'status' => 0,
                'message' => 'Id persuratan tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            if ($this->tr_surat_penerima->cek($pegawai_id, $id_persuratan)->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Anda tidak mendapatkan disposisi surat ini.'
                ];
                $this->response($output, Auth::HTTP_BAD_REQUEST);
            } else {
                $valid = false;
                $pegawai_list_len = count($pegawai_list);
                for ($i = 0; $i < $pegawai_list_len; $i++) {
                    $valid = $pegawai_list[$i];
                }
                if ($valid == false) {
                    //echo var_dump($_POST);
                    $output = [
                        'status' => 0,
                        'message' => 'Gagal mendisposisikan surat, penerima surat tidak boleh kosong.'
                    ];
                    $this->response($output, Auth::HTTP_OK);
                } else {
                    $next = true;
                    $exten = '';
                   

                    if ($next == true) {
                        // $senddata = array(
                        // 					'apikey' => SMSGATEWAY_APIKEY,
                        // 					'callbackurl' => SMSGATEWAY_CALLBACKURL,
                        // 					'datapacket'=> array()
                        // 				);
                        $catatan = $this->post('catatan');
                        $surat = $this->mymodel->select('tmpersuratan', 'id', $id_persuratan)->row();
                        $pegawai = $this->mymodel->select('tmpegawai', 'id', $pegawai_id)->row();
                        $jabatan = $this->mymodel->select('tmjabatan', 'id', $pegawai->tmjabatan_id)->row()->n_jabatan;
                        $unitkerja = $this->mymodel->select('tmunitkerja', 'id', $pegawai->tmunitkerja_id)->row()->initial;
                        $send = 'Ada disposisi surat dari ' . $jabatan . ' ' . $unitkerja . '. Silahkan masuk ke SISUMAKER untuk melihat isi surat';
                        $true = false;
                        $d_entry = date('Y-m-d H:i:s');
                        for ($i = 0; $i < $pegawai_list_len; $i++) {
                            if ($surat->d_awal_kegiatan != '' && $surat->d_akhir_kegiatan != '') {
                                $data = [
                                    'id_persuratan' => $id_persuratan,
                                    'id_pegawai_dari' => $pegawai_id,
                                    'id_pegawai_ke' => $pegawai_list[$i],
                                    'catatan' => $catatan,
                                    'file_surat_koreksi' => $file_surat_koreksi,
                                    'lampiran_file_koreksi' => $lampiran_file_koreksi,
                                    'status' => 0,
                                    'd_entry' => $d_entry,
                                    'id_skpd_dari' => $skpd_id,
                                    'd_awal_kegiatan' => $surat->d_awal_kegiatan,
                                    'd_akhir_kegiatan' => $surat->d_akhir_kegiatan,
                                    'v_kegiatan' => $surat->v_kegiatan,
                                    'file_coretan' => $file_coretan
                                ];
                            } else {
                                $data = [
                                    'id_persuratan' => $id_persuratan,
                                    'id_pegawai_dari' => $pegawai_id,
                                    'id_pegawai_ke' => $pegawai_list[$i],
                                    'catatan' => $catatan,
                                    'file_surat_koreksi' => $file_surat_koreksi,
                                    'lampiran_file_koreksi' => $lampiran_file_koreksi,
                                    'status' => 0,
                                    'd_entry' => $d_entry,
                                    'id_skpd_dari' => $skpd_id,
                                    'file_coretan' => $file_coretan
                                ];
                            }
                            $this->db_dml->insert('tr_surat_penerima', $data);

                            $telp = $this->db_select->select_where('tmpegawai', ['id' => $pegawai_list[$i]])->telp;
                            if ($telp != '-') {
                                array_push($senddata['datapacket'], [
                                    'number' => trim($telp),
                                    'message' => urlencode(stripslashes(utf8_encode($send))),
                                    'sendingdatetime' => '']);

                                //------- Telp hanya untuk SEMENTARA
                                //------- 28 April 2017, sms untuk ajudan pa wakil walikota
                                if ($pegawai_list[$i] == '211') {
                                    $number = '081286571031';
                                    $sendingdatetime = '';
                                    array_push($senddata['datapacket'], [
                                        'number' => trim($number),
                                        'message' => urlencode(stripslashes(utf8_encode($send))),
                                        'sendingdatetime' => $sendingdatetime]);
                                }
                            }
                            $true = true;
                            $Onesignal = [
                                'dari' => $dari,
                                'ke' => $pegawai_list[$i],
                                'perihal' =>  $surat->prihal
                            ];
        
                            $this->notif($Onesignal);
                        }
                        // $sms = new api_sms_class_reguler_json();
                        // 				$sms->setIp(SMSGATEWAY_IPSERVER);
                        // 				$sms->setData($senddata);
                        // 				$responjson = $sms->send();
                        // 				$this->saldo_sms->save($responjson);

                        $output = [
                            'status' => 1,
                            'message' => 'Surat berhasil terkirim.'
                        ];
                        $this->response($output, Auth::HTTP_OK);
                    }
                }
            }
        }
    }

    public function fileCoret_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;

        if ($_FILES['coret']['name'] != '') {
            $name = date('YmdHis') . $auth_id;
            $explode = explode('.', str_replace(' ', '', $_FILES['coret']['name']));
            $endof = '.' . end($explode);
            $exten = 'uploads/coret_dispo/' . $name . $endof;
            $config['file_name'] = $name;
            $config['allowed_types'] = 'png';
            $config['upload_path'] = '../uploads/coret_dispo/';
            $config['max_size'] = '30000';
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('coret')) {
                $output = [
                    'status' => false,
                    'message' => 'error ' . $this->upload->display_errors()
                ];
                // return false;
                $this->response($output, Auth::HTTP_BAD_REQUEST);
            } else {
                $output = [
                    'status' => true,
                    'message' => 'berhasil di upload',
                    'path' => $exten,
                    'filename' => $name . $endof
                ];
                $this->response($output, Auth::HTTP_OK);
                // return $exten;
            }
        } else {
            $output = [
                'status' => false,
                'message' => 'Berkas yang di-upload Error!'
            ];
            // return false;
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }



    public function ttd_post()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $auth_id = $decode->auth_id;
        $skpd_id = $decode->skpd_id;
        $id_persuratan = $this->post('id_persuratan', true);
        $output = [];

        if (!$id_persuratan) {
            $output = [
                'status' => 0,
                'message' => 'Id persuratan tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            if ($this->tr_surat_penerima->cek($pegawai_id, $id_persuratan)->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Surat tidak ada atau Anda tidak mendapatkan disposisi surat ini.'
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                $row = $this->db_select->select_where('tmpersuratan', ['id' => $id_persuratan]);
                if ($pegawai_id != $row->id_pegawaittd) {
                    $output = [
                        'status' => 0,
                        'message' => 'Gagal melakukan TTD, pengguna tidak memiliki akses untuk menandatangani surat ini.'
                    ];
                    $this->response($output, Auth::HTTP_OK);
                } else {
                    if ($row->status_setujui != 0) {
                        $output = [
                            'status' => 0,
                            'message' => 'Gagal melakukan TTD, surat sudah pernah di TTD.'
                        ];
                        $this->response($output, Auth::HTTP_OK);
                    } else {
                        $id_skpd_in = $row->id_skpd_in;
                        $result = $this->mymodel->id_tu($id_skpd_in);
                        if ($result->num_rows() == 0) {
                            $output = [
                                'status' => 0,
                                'message' => 'Gagal melakukan TTD, sistem tidak dapat menemukan user Bag. Umum. Silahkan hubungi Admin SISUMAKER'
                            ];
                            $this->response($output, Auth::HTTP_OK);
                        } else {
                            // if ($_FILES['userfile']['name'] == '') {
                            //     $output = [
                            //         'status' => 0,
                            //         'message' => 'File hasil TTD tidak boleh kosong.'
                            //     ];
                            //     $this->response($output, Auth::HTTP_BAD_REQUEST);
                            // } else {
                            // $name = date('YmdHis') . $auth_id;
                            // $explode = explode('.', str_replace(' ', '', $_FILES['userfile']['name']));
                            // $endof = '.' . end($explode);
                            // $exten = 'uploads/surat_koreksi/' . $name . $endof;
                            // $config['file_name'] = $name;
                            // $config['allowed_types'] = 'pdf|doc|docx';
                            // $config['upload_path'] = '../uploads/surat_koreksi/';
                            // $config['max_size'] = '30000000';
                            // $config['overwrite'] = true;
                            // $this->load->library('upload', $config);
                            // if (!$this->upload->do_upload()) {
                            //     $output = [
                            //         'status' => 0,
                            //         'message' => "Error saat mengunggah : '" . $this->upload->display_errors() . "'"
                            //     ];
                            //     $this->response($output, Auth::HTTP_OK);
                            // } else {
                            // Send A Message
                            // foreach ($result->result() as $row) {
                            //     $telp = $row->telp;
                            //     $id_tu = $row->id;
                            // }
                            $data = [
                                        'id_persuratan' => $id_persuratan,
                                        'id_pegawai_dari' => $pegawai_id,
                                        'id_pegawai_ke' => $id_tu,
                                        'catatan' => '',
                                        
                                        'status' => 0,
                                        'd_entry' => date('Y-m-d H:i:s'),
                                        'id_skpd_dari' => $skpd_id
                                    ];
                            $this->db_dml->insert('tr_surat_penerima', $data);

                            $nomor = $this->mymodel->nomor_save($id_skpd_in, 2);
                            $data = [
                                        'status_setujui' => 1,
                                        //'no_surat' => $nomor_surat,
                                        'no_agenda' => 'K-' . $nomor,
                                        'id_pegawai_setujui' => $pegawai_id
                                    ];
                            $this->db_dml->update('tmpersuratan', $data, ['id' => $id_persuratan]);

                            //notifikasi



                            $output = [
                                        'status' => 1,
                                        'message' => 'Surat berhasil di TTD.'
                                    ];
                            $this->response($output, Auth::HTTP_OK);
                            // }
                            // }
                        }
                    }
                }
            }
        }
    }

    public function fileSuratKoreksi_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;

        if ($_FILES['userfile']['name'] != '') {
            $name = date('YmdHis') . $auth_id;
            $explode = explode('.', str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.' . end($explode);
            $exten = 'uploads/surat_koreksi/' . $name . $endof;
            $config['file_name'] = $name;
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
            $config['upload_path'] = '../uploads/surat_koreksi/';
            $config['max_size'] = '30000';
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $output = [
                    'status' => false,
                    'message' => 'error ' . $this->upload->display_errors()
                ];
                $this->response($output, Auth::HTTP_BAD_REQUEST);
            } else {
                $output = [
                    'status' => true,
                    'message' => 'berhasil di upload',
                    'path' => $exten,
                    'filename' => $name . $endof
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        } else {
            $output = [
                'status' => false,
                'message' => 'Berkas yang di-upload Error!'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }

    public function fileLampiranKoreksi_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $name = date('YmdHis') . $auth_id;
        $file_lampiran = '';
        if ($_FILES['file_lampiran']['name'] != '') {
            $name_l = $name . '_l';
            $explode_l = explode('.', str_replace(' ', '', $_FILES['file_lampiran']['name']));
            $endof_l = '.' . end($explode_l);
            $file_lampiran = 'uploads/surat_koreksi/' . $name_l . $endof_l;
            $config_l['file_name'] = $name_l;
            $config_l['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
            $config_l['upload_path'] = '../uploads/surat_koreksi/';
            $config_l['max_size'] = '30000';
            $config_l['overwrite'] = true;
            $this->load->library('upload', $config_l);
            if (!$this->upload->do_upload('file_lampiran')) {
                $output = [
                    'status' => false,
                    'message' => '2 ' . $this->upload->display_errors()
                ];
                $this->response($output, Auth::HTTP_BAD_REQUEST);
                $next = false;
                exit();
            } else {
                $output = [
                    'status' => true,
                    'message' => 'berhasil di upload',
                    'path' => $file_lampiran,
                    'filename' => $name_l . $endof_l
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        } else {
            $output = [
                'status' => false,
                'message' => 'Berkas yang di-upload Error!'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }
}
