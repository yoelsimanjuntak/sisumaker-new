<?php if( count($scheduled_call) > 0 ) : ?>
<div class="row-fluid" id="hasActiveCall">
	<div class="span12">
		<div class="widgetbox box-inverse">                       
			<div class="headtitle">
				<h4 class="widgettitle">Panggilan Video Terjadwal</h4>
			</div>
			<div class="widgetcontent">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="2%">No</th>
							<th>Nama Kegiatan</th>
							<th>Tanggal</th>
							<th class="text-center">Jam Conference</th>
							<th class="text-center">#</th>
						</tr>
					</thead>
					<tbody>
						<?php $index=0; foreach( $scheduled_call as $schedule ): $index++; ?>
							<tr>
								<td><?php echo $index; ?></td>
								<td><?php echo $schedule->agenda ?></td>
								<td><?php echo date('d F Y', strtotime( $schedule->start)) ?></td>
								<td class="text-center"><?php echo date('H:i', strtotime( $schedule->start)) ?></td>
								<td class="text-center">
									<a class="btn btn-default" onclick="return leaveVideoGroup(this, <?php echo $schedule->id ?>, <?php echo $schedule->create_by ?>)"><i class="iconfa-signout"></i> Leave Room</a>
									<a class="btn btn-primary" onclick="return joinVideoCall( <?php echo $schedule->id ?>, <?php echo $schedule->room_name ?>, <?php echo $schedule->create_by ?>)"><i class="iconfa-signin"></i> Enter Room</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div><!--widgetcontent-->
		</div><!--widgetbox-->
	</div>
</div>
<?php endif; ?>

<div class="row-fluid">

<div class="span4">
	<div class="widgetbox box-inverse">                       
		<div class="headtitle">
			<h4 class="widgettitle">Invite Users</h4>
		</div>
		<div class="widgetcontent nopadding" style="min-height:310px;">
			<ul class="chatusers vc-list-invite-user" id="invite-user">
				<?php foreach($chatlist as $row_chat): ?>
					<li data-id="<?php echo $row_chat->id ?>">
						<a href="#">
							<img src="<?php echo base_url().'uploads/photo/'.$row_chat->photo;?>" alt="img"/>
							<span>
								<?php echo $row_chat->n_pegawai; ?>
							</span>
						</a>
						<a class="pull-right vc-btn-direct-call" onclick="return NatievaVideoCall.inviteUserToVideoCall(this, '<?php echo $row_chat->id?>', '<?php echo $row_chat->n_pegawai;  ?>')">
							<span class="iconfa-facetime-video"></span>
						</a>
						<a class="pull-right vc-btn-invite" onclick="return NatievaVideoCall.addUser(this)">
							<span class="iconfa-plus-sign"></span>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div><!--widgetcontent-->
	</div><!--widgetbox-->	
	
</div><!--span4-->

<div class="span4">
	<div class="widgetbox box-inverse">                       
		<div class="headtitle">
			<h4 class="widgettitle">Users Invited</h4>
		</div>
		<div class="widgetcontent nopadding" style="height:310px;">
			<ul class="chatusers vc-list-invite-user" id="invited-user"></ul>
		</div><!--widgetcontent-->
	</div><!--widgetbox-->
</div>

<div class="span4 chatcontent">
	<div class="widgetbox box-inverse">                       
		<div class="headtitle">
			<h4 class="widgettitle">Buat Jadwal Panggilan Video</h4>
		</div>
		<div class="widgetcontent">
			<form action="<?php echo base_url() . 'video_call/room/save_room' ?>">
				<input type="hidden" name="user_invited">
				<div class="control-group">
					<label class="control-label" for="inputPassword">Judul Agenda Kegiatan</label>
					<div class="controls">
						<input type="text" name="agenda" placeholder="Agenda Kegiatan" required>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="inputEmail">Tanggal</label>
					<div class="controls">
						<input type="text" name="startdate" class="datetimepicker" placeholder="Tanggal">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassword">Jam</label>
					<div class="controls">
						<input type="text" name="starttime" class="timepicker input-small" placeholder="Jam">
					</div>
				</div>
				
				<div class="control-group">
					<div class="controls">
						<button type="button" class="btn btn-primary" onclick="return NatievaVideoCall.createRoom(this)">Simpan Jadwal Panggilan</button>
					</div>
				</div>
			</form>
		</div>		
	</div>
	
</div><!--span4-->
</div>

<script>
    jQuery(document).ready( function(){
		NatievaVideoCall.initCreateRoom();
		
		window.joinVideoCall = function(roomid, room, create_by) {

			localStorage.setItem('room', JSON.stringify({
				id: roomid,
				room: room,
				create_by: create_by,
				call_type: 'groub' 
			}))
				
			NatievaVideoCall.openVideoCallBrowser(room);
		}
		
		window.leaveVideoGroup = function(ele, id, create_by) {

			NatievaVideoCall.customSwalAlert({
				type: 'warning',
				title: 'Anda yakin?',
				html: 'Apakah anda yakin ingin keluar dari panggilan ini?',
				showCancelButton: true,
				showConfirmButton: true,
				confirmButtonText: 'Ya, Yakin.',
				cancelButtonText: 'Batal',
				cancelButtonColor: '#d33',
				confirmButtonColor: '#3085d6',
				reverseButtons: true,
			}).then(function(result){
	
				if (result.value) {
					
					NatievaVideoCall.showLoadingDialog('Sedang memproses...')
					jQuery.ajax({
						type: 'POST',
						url: base + 'video_call/room/leave_video_call',
						data: {
							roomid: id,
							creatorid: create_by
						},
						dataType: 'json',
						success: function( r ) {
							if( r.status == 'ok' ) {
								jQuery(ele).closest('tr').remove();
								NatievaVideoCall.customSwalAlert({
									type: 'success',
									title: 'Sukses',
									showConfirmButton: true,
									html: 'Anda berhasil keluar dari grub panggilan'
								})
							} else {
								NatievaVideoCall.customSwalAlert({
									type: 'error',
									title: 'error',
									showConfirmButton: true,
									html: 'Terjadi kesalahan dalam sistem. Silahkan coba kembali'
								})
							}
						},
						error: function() {
							NatievaVideoCall.customSwalAlert({
								type: 'error',
								title: 'error',
								showConfirmButton: true,
								html: 'Terjadi kesalahan dalam sistem. Silahkan coba kembali'
							})
						}
					})

				} else {
					swal.close();
				}
			})


		}

    });
</script>