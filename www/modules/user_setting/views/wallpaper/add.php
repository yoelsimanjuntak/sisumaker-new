<div class="widgetbox box-inverse view_load">
    <?php echo $this->load->view($this->view . 'validate'); ?>
    <h4 class="widgettitle"></h4>
    <div class="widgetcontent wc1">
        <form id="form1" class="stdform" method="post" action="<?php echo site_url() . $this->page . 'save'; ?>" enctype="multipart/form-data">
            <div class="par control-group">
                <label class="control-label" for="n_wallpaper">Nama</label>
                <div class="controls"><input type="text" name="n_wallpaper" id="n_wallpaper" class="input-large"></div>
            </div>
            <div class="par control-group">
                <label class="control-label" for="userfile">File Wallpaper</label>
                <div class="controls"><input type="file" name="userfile" id="userfile" class="input-large"></div>
            </div>
            <div class="par control-group">
                <label class="control-label" for="c_status">Status</label>
                <div class="controls">
                    <select name="c_status">
                        <option value="">Pilih</option>
                        <option value="1">Aktif</option>
                        <option value="2">Non-Aktif</option>
                    </select>
                </div>
            </div>
            <p class="stdformbutton">
                <button class="btn btn-primary" id="action">Simpan</button>
                <a href="#" id="close_add" class="btn btn-default">Batal</a>
            </p>
        </form>
    </div>
</div>