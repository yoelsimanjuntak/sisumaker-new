<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#form1")
			.validate({
				rules: {
					n_jnissurat: "required",
					userfile: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				}
			});
	});
</script>