<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Auth
{
	var $CI = false;

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->helper('url');
	}

	// function login($user, $pass)
	// {
		// $query = $this->CI->db
			// ->select('
				// user.nouser,
				// user.nama,
				// user.user,
				// user.pass,
				// user.nouserstatus
			// ')
			// ->from('user')
			// ->where('user', $user)
			// ->where('pass', $pass)
			// ->get();

		// if($query->num_rows() > 0) 
		// {
			// foreach($query->result() as $row)
			// {
				// if($row->nouserstatus == 1)
				// {
					// if($row->user == $user)
					// {
						// $data = array(
							// 'nouser' => $row->nouser,
							// 'nama' => $row->nama,
							// 'user' => $row->user,
							// 'pass' => $row->pass,
							// 'nouserstatus' => $row->nouserstatus
						// );
						// $this->CI->session->set_userdata($data);
						// return true;
					// }
				// }
				// else
				// {
					// return false;
				// }
			// }
		// }
		// else
		// {
			// return false;
		// }
			
	// }

	function restrict($to)
	{
		if(!$this->CI->session->userdata('userid'))
		{
			redirect();
		}
		else
		{
			$query = $this->CI->db
				->from('tmuser_userauth')
				->where('tmuser_id', $this->CI->session->userdata('userid'))
				->where('userauth_id', $to)
				->get();
			if($query->num_rows() == 0)
			{
				
				redirect('error');
			}
		}
	}
}