<div class="span8 chatcontent">
	<div id="chatmessage" class="chatmessage">
		<div id="chatmessageinner">
			<?php 
			if($result->num_rows() != 0){
				foreach($result->result() as $row){
					$c = 'reply';
					$img = $photo;
					$name = $n_pegawai;
					if($row->from == $this->session->userdata('auth_id')){
						$c = '';
						$img = base_url().'uploads/photo/'.$this->session->userdata('photo');
						$name = 'Anda';
					} ?>
					
			<p class="<?php echo $c;?>">
				<img src="<?php echo $img;?>" alt="" />
				<span class="msgblock">
					<b><?php echo $name;?></b>
					<span class="time">- <?php echo indo_date_time($row->send);?></span>
					<span class="msg"><?php echo $row->message;?></span>
				</span>
			</p>
					<?php
				}
			}
			else
			?>
		</div><!--chatmessageinner-->
	</div><!--chatmessage-->
	 
	<div class="messagebox">
		<span class="inputbox">
		   <input type="text" id="msgbox" name="msgbox" class="input-block-level" placeholder="Type message and hit enter..." user_id="<?php echo $id;?>"/>
		</span>
	</div>	
</div><!--span8-->

<div class="span4">
	<div class="widgetbox box-inverse">                       
		<div class="headtitle">
			<h4 class="widgettitle">Users</h4>
		</div>
		<div class="widgetcontent nopadding" style="height:410px;">
			<!--<div class="chatsearch">
			   <input type="text" name="" class="input-block-level" placeholder="Search User" />
			</div>-->
			<ul class="chatusers">
				<?php foreach($chatlist as $row_chat){ ?>
				<li<?php if($row_chat->id == $id){ ?> class="active"<?php }?>><a href="<?php echo base_url().$this->page.'read/'.$row_chat->id;?>"><img src="<?php echo base_url().'uploads/photo/'.$row_chat->photo;?>" alt="img"/> <span><?php echo $row_chat->n_pegawai;?><?php if($row_chat->new_msg != 0){ ?><span class="msgcount"><?php echo $row_chat->new_msg;?></span><?php } ?></span></a></li>
				<?php }?>
			</ul>
		</div><!--widgetcontent-->
	</div><!--widgetbox-->
	
	
</div><!--span4-->
<script type="text/javascript">
jQuery(document).ready(function(){
	
	var he = jQuery('#chatmessage')[0].scrollHeight;
	jQuery('#chatmessage').scrollTop(he);
	jQuery('#msgbox').focus();
	
	// chat slim scroll
	if(jQuery('.chatusers').length > 0) {
	   
		jQuery('.chatusers').slimscroll({
			color: '#000',
			size: '10px',
			width: 'auto',
			height: '400px'
		});
	}
	
	///// SUBMIT A MESSAGE USING ENTER KEY PRESS /////
	jQuery('.messagebox input').keypress(function(e){
		if(e.which == 13)
			enterMessage();
	});
	
	function enterMessage() {
		var msg = jQuery('.messagebox input').val();
		var msg_data = jQuery('.messagebox input');
		var id = msg_data.attr('user_id');
		
		if(msg != '') {
			jQuery.ajax({
				type:'post',
				url: base + "chat/message/save_msg/" + id,
				cache: false,
				data: msg_data,
				dataType: 'json',
				success: function(data) {
					if(data.status == 1)
					{
						jQuery('#chatmessageinner').append('<p><img src="<?php echo base_url().'uploads/photo/'.$this->session->userdata('photo') ?>" alt="" />'
														   +'<span class="msgblock radius2"><b>Anda</b> <span class="time">- ' + data.send + '</span>'
														   +'<span class="msg">'+msg+'</span></span></p>');
						jQuery('.messagebox input').val('');
						jQuery('.messagebox input').focus();
						
						var he = jQuery('#chatmessage')[0].scrollHeight;
						jQuery('#chatmessage').scrollTop(he);
					}
					else
					{
						jQuery('#chatmessageinner').append('<p><img src="<?php echo base_url().'uploads/photo/'.$this->session->userdata('photo') ?>" alt="" />'
														   +'<span class="msgblock radius2"><b>Anda</b> <span class="time">- ERROR!</span>'
														   +'<span class="msg">ERROR! '+msg+'</span></span></p>');
						jQuery('.messagebox input').val('');
						jQuery('.messagebox input').focus();
						
						var he = jQuery('#chatmessage')[0].scrollHeight;
						jQuery('#chatmessage').scrollTop(he);
					}
				}
			});
		}	
	}
});
</script>