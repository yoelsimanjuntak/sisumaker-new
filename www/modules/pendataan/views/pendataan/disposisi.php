<?php
echo $this->lib_load_css_js->load_css(base_url() , "assets/css/", "jquery.chosen.css");
echo $this->lib_load_css_js->load_js(base_url() , "assets/js/", "chosen.jquery.min.js"); ?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".chzn-select").chosen();
		jQuery('.closer').live('click',function(){
			parent.jQuery.fancybox.close();
		});
	});
</script>
<?php if ($result->num_rows() != 0) { foreach ($result->result() as $row) { ?>
<div class="widgetbox box-inverse">
    <h4 class="widgettitle"></h4>
    <div class="widgetcontent wc1 stdform">
		<form id="form4" class="stdform" method="post" action="<?php echo base_url().$this->page.'disposisi_submit/'.$row->id;?>" novalidate="novalidate">
			<input type="hidden" name="d_awal_kegiatan" value="<?php echo $row->d_awal_kegiatan;?>"/>
			<input type="hidden" name="d_akhir_kegiatan" value="<?php echo $row->d_akhir_kegiatan;?>"/>
			<input type="hidden" name="v_kegiatan" value="<?php echo $row->v_kegiatan;?>"/>
			<table class="table table-bordered table-invoice">
				<colgroup>
					<col class="con1" />
					<col class="con0" />
					<col class="con1" />
					<col class="con0" />
				</colgroup>
				<tbody>
					<tr>
						<td width="120px">Jenis Surat</td>
						<td colspan="3"><?php echo $this->surat->where("id = '".$row->id_surat."'")->get()->n_surat;?></td>
					</tr>
					<tr>
						<td width="120px">Surat Dari</td>
						<td><?php echo $row->dari;?></td>
						<td width="120px">Diterima Tanggal</td>
						<td><?php echo indonesian_date($row->tgl_terima_surat);?></td>
					</tr>
					<tr>
						<td>No. Surat</td>
						<td><?php echo $row->no_surat;?></td>
						<td>No. Agenda</td>
						<td><?php echo $row->no_agenda;?></td>
					</tr>
					<tr>
						<td>Tanggal Surat</td>
						<td><?php echo indonesian_date($row->tgl_surat);?></td>
						<td>Sifat</td>
						<td><?php echo $this->tmsifat_surat->where("id = '".$row->id_sifatsurat."'")->get()->n_sifatsurat;?></td>
					</tr>
					<tr>
						<td>Perihal</td>
						<td><?php echo $row->prihal;?></td>
						<td colspan="2">							
							<?php if($row->d_awal_kegiatan != '' && $row->d_akhir_kegiatan != '' && $row->v_kegiatan != ''){ ?>
							Kegiatan:<br/>
							<?php echo indo_date_time($row->d_awal_kegiatan);?> <sup>s</sup>/<sub>d</sub> <?php echo indo_date_time($row->d_akhir_kegiatan);?><br/>
							Ket: <?php echo $row->v_kegiatan;} ?>
						</td>
					</tr>
					<tr>
						<td>Kirim Ke</td>
						<td colspan="3">
							<select id="pegawai" name="pegawai[]" required="" data-placeholder="Nama Pemgawai - Jabatan" class="chzn-select span8" multiple="multiple" style="width:700px;z-index:99999;" tabindex="3">
								<?php foreach($result_disposisi->result() as $row_disposisi){?>
								<option value="<?php echo $row_disposisi->id;?>"><?php echo $row_disposisi->n_pegawai.' - '.$row_disposisi->n_jabatan.' '.$row_disposisi->n_unitkerja;?></option>
								<?php }?>
							</select>
						</td>
					</tr>
				</body>
			</table>
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action"><i class="iconfa-plane"></i> Kirim</button>
				&nbsp; <a href="#" class="closer">Batal</a>
			</p>
		</form>
    </div>
</div>
<?php } } else { ?><p>Data Tidak Ditemukan, <a href="#" id="refresh">Segarkan Tabel</a></p><?php } ?>