<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of MY_Controller class
 *
 * @author Dichi Al Faridi
 */

class MY_Controller extends Controller {

    public function __construct() {
        parent::Controller();
    }

}
