<!DOCTYPE HTML>
<html lang="en">
	<head>
		<title>Sistem Informasi Persuratan</title>
		<link rel="icon" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA5UlEQVQ4y62TOw6CQBCGuREFEAIlr4YOGkJoIDwPYTyHx7HxFNqCCj4vgP4FKDIICRR/Jrsz8+3u7AxT1zUzR8ziAI7jnqIoPijBNwowTfPiOM6ZEnxTnrAZUR/AsuxaEIRKluW7pmlXwzBIKYpykyRpx/N8hZwWoOt6Zdt2OXT1RnEcH97xqyAI9qqqli0AyUmS5FmWDQp+z/NOlmVtXdeF/QBA/5cMpWma+75/BAQWOT1AFEXFWBHDMCwAIwGgY00VEnvwIQaxHcDvN1G98NUDnViyPamaNCdOmoXZgKlduMg0vgBgZppW4v1eXgAAAABJRU5ErkJggg==">
		<?php 
			$base_url = base_url();
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "bootstrap.min.css");
		?>
	</head>
	<body>
		<?php $this->load->view($this->view.$content);?>
	</body>
</html>
<script type="text/javascript" language="javascript">window.print();</script>