<div class="row">
	<div class="col l8">
		<div class="bg-white">
			<div class="panel-header clearfix">
				<div class="left">
					<span class="brand">SISUMAKER</span> <span class="tagline">Video Call</span>
				</div>
				<div class="right">
					<?php 
					if( ! empty( $dataRoom->agenda ) ) :
						echo 'Agenda : ' .  $dataRoom->agenda;
					endif;
					?>
					
				</div>
			</div>
			<div class="panel-body">
				<button class="mobile-menu"></button>
				<div class="vc-video-wrapper" id="callMeta" data-create-by="<?php echo $dataRoom->create_by ?>" data-callid="<?php echo $roomName ?>">
					<div id="callVideoPlaceholder"></div>                   
				</div>
				<button class="mobile-menu right" onclick="return NatievaVideoCall.openMobileMenu(this)">
					<i class="iconfa-align-center"></i>
				</button>

			</div>

		</div>
	</div>
	<div class="col l4 mobile-sidebar">
		<div class="bg-white">
			<div class="user-connected">
				<div class="panel-header clearfix">
					<div class="left title">
						Pengguna Terhubung
					</div>
					<div class="right">
						<a class="add-user" onclick="return NatievaVideoCall.openInviteList(this, _DATAROOM.id)">
							<span class="iconfa-plus"></span>
						</a>
					</div>
				</div>
	
				<div class="panel-body">
					<ul class="list-user connected-user">
						<?php foreach( $connectedUser as $user ) : ?>
							<?php if( $user->id != $userLogged['auth_id'] ) : ?>
								<li class="userid-<?php echo $user->id ?>">
									<div class="photo">
										<img src="<?php echo base_url()  .'/uploads/photo/'. $user->photo ?>">
									</div>
									<span class="name"><?php echo ucwords(strtolower($user->realname)) ?></span>

									<?php if( $user->status > 0 ) : ?>
										<div class="status ok">
											<i class="iconfa-ok"></i>
										</div>
									<?php else: ?>
										<div class="status"></div>
									<?php endif; ?>

								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>					
				</div>
			</div>

			<div class="user-invite" style="display: none">
				<div class="panel-header clearfix">
					<div class="left title">
						Undang Pengguna
					</div>
					<div class="right">
						<a class="close-panel" onclick="return NatievaVideoCall.closeInviteList(this)">
							<span class="iconfa-minus-sign"></span>
						</a>
					</div>
				</div>
	
				<div class="panel-body">
					<div class="loader">
						<img src="<?php echo base_url().'assets/video-call/loader.gif'?>" id="imageLoader">
					</div>
					<ul class="list-user"></ul>					
				</div>
			</div>

			<button class="close-mobile-menu" onclick="return NatievaVideoCall.closeMobileMenu(this)">Tutup</button>
		</div>
	</div>
</div>

<img src="<?php echo base_url().'assets/video-call/loader.gif'?>" id="imageLoader" style="display: none">
<div id="redirect" data-url="<?php echo base_url().'main/inbox'?>"></div>
<script>
	
    jQuery(document).ready( function(){
		
		NatievaVideoCall.call();
		
	});


</script>