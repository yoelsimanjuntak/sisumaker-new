<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Log_verifikasi extends DataMapper 
{

    var $table = 'log_verifikasi';
    var $key = 'id';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here
	function select($key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($this->table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($this->table);
		}
	}
	
    function insert($form) 
	{
        return $this->db
			->insert($this->table, $form);
    }

    function update($id, $form) 
	{
        return $this->db
			->where($this->key, $id)
			->update($this->table, $form);
    }

	function del_log($tmuser_id)
	{
        return $this->db
			->where('tmuser_id', $tmuser_id)
			->delete($this->table);
	}
	
    function delete($id) 
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }

}

?>
