<script type="text/javascript">
	var oTable;
	datatables_ps("<?php echo site_url().$this->page.'data';?>", <?php echo $sort;?>);
	fancybox();
	jQuery(document).ready(function(){
		jQuery('#delete').live('click', function(){
			if(confirm("Yakin Ingin Menghapus data ini?")){
				jQuery.ajax({
					"type":"post",
					"cache":false,
					"url":jQuery(this).attr("to"),
					"success":function(){
						jQuery('#result').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button><strong style="font-weight:bold">Sukses!</strong> Data Berhasil Dihapus.</div>').fadeIn('slow');
						oTable.fnDraw();
					}
				});
			}
			return false;
		});
	});
	function cek_outbox() {
		jQuery("#count_outbox").html('...');
		jQuery.ajax({
			type:'post',
			url: "<?php echo base_url();?>pendataan/s_outbox/cek_min",
			cache: false,
			dataType: 'json',
			success: function(data) {
				if(data.count != 0)
				{
					jQuery("#count_outbox").html(data.count);
				}
				else
				{
					jQuery("#count_outbox").html('');
				}
			}
		});
		var waktu = setTimeout("cek_outbox()", 40000);
	}

	function cek_sentitems() {
		jQuery("#count_sentitems").html('...');
		jQuery.ajax({
			type:'post',
			url: "<?php echo base_url();?>pendataan/s_senditem/cek_min",
			cache: false,
			dataType: 'json',
			success: function(data) {
				if(data.count != 0)
				{
					jQuery("#count_sentitems").html(data.count);
				}
				else
				{
					jQuery("#count_sentitems").html('');
				}
			}
		});
		var waktu = setTimeout("cek_sentitems()", 50000);
	}
</script>
<div id="result"></div>
<a href="<?php echo base_url().$this->page.'add';?>"><button class="btn btn-success btn-large">Pendataan Surat</button></a>
<div class="load" style="display:none;"></div>
<table id="dyntable" class="table table-bordered">
    <colgroup>
        <col class="con0" style="align: center; width: 4%" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
    </colgroup>
    <thead>        
		<tr>
			<th width="15px">No</th>
			<th width="80px">No Agenda</th>
			<th width="120px">No Surat</th>
			<th>Dari</th>
			<th width="120px">Tanggal Surat</th>
			<th width="120px">Tanggal Terima</th>
			<th width="100px">Sifat Surat</th>
			<th width="115px">Disposisi</th>
			<th width="100px">Aksi</th>
		</tr>
    </thead>
	<tbody>
		<tr>
			<td valign="top" colspan="9" class="dataTables_empty">Loading <img src="<?php echo base_url().'assets/images/loaders/loader19.gif';?>"/></td>
		</tr>
	</tbody>
</table>