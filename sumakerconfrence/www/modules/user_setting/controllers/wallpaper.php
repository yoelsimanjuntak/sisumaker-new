<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class wallpaper extends ICA_AdminCont {

    var $page = "user_setting/wallpaper/";
    var $view = "user_setting/wallpaper/";
    var $icon = "picture";

    function __construct() {
        parent::__construct();
		$this->restrict('9');
        $this->set_wallpaper = new Set_wallpaper();
        $this->load->helper('function_helper');
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Set Wallpaper ",
            'title' => 'Wallpaper',
            'content' => 'table',
            'sort' => 1
        );
        $this->load->view('template', $data);
    }

    function add() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> <a href='".base_url().$this->page."'>Wallpaper</a> <span class='separator'></span> Tambah",
            'title' => 'Wallpaper',
            'content' => 'add',
            'sort' => 1
        );
        $this->load->view('template', $data);
    }

    function edit($id) {
		$data = array(
			'result' => $this->set_wallpaper->select('id', $id),
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "bootstrap-fileupload.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "bootstrap-fileupload.min.css")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> <a href='".base_url().$this->page."'>Wallpaper</a> <span class='separator'></span> Edit",
            'title' => 'Wallpaper',
            'content' => 'edit'
        );
        $this->load->view('template', $data);
    }

    function save($id = false) {
        if ($id == false) {
            if ($this->set_wallpaper->select('n_wallpaper', $this->input->post('n_wallpaper'))->num_rows() == 0) {
				
				//Upload template berkas
				if($_FILES['userfile']['name'] != '')
				{
					$name = str_replace(" ", "_", $this->input->post('n_wallpaper'));
					$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
					$endof = '.'.end($explode);
					$exten = 'uploads/wallpaper/'.$name.date('ymd').$endof;
					$config['file_name'] = $name.date('ymd');
					$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
					$config['upload_path'] = 'uploads/wallpaper/';
					$config['max_size']	= '30000';
					$config['overwrite'] = true;
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload())
					{
						echo '
							<script type="text/javascript">
								alert("'.$this->upload->display_errors().'");
								history.go(-1);
							</script>';
						$next = false;
					}
					else
					{
						$data = array(
							'n_wallpaper' => $this->input->post('n_wallpaper'),
							'c_status' => $this->input->post('c_status'),
							'link' => $exten
						);
						$this->set_wallpaper->insert($data);
						redirect(base_url().$this->page);
					}
				}
				else
				{
					echo '
						<script type="text/javascript">
							alert("Anda belum memilih berkas untuk di-upload.");
							history.go(-1);
						</script>';
				}
            } else {
				echo '
					<script type="text/javascript">
						alert("Nama wallpaper surat sudah ada.");
						history.go(-1);
					</script>';
            }
        } else {
            if ($this->input->post('n_wallpaper_temp') == $this->input->post('n_wallpaper')) {
				$data = array(
					'c_status' => $this->input->post('c_status')
				);
				$this->set_wallpaper->update($id, $data);
				redirect(base_url().$this->page);
            } else {
                if ($this->set_wallpaper->select('n_wallpaper', $this->input->post('n_wallpaper'))->num_rows() == 0) {
					$data = array(
						'n_wallpaper' => $this->input->post('n_wallpaper'),
						'c_status' => $this->input->post('c_status')
					);
					$this->set_wallpaper->update($id, $data);
					redirect(base_url().$this->page);
                } else {
					echo '
						<script type="text/javascript">
							alert("Nama wallpaper surat sudah ada.");
							history.go(-1);
						</script>';
                }
            }
        }
    }

	function save_file($id)
	{
		if ($_FILES['userfile']['name'] != "") 
		{
			$name = str_replace(" ", "_", $this->input->post('n_wallpaper'));
			$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
			$endof = '.'.end($explode);
			$exten = 'uploads/wallpaper/'.$name.date('ymd').$endof;
			$config['file_name'] = $name.date('ymd');
			$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config['upload_path'] = 'uploads/wallpaper/';
			$config['max_size']	= '30000';
			$config['overwrite'] = true;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				echo '
					<script type="text/javascript">
						alert("'.$this->upload->display_errors().'");
						history.go(-1);
					</script>';
			}
			else
			{
				$temp_file = $this->set_wallpaper->where("id = '".$id."'")->get()->link;
				if(file_exists('./'.$temp_file)){
					unlink('./'.$temp_file);
				}
				$data = array(
					'link' => $exten
				);
				$this->set_wallpaper->update($id, $data);
				redirect(base_url().$this->page);
			}
		}
		else 
		{
            echo '
			<script type="text/javascript">
				alert("File Surat Harus Diisi");
				history.go(-1);
			</script>';
        }
	}
	
    function delete($id) {
		$temp_file = $this->set_wallpaper->where("id = '$id'")->get()->link;
		if(file_exists('./'.$temp_file)){
			unlink('./'.$temp_file);
		}
        $this->set_wallpaper->delete($id);
    }

    function data() {
		function cek_status($id)
		{
			$return = "";
			if($id == 2)
			{
				$return = "Non Aktif";
			}
			elseif($id == 1)
			{
				$return = "Aktif";
			}
			return $return;
		}
		
        $this->load->library('datatables');
        echo $this->datatables
                ->select('
					set_wallpaper.id,
					set_wallpaper.n_wallpaper,
					set_wallpaper.link,
					set_wallpaper.c_status
				')
                ->from('set_wallpaper')
				->edit_column('set_wallpaper.link', '<center>
					<img src="'.base_url().'$1" alt=""/>
					<center>','set_wallpaper.link')
				->edit_column('set_wallpaper.c_status', '<center>
					$1
					<center>','cek_status(set_wallpaper.c_status)')
                ->add_column('aksi', '<center>
					<a href="' . base_url() . $this->page . 'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
					<a id="delete" href="#" to="' . base_url() . $this->page . 'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>', 'set_wallpaper.id')
                ->generate();
    }

}