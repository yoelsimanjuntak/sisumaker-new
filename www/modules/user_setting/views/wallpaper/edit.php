<div id="result"></div>
<div class="widgetbox box-inverse">
    <?php echo $this->load->view($this->view . 'validate_edit'); ?>
    <h4 class="widgettitle"></h4>
    <div class="widgetcontent wc1">
		<?php if($result->num_rows() != 0){ foreach($result->result() as $row){ ?>
		<div class="row-fluid">
			<div class="span6">
				<form id="form1" class="stdform" method="post" action="<?php echo site_url() . $this->page . 'save/' . $row->id; ?>" novalidate="novalidate">
					<input type="hidden" name="n_wallpaper_temp"" class="input-large" value="<?php echo $row->n_wallpaper;?>">
					<div class="par control-group">
						<label class="control-label" for="n_wallpaper">Nama</label>
						<div class="controls"><input type="text" name="n_wallpaper" id="n_wallpaper" class="input-large" value="<?php echo $row->n_wallpaper;?>"></div>
					</div>
					<div class="par control-group">
						<label class="control-label" for="c_status">Status</label>
						<div class="controls">
							<select name="c_status">
								<option value="">Pilih</option>
								<option value="1"<?php if($row->c_status == 1){ ?> selected="selected"<?php }?>>Aktif</option>
								<option value="2"<?php if($row->c_status == 2){ ?> selected="selected"<?php }?>>Non-Aktif</option>
							</select>
						</div>
					</div>
					<p class="stdformbutton">
						<button class="btn btn-primary" id="action">Simpan</button>
						<a href="<?php echo base_url() . $this->page; ?>" class="btn btn-default">Batal</a>
					</p>
				</form>
			</div>
			<div class="span6">
				<br/>
				<form action="<?php echo base_url().$this->page.'save_file/'.$row->id;?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="n_wallpaper" value="<?php echo $row->n_wallpaper;?>"/>
					<div class="fileupload fileupload-new" data-provides="fileupload" id="fileupload1">
						<b>Ubah Wallpaper : </b> 
						<div class="input-append">
							<div class="uneditable-input span2">
								<i class="iconfa-file fileupload-exists"></i>
								<span class="fileupload-preview"></span>
							</div>
							<span class="btn btn-file">
								<span class="fileupload-new">Pilih file</span>
								<span class="fileupload-exists" name="file_upload_a">Ubah</span>
								<input type="file" name="userfile"/>
							</span>
							<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" name="file_upload_a">Hapus</a>
							<button type="submit" class="btn fileupload-exists" style="height:30px;"><i class="icon-ok-circle"></i></button>
						</div>
					</div>
				</form>
				<img src="<?php echo base_url().$row->link;?>" alt="" style="max-weight:500px;"/>
			</div>
		</div>
		<?php }
		} else {
			?><p>Data Tidak Ditemukan, <a href="#" id="refresh">Segarkan Tabel</a></p><?php } ?>
    </div>
</div>