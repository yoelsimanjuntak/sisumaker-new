<style>label.error{display:none !important;}</style>
<div class="row-float">
	<div class="span7">
		<div class="widgetbox box-inverse">	
			<?php echo $this->load->view($this->view.'validate');?>
			<h4 class="widgettitle">Kirim Sms Ke User</strong></h4>
			<div class="widgetcontent wc1">
				<span id="result"></span>
				<form id="form1" method="post" action="<?php echo site_url().$this->page.'send_sms';?>" novalidate="novalidate">
					<div class="par control-group">
						<label class="control-label" for="telp">No. Telp</label>
						<div class="controls">
							<select id="pegawai" name="pegawai[]" required="" data-placeholder="Nama Pemgawai" class="chzn-select" multiple="multiple" style="width:100%;z-index:99999;" tabindex="3">
								<?php foreach($result->result() as $row_disposisi){?>
								<option value="<?php echo $row_disposisi->telp;?>"><?php echo $row_disposisi->n_pegawai.' '.$row_disposisi->telp;?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="par control-group">
						<label class="control-label" for="sms">Pesan</label>
						<div class="controls"><textarea placeholder="Text:" style="margin-top: 0px; margin-bottom: 10px; height: 155px;width: 98%" name="sms"></textarea></div>
					</div>
					<p class="stdformbutton">
						<button class="btn btn-primary" id="action">Kirim</button>
					</p>
				</form>
			</div>
		</div>
	</div>
	<div class="span5">
		<div class="widgetbox box-inverse">	
			<h4 class="widgettitle">Kirim Sms</strong></h4>
			<div class="widgetcontent wc1">
				<span id="result2"></span>
				<form id="form2" method="post" action="<?php echo site_url().$this->page.'send_sms_one';?>" novalidate="novalidate">
					<div class="par control-group">
						<label class="control-label" for="telp">No. Telp</label>
						<div class="controls">
							<input type="text" name="telp2" id="telp" class="span6"/>
						</div>
					</div>
					<div class="par control-group">
						<label class="control-label" for="sms">Pesan</label>
						<div class="controls"><textarea placeholder="Text:" style="margin-top: 0px; margin-bottom: 10px; height: 155px;width: 98%" name="sms2"></textarea></div>
					</div>
					<p class="stdformbutton">
						<button class="btn btn-primary" id="action2">Kirim</button>
					</p>
				</form>
			</div>
		</div>
	</div>
</div>