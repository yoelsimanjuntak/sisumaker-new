<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Jadwal extends ICA_AdminCont {

    var $page = "agenda/jadwal/";
    var $view = "agenda/jadwal/";
    var $icon = "briefcase";

    function __construct() {
        parent::__construct();
		$this->restrict('14');
		$this->tmpersuratan = new Tmpersuratan();
		$this->penomoran = new Penomoran();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->tmskpd = new Tmskpd();
		$this->surat = new Surat();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->tmuser_userauth = new Tmuser_userauth();
		$this->main_inbox = new Main_inbox();
		$this->tmpegawai = new Tmpegawai();
		$this->outbox = new Outbox();
        $this->load->helper('function_helper');
    }

    function index()
	{
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5"),
                $this->lib_load_css_js->load_css(base_url(), "assets/js/fullcalendar/", "fullcalendar.min.css"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/lib/", "moment.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/", "fullcalendar.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/", "lang-all.js")
            ),
            'breadcrubs' => " Agenda",
            'title' => 'Kalender Agenda',
            'content' => 'jadwal',
			'value_search1' => '',
			'value_search2' => ''
        );
        $this->load->view('template_mail', $data);
    }
	
	function data()
	{		
		$id_skpd = $this->session->userdata('skpd_id');
		function cek_absen($id)
		{
			$return = '';
			$query = mysql_query("SELECT tmpegawai.id, tmpegawai.n_pegawai, tr_surat_penerima.d_absen FROM tr_surat_penerima JOIN tmpegawai ON tr_surat_penerima.id_pegawai_ke = tmpegawai.id WHERE tr_surat_penerima.id_persuratan = '".$id."' AND tr_surat_penerima.c_absen = 1 ORDER BY tmpegawai.n_pegawai ASC;");
			$count = mysql_num_rows($query);
			if($count != 0)
			{
				$return = "<ol class='list-ordered'>";
				while($row = mysql_fetch_array($query))
				{
					$return .= "<li title='Absen Pada : ".indo_date_time($row['d_absen'])."'>".$row['n_pegawai']."</li>";
				}
				$return .= "</ol>";
			}
			else
			{
				$return = "<span class='text-warning'>* Belum Ada Yang Hadir.</span>";
			}
			return $return;
		}
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				tmpersuratan.id,
				tmpersuratan.prihal,
				tmpersuratan.dari,
				tmpersuratan.d_awal_kegiatan,
				tmpersuratan.d_akhir_kegiatan,
				tmpersuratan.v_kegiatan,
				tmpersuratan.no_surat
			')
			->where('tmpersuratan.id_jnssurat', 1)
			->where('tmpersuratan.d_awal_kegiatan <>', '')
			->where('tmpersuratan.d_akhir_kegiatan <>', '')
			->where('tmpersuratan.v_kegiatan <>', '')
			->where('tmpersuratan.id_skpd_in', $id_skpd)
			->from('tmpersuratan')
			->join('tmskpd', 'tmpersuratan.id_skpd_in = tmskpd.id')
			->edit_column('tmpersuratan.d_awal_kegiatan',
				'<center>$1 <br/><sup>s</sup>/<sub>d</sub><br/>$2</center>',
				'indo_date_time(tmpersuratan.d_awal_kegiatan), indo_date_time(tmpersuratan.d_akhir_kegiatan)'
			)
			->edit_column('tmpersuratan.no_surat',
				'$1',
				'cek_absen(tmpersuratan.id)'
			)
			->unset_column('tmpersuratan.d_akhir_kegiatan')
			->generate();
	}
	
	function data_keluar()
	{		
		$id_skpd = $this->session->userdata('skpd_id');
		function cek_absen($id)
		{
			$return = '';
			$query = mysql_query("SELECT tmpegawai.id, tmpegawai.n_pegawai, tr_surat_penerima.d_absen FROM tr_surat_penerima JOIN tmpegawai ON tr_surat_penerima.id_pegawai_ke = tmpegawai.id WHERE tr_surat_penerima.id_persuratan = '".$id."' AND tr_surat_penerima.c_absen = 1 ORDER BY tmpegawai.n_pegawai ASC;");
			$count = mysql_num_rows($query);
			if($count != 0)
			{
				$return = "<ol class='list-ordered'>";
				while($row = mysql_fetch_array($query))
				{
					$return .= "<li title='Absen Pada : ".indo_date_time($row['d_absen'])."'>".$row['n_pegawai']."</li>";
				}
				$return .= "</ol>";
			}
			else
			{
				$return = "<span class='text-warning'>* Belum Ada Yang Hadir.</span>";
			}
			return $return;
		}
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				tmpersuratan.id,
				tmpersuratan.prihal,
				tmpersuratan.d_awal_kegiatan,
				tmpersuratan.d_akhir_kegiatan,
				tmpersuratan.v_kegiatan,
				tmpersuratan.no_surat
			')
			->where('tmpersuratan.id_jnssurat', 2)
			->where('tmpersuratan.d_awal_kegiatan <>', '')
			->where('tmpersuratan.d_akhir_kegiatan <>', '')
			->where('tmpersuratan.v_kegiatan <>', '')
			->where('tmpersuratan.id_skpd_in', $id_skpd)
			->where('tmpersuratan.id_skpd_out', $id_skpd)
			->where('tmpersuratan.status_setujui', 1)
			->from('tmpersuratan')
			->join('tmskpd', 'tmpersuratan.id_skpd_in = tmskpd.id')
			->edit_column('tmpersuratan.d_awal_kegiatan',
				'<center>$1 <br/><sup>s</sup>/<sub>d</sub><br/>$2</center>',
				'indo_date_time(tmpersuratan.d_awal_kegiatan), indo_date_time(tmpersuratan.d_akhir_kegiatan)'
			)
			->edit_column('tmpersuratan.no_surat',
				'$1',
				'cek_absen(tmpersuratan.id)'
			)
			->unset_column('tmpersuratan.d_akhir_kegiatan')
			->generate();
	}
}