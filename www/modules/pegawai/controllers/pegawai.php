<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Pegawai extends ICA_AdminCont
{
	var $page = "pegawai/pegawai/";
	var $view = "pegawai/pegawai/";
	var $icon = "cogs";

    function __construct()
	{
        parent::__construct();
		$this->restrict('7');
		$this->tmpegawai = new Tmpegawai();
		$this->tmgolongan = new Tmgolongan();
		$this->tmjabatan = new Tmjabatan();
		$this->tmunitkerja = new Tmunitkerja();
		$this->truser_kirim = new Truser_kirim();
		$this->tmskpd = new Tmskpd();
		$this->tmuser = new Tmuser();
		$this->userauth = new Userauth();
		$this->tmuser_userauth = new Tmuser_userauth();
    }

    function nip_reff()
    {
    	foreach($this->tmpegawai->select()->result() as $row)
    	{
    		$vowels = array("-", " ", "NIP", ".");
			$onlynip = str_replace($vowels, "", $row->nip);

    		$data = array(
    			'nip' => $onlynip
    		);
    		$this->tmpegawai->update($row->id, $data);
    	}
    }

	function index()
	{
		$data = array(
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.dataTables.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js")
				),
			'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> Pegawai <span class='separator'></span> Data Master",
			'title' => 'Master Pegawai',
			'content' => 'table',
			'sort' => 1
		);
		$this->load->view('template', $data);
	}

	function add()
	{
		$data = array(
			'result_tmjabatan' => $this->tmjabatan->select(),
			'result_tmunitkerja' => $this->tmunitkerja->select_skpd($this->session->userdata('skpd_id')),
			'result_tmgolongan' => $this->tmgolongan->select(),
			'result_tmskpd' => $this->tmskpd->select()
		);
		$this->load->view($this->view.'add', $data);
	}

	function edit($id)
	{
		$data = array(
			'result_tmjabatan' => $this->tmjabatan->select(),
			'result_tmunitkerja' => $this->tmunitkerja->select_skpd($this->session->userdata('skpd_id')),
			'result_tmgolongan' => $this->tmgolongan->select(),
			'result_tmskpd' => $this->tmskpd->select(),
			'result' => $this->tmpegawai->select('id', $id)
		);
		$this->load->view($this->view.'edit', $data);
	}

	function flag($id)
	{
		$data = array(
			'result' => $this->tmpegawai->select('id', $id),
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "bootstrap-fileupload.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/css/", "bootstrap-fileupload.min.css")
			),
			'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> Pegawai <span class='separator'></span> <a href='".base_url().$this->page."'>Data Master</a> <span class='separator'></span> Upload Paraf/ TTD",
			'title' => 'Upload Paraf/ TTD',
			'content' => 'uploads',
			'id' => $id,
			'paraf' => '',
			'ttd' => ''
		);
		$this->load->view('template', $data);
	}

	function save($id = false)
	{
		if($id == false)
		{
			if($this->tmpegawai->select('nip', $this->input->post('nip'))->num_rows() == 0)
			{
				$data = array(
					'nip' => $this->input->post('nip'),
					'n_pegawai' => $this->input->post('n_pegawai'),
					'telp' => $this->input->post('telp'),
					'tmskpd_id' => $this->input->post('tmskpd_id'),
					'tmjabatan_id' => $this->input->post('tmjabatan_id'),
					'tmunitkerja_id' => $this->input->post('tmunitkerja_id'),
					'tmgolongan_id' => $this->input->post('tmgolongan_id'),
					'c_status' => $this->input->post('c_status')
				);
				$this->tmpegawai->insert($data);
				$output = array(
					'status' => 1
				);
				echo json_encode($output);
			}
			else
			{
				$output = array(
					'status' => 11
				);
				echo json_encode($output);
			}
		}
		else
		{
			if($this->input->post('nip_temp') == $this->input->post('nip'))
			{
				$data = array(
					'n_pegawai' => $this->input->post('n_pegawai'),
					'telp' => $this->input->post('telp'),
					'tmskpd_id' => $this->input->post('tmskpd_id'),
					'tmjabatan_id' => $this->input->post('tmjabatan_id'),
					'tmunitkerja_id' => $this->input->post('tmunitkerja_id'),
					'tmgolongan_id' => $this->input->post('tmgolongan_id'),
					'c_status' => $this->input->post('c_status')
				);
				$this->tmpegawai->update($id, $data);
				$output = array(
					'status' => 2
				);
				echo json_encode($output);
			}
			else
			{
				if($this->tmpegawai->select('nip', $this->input->post('nip'))->num_rows() == 0)
				{
					$data = array(
						'nip' => $this->input->post('nip'),
						'n_pegawai' => $this->input->post('n_pegawai'),
						'telp' => $this->input->post('telp'),
						'tmskpd_id' => $this->input->post('tmskpd_id'),
						'tmjabatan_id' => $this->input->post('tmjabatan_id'),
						'tmunitkerja_id' => $this->input->post('tmunitkerja_id'),
						'tmgolongan_id' => $this->input->post('tmgolongan_id'),
						'c_status' => $this->input->post('c_status')
					);
					$this->tmpegawai->update($id, $data);
					$output = array(
						'status' => 2
					);
					echo json_encode($output);
				}
				else
				{
					$output = array(
						'status' => 22
					);
					echo json_encode($output);
				}
			}
		}
	}

	function save_paraf()
	{
		$id = $this->input->post('id');
		if ($_FILES['userfile']['name'] != "") {
            $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.' . end($explode);
            $exten = 'uploads/paraf/'.sha1('F52'.$id.'aPt').$endof;
            $config['file_name'] = sha1('F52'.$id.'aPt');
            $config['upload_path'] = './uploads/paraf/';
            $config['allowed_types'] = 'jpg|png|bmp|jpeg';
            $config['max_size'] = '1000000';
            $config['overwrite'] = true;
          //  $config['max_width'] = '1024';
           // $config['max_height'] = '768';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                echo '
					<script type="text/javascript">
						alert("' . $this->upload->display_errors() . '");
						document.location.href = "' . base_url() . $this->page . 'flag/'.$id.'";
					</script>
				';
            } else {
                $data = array(
                    'file_paraf' => $exten
                );
                $this->tmpegawai->update($id, $data);
                redirect(base_url() . $this->page . 'flag/'. $id);
            }
        } else {
            echo '
			<script type="text/javascript">
				alert("File Paraf Harus Diisi");
				document.location.href = "' . base_url() . $this->page . 'flag/'.$id.'";
			</script>';
        }
	}

	function save_ttd()
	{
		$id = $this->input->post('id');
		if ($_FILES['userfile']['name'] != "") {
            $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.' . end($explode);
            $exten = 'uploads/ttd/'.sha1('P)0'.$id.'Ar2').$endof;
            $config['file_name'] = sha1('P)0'.$id.'Ar2');
            $config['upload_path'] = './uploads/ttd/';
            $config['allowed_types'] = 'jpg|png|bmp|jpeg';
            $config['max_size'] = '1000000';
            $config['overwrite'] = true;
            // $config['max_width'] = '1024';
            // $config['max_height'] = '768';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                echo '
					<script type="text/javascript">
						alert("' . $this->upload->display_errors() . '");
						document.location.href = "' . base_url() . $this->page . 'flag/'.$id.'";
					</script>
				';
            } else {
                $data = array(
                    'file_ttd' => $exten
                );
                $this->tmpegawai->update($id, $data);
                redirect(base_url() . $this->page . 'flag/'. $id);
            }
        } else {
            echo '
			<script type="text/javascript">
				alert("File TTD Harus Diisi");
				document.location.href = "' . base_url() . $this->page . 'flag/'.$id.'";
			</script>';
        }
	}

	function pegawai_user($action, $id)
	{
		if($action == 'add')
		{
			$user_id = $this->tmpegawai->where("id = '".$id."'")->get()->tmuser_id;
			if($this->tmuser->where("id = '".$user_id."'")->count() == 0)
			{
				$date = date('Y-m-d H:i:s');
				$n_pegawai = $this->tmpegawai->where("id = '".$id."'")->get()->n_pegawai;
				$username = str_replace(' ', '', $n_pegawai.$id);
				$data = array(
					'realname' => $n_pegawai,
					'username' => $username,
					'password' => md5($username),
					'c_status' => '1',
					'd_entry' => $date,
					'photo' => 'default.png'
				);
				$this->tmuser->insert($data);
				$tmuser_id = $this->tmuser->where("password = '".md5($username)."' and d_entry = '".$date."'")->get()->id;
				$data = array(
					'tmuser_id' => $tmuser_id
				);
				$this->tmpegawai->update($id, $data);

				$id = $tmuser_id;
				for($i=2;$i<=4;$i++){
					$data = array(
						'tmuser_id' => $id,
						'userauth_id' => $i
					);
					$this->tmuser_userauth->insert($data);
				}

				if($this->session->userdata('skpd_id') == 0)
				{
					$a = array('1' => 7, '2' => 10);
					for($i=1;$i<=2;$i++){
						$data = array(
							'tmuser_id' => $id,
							'userauth_id' => $a[$i]
						);
						$this->tmuser_userauth->insert($data);
					}
				}

				//Agenda (Calendar)
				$data = array(
					'tmuser_id' => $id,
					'userauth_id' => 13
				);
				$this->tmuser_userauth->insert($data);
			}
			else
			{
				$id = $this->tmpegawai->where("id = '".$id."'")->get()->tmuser_id;
			}
		}
		$skpd_id = $this->session->userdata('skpd_id');
		if($skpd_id != 0){
			$result_auth = $this->userauth->select_skpd();
			$result_user = $this->tmuser->select_skpd($id, $skpd_id);
		}
		else{
			$result_auth = $this->userauth->select();
			$result_user = $this->tmuser->select_skpd($id);
		}
		$data = array(
			'result_user' => $result_user,
			'result_userauth' => $result_auth,
			'result' => $this->tmuser->select('id', $id),
			'assets' => array(
				$this->lib_load_css_js->load_css(base_url() , "assets/css/", "jquery.chosen.css"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "chosen.jquery.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.dataTables.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js")
				),
			'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> Pegawai <span class='separator'></span> <a href='".base_url().$this->page."'>Data Master</a> <span class='separator'></span> Setting User",
			'title' => 'Setting User',
			'content' => 'edit_user'
		);
		$this->load->view('template', $data);
	}

	function save_name($id)
	{
		$data = array(
			'realname' => $this->input->post('realname'),
			'd_update' => date('Y-m-d H:i:s')
		);
		$this->tmuser->update($id, $data);
		$output = array(
			'status' => 2
		);
		echo json_encode($output);
	}

	function save_username($id)
	{
		if($this->tmuser->select('username', $this->input->post('username'))->num_rows() == 0)
		{
			$data = array(
				'username' => $this->input->post('username'),
				'd_update' => date('Y-m-d H:i:s')
			);
			$this->tmuser->update($id, $data);
			$output = array(
				'status' => 2
			);
			echo json_encode($output);
		}
		else
		{
			$output = array(
				'status' => 22
			);
			echo json_encode($output);
		}
	}

	function save_password($id)
	{
		$data = array(
			'password' => md5($this->input->post('password')),
			'd_update' => date('Y-m-d H:i:s')
		);
		$this->tmuser->update($id, $data);
		$output = array(
			'status' => 2
		);
		echo json_encode($output);
	}

	function save_userauth($id)
	{
		$true = false;
		$userauth_list = $this->input->post('userauth_id');
		$userauth_list_len = count($userauth_list);
		for($i=0; $i<$userauth_list_len; $i++)
		{
			if($this->tmuser_userauth->where("tmuser_id = '".$id."' and userauth_id = '".$userauth_list[$i]."'")->count() == 0)
			{
				$data = array(
					'tmuser_id' => $id,
					'userauth_id' => $userauth_list[$i]
				);
				$this->tmuser_userauth->insert($data);
				$true = true;
			}
		}
		if($true == true)
		{
			$output = array(
				'status' => 2
			);
			echo json_encode($output);
		}
		else
		{
			$output = array(
				'status' => 22
			);
			echo json_encode($output);
		}
	}

	function save_userkirim($id)
	{
		$true = false;
		$userkirim_list = $this->input->post('userkirim_id');
		$userauth_list_len = count($userkirim_list);
		for($i=0; $i<$userauth_list_len; $i++)
		{
			if($this->truser_kirim->where("id_user = '".$id."' and id_kirim = '".$userkirim_list[$i]."'")->count() == 0)
			{
				$data = array(
					'id_user' => $id,
					'id_kirim' => $userkirim_list[$i]
				);
				$this->truser_kirim->insert($data);
				$true = true;
			}
		}
		if($true == true)
		{
			$output = array(
				'status' => 2
			);
			echo json_encode($output);
		}
		else
		{
			$output = array(
				'status' => 22
			);
			echo json_encode($output);
		}
	}

	function delete($id)
	{
		$this->tmpegawai->delete($id);
	}

	function delete_userauth($id)
	{
		$this->tmuser_userauth->delete($id);
	}

	function delete_userkirim($id)
	{
		$this->truser_kirim->delete($id);
	}

	function data_userauth($id)
	{
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				tmuser_userauth.id,
				userauth.description
			')
			->from('tmuser_userauth')
			->where('tmuser_userauth.tmuser_id', $id)
			->join('userauth', 'tmuser_userauth.userauth_id = userauth.id_role')
			->add_column('aksi',
				'<center>
					<a id="delete_userauth" href="#" to="'.base_url().$this->page.'delete_userauth/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>',
				'tmuser_userauth.id')
			->generate();

	}

	function data_userkirim($id)
	{
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				truser_kirim.id,
				tmuser.realname
			')
			->from('truser_kirim')
			->where('truser_kirim.id_user', $id)
			->join('tmuser', 'truser_kirim.id_kirim = tmuser.id')
			->add_column('aksi',
				'<center>
					<a id="delete_userkirim" href="#" to="'.base_url().$this->page.'delete_userkirim/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>',
				'truser_kirim.id')
			->generate();

	}

	function data()
	{
		$skpd_id = $this->session->userdata('skpd_id');
		$pegawai_id = $this->session->userdata('pegawai_id');
		function cek_del($a, $b){
			$return = '<a id="delete" href="#" to="'.base_url().'pegawai/delete/'.$a.'"><i class="icon-remove" title="Hapus data"></i></a>';
			if($a == $b){
				$return = '&nbsp; &nbsp';
			}
			return $return;
		}
		function cek_user($a, $b = false){
			if($b != false){
				$return = "<span class='label label-success'>Ya</span> <a href='".base_url()."pegawai/pegawai_user/edit/".$b."' class='pull-right'>Edit <i class='iconfa-user'></i></a>";
			}else{
				$return = "<span class='label'>Tidak</span> <a href='".base_url()."pegawai/pegawai_user/add/".$a."' class='pull-right'>Buat <i class='iconfa-user'></i></a>";
			}
			return $return;
		}
		function cek_flag($f){
			$return = '';
			if($f != ''){
				$return = '<img src="'.base_url().$f.'" alt="">';
			}
			return $return;
		}
		$this->load->library('datatables');
		if($skpd_id != 0)
		{
			echo $this->datatables
				->select('
					tmpegawai.id,
					tmpegawai.nip,
					tmpegawai.n_pegawai,
					tmpegawai.telp,
					tmskpd.n_skpd,
					tmjabatan.n_jabatan,
					tmunitkerja.n_unitkerja,
					tmpegawai.file_paraf,
					tmpegawai.file_ttd,
					tmpegawai.tmuser_id
				')
				->from('tmpegawai')
				->where('tmpegawai.tmskpd_id', $skpd_id)
				->join('tmskpd','tmpegawai.tmskpd_id = tmskpd.id')
				->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
				->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
				->edit_column('tmpegawai.file_paraf',
					'$1',
					'cek_flag(tmpegawai.file_paraf)')
				->edit_column('tmpegawai.file_ttd',
					'$1',
					'cek_flag(tmpegawai.file_ttd)')
				->edit_column('tmpegawai.tmuser_id',
					'$1',
					'cek_user(tmpegawai.id, tmpegawai.tmuser_id)')
				->add_column('aksi',
					'<center>
						<a href="'.base_url().$this->page.'flag/$1"><i class="icon-flag" title="Upload paraf atau tanda tangan"></i></a>
						<a id="edit" href="#" to="'.base_url().$this->page.'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
						$2
					</center>',
					'tmpegawai.id, cek_del(tmpegawai.id, '.$pegawai_id.')')
				->generate();
		}
		else
		{
			echo $this->datatables
				->select('
					tmpegawai.id,
					tmpegawai.nip,
					tmpegawai.n_pegawai,
					tmpegawai.telp,
					tmskpd.n_skpd,
					tmjabatan.n_jabatan,
					tmunitkerja.n_unitkerja,
					tmpegawai.file_paraf,
					tmpegawai.file_ttd,
					tmpegawai.tmuser_id
				')
				->from('tmpegawai')
				->join('tmskpd','tmpegawai.tmskpd_id = tmskpd.id')
				->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
				->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
				->edit_column('tmpegawai.file_paraf',
					'$1',
					'cek_flag(tmpegawai.file_paraf)')
				->edit_column('tmpegawai.file_ttd',
					'$1',
					'cek_flag(tmpegawai.file_ttd)')
				->edit_column('tmpegawai.tmuser_id',
					'$1',
					'cek_user(tmpegawai.id, tmpegawai.tmuser_id)')
				->add_column('aksi',
					'<center>
						<a href="'.base_url().$this->page.'flag/$1"><i class="icon-flag" title="Upload paraf atau tanda tangan"></i></a>
						<a id="edit" href="#" to="'.base_url().$this->page.'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
						$2
					</center>',
					'tmpegawai.id, cek_del(tmpegawai.id, '.$pegawai_id.')')
				->generate();
		}
	}
}
