<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Masuk extends ICA_AdminCont
{
	var $page = "rekapitulasi/masuk/";
	var $view = "rekapitulasi/masuk/";
	var $icon = "group";

    function __construct() 
	{
        parent::__construct();
		$this->restrict('6');
		$this->tmpersuratan = new Tmpersuratan();
		$this->main_inbox = new Main_inbox();
		$this->main_outbox = new Main_outbox();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->surat = new Surat();
		$this->tmpegawai = new Tmpegawai();
        $this->load->helper('function_helper');
    }

	function index()
	{
		$data = array(
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js")
				),
			'breadcrubs' => " Rekapitulasi Surat <span class='separator'></span> Masuk",
			'title' => 'Rekapitulasi Surat Masuk',
			'content' => 'filter'
		);
		$this->load->view('template', $data);
	}
	
	function filter()
	{
		$d_dari = $this->input->post('d_dari');
		$d_sampai = $this->input->post('d_sampai');
		
		$data = array(
			'd_dari' => $d_dari,
			'd_sampai' => $d_sampai,
			'result' => $this->query($d_dari, $d_sampai),
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.dataTables.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
				),
			'breadcrubs' => " Rekapitulasi Surat <span class='separator'></span> <a href='".base_url().$this->page."'>Masuk</a> <span class='separator'></span> Filter",
			'title' => 'Rekapitulasi Surat Masuk',
			'content' => 'table',
			'sort' => 3
		);
		$this->load->view('template', $data);
	}
	
	function query($d_dari, $d_sampai)
	{
		$id_skpd = $this->session->userdata('skpd_id');
		return $this->db
			->select('
				tmpersuratan.id,
				tmpersuratan.id_surat,
				tmpersuratan.id_pegawai,
				tmpersuratan.no_surat,
				tmpersuratan.dari,
				tmpersuratan.tgl_surat,
				tmpersuratan.tgl_terima_surat,
				tmpersuratan.no_agenda,
				tmpersuratan.file,
				tmpersuratan.prihal
			')
			->from('tmpersuratan')
			->where('tmpersuratan.tgl_surat between "'.$d_dari.'" and "'.$d_sampai.'"')
			->where('tmpersuratan.id_jnssurat', 1)
			->where('tmpersuratan.id_skpd_in', $id_skpd)
			->where('tmpersuratan.id_skpd_out != ', $id_skpd)
			->get();
	}
	
	function read($id_persuratan)
	{
		$id = $this->tr_surat_penerima->where("id_persuratan = '".$id_persuratan."'")->get()->id;
		foreach($this->tr_surat_penerima->read_all($this->session->userdata('pegawai_id'), $id_persuratan)->result() as $row)
		{
			$data = array(
				'status' => 1,
				'd_read' => date('Y-m-d H:i:s')
			);
			$this->tr_surat_penerima->update($row->id, $data);
		}
		
		$data = array(
			'result' => $this->main_inbox->read($id),
			'result_history' => $this->main_inbox->history_all($id_persuratan),
			'content' => 'type1'
		);
		$this->load->view('view', $data);
	}
	
	function info($id_persuratan)
	{
		$id = $this->tr_surat_penerima->where("id_persuratan = '".$id_persuratan."'")->get()->id;
		$data = array(
			'result' => $this->main_outbox->read($id),
			'result_info' => $this->tr_surat_penerima->info($id_persuratan),
			'content' => 'info'
		);
		$this->load->view('view', $data);
	}
	
	function print_all($d_dari, $d_sampai)
	{
		$nama_surat = "rekapitulasi_".$d_dari."-".$d_sampai;
		$this->load->plugin('odf');
		$odf = new odf('assets/odt/rekapitulasi_masuk.odt');
		
		$a = 1;
		$result = $this->query($d_dari, $d_sampai);
		$article = $odf->setSegment('articles');
		foreach($result->result() as $row)
		{
			$article->titreArticle($a);
			$article->texteArticle3($row->no_surat);
			$article->texteArticle4($row->dari);
			$article->texteArticle5($this->lib_date->mysql_to_human($row->tgl_surat));
			$article->texteArticle6($this->lib_date->mysql_to_human($row->tgl_terima_surat));
			$article->texteArticle7($row->no_agenda);
			$article->texteArticle8($row->prihal);
			$article->merge();
			$a++;
		}
		$odf->mergeSegment($article);
		$odf->setVars('judul', 'Laporan Rekapitulasi Surat Masuk per Tanggal ' . $this->lib_date->mysql_to_human($d_dari) . ' - ' . $this->lib_date->mysql_to_human($d_sampai));
		$odf->exportAsAttachedFile($nama_surat.'.odt');
	}
}