<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Tmpersuratan extends CI_Model
{

    var $table = 'tmpersuratan';
    var $key = 'id';

    function __construct()
	{
		parent::__construct();
		$this->load->database();
    }

//put your code here
	function select($key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($this->table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($this->table);
		}
	}

	function select_max()
	{
		$query = $this->db
			->select_max($this->key)
			->get($this->table);
		foreach($query->result() as $row)
		{
			$return = $row->id;
		}
		return $return;
	}

    function insert($form)
	{
        return $this->db
			->insert($this->table, $form);
    }

    function update($id, $form)
	{
        return $this->db
			->where($this->key, $id)
			->update($this->table, $form);
    }

    function delete($id)
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }

}

?>
