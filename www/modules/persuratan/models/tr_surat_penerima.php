<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Tr_surat_penerima extends DataMapper 
{

    var $table = 'tr_surat_penerima';
    var $key = 'id';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here
	function read_all($id_pegawai_ke, $id_persuratan)
	{
		return $this->db
			->from($this->table)
			->where('id_pegawai_ke', $id_pegawai_ke)
			->where('id_persuratan', $id_persuratan)
			->where('status', 0)
			->get();
	}
	
	function info($id)
	{
		return $this->db
			->select('
				a1.n_pegawai as n_pegawai_dari,
				j1.n_jabatan as n_jabatan_dari,
				u1.n_unitkerja as n_unitkerja_dari,
				a1.file_paraf as paraf,
				a2.n_pegawai as n_pegawai_ke,
				j2.n_jabatan as n_jabatan_ke,
				u2.n_unitkerja as n_unitkerja_ke,
				tr_surat_penerima.d_entry,
				tr_surat_penerima.d_read,
				tr_surat_penerima.catatan,
				tr_surat_penerima.file_surat_koreksi
			')
			->from($this->table)
			->where('tr_surat_penerima.id_persuratan', $id)
			->join('tmpegawai as a1', 'tr_surat_penerima.id_pegawai_dari = a1.id')
			->join('tmjabatan as j1','a1.tmjabatan_id = j1.id')
			->join('tmunitkerja as u1','a1.tmunitkerja_id = u1.id')
			
			->join('tmpegawai as a2', 'tr_surat_penerima.id_pegawai_ke = a2.id')
			->join('tmjabatan as j2','a2.tmjabatan_id = j2.id')
			->join('tmunitkerja as u2','a2.tmunitkerja_id = u2.id')
			->get();
	}

	function select($key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($this->table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($this->table);
		}
	}
	
    function insert($form) 
	{
        return $this->db
			->insert($this->table, $form);
    }

    function update($id, $form) 
	{
        return $this->db
			->where($this->key, $id)
			->update($this->table, $form);
    }

    function delete($id) 
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }

}

?>
