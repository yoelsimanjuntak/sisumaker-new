<div class="widgetbox box-inverse view_load">
    <?php echo $this->load->view($this->view . 'validate'); ?>
    <h4 class="widgettitle"></h4>
    <div class="widgetcontent wc1">
        <form id="form1" class="stdform" method="post" action="<?php echo site_url() . $this->page . 'save'; ?>" novalidate="novalidate">
            <div class="par control-group">
                <label class="control-label" for="realname">Nama</label>
                <div class="controls"><input type="text" name="realname" id="realname" class="input-large"></div>
            </div>
            <div class="par control-group">
                <label class="control-label" for="username">Username</label>
                <div class="controls"><input type="text" name="username" id="username" class="input-large" onkeyup="nospaces(this)"></div>
            </div>
            <div class="par control-group">
                <label class="control-label" for="password">Password</label>
                <div class="controls"><input type="password" name="password" id="password" class="input-large"></div>
            </div>
            <div class="par control-group">
                <label class="control-label" for="cpassword">Ulangi Password</label>
                <div class="controls"><input type="password" name="cpassword" id="cpassword" class="input-large"/></div>
            </div>
            <div class="par control-group">
                <label class="control-label" for="c_status">Status</label>
                <div class="controls">
                    <select name="c_status">
                        <option value="">Pilih</option>
                        <option value="1">Aktif</option>
                        <option value="2">Non-Aktif</option>
                    </select>
                </div>
            </div>
            <p class="stdformbutton">
                <button class="btn btn-primary" id="action">Simpan</button>
                <a href="#" id="close_add" class="btn btn-default">Batal</a>
            </p>
        </form>
    </div>
</div>