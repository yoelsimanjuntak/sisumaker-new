<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>SISUMAKER :: Login</title>
		<?php $base_url = base_url(); ?>
		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/images/favicon.png"/>
        <?php
			$base_url = base_url();
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.login.css");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-1.9.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.1.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.slimscroll.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.validate.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "chat.js");
			?>
		<script type="text/javascript">
			base='<?php echo $base_url; ?>';
			jQuery(document).ready(function(){
				
				//alert('Mohon tunggu beberapa saat kode verifikasi akan dikirim ke nomor ponsel Anda.');
				jQuery('body').removeClass('chatenabled');
				jQuery.removeCookie('enable-chat', { path: '/' });
				jQuery('#verifikasi').focus();
				jQuery("form")
					.validate({
						rules: {
							verifikasi: "required"
						},
						highlight: function(label) {
							jQuery(label).closest('.inputwrapper').addClass('error');
						},
						success: function(label) {
							label
								.closest('.inputwrapper').removeClass('error');
						},
						submitHandler:function(form){
							jQuery('#action').button('loading');
							jQuery('.login-alert').fadeOut('fast');
							jQuery.ajax({
								type:'post',
								url:jQuery(form).attr('action'),
								data:jQuery(form).serialize(),
								dataType:'json',
								success:function(data){
									if(data.status == 1)
									{
										jQuery('.login-alert').html("<div class='alert alert-success'>Login Berhasil...<a href='<?php echo $base_url.$this->page;?>' style='font-weight:bold;' title='Klik, jika tidak dipindahkan oleh browser.'>Klik <span class='iconfa-info-sign'></span></a></div>");
										document.location.href = "<?php echo $base_url.$this->page;?>";
									}
									else if(data.status == 0)
									{
										jQuery('.login-alert').html("<div class='alert alert-error'><b>Kode verifikasi salah.</b><br/>Masukkan kode verifikasi terakhir.</div>");
									}
									jQuery('#action').button('reset');
									jQuery('.login-alert').fadeIn('fast');
								}
							});
							return false;
						}
					});
			});


			<?php $minutes = date('i') + 1;?>
			var countDownDate = new Date(new Date().getTime() + 10*60000);

			var x = setInterval(function() {
			    var now = new Date().getTime();
			    var distance = countDownDate - now;
			    
			    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			    
			    jQuery("#time").html("Tunggu : <b>" + minutes + "</b>m <b>" + seconds + "</b>s ");
			    
			    if (distance < 0) {
			        clearInterval(x);
					var msgWA = "https://api.whatsapp.com/send?phone=6282122756641&text=Dear,%20Admin%20Sisumaker%20Sudah%2010%20Menit%20Sms%20Token%20Tidak%20Terkirim%20Ke%20Nomor%20<?php echo $no_telp;?>%20Dengan%20Username%20<?php echo $username;?>%20Waktu%20<?php echo $date_now;?>%20Token%20<?php echo $bukti;?>%20&source=https://sisumaker.tangerangselatankota.go.id/&data=";
			        jQuery("#time").html("Belum menerima SMS?<br/><b><a href='" + msgWA + "'>Laporkan Token Tidak Terkirim</a>.</b>");
			    }
			}, 1000);
		</script>
	</head>
	<body style="background-size:cover;background-attachment:fixed;">
		<div class="overlay">
			<div class="loginpage">
				<div class="loginpanel">
					<div class="loginpanelinner" style="background-color:#FFF;background-image:url('<?php echo $base_url.'assets/images/header_login.png';?>');background-size: contain;background-repeat: no-repeat;border-radius:7px;box-shadow: 0 1px 1px rgba(0,0,0,.05);">
						<div class="logo animate0 bounceIn">&nbsp;</div><br/><br/><br/><br/>
						<!-- <div class="underTitleLine gradientbg"></div> -->
						<form id="form" action="<?php echo $base_url.'panel/submit_verifikasi';?>" method="post">
							<input type="hidden" name="secure" value="<?php echo $secure;?>"/>
							<input type="hidden" name="encrip" value="<?php echo $encrip;?>"/>
							<input type="hidden" name="microtime" value="<?php $microtime = microtime();echo $microtime;?>"/>
							<input type="hidden" name="token" value="<?php echo sha1($secure.$encrip.$microtime);?>"/>
							<div class="inputwrapper login-alert"></div>
							<b>Verifikasi tahap 2</b>
							<p style="margin-bottom:7px;">Sebuah pesan text dengan kode verifikasi telah<br/>dikirim ke nomor <?php echo $dash_no_telp;?></p>
							<center class="inputwrapper animate2 bounceIn" style="width:270px">
								<input type="password" name="verifikasi" id="verifikasi" class="themeInputs" placeholder="Masukkan kode verifikasi" autocomplete="new-password" value="" maxlength="5" style="margin-bottom:10px;width:165px"/>
							</center>
							<div class="animate3 bounceIn" style="margin-bottom:5px;">
								<center id="time"><b></b>Mohon tunggu beberapa saat.</center>
							</div>
							<div class="animate3 bounceIn">
								<button name="submit" id="action" class="gradientBtn">Sign In</button>
							</div>
						</form>
					</div>
				</div>
				<div class="loginfooter">
					<p>Copyright &copy; 2015. Aplikasi Persuratan.</p>
				</div>
				<div style="clear:both"></div>
			</div>
		</div>
	</body>
</html>