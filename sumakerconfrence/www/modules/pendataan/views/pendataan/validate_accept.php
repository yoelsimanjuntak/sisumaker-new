<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#tgl_surat").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_awal_kegiatan").datepicker("option", "minDate", selectedDate);
				if(jQuery("#d_awal_kegiatan").val() == ''){
					jQuery("#d_akhir_kegiatan").datepicker("option", "minDate", selectedDate);
				}
			}
		});
		jQuery("#d_awal_kegiatan").datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm:ss',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_akhir_kegiatan").datepicker("option", "minDate", selectedDate);
			}
		});
		jQuery("#d_akhir_kegiatan").datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm:ss',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_awal_kegiatan").datepicker("option", "maxDate", selectedDate);
			}
		});
		jQuery("#form1")
			.validate({
				rules: {
					id_surat: "required",
					dari: "required",
					no_surat: "required",
					tgl_surat: "required",
					tgl_terima_surat: "required",
					no_agenda: "required",
					id_sifatsurat: "required",
					prihal: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 2)
							{
								jQuery('#result').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button><strong style="font-weight:bold">Sukses!</strong> Surat berhasil diterima.</div>');
								document.location.href = base + 'pendataan';
							}
							else if(data.status == 22)
							{
								jQuery('#result').html('<div class="alert alert-error"><button data-dismiss="alert" class="close" type="button">×</button><strong style="font-weight:bold">Gagal!</strong> terdapat kesalahan saat menerima surat. Kemungkinan surat surat sudah pernah diterima.</div>');
							}
							jQuery('#action').button('reset');
							jQuery('#result').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
	});
</script>