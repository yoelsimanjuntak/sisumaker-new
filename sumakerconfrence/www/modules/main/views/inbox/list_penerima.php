<?php
$base_url = base_url();
echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.red.css");
echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.mail.css");
echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "jquery.ui.css");
echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "responsive-tables.css");
echo $this->lib_load_css_js->load_css($base_url, "assets/prettify/", "prettify.css");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-2.0.3.min.js");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.2.1.min.js");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.slimscroll.js");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "function.js");
echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "jquery.chosen.css");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "chosen.jquery.min.js"); 
echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.validate.min.js");
echo $this->lib_load_css_js->load_css($base_url, "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5");
echo $this->lib_load_css_js->load_js($base_url, "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5");

?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".chzn-select").chosen();
	})
</script>
<select id="pegawai" name="pegawai[]" required="" data-placeholder="Nama Pegawai - Jabatan" class="chzn-select" multiple="multiple" style="width:100%;z-index:99999;" tabindex="3">
	<?php foreach($result->result() as $row_disposisi){?>
	<option value="<?php echo $row_disposisi->id;?>" <?php if(in_array($row_disposisi->id_kirim, $id_kirim) && in_array($row_disposisi->id_jabatan, $id_jabatan)){ ?> selected="selected"<?php }?>><?php echo $row_disposisi->n_jabatan.' '.$row_disposisi->n_unitkerja.' - '.$row_disposisi->n_pegawai;?></option>
	<?php }?>
</select>