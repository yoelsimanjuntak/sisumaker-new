<div class="widgetbox box-inverse view_load">
	<?php echo $this->load->view($this->view.'validate');?>
	<h4 class="widgettitle"></h4>
	<div class="widgetcontent wc1">
		<?php if($result->num_rows() != 0){foreach($result->result() as $row){ ?>
		<form id="form1" class="stdform" method="post" action="<?php echo site_url().$this->page.'save/'.$row->id;?>" novalidate="novalidate">
			<input type="hidden" name="n_jenisdokumen_temp" value="<?=$row->n_jenisdokumen;?>"/>
			<div class="par control-group">
				<label class="control-label" for="n_jenisdokumen">Nama Jenis Dokumen</label>
				<div class="controls"><input type="text" name="n_jenisdokumen" id="n_jenisdokumen" class="input-large" value="<?php echo $row->n_jenisdokumen;?>"/></div>
			</div>
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action">Simpan Perubahan</button>
				<a href="#" id="close_add" class="btn btn-default">Batal</a>
			</p>
		</form>
		<?php }}else{?><p>Data Tidak Ditemukan, <a href="#" id="refresh">Segarkan Tabel</a></p><?php }?>
	</div>
</div>