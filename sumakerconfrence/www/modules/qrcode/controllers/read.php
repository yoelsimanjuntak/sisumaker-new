<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Read extends Controller {

    var $page = "qrcode/read/";
    var $view = "qrcode/read/";
    var $icon = "check";

    function __construct() {
        parent::__construct();
		$this->tmpersuratan = new Tmpersuratan();
		$this->penomoran = new Penomoran();
		$this->tmjenis_surat = new Tmjenis_surat();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->tmskpd = new Tmskpd();
		$this->surat = new Surat();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->main_inbox = new Main_inbox();
		$this->tmpegawai = new Tmpegawai();
		$this->outbox = new Outbox();
        $this->load->helper('function_helper');
    }
	
    function index()
	{
        echo "Null.";
        exit();
    }
	
	function s($skpd, $uniq, $id)
	{
		$file_pdf = '';
		$uniq_key = sha1($id.',*&R34D^%'.$skpd);
		if($uniq_key === $uniq)
		{
			$row = $this->tmpersuratan->where('id = '.$id.' and id_skpd_in = '.$skpd.' and id_skpd_out = '.$skpd)->get();
			$data = array(
				'no_surat' => $row->no_surat,
				'dari' => $this->tmskpd->where('id', $row->id_skpd_in)->get()->n_skpd,
				'prihal' => $row->prihal,
				'tgl_surat' => indonesian_date($row->tgl_surat)
			);
			$this->load->view($this->view.'read', $data);
		}
		else
		{
			echo "File tidak ada!";
			exit();
		}
	}
	
	
}