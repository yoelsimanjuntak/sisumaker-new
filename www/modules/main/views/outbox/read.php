<div class="messagepanel">
	<div class="messagemenu">
		<ul>
			<li class="no-active"><a href="<?php echo base_url().'main/inbox';?>"><span class="iconfa-inbox"></span> Kotak Masuk <?php if($newpesan != 0){ ?><span class="new-pesan"><?php echo $newpesan;?> baru</span><?php }?></a></li>
			<li class="active"><a href="<?php echo base_url().'main/outbox';?>"><span class="iconfa-plane"></span> Pesan Terkirim</a></li>
			<!--<?php if($auth_masuk != 0){ ?>
			<li class="no-active"><a href="<?php echo base_url().'main/surat_masuk';?>"><span class="iconfa-envelope"></span> Surat Masuk <?php if($newsurat_masuk != 0){ ?><span class="new-pesan"><?php echo $newsurat_masuk;?> baru</span><?php }?></a></li>
			<?php }?>-->
		</ul>
	</div>
	<div class="messagecontent" style="border-right:1px solid #bb2f0e">
		<div class="messageleft" style="width:100%;">
			<?php $this->load->view($this->view.'type1');?>
		</div>
	</div>
</div>