<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

//uncomment di bawah ini atau gunakan autoload yang di config->config->composer_autoload default ada di composer_autoload
//require_once FCPATH . 'vendor/autoload.php';

use Restserver\Libraries\REST_Controller;

use \Firebase\JWT\JWT;

class Restdata extends REST_Controller{

  private $secretkey = ')(*&^%5u8h4n4ll0H!@#$%';

  public function __construct(){
    parent::__construct();
    header("Access-Control-Allow-Origin: *"); 
	header("Access-Control-Allow-Credential: true"); 
	header("Access-Control-Max-Age: 86400"); 
	header("Access-Control-Allow-Methods:GET,POST,OPTIONS"); 
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { 
		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) 
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); 
		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) 
			header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}"); 
		exit(0); 
	}
    $this->load->library('form_validation');
  }


  //method untuk not found 404
  public function notfound($pesan){
    $this->response([
      'status'=>FALSE,
      'message'=>$pesan
    ],REST_Controller::HTTP_NOT_FOUND);
  }

  //method untuk bad request 400
  public function badreq($pesan){
    $this->response([
      'status'=>FALSE,
      'message'=>$pesan
    ],REST_Controller::HTTP_BAD_REQUEST);
  }

  //method untuk melihat token pada user
  public function viewtoken_post(){
    $this->load->model('loginmodel');

    $date = new DateTime();

    $user = $this->post('username',TRUE);
    $pass = $this->post('password',TRUE);
    $telp = $this->post('telp',TRUE);

    $row = $this->loginmodel->login($user, md5($pass), $telp);

    if($row){
		$payload['auth_id'] = $row->id;
		$payload['pegawai_id'] = $row->pegawai_id;
		$payload['username'] = $row->username;
		$payload['username_p'] = sha1('%U'.$payload['auth_id'].')'.$payload['username'].'*&'.$payload['pegawai_id']);
		$payload['iat'] = $date->getTimestamp(); //waktu di buat
		$payload['exp'] = $date->getTimestamp() + 2629746; //satu bulan

		$output = array(
			'id_token' => JWT::encode($payload,$this->secretkey),
			'realname' => $row->realname,
			'username' => $row->username,
			'n_skpd' => $row->n_skpd,
			'photo' => $row->photo,
			'nip' => $row->nip
		);
		$this->response($output,REST_Controller::HTTP_OK);
    }else{
     	$this->viewtokenfail($user,$pass);
    }
  }

  //method untuk jika view token diatas fail
  public function viewtokenfail($user,$pass){
    $this->response([
      'status'=>FALSE,
      'username'=>$user,
      'password'=>$pass,
      'message'=>'Invalid Credentials!'
      ],REST_Controller::HTTP_BAD_REQUEST);
  }

//method untuk mengecek token setiap melakukan post, put, etc
  public function cektoken(){
    $this->load->model('loginmodel');
    $jwt = $this->input->get_request_header('Authorization');

    try {

      $decode = JWT::decode($jwt,$this->secretkey,array('HS256'));
      //melakukan pengecekan database, jika username tersedia di database maka return true
      if ($this->loginmodel->is_valid_num($decode->username)>0) {
        return true;
      }

    } catch (Exception $e) {
      exit('Wrong Token');
    }


  }




}
