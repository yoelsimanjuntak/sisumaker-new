<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Notadinas extends ICA_AdminCont
{
    public $page = "main/notadinas/";
    public $view = "main/notadinas/";
    public $icon = "envelope";

    public function __construct()
    {
        parent::__construct();
        $this->restrict('2');
        $this->tmuser = new Tmuser();
        $this->tmpersuratan = new Tmpersuratan();
        $this->penomoran = new Penomoran();
        $this->tmsifat_surat = new Tmsifat_surat();
        $this->surat = new Surat();
        $this->tmjenis_surat = new Tmjenis_surat();
        $this->tr_surat_penerima = new Tr_surat_penerima();
        $this->tmpegawai = new Tmpegawai();
        $this->tmunitkerja = new Tmunitkerja();
        $this->tmjabatan = new Tmjabatan();
        $this->outbox = new Outbox();
        $this->main_inbox = new Main_inbox();
        $this->load->helper('function_helper');
        $this->load->library('api_sms_class_reguler_json');
        $this->load->library('onesignal');
    }
    public function notif($data)
    {
        $notif = new onesignal();
        $notif->setData($data);
        $responjson1 = $notif->ios();
        $responjson2 = $notif->android();



        // return $responjson;
    }

    public function notifTest()
    {
        $Onesignal = [
            'dari' => 'Aldi Ardianto',
            'ke' => 3171,
            'perihal' =>  'Ajakan untuk bermain game'
        ];

        $notif = $this->notif($Onesignal);
        $this->response($notif, Auth::HTTP_OK);
    }

    public function index()
    {
        $data = array(
            'result_jnssurat' => $this->tmjenis_surat->select(),
            'result_tmsifat_surat' => $this->tmsifat_surat->select(),
            'result_ttdsekda' => $this->main_inbox->penandatangan_sekda(19),
            'result_ttdskpd' => $this->main_inbox->penandatangan_skpd($this->session->userdata('skpd_id')),
            'result_disposisi' => $this->main_inbox->disposisi($this->session->userdata('auth_id')),
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.ui.timepicker.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "tinymce/jquery.tinymce.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "wysiwyg.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "jquery.chosen.css"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "chosen.jquery.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "bootstrap-fileupload.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "bootstrap-fileupload.min.css"),
                $this->lib_load_css_js->load_css(base_url(), "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
                ),
            'breadcrubs' => " Compose",
            'title' => 'Compose',
            'content' => 'add'
        );
        $this->load->view('template_mail', $data);
    }
    
    public function list_skpd($c_skpd, $c_kecamatan, $c_kelurahan, $puskesmas, $c_sekolah, $c_rs, $c_uptpendidikan)
    {
        $skpd = explode('-', $c_skpd);
        $kecamatan = explode('-', $c_kecamatan);
        $kelurahan = explode('-', $c_kelurahan);
        $puskesmas = explode('-', $puskesmas);
        $sekolah = explode('-', $c_sekolah);
        $rs = explode('-', $c_rs);
        $uptpendidikan = explode('-', $c_uptpendidikan);
        $data = array(
            'id_kirim' => array($skpd[0], $kecamatan[0], $kelurahan[0], $puskesmas[0], $sekolah[0], $rs[0], $uptpendidikan[0]),
            'id_jabatan' => array($skpd[1], $kecamatan[1], $kelurahan[1], $puskesmas[1], $sekolah[1], $rs[1], $uptpendidikan[1]),
            'result' => $this->main_inbox->disposisi($this->session->userdata('auth_id')),
        );
        $this->load->view($this->view.'list_penerima', $data);
    }
    
    public function download_file($id_jnssurat)
    {
        //$file = $this->tmjenis_surat->where("id = '".$id_jnssurat."'")->get()->file_surat;
        //echo ' <a href="'.base_url().$file.'">Download File</a>';
    }
    
    public function varifikasi_pass()
    {
        $data = array(
            'assets' => array(),
            'content' => 'verifikasi_pass'
        );
        $this->load->view('view', $data);
    }
    
    public function submit_pass()
    {
        $num = $this->tmuser->where("username = '" . $this->session->userdata('username') . "' and password = '" . md5($this->input->post('password')) . "' and c_status=1")->count();
        if ($num == 0) {
            $result = array(
                'status' => 0
            );
            echo json_encode($result);
        } else {
            $result = array(
                'status' => 1
            );
            echo json_encode($result);
        }
    }
    
    public function send_paraf()
    {
        $paraf = $this->tmpegawai->where('id', $this->session->userdata('pegawai_id'))->get()->file_paraf;
        $data = array(
            'assets' => array(),
            'content' => 'send_paraf',
            'paraf' => $paraf
        );
        $this->load->view('view', $data);
    }
    
    //----- Send SMS With Raja-sms.com
    public function save()
    {
        if ($this->session->userdata('pegawai_id') != 0) {
                    
                //Validasi Disposisi
            if (count($this->input->post('pegawai') != 0)) {
                $next = true;
                if ($this->input->post('d_awal_kegiatan') != '' || $this->input->post('d_akhir_kegiatan') != '' || $this->input->post('v_kegiatan') != '') {
                    if ($this->input->post('d_awal_kegiatan') == '' || $this->input->post('d_akhir_kegiatan') == '' || $this->input->post('v_kegiatan') == '') {
                        echo '
							<script type="text/javascript">
								alert("Inputan kegiatan harus diisi lengkap.");
								history.go(-1);
							</script>';
                        $next = false;
                    }
                }
                if ($next == true) {
                    if ($_FILES['userfile']['name'] != '') {
                        $name = date('YmdHis').$this->session->userdata('auth_id');
                        $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
                        $endof = '.'.end($explode);
                        $exten = 'uploads/surat_intern/'.$name.$endof;
                        $config['file_name'] = $name;
                        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
                        $config['upload_path'] = 'uploads/surat_intern/';
                        $config['max_size']	= '30000';
                        $config['overwrite'] = true;
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('userfile')) {
                            echo '
								<script type="text/javascript">
									alert("1 '.$this->upload->display_errors().'");
									history.go(-1);
								</script>';
                            $next = false;
                        } else {
                            $file_lampiran = '';
                            if ($_FILES['file_lampiran']['name'] != '') {
                                $name_l = $name;
                                $explode_l = explode(".", str_replace(' ', '', $_FILES['file_lampiran']['name']));
                                $endof_l = '.'.end($explode_l);
                                $file_lampiran = 'uploads/surat_intern/'.$name_l.$endof_l;
                                $config_l['file_name'] = $name_l;
                                $config_l['allowed_types'] = 'xls|xlsx';
                                $config_l['upload_path'] = 'uploads/surat_intern/';
                                $config_l['max_size']	= '30000';
                                $config_l['overwrite'] = true;
                                $this->load->library('upload', $config_l);
                                if (!$this->upload->do_upload('file_lampiran')) {
                                    echo '
										<script type="text/javascript">
											alert("2 '.$this->upload->display_errors().'");
											history.go(-1);
										</script>';
                                    $next = false;
                                    exit();
                                }
                            }
                    
                            $dari = $this->session->userdata('pegawai_id');
                            $unitkerja = "";
                            if ($this->session->userdata('unitkerja_id') != 0) {
                                $unitkerja = " (".$this->tmunitkerja->where('id', $this->session->userdata('unitkerja_id'))->get()->n_unitkerja.")";
                            }
                            
                            $id_compose = $this->input->post('id_compose');
                            $no_surat = $this->input->post('no_surat');
                            $id_pegawaittd = $this->input->post('id_pegawaittd');
                            if ($id_compose == 1) {
                                $id_pegawaittd = 0;
                            } elseif ($id_compose == 2) {
                                $no_surat = "";
                            }
                            
                            //$nomor = $this->penomoran->nomor_save($this->session->userdata('skpd_id'), 2);
                            $data = array(
                                'dari' => $this->session->userdata('realname').$unitkerja,
                                'id_compose' => $id_compose,
                                'no_surat' => $no_surat,
                                'id_pegawaittd' => $id_pegawaittd,
                                'tgl_surat' => $this->input->post('tgl_surat'),
                                'id_surat' => 1,
                                'id_jnssurat' => 2,
                                'id_sifatsurat' => $this->input->post('id_sifatsurat'),
                                'prihal' => $this->input->post('prihal'),
                                'file' => $exten,
                                'file_lampiran' => $file_lampiran,
                                'isi_surat' => $this->input->post('isi_surat'),
                                'id_pegawai' => $dari,
                                'id_skpd_in' => $this->session->userdata('skpd_id'),
                                'id_skpd_out' => $this->session->userdata('skpd_id'),
                                'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
                                'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
                                'v_kegiatan' => $this->input->post('v_kegiatan'),
                                'd_entry' => date('Y-m-d')
                            );
                            $this->tmpersuratan->insert($data);
                            $id = $this->tmpersuratan->select_max();
                            
                            $pegawai = $this->tmpegawai->where("id = '".$dari."'")->get();
                            $jabatan = $this->tmjabatan->where("id = '".$pegawai->tmjabatan_id."'")->get()->n_jabatan;
                            $unitkerja = $this->tmunitkerja->where("id ='".$pegawai->tmunitkerja_id."'")->get()->initial;
                            
                            $senddata = array(
                                'apikey' => SMSGATEWAY_APIKEY,
                                'callbackurl' => SMSGATEWAY_CALLBACKURL,
                                'datapacket'=> array()
                            );
                                
                            //Kepada:
                            $d_entry = date('Y-m-d H:i:s');
                            $send = "Ada disposisi surat dari ".$jabatan.' '.$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
                            $pegawai_list = $this->input->post('pegawai');
                            $pegawai_list_len = count($pegawai_list);
                            for ($i=0; $i<$pegawai_list_len; $i++) {
                                $data = array(
                                    'id_persuratan' => $id,
                                    'id_pegawai_dari' => $dari,
                                    'id_pegawai_ke' => $pegawai_list[$i],
                                    'status' => 0,
                                    'cc' => 0,
                                    'd_entry' => $d_entry,
                                    'id_skpd_dari' => $this->session->userdata('skpd_id'),
                                    'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
                                    'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
                                    'v_kegiatan' => $this->input->post('v_kegiatan')
                                );
                                $this->tr_surat_penerima->insert($data);
                                
                                $telp = $this->tmpegawai->where("id = '".$pegawai_list[$i]."'")->get()->telp;
                                if ($telp != '-') {
                                    array_push($senddata['datapacket'], array(
                                        'number' => trim($telp),
                                        'message' => urlencode(stripslashes(utf8_encode($send))),
                                        'sendingdatetime' => ""));
                            
                                    //------- Telp hanya untuk SEMENTARA
                                    //------- 28 April 2017, sms untuk ajudan pa wakil walikota
                                    if ($pegawai_list[$i] == '211') {
                                        $number = "081286571031";
                                        $sendingdatetime = "";
                                        array_push($senddata['datapacket'], array(
                                            'number' => trim($number),
                                            'message' => urlencode(stripslashes(utf8_encode($send))),
                                            'sendingdatetime' => $sendingdatetime));
                                    }
                                }
                                //$Onesignal = [
                                //    'type' => 1,
                                //    'id_surat' => $id,
                                //    'dari' => $this->session->userdata('realname').$unitkerja,
                                //    'ke' => $pegawai_list[$i],
                                //    'perihal' =>  $this->input->post('prihal')
                                //];
            
                                //$this->notif($Onesignal);
                            }
                            
                            //CC:
                            $valid_cc = false;
                            $send = "Ada CC surat dari ".$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
                            $pegawai_cc_list = $this->input->post('pegawai_cc');
                            $pegawai_cc_list_len = count($pegawai_cc_list);
                            for ($i=0; $i<$pegawai_cc_list_len; $i++) {
                                $valid_cc = $pegawai_cc_list[$i];
                            }
                            if ($valid_cc != false) {
                                for ($i=0; $i<$pegawai_cc_list_len; $i++) {
                                    $data = array(
                                        'id_persuratan' => $id,
                                        'id_pegawai_dari' => $dari,
                                        'id_pegawai_ke' => $pegawai_cc_list[$i],
                                        'status' => 0,
                                        'cc' => 1,
                                        'd_entry' => $d_entry,
                                        'id_skpd_dari' => $this->session->userdata('skpd_id'),
                                        'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
                                        'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
                                        'v_kegiatan' => $this->input->post('v_kegiatan')
                                    );
                                    $this->tr_surat_penerima->insert($data);
                                    
                                    $telp = $this->tmpegawai->where("id = '".$pegawai_cc_list[$i]."'")->get()->telp;
                                    if ($telp != '-') {
                                        array_push($senddata['datapacket'], array(
                                            'number' => trim($telp),
                                            'message' => urlencode(stripslashes(utf8_encode($send))),
                                            'sendingdatetime' => ""));
                                                                        
                                        //------- Telp hanya untuk SEMENTARA
                                        //------- 28 April 2017, sms untuk ajudan pa wakil walikota
                                        if ($pegawai_cc_list[$i] == '211') {
                                            $number = "081286571031";
                                            $sendingdatetime = "";
                                            array_push($senddata['datapacket'], array(
                                                'number' => trim($number),
                                                'message' => urlencode(stripslashes(utf8_encode($send))),
                                                'sendingdatetime' => $sendingdatetime));
                                        }
                                    }
                                    
                                    //$Onesignal = [
                                    //    'type' => 1,
                                    //    'id_surat' => $id,
                                    //    'dari' => $this->session->userdata('realname').$unitkerja,
                                    //    'ke' => $pegawai_cc_list[$i],
                                    //    'perihal' =>  $this->input->post('prihal')
                                    //];
                
                                    //$this->notif($Onesignal);
                                }
                            }
                            $sms = new api_sms_class_reguler_json();
                            $sms->setIp(SMSGATEWAY_IPSERVER);
                            $sms->setData($senddata);
                            $responjson = $sms->send();
                            $this->saldo_sms->save($responjson);
                            redirect(base_url().'main/outbox');
                        }
                    } else {
                        echo '
							<script type="text/javascript">
								alert("Berkas yang di-upload Error!");
								history.go(-1);
							</script>';
                    }
                }
            } else {
                echo '
					<script type="text/javascript">
						alert("Inputan kepada harus di isi.");
						history.go(-1);
					</script>';
            }
        } else {
            echo '
				<script type="text/javascript">
					alert("Id pegawai Tidak ada.");
					history.go(-1);
				</script>';
        }
    }
    
    //----- Send SMS With Gammu
    /*function save()
    {
        if($this->session->userdata('pegawai_id') != 0)
        {

                //Validasi Disposisi
            if(count($this->input->post('pegawai') != 0))
            {
                $next = true;
                if($this->input->post('d_awal_kegiatan') != '' || $this->input->post('d_akhir_kegiatan') != '' || $this->input->post('v_kegiatan') != '')
                {
                    if($this->input->post('d_awal_kegiatan') == '' || $this->input->post('d_akhir_kegiatan') == '' || $this->input->post('v_kegiatan') == '')
                    {
                        echo '
                            <script type="text/javascript">
                                alert("Inputan kegiatan harus diisi lengkap.");
                                history.go(-1);
                            </script>';
                        $next = false;
                    }
                }
                if($next == true)
                {
                    if($_FILES['userfile']['name'] != '')
                    {
                        $name = date('YmdHis').$this->session->userdata('auth_id');
                        $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
                        $endof = '.'.end($explode);
                        $exten = 'uploads/surat_intern/'.$name.$endof;
                        $config['file_name'] = $name;
                        $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
                        $config['upload_path'] = 'uploads/surat_intern/';
                        $config['max_size']	= '30000';
                        $config['overwrite'] = true;
                        $this->load->library('upload', $config);
                        if(!$this->upload->do_upload('userfile')){
                            echo '
                                <script type="text/javascript">
                                    alert("1 '.$this->upload->display_errors().'");
                                    history.go(-1);
                                </script>';
                            $next = false;
                        }
                        else
                        {
                            $file_lampiran = '';
                            if($_FILES['file_lampiran']['name'] != '')
                            {
                                $name_l = $name;
                                $explode_l = explode(".", str_replace(' ', '', $_FILES['file_lampiran']['name']));
                                $endof_l = '.'.end($explode_l);
                                $file_lampiran = 'uploads/surat_intern/'.$name_l.$endof_l;
                                $config_l['file_name'] = $name_l;
                                $config_l['allowed_types'] = 'xls|xlsx';
                                $config_l['upload_path'] = 'uploads/surat_intern/';
                                $config_l['max_size']	= '30000';
                                $config_l['overwrite'] = true;
                                $this->load->library('upload', $config_l);
                                if(!$this->upload->do_upload('file_lampiran')){
                                    echo '
                                        <script type="text/javascript">
                                            alert("2 '.$this->upload->display_errors().'");
                                            history.go(-1);
                                        </script>';
                                    $next = false;
                                    exit();
                                }
                            }

                            $dari = $this->session->userdata('pegawai_id');
                            $unitkerja = "";
                            if($this->session->userdata('unitkerja_id') != 0){
                                $unitkerja = " (".$this->tmunitkerja->where('id', $this->session->userdata('unitkerja_id'))->get()->n_unitkerja.")";
                            }

                            //$nomor = $this->penomoran->nomor_save($this->session->userdata('skpd_id'), 2);
                            $data = array(
                                'dari' => $this->session->userdata('realname').$unitkerja,
                                'id_compose' => $this->input->post('id_compose'),
                                'no_surat' => $this->input->post('no_surat'),
                                'tgl_surat' => $this->input->post('tgl_surat'),
                                'id_surat' => 1,
                                'id_jnssurat' => 2,
                                'id_sifatsurat' => $this->input->post('id_sifatsurat'),
                                'prihal' => $this->input->post('prihal'),
                                'file' => $exten,
                                'file_lampiran' => $file_lampiran,
                                'isi_surat' => $this->input->post('isi_surat'),
                                'id_pegawai' => $dari,
                                'id_skpd_in' => $this->session->userdata('skpd_id'),
                                'id_skpd_out' => $this->session->userdata('skpd_id'),
                                'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
                                'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
                                'v_kegiatan' => $this->input->post('v_kegiatan'),
                                'd_entry' => date('Y-m-d')
                            );
                            $this->tmpersuratan->insert($data);
                            $id = $this->tmpersuratan->select_max();

                            $pegawai = $this->tmpegawai->where("id = '".$dari."'")->get();
                            $jabatan = $this->tmjabatan->where("id = '".$pegawai->tmjabatan_id."'")->get()->n_jabatan;
                            $unitkerja = $this->tmunitkerja->where("id ='".$pegawai->tmunitkerja_id."'")->get()->initial;

                                //Kepada:
                            $d_entry = date('Y-m-d H:i:s');
                            $send = "Ada disposisi surat dari ".$jabatan.' '.$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
                            $pegawai_list = $this->input->post('pegawai');
                            $pegawai_list_len = count($pegawai_list);
                            for($i=0; $i<$pegawai_list_len; $i++)
                            {
                                $data = array(
                                    'id_persuratan' => $id,
                                    'id_pegawai_dari' => $dari,
                                    'id_pegawai_ke' => $pegawai_list[$i],
                                    'status' => 0,
                                    'cc' => 0,
                                    'd_entry' => $d_entry,
                                    'id_skpd_dari' => $this->session->userdata('skpd_id'),
                                    'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
                                    'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
                                    'v_kegiatan' => $this->input->post('v_kegiatan')
                                );
                                $this->tr_surat_penerima->insert($data);

                                $telp = $this->tmpegawai->where("id = '".$pegawai_list[$i]."'")->get()->telp;
                                if($telp != '-')
                                {
                                    $data = array(
                                        'DestinationNumber' => $telp,
                                        'TextDecoded' => $send,
                                        'CreatorID' => 'Gammu',
                                        'DeliveryReport' => 'yes'
                                    );
                                    $this->outbox->insert($data);

                                        //------- Telp hanya untuk SEMENTARA
                                        //------- 28 April 2017, sms untuk ajudan pa wakil walikota
                                    if($pegawai_list[$i] == '211')
                                    {
                                        $data = array(
                                            'DestinationNumber' => '081286571031',
                                            'TextDecoded' => $send,
                                            'CreatorID' => 'Gammu',
                                            'DeliveryReport' => 'yes'
                                        );
                                        $this->outbox->insert($data);
                                    }
                                }
                            }

                                //CC:
                            $valid_cc = false;
                            $send = "Ada CC surat dari ".$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
                            $pegawai_cc_list = $this->input->post('pegawai_cc');
                            $pegawai_cc_list_len = count($pegawai_cc_list);
                            for($i=0; $i<$pegawai_cc_list_len; $i++)
                            {
                                $valid_cc = $pegawai_cc_list[$i];
                            }
                            if($valid_cc != false)
                            {
                                for($i=0; $i<$pegawai_cc_list_len; $i++)
                                {
                                    $data = array(
                                        'id_persuratan' => $id,
                                        'id_pegawai_dari' => $dari,
                                        'id_pegawai_ke' => $pegawai_cc_list[$i],
                                        'status' => 0,
                                        'cc' => 1,
                                        'd_entry' => $d_entry,
                                        'id_skpd_dari' => $this->session->userdata('skpd_id'),
                                        'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
                                        'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
                                        'v_kegiatan' => $this->input->post('v_kegiatan')
                                    );
                                    $this->tr_surat_penerima->insert($data);

                                    $telp = $this->tmpegawai->where("id = '".$pegawai_cc_list[$i]."'")->get()->telp;
                                    if($telp != '-')
                                    {
                                        $data = array(
                                            'DestinationNumber' => $telp,
                                            'TextDecoded' => $send,
                                            'CreatorID' => 'Gammu',
                                            'DeliveryReport' => 'yes'
                                        );
                                        $this->outbox->insert($data);

                                            //------- Telp hanya untuk SEMENTARA
                                            //------- 28 April 2017, sms untuk ajudan pa wakil walikota
                                        if($pegawai_cc_list[$i] == '211')
                                        {
                                            $data = array(
                                                'DestinationNumber' => '081286571031',
                                                'TextDecoded' => $send,
                                                'CreatorID' => 'Gammu',
                                                'DeliveryReport' => 'yes'
                                            );
                                            $this->outbox->insert($data);
                                        }
                                    }
                                }
                            }
                            redirect(base_url().'main/outbox');
                        }
                    }
                    else
                    {
                        echo '
                            <script type="text/javascript">
                                alert("Berkas yang di-upload Error!");
                                history.go(-1);
                            </script>';
                    }
                }
            }
            else
            {
                echo '
                    <script type="text/javascript">
                        alert("Inputan kepada harus di isi.");
                        history.go(-1);
                    </script>';
            }
        }
        else
        {
            echo '
                <script type="text/javascript">
                    alert("Id pegawai Tidak ada.");
                    history.go(-1);
                </script>';
        }
    }*/
}
