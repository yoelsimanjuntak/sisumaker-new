<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Pegawai_jenisdokumen extends ICA_AdminCont
{
	var $page = "pegawai/pegawai_jenisdokumen/";
	var $view = "pegawai/pegawai_jenisdokumen/";
	var $icon = "cogs";

    function __construct() 
	{
        parent::__construct();
		$this->restrict('9');
		$this->tmjenisdokumen = new Tmjenisdokumen();
		$this->tmpegawai_tmjenisdokumen = new Tmpegawai_tmjenisdokumen();
    }

	function index($tmpegawai_id)
	{
		$data = array(
			'tmpegawai_id' => $tmpegawai_id,
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.dataTables.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js")
				),
			'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> Pegawai <span class='separator'></span> <a href='".base_url()."pegawai'>Data Master</a> <span class='separator'></span> Set Tanda Tangan",
			'title' => 'Set Tanda Tangan',
			'content' => 'table',
			'sort' => 1
		);
		$this->load->view('template', $data);
	}
	
	function add($tmpegawai_id)
	{
		$data = array(
			'result_tmjenisdokumen' => $this->tmjenisdokumen->select(),
			'tmpegawai_id' => $tmpegawai_id
		);
		$this->load->view($this->view.'add', $data);
	}
	
	function save()
	{
		$tmpegawai_id = $this->input->post('tmpegawai_id');
		$tmjenisdokumen_id = $this->input->post('tmjenisdokumen_id');
		if($this->tmpegawai_tmjenisdokumen->where('tmpegawai_id = "'.$tmpegawai_id.'" and tmjenisdokumen_id = "'.$tmjenisdokumen_id.'"')->count() == 0)
		{
			$data = array(
				'tmpegawai_id' => $tmpegawai_id,
				'tmjenisdokumen_id' => $tmjenisdokumen_id
			);
			$this->tmpegawai_tmjenisdokumen->insert($data);
			$output = array(
				'status' => 1
			);
			echo json_encode($output);
		}
		else
		{
			$output = array(
				'status' => 11
			);
			echo json_encode($output);
		}
	}
	
	function delete($id)
	{
		$this->tmpegawai_tmjenisdokumen->delete($id);
	}
	
	function data($tmpegawai_id)
	{
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				tmpegawai_tmjenisdokumen.id,
				tmjenisdokumen.n_jenisdokumen
			')
			->from('tmpegawai_tmjenisdokumen')
			->join('tmjenisdokumen','tmpegawai_tmjenisdokumen.tmjenisdokumen_id = tmjenisdokumen.id')
			->where('tmpegawai_tmjenisdokumen.tmpegawai_id', $tmpegawai_id)
			->add_column('aksi',
				'<center>
					<a id="delete" href="#" to="'.base_url().$this->page.'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>',
				'tmjenisdokumen.id')
			->generate();
	}
}