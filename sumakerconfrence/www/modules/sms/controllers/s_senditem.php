<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class S_Senditem extends ICA_AdminCont
{
	var $page = "sms/s_senditem/";
	var $view = "sms/s_senditem/";
	var $icon = "phone";

    function __construct() 
	{
        parent::__construct();
		$this->outbox = new Outbox();
		$this->sentitems = new sentitems();
        $this->load->helper('function_helper');
    }

	function cek_min(){$return = $this->sentitems->where("sentitems.status <> 'SendingOKNoReport' and sentitems.status <>'SendingOK'")->count();$output = array('count' => $return);echo json_encode($output);}
	
	function cek()
	{
		$return = $this->sentitems->where("sentitems.status <> 'SendingOKNoReport' and sentitems.status <>'SendingOK'")->count();
		$output = array('count' => $return);
		echo json_encode($output);
	}
	
	function index()
	{
		$data = array(
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.dataTables.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
				),
			'breadcrubs' => " Send Items",
			'title' => 'Sms Gateway <h5>Error Sending </h5>',
			'content' => 'table',
			'sort' => 4,
			'result' => $this->outbox->not_send()
		);
		$this->load->view('template', $data);
	}
	
	function send($id)
	{
		$row = $this->sentitems->where("ID = '".$id."'")->get();
		$data = array(
			'DestinationNumber' => $row->DestinationNumber,
			'TextDecoded' => $row->TextDecoded,
			'CreatorID' => 'Gammu',
			'DeliveryReport' => 'yes'
		);
		$this->outbox->insert($data);
		$this->sentitems->delete($id);
		redirect(base_url().$this->page);
	}
	
	function data()
	{
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				sentitems.ID,
				sentitems.DestinationNumber,
				tmskpd.n_skpd,
				tmpegawai.n_pegawai,
				sentitems.TextDecoded,
				sentitems.SendingDateTime
			')
			->from('sentitems')
			->where('sentitems.status <>', 'SendingOKNoReport')
			->where('sentitems.status <>', 'SendingOK')
			->join('tmpegawai','sentitems.DestinationNumber = tmpegawai.telp')
			->join('tmskpd','tmpegawai.tmskpd_id = tmskpd.id')
			->add_column('aksi',
				'<center>
					<a href="'.base_url().$this->page.'send/$1"><i class=" iconfa-envelope-alt"></i> Kirim Ulang</a>
				</center>',
				'sentitems.ID')
			->generate();
	}
}