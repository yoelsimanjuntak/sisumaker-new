<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Outbox extends ICA_AdminCont
{
	var $page = "main/outbox/";
	var $view = "main/outbox/";
    var $icon = "plane";

    function __construct() 
	{
        parent::__construct();
		$this->restrict('4');
		$this->tmpersuratan = new Tmpersuratan();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->surat = new Surat();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->tmuser_userauth = new Tmuser_userauth();
		$this->main_outbox = new Main_outbox();
		$this->tmpegawai = new Tmpegawai();
		$this->tmuser = new Tmuser();
        $this->load->helper('text');
        $this->load->helper('function_helper');
		$this->load->library("pagination");
    }
	
	function search_suges()
	{
		$v_search = $this->input->post('ss');
		$where = " 	AND (tmpersuratan.prihal LIKE '%".$v_search."%'
							OR tmpersuratan.dari LIKE '%".$v_search."%'
							OR tmpersuratan.no_surat LIKE '%".$v_search."%'
							OR tmpersuratan.tgl_surat LIKE '%".$v_search."%'
							OR tmpersuratan.d_awal_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.d_akhir_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.v_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.isi_surat LIKE '%".$v_search."%'
							OR tmpegawai.n_pegawai LIKE '%".$v_search."%') GROUP BY tmpersuratan.id";
		$result = $this->query(10, 0, $where);
		foreach($result->result() as $row){
			$data[] = $row->prihal;
		}
		echo json_encode($data);
	}
	
	function index($search = NULL, $offset = NULL)
	{
		$where = '';
		$filter_text = '<i class="iconfa-reorder"></i>&nbsp; Semua';
		if($offset == ''){$offset = 0;}
		if($search != NULL || $search != 0)
		{
			$exp = explode('-', $search);
			if($exp[0] == 'b')
			{
				$where = " AND tr_surat_penerima.status = ".$exp[1];
				if($exp[1] == 0){ $filter_text = '<i class="iconfa-exclamation-sign"></i>&nbsp; Belum dibaca';}
				elseif($exp[1] == 1){ $filter_text = '<i class="iconfa-ok-sign"></i>&nbsp; Sudah dibaca';}
			}
			elseif($exp[0] == 'c')
			{
				$where = " AND tmpersuratan.id_sifatsurat = ".$exp[1];
				if($exp[1] == 1){ $filter_text = '<span class="label label-important">&nbsp;</span> &nbsp;Sangat Segera';}
				elseif($exp[1] == 2){ $filter_text = '<span class="label label-warning">&nbsp;</span> &nbsp;Segera';}
				elseif($exp[1] == 3){ $filter_text = '<span class="label label-inverse">&nbsp;</span> &nbsp;Rahasia';}
				elseif($exp[1] == 4){ $filter_text = '<span class="label label-info">&nbsp;</span> &nbsp;Biasa';}
			}
			elseif($exp[0] == 's')
			{
				$v_search = substr($search, 2);
				$where = " 	AND (tmpersuratan.prihal LIKE '%".$v_search."%'
							OR tmpersuratan.dari LIKE '%".$v_search."%'
							OR tmpersuratan.no_surat LIKE '%".$v_search."%'
							OR tmpersuratan.tgl_surat LIKE '%".$v_search."%'
							OR tmpersuratan.d_awal_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.d_akhir_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.v_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.isi_surat LIKE '%".$v_search."%'
							OR tmpegawai.n_pegawai LIKE '%".$v_search."%')";
			}
		}else{$search = 0;}
		$jml = $this->query_count($where);
		$config['base_url'] = base_url().'main/outbox/index/'.$search.'/';
		$config['total_rows'] = $jml->num_rows();
		$config['per_page'] = '25';
		$config['num_links'] = 0;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['next_link'] = '<i class="iconfa-chevron-right" title="Selanjutnya"></i>';
		$config['prev_link'] = '<i class="iconfa-chevron-left" title="Sebelumnya"></i>';
		$config['cur_tag_open'] = '';
		$config['cur_tag_close'] = '';
		$this->pagination->initialize($config);

		$data = array(
			'breadcrubs' => " Outbox",
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.dataTables.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
			),
			'title' => 'Outbox',
			'content' => 'table',
			'paging' => $this->pagination->create_links(),
			'result' => $this->query($config['per_page'], $offset, $where),
			'num_offset' => $offset,
			'per_page' => $config['per_page'],
			'total_rows' => $config['total_rows'],
			'search' => $search,
			'filter_text' => $filter_text,
			'newpesan' => $this->main_outbox->newpesan($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
			'newsurat_masuk' => $this->main_outbox->newsurat_masuk($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
			'auth_masuk' => $this->tmuser_userauth->cek($this->session->userdata('auth_id'), 11)
		);
		$this->load->view('template_mail', $data);
	}
	
	function query($num, $offset, $where = false)
	{
		if($offset == ''){ $offset = 0; }
		$pegawai_id = $this->session->userdata('pegawai_id');
		return $this->db
			->query('
				SELECT 
				tr_surat_penerima.id,
				tr_surat_penerima.d_entry,
				tr_surat_penerima.status,
				tmpegawai.n_pegawai,
				tmpersuratan.dari,
				tmpersuratan.tgl_surat,
				tmpersuratan.tgl_terima_surat,
				tmpersuratan.prihal,
				tmpersuratan.id_sifatsurat
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_ke = tmpegawai.id
				WHERE tr_surat_penerima.id_pegawai_dari = '.$pegawai_id.$where.'
				ORDER BY tr_surat_penerima.d_entry DESC
				LIMIT '.$offset.', '.$num);
	}
	
	function query_count($where = false)
	{
		$pegawai_id = $this->session->userdata('pegawai_id');
		return $this->db
			->query('
				SELECT
				tr_surat_penerima.id
				FROM tr_surat_penerima
				JOIN tmpersuratan on tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai on tr_surat_penerima.id_pegawai_ke = tmpegawai.id
				WHERE tr_surat_penerima.id_pegawai_dari = '.$pegawai_id.$where);
	}
	
	function valid_surat($encrip)
	{
		$id = decrip($encrip);
		$id_persuratan = $this->tr_surat_penerima->where("id = '".$id."'")->get()->id_persuratan;
		$row = $this->tmpersuratan->where("id = '".$id_persuratan."'")->get();
		if($row->id_sifatsurat == 3)
		{
			$data = array(
				'id' => $id,
				'encrip' => $encrip,
				'title' => 'Passsword',
				'assets' => array(),
				'content' => 'verifikasi_pesan'
			);
			$this->load->view('view', $data);
		}
		else
		{
			redirect(base_url().$this->page.'read/'.$encrip.'/'.sha1($id.$encrip));
		}
	}
	
	function submit_pass()
	{
		$num = $this->tmuser->where("username = '" . $this->session->userdata('username') . "' and password = '" . md5($this->input->post('password')) . "' and c_status=1")->count();
		if($num == 0)
		{
			$result = array(
				'status' => 0
			);
			echo json_encode($result);
		}
		else
		{
			$result = array(
				'status' => 1
			);
			echo json_encode($result);
		}
	}
	
	function read($encrip, $valid)
	{
		$id = decrip($encrip);
		if(sha1($id.$encrip) === $valid)
		{
			$id_persuratan = $this->tr_surat_penerima->where("id = '".$id."'")->get()->id_persuratan;
			$data = array(
				'result' => $this->main_outbox->read($id),
				'result_disposisi' => $this->main_outbox->disposisi($this->session->userdata('pegawai_id')),
				'result_history' => $this->main_outbox->history($this->session->userdata('pegawai_id'), $id_persuratan),
				'dari' => $this->main_outbox->surat_dari($id_persuratan),
				'result_to' => $this->main_outbox->to($id_persuratan),
				'result_cc' => $this->main_outbox->to($id_persuratan, 1),
				'breadcrubs' => " Outbox",
				'assets' => array(),
				'title' => 'Outbox',
				'content' => 'read',
				'newpesan' => $this->main_outbox->newpesan($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'newsurat_masuk' => $this->main_outbox->newsurat_masuk($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'auth_masuk' => $this->tmuser_userauth->cek($this->session->userdata('auth_id'), 11),
				'c_read' => $this->main_outbox->auth_read($id, $this->session->userdata('pegawai_id'))
			);
			$this->load->view('template_mail', $data);
		}
		else
		{
			$id = 0;
			$id_persuratan = 0;
			$data = array(
				'result' => $this->main_outbox->read($id),
				'result_disposisi' => $this->main_outbox->disposisi($this->session->userdata('pegawai_id')),
				'result_history' => $this->main_outbox->history($this->session->userdata('pegawai_id'), $id_persuratan),
				'dari' => $this->main_outbox->surat_dari($id_persuratan),
				'result_to' => $this->main_outbox->to($id_persuratan),
				'result_cc' => $this->main_outbox->to($id_persuratan, 1),
				'breadcrubs' => " Outbox",
				'assets' => array(),
				'title' => 'Outbox',
				'content' => 'read',
				'newpesan' => $this->main_outbox->newpesan($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'newsurat_masuk' => $this->main_outbox->newsurat_masuk($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'auth_masuk' => $this->tmuser_userauth->cek($this->session->userdata('auth_id'), 11),
				'c_read' => $this->main_outbox->auth_read($id, $this->session->userdata('pegawai_id'))
			);
			$this->load->view('template_mail', $data);
		}
	}
	
	function info($encrip)
	{
		$id = decrip($encrip);
		$id_persuratan = $this->tr_surat_penerima->where("id = '".$id."'")->get()->id_persuratan;
		$data = array(
			'result' => $this->main_outbox->read($id),
			'result_info' => $this->tr_surat_penerima->info($id_persuratan),
			'content' => 'info'
		);
		$this->load->view('view', $data);
	}
	
	function info_suratmasuk($id_persuratan)
	{
		$data = array(
			'result' => $this->main_outbox->read($id),
			'result_info' => $this->tr_surat_penerima->info($id_persuratan),
			'content' => 'info'
		);
		$this->load->view('view', $data);
	}
	
	function data($pegawai_id)
	{
		$skpd_id = $this->session->userdata('skpd_id');
		function cek_read($a){
			if($a == 1){
				$return = '<em class="text-success"> <i class="iconfa-ok-sign"></i> sudah dibaca </em>';
			}else{
				$return = '<b class="text-warning"> <i class="iconfa-exclamation-sign"></i> belum dibaca </b>';
			}
			return $return;
		}
		function cek_jenis($id)
		{
			if($id == '1'){
				$return = "<span class='label label-important'>Sangat Segera</span>";
			}elseif($id == '2'){
				$return = "<span class='label label-warning'>Segera</span>";
			}elseif($id == '3'){
				$return = "<span class='label label-inverse'>Rahasia</span>";
			}elseif($id == '4'){
				$return = "<span class='label label-info'>Biasa</span>";
			}
			return $return;
		}
		function name($n, $j, $u)
		{
			return $n.' ('.$j.' '.$u.')';
		}
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				tr_surat_penerima.id,
				tmpegawai.n_pegawai,
				tmpersuratan.tgl_surat,
				tmpersuratan.prihal,
				tmpersuratan.id_sifatsurat,
				tr_surat_penerima.status,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
			->from('tr_surat_penerima')
			->join('tmpersuratan','tr_surat_penerima.id_persuratan = tmpersuratan.id')
			->join('tmpegawai','tr_surat_penerima.id_pegawai_ke = tmpegawai.id')
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->where('tr_surat_penerima.id_pegawai_dari', $pegawai_id)
			->edit_column('tmpegawai.n_pegawai',
				'$1',
				'name(tmpegawai.n_pegawai, tmjabatan.n_jabatan, tmunitkerja.n_unitkerja)')
			->edit_column('tmpersuratan.tgl_surat',
				'$1',
				'indonesian_date(tmpersuratan.tgl_surat)')
			->edit_column('tmpersuratan.id_sifatsurat',
				'$1',
				'cek_jenis(tmpersuratan.id_sifatsurat)')
			->edit_column('tr_surat_penerima.status',
				'<center>
					$1
				</center>',
				'cek_read(tr_surat_penerima.status)')
			->add_column('aksi',
				'<center>
					
					<a href="'.base_url().$this->page.'read/$1"><i class="iconfa-external-link"></i> Lihat</a>
				</center>',
				'tr_surat_penerima.id')
			->unset_column('tmjabatan.n_jabatan')
			->unset_column('tmunitkerja.n_unitkerja')
			->generate();
	}
}