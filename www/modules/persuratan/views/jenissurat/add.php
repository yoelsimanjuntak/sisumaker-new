<div class="widgetbox box-inverse view_load">
    <?php echo $this->load->view($this->view . 'validate'); ?>
    <h4 class="widgettitle"></h4>
    <div class="widgetcontent wc1">
        <form id="form1" class="stdform" method="post" action="<?php echo site_url() . $this->page . 'save'; ?>" novalidate="novalidate" enctype="multipart/form-data">
			<div class="par control-group">
                <label class="control-label" for="id_surat">Kode</label>
                <div class="controls"><input type="text" name="id_surat" id="id_surat" class="input-large"></div>
            </div>
			<div class="par control-group">
                <label class="control-label" for="n_jnissurat">Jenis Surat</label>
                <div class="controls"><input type="text" name="n_jnissurat" id="n_jnissurat" class="input-large"></div>
            </div>
			<div class="par control-group">
                <label class="control-label" for="userfile">File</label>
                <div class="controls">
					<input type="file" name="userfile" id="userfile" class="input-large">
					&nbsp; <sup class="text-info label label-info" style="font-weight:bold"> &nbsp; .doc&nbsp;</sup>
				</div>
            </div>
            <p class="stdformbutton">
                <button class="btn btn-primary" id="action">Simpan</button>
                <a href="<?php echo base_url().$this->page;?>" id="close_add" class="btn btn-default">Batal</a>
            </p>
        </form>
    </div>
</div>