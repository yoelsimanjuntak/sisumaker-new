<style>
.highcharts-background
{
	fill: rgba(255,255,255,.30);
	color: #ffffff;
}
.table th, .table td{
	border-color: rgba(255,255,255,.20);
}
.table .con0{
	background: rgba(255,255,255,.40)
}
.table .con1{
	background: rgba(255,255,255,.30)
}
.t-count{
	color: #bb2f0e;
}
</style>
<?php
$total =  $count_masuk + $count_keluar;
if($count_masuk == 0){
	$masuk = 0;
}else{
	$masuk = round($count_masuk / $total * 100);
}
if($count_keluar== 0){
	$keluar = 0;
}else{
	$keluar = round($count_keluar / $total * 100);
}
?>

<script type="text/javascript">
jQuery(document).ready(function(){
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
		return {
			radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
			stops: [
				[0, color],
				[1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
			]
		};
	});
    jQuery('#chartplace').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
		credits: {
			enabled: false
		},
        title: {
            text: 'Perhitungan Jumlah Surat Masuk dan Keluar, 2015'
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Jumlah',
            data: [
                ['Surat Keluar',  <?php echo $keluar;?>],
                {
                    name: 'Surat Masuk',
                    y: <?php echo $masuk;?>,
                    sliced: true,
                    selected: true
                }
            ]
        }]
    });
});
</script>
<div class="row-fluid">
	<div class="span7">
		<table class="table">
			<colgroup>
				<col class="con0" style="align: center; width: 4%" />
				<col class="con1" />
				<col class="con0" />
			</colgroup>
			<thead>
				<tr>
					<td colspan="3" style="text-align: center;">PERHITUNGAN SURAT MASUK DAN KELUAR</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="width10" style="text-align: center;"><center><div class="pageicon"><span class="iconfa-edit"></span></div></center></td>
					<td class="width30">
						<span class="t-count">Surat Masuk:</span><br/>
						<h1><?php echo $count_masuk;?></h1>
					</td>
					<td class="width30">
						<span class="t-count">Surat Keluar:</span><br/>
						<h1><?php echo $count_keluar;?></h1>
					</td>
				</tr>
			</tbody>
		</table>
		
		<table class="table">
			<colgroup>
				<col class="con0" style="align: center; width: 4%" />
				<col class="con1" />
				<col class="con0" />
			</colgroup>
			<tbody>
				<tr>
					<td class="width10">IP</td>
					<td class="width60">
						<?php
						if (getenv("HTTP_CLIENT_IP")){
							$ip = getenv("HTTP_CLIENT_IP");
						}
						else if (getenv("HTTP_X_FORWARDED_FOR")){
							$ip = getenv("HTTP_X_FORWARDED_FOR");
						}
						else if (getenv("REMOTE_ADDR")){
							$ip = getenv("REMOTE_ADDR");
						}
						else{
							$ip = "UNKNOWN";
						}
						echo $ip;?>
					</td>
				</tr>
				<tr>
					<td>Browser</td>
					<td><?php echo $this->input->user_agent();?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="span5">
				<div style="width:100%">
					<div id="chartplace" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
				</div>
	</div>
</div>