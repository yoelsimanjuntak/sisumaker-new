<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Agenda extends ICA_AdminCont {

    var $page = "agenda/agenda/";
    var $view = "agenda/agenda/";
    var $icon = "calendar";

    function __construct() {
        parent::__construct();
		$this->restrict('13');
		$this->tmpersuratan = new Tmpersuratan();
		$this->penomoran = new Penomoran();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->tmskpd = new Tmskpd();
		$this->surat = new Surat();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->tmuser_userauth = new Tmuser_userauth();
		$this->main_inbox = new Main_inbox();
		$this->tmpegawai = new Tmpegawai();
		$this->outbox = new Outbox();
        $this->load->helper('function_helper');
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5"),
                $this->lib_load_css_js->load_css(base_url(), "assets/js/fullcalendar/", "fullcalendar.min.css"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/lib/", "moment.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/", "fullcalendar.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/", "lang-all.js")
            ),
            'breadcrubs' => " Agenda",
            'title' => 'Kalender Agenda',
            'content' => 'calendar',
            'sort' => 1
        );
        $this->load->view('template_mail', $data);
    }
	
	function get_data()
	{
		$jsonevents = array();
		$pegawai_id = $this->session->userdata('pegawai_id');
		$smasuk = $this->query_smasuk($pegawai_id);
		foreach($smasuk->result() as $row)
		{
			if($row->d_awal_kegiatan != '' && $row->d_akhir_kegiatan != '' && $row->v_kegiatan != '')
			{	
				$start = explode(' ', $row->d_awal_kegiatan);
				$end = explode(' ', $row->d_akhir_kegiatan);
				$jsonevents[] = array(
					'id' => $row->id,
					'title' => $row->prihal,
					'start' => $start[0].'T'.$start[1],
					'end' => $end[0].'T'.$end[1],
					'url' => base_url().$this->page.'v_surat/'.$row->id
				);
			}
		}
		$skeluar = $this->query_skeluar($pegawai_id);
		foreach($skeluar->result() as $row)
		{
			if($row->d_awal_kegiatan != '' && $row->d_akhir_kegiatan != '' && $row->v_kegiatan != '')
			{	
				$start = explode(' ', $row->d_awal_kegiatan);
				$end = explode(' ', $row->d_akhir_kegiatan);
				$jsonevents[] = array(
					'id' => $row->id,
					'title' => $row->prihal,
					'start' => $start[0].'T'.$start[1],
					'end' => $end[0].'T'.$end[1],
					'url' => base_url().$this->page.'v_surat/'.$row->id
				);
			}
		}
		echo json_encode($jsonevents);
    }
	
	function query_smasuk($pegawai_id)
	{
		return $this->db
				->select('
					tr_surat_penerima.id,
					tr_surat_penerima.d_awal_kegiatan,
					tr_surat_penerima.d_akhir_kegiatan,
					tr_surat_penerima.v_kegiatan,
					tmpersuratan.prihal
				')
				->where('tr_surat_penerima.id_pegawai_ke', $pegawai_id)
				->where('tmpersuratan.id_jnssurat', 1)
				->from('tr_surat_penerima')
				->join('tmpersuratan', 'tr_surat_penerima.id_persuratan = tmpersuratan.id')
				->group_by('tr_surat_penerima.id_persuratan')
				->get();
	}
	
	function query_skeluar($pegawai_id)
	{
		return $this->db
				->select('
					tr_surat_penerima.id,
					tr_surat_penerima.d_awal_kegiatan,
					tr_surat_penerima.d_akhir_kegiatan,
					tr_surat_penerima.v_kegiatan,
					tmpersuratan.prihal
				')
				->where('tr_surat_penerima.id_pegawai_ke', $pegawai_id)
				->where('tmpersuratan.id_jnssurat', 2)
				->where('tmpersuratan.status_setujui', 1)
				->from('tr_surat_penerima')
				->join('tmpersuratan', 'tr_surat_penerima.id_persuratan = tmpersuratan.id')
				->group_by('tr_surat_penerima.id_persuratan')
				->get();
	}
	
	function v_surat($id)
	{
		$id_persuratan = $this->tr_surat_penerima->where("id = '".$id."'")->get()->id_persuratan;
		$data = array(
			'id' => $id,
			'id_persuratan' => $id_persuratan,
			'result' => $this->main_inbox->read($id),
			'assets' => array(),
			'title' => 'Read Inbox',
			'content' => 'v_surat'
		);
		$this->load->view('view', $data);
	}
	
	function absen_submit($id)
	{
		$data = array(
			'c_absen' => '1',
			'd_absen' => date('Y-m-d H:i:s')
		);
		$this->tr_surat_penerima->update($id, $data);
		redirect(base_url().$this->page.'v_surat/'.$id);
	}

}