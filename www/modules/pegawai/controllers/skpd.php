<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Skpd extends ICA_AdminCont {

    var $page = "pegawai/skpd/";
    var $view = "pegawai/skpd/";
    var $icon = "cogs";

    function __construct() {
        parent::__construct();
		$this->restrict('9');
        $this->tmskpd = new TmSkpd();
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> Pegawai <span class='separator'></span> SKPD",
            'title' => 'Master SKPD',
            'content' => 'table',
            'sort' => 1
        );
        $this->load->view('template', $data);
    }

    function add() {
        $this->load->view($this->view . 'add');
    }

    function edit($id) {
        $data = array(
            'result' => $this->tmskpd->select('id', $id)
        );
        $this->load->view($this->view . 'edit', $data);
    }

    function save($id = false) {
        if ($id == false) {
            if ($this->tmskpd->select('n_skpd', $this->input->post('n_skpd'))->num_rows() == 0) {
                $data = array(
                    'n_skpd' => $this->input->post('n_skpd'),
                    'initial' => $this->input->post('initial')
                );
                $this->tmskpd->insert($data);
                $output = array(
                    'status' => 1
                );
                echo json_encode($output);
            } else {
                $output = array(
                    'status' => 11
                );
                echo json_encode($output);
            }
        } else {
            if ($this->input->post('n_skpd_temp') == $this->input->post('n_skpd')) {
                $data = array(
                    'n_skpd' => $this->input->post('n_skpd'),
                    'initial' => $this->input->post('initial')
                );
                $this->tmskpd->update($id, $data);
                $output = array(
                    'status' => 2
                );
                echo json_encode($output);
            } else {
                if ($this->tmskpd->select('n_skpd', $this->input->post('n_skpd'))->num_rows() == 0) {
                    $data = array(
                        'n_skpd' => $this->input->post('n_skpd'),
                        'initial' => $this->input->post('initial')
                    );
                    $this->tmskpd->update($id, $data);
                    $output = array(
                        'status' => 2
                    );
                    echo json_encode($output);
                } else {
                    $output = array(
                        'status' => 22
                    );
                    echo json_encode($output);
                }
            }
        }
    }

    function delete($id) {
        $this->tmskpd->delete($id);
    }

    function data() {
        $this->load->library('datatables');
        echo $this->datatables
                ->select('
				tmskpd.id,
				tmskpd.n_skpd,
				tmskpd.initial
			')
                ->from('tmskpd')
                ->add_column('aksi', '<center>
					<a id="edit" href="#" to="' . base_url() . $this->page . 'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
					<a id="delete" href="#" to="' . base_url() . $this->page . 'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>', 'tmskpd.id')
                ->generate();
    }

}
