<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Halaman Tidak Tersedia</title>
		<link rel="stylesheet" href="assets/css/style.default.css" type="text/css" />
		<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery-migrate-1.1.1.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery-ui-1.9.2.min.js"></script>
		<script type="text/javascript" src="assets/js/modernizr.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery.cookie.js"></script>
		<script type="text/javascript" src="assets/js/custom.js"></script>
	</head>
	<body class="errorpage">
		<div class="mainwrapper">
			<div class="errortitle">
				<h4 class="animate0 fadeInUp">Ups.. Halaman yang anda cari tidak tersedia...</h4>
				<span class="animate1 bounceIn">4</span>
				<span class="animate2 bounceIn">0</span>
				<span class="animate3 bounceIn">4</span>
				<div class="errorbtns animate4 fadeInUp">
					<a onclick="history.back()" class="btn btn-primary btn-large">Go to Previous Page</a>
				</div>
			</div>
		</div><!--mainwrapper-->
	</body>
</html>