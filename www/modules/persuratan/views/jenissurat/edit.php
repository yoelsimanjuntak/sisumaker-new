<?php if($result->num_rows() != 0){foreach($result->result() as $row){ ?>
<div class="widgetbox box-inverse view_load">
	<?php echo $this->load->view($this->view.'validate');?>
	<h4 class="widgettitle"></h4>
	<div class="widgetcontent wc1">
		<form id="form1" class="stdform" method="post" action="<?php echo site_url().$this->page.'save/'.$row->id;?>" novalidate="novalidate" enctype="multipart/form-data">
			<input type="hidden" name="n_jnissurat_temp" value="<?php echo $row->n_jnissurat;?>"/>
			<div class="par control-group">
                <label class="control-label" for="id_surat">Kode</label>
                <div class="controls"><input type="text" name="id_surat" id="id_surat" class="input-large" value="<?php echo $row->id_surat;?>"/></div>
            </div>
			<div class="par control-group">
				<label class="control-label" for="n_jnissurat">Jenis Surat</label>
				<div class="controls"><input type="text" name="n_jnissurat" id="n_jenisdokumen" class="input-large" value="<?php echo $row->n_jnissurat;?>"/></div>
			</div>
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action">Simpan Perubahan</button>
				<a href="#" id="close_add" class="btn btn-default">Batal</a>
			</p>
		</form>
	</div>
</div>
<div class="widgetbox box-inverse">
    <h4 class="widgettitle">File Surat</h4>
    <div class="widgetcontent wc1">
		<form class="stdform" method="post" action="<?php echo site_url().$this->page . 'save_file/'.$row->id; ?>" enctype="multipart/form-data">
			<div class="row-fluid">
				<div class="span7">
					<div class="control-group">
						<label class="control-label" for="file">File Surat</label>
						<div class="controls">
							<input type="file" name="userfile" id="userfile" class="input-large">
							&nbsp; <sup class="text-info label label-info" style="font-weight:bold"> &nbsp; .doc&nbsp;</sup>
						</div>
					</div>
					<p class="stdformbutton">
						<button class="btn btn-primary" id="action">Simpan File Baru</button>
					</p>
				</div>
				<div class="span5">
					<ul class="shortcuts" style="margin-top:0px;float:right;">
						<li class="archive">
							<a href="<?php echo base_url().$row->file_surat;?>">
								<span class="shortcuts-icon iconsi-archive"></span>
								<span class="shortcuts-label">File</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</form>
    </div>
</div>
<?php }}else{?><p>Data Tidak Ditemukan, <a href="#" id="refresh">Segarkan Tabel</a></p><?php }?>