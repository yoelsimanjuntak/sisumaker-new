<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Userauth extends DataMapper 
{

    var $table = 'userauth';
    var $key = 'id';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here
	function select($key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($this->table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($this->table);
		}
	}
	
	function select_skpd()
	{
		return $this->db
			->where_in('id', '2')
			->or_where_in('id', '3')
			->or_where_in('id', '4')
			->or_where_in('id', '5')
			->or_where_in('id', '6')
			->or_where_in('id', '11')
			->or_where_in('id', '12')
			->or_where_in('id', '13')
			->or_where_in('id', '14')
			->or_where_in('id', '15')
			->from($this->table)
			->get();
	}
	
    function insert($form) 
	{
        return $this->db
			->insert($this->table, $form);
    }

    function update($id, $form) 
	{
        return $this->db
			->where($this->key, $id)
			->update($this->table, $form);
    }

    function delete($id) 
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }

}

?>
