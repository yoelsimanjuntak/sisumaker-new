<script type="text/javascript">
	jQuery(document).ready(function(){
		ret = '';
		parent.jQuery('#pegawai option:selected').each(function(i, selected){ 
			ret = ret + '<li>' + jQuery(selected).text() + '</li>'; 
		});
		ret_cc = '';
		parent.jQuery('#pegawai_cc option:selected').each(function(i, selected){ 
			ret_cc = ret_cc + '<li>' + jQuery(selected).text() + '</li>'; 
		});
		if(ret == '')
		{
			alert('Anda harus menyertakan penerima disposisi.');
			parent.jQuery.fancybox.close();
		}
		koreksi = 'Tidak';
		if(parent.jQuery('input[type="radio"]:checked').val() != 0)
		{
			kodreksi = 'Ya';
		}
		else
		{
			jQuery('#f_koreksi_t').remove();
			jQuery('#koreksi_t').remove();
		}
		jQuery('#list_penerima').html(ret);
		jQuery('#list_penerima_cc').html(ret_cc);
		jQuery('#prihal').html(parent.jQuery('#prihal').val());
		jQuery('#tgl_surat').html(parent.jQuery('#tgl_surat').val());
		jQuery('#d_awal_kegiatan').html(parent.jQuery('#d_awal_kegiatan').val());
		jQuery('#d_akhir_kegiatan').html(parent.jQuery('#d_akhir_kegiatan').val());
		jQuery('#v_kegiatan').html(parent.jQuery('#v_kegiatan').val());
		jQuery('#id_sifatsurat').html(parent.jQuery('#id_sifatsurat option:selected').text());
		jQuery('#isi_surat').html(parent.jQuery('#mce_0').val());
		jQuery('#surat').html(parent.jQuery('#userfile').val().split('\\').pop());
		jQuery('#surat_lampiran').html(parent.jQuery('#file_lampiran').val().split('\\').pop());
		jQuery('#koreksi').html(koreksi);
		jQuery('#send').live('click', function(){
			parent.jQuery.fancybox.close();
			parent.jQuery('#form1').submit();
		})
	});
</script>
<table class="table table-bordered table-invoice">
	<colgroup>
		<col class="con1" />
		<col class="con0" />
	</colgroup>
	<tbody>
		<tr>
			<td width="140px">Paraf</td>
			<td>Dikirim Ke</td>
		</tr>
		<tr>
			<td rowspan="3">
				<?php if($paraf != ''){ ?>
				<img src="<?php echo base_url().$paraf;?>" alt=""/>
				<?php } ?>
			</td>
			<td>
				<ol id="list_penerima" class="list-ordered"></ol>
			</td>
		</tr>
		<tr>
			<td>
				CC:
			</td>
		</tr>
		<tr>
			<td>
				<ol id="list_penerima_cc" class="list-ordered"></ol>
			</td>
		</tr>
	</tbody>
</table>
<table class="table table-bordered table-invoice">
	<colgroup>
		<col class="con1" />
		<col class="con0" />
		<col class="con1" />
		<col class="con0" />
	</colgroup>
	<tbody>
		<tr>
			<td width="140px">Tanggal Surat</td>
			<td id="tgl_surat"></td>
			<td colspan="2">Kegiatan</td>
		</tr>
		<tr>
			<td>Perihal</td>
			<td id="prihal"></td>
			<td width="140px">Tanggal Mulai</td>
			<td width="140px" id="d_awal_kegiatan"></td>
		</tr>
		<tr>
			<td>Sifat Surat</td>
			<td id="id_sifatsurat"></td>
			<td>Tanggal Akhir</td>
			<td id="d_akhir_kegiatan"></td>
		</tr>
		<tr>
			<td>Memo Surat</td>
			<td id="isi_surat"></td>
			<td>Lokasi</td>
			<td id="v_kegiatan"></td>
		</tr>
		<tr>
			<td>File Surat</td>
			<td id="surat" colspan="3"></td>
		</tr>
		<tr>
			<td>File Lampiran</td>
			<td id="surat_lampiran" colspan="3"></td>
		</tr>
	</tbody>
</table>

<a href="#" class="btn btn-primary pull-right" id="send"><i class="iconfa-plane"></i> Kirim Surat</a>
<div style="clear:both"></div>