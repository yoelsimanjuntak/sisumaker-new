<div class="widgetbox box-inverse view_load">
    <?php echo $this->load->view($this->view . 'validate'); ?>
    <h4 class="widgettitle"></h4>
    <div class="widgetcontent wc1">
        <?php if ($result->num_rows() != 0) {
            foreach ($result->result() as $row) { ?>
                <form id="form1" class="stdform" method="post" action="<?php echo site_url() . $this->page . 'save/' . $row->id; ?>" novalidate="novalidate">
                    <input type="hidden" name="nip_temp" value="<?php echo $row->nip; ?>"/>
                    <div class="par control-group">
                        <label class="control-label" for="nip">NIP</label>
                        <div class="controls"><input type="text" name="nip" id="nip" class="input-large" value="<?php echo $row->nip; ?>"></div>
                    </div>
                    <div class="par control-group">
                        <label class="control-label" for="n_pegawai">Nama Pegawai</label>
                        <div class="controls"><input type="text" name="n_pegawai" id="n_pegawai"  class="input-large" value="<?php echo $row->n_pegawai; ?>"></div>
                    </div>
                    <div class="par control-group">
                        <label class="control-label" for="telp">No Telp</label>
                        <div class="controls"><input type="text" name="telp" id="telp" class="input-large" value="<?php echo $row->telp; ?>"></div>
                    </div>
					<div class="par control-group">
						<label class="control-label" for="tmskpd_id">SKPD</label>
						<div class="controls">
							<?php 
							$id_skpd = $this->session->userdata('skpd_id');
							if($id_skpd != 0){
								$n_skpd = $this->tmskpd->where("id = '".$id_skpd."'")->get()->n_skpd; ?>
								<input type="text" name="tmskpd_id" value="<?php echo $id_skpd;?>" style="display:none">
								<label class="input-label"><?php echo $n_skpd;?></label>
							<?php
							}else{ ?>
								<select name="tmskpd_id">
									<option value="">Pilih</option>
									<?php foreach($result_tmskpd->result() as $row_tmskpd){ ?>
									<option value="<?php echo $row_tmskpd->id;?>"<?php if ($row->tmskpd_id == $row_tmskpd->id) { ?> selected="selected"<?php } ?>><?php echo $row_tmskpd->n_skpd;?></option>
									<?php }?>	
								</select>
							<?php } ?>
						</div>
					</div>
                    <div class="par control-group">
                        <label class="control-label" for="tmjabatan_id">Jabatan</label>
                        <div class="controls">
                            <select name="tmjabatan_id">
                                <option value="">Pilih</option>
                                <?php foreach ($result_tmjabatan->result() as $row_tmjabatan) { ?>
                                    <option value="<?php echo $row_tmjabatan->id; ?>"<?php if ($row->tmjabatan_id == $row_tmjabatan->id) { ?> selected="selected"<?php } ?>><?php echo $row_tmjabatan->n_jabatan; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="par control-group">
                        <label class="control-label" for="tmunitkerja_id">Unit Kerja</label>
                        <div class="controls">
                            <select name="tmunitkerja_id">
                                <option value="">Pilih</option>
                                <?php foreach ($result_tmunitkerja->result() as $row_tmunitkerja) { ?>
                                    <option value="<?php echo $row_tmunitkerja->id; ?>"<?php if ($row->tmunitkerja_id == $row_tmunitkerja->id) { ?> selected="selected"<?php } ?>><?php echo $row_tmunitkerja->n_unitkerja; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div><div class="par control-group">
                        <label class="control-label" for="tmgolongan_id">Golongan</label>
                        <div class="controls">
                            <select name="tmgolongan_id">
                                <option value="">Pilih</option>
                                <?php foreach ($result_tmgolongan->result() as $row_tmgolongan) { ?>
                                    <option value="<?php echo $row_tmgolongan->id; ?>"<?php if ($row->tmgolongan_id == $row_tmgolongan->id) { ?> selected="selected"<?php } ?>><?php echo $row_tmgolongan->n_golongan; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="par control-group">
                        <label class="control-label" for="c_status">Status</label>
                        <div class="controls">
                            <select name="c_status">
                                <option value="">Pilih</option>
                                <option value="1"<?php if ($row->c_status == 1) { ?> selected="selected"<?php } ?>>Aktif</option>
                                <option value="2"<?php if ($row->c_status == 2) { ?> selected="selected"<?php } ?>>Non-Aktif</option>
                            </select>
                        </div>
                    </div>
                    <p class="stdformbutton">
                        <button class="btn btn-primary" id="action">Simpan Perubahan</button>
                        <a href="#" id="close_add" class="btn btn-default">Batal</a>
                    </p>
                </form>
            <?php }
        } else { ?><p>Data Tidak Ditemukan, <a href="#" id="refresh">Segarkan Tabel</a></p><?php } ?>
    </div>
</div>