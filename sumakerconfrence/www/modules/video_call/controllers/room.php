<?php

include APPPATH . 'modules/video_call/libraries/cache-master/src/Kminek_Cache.php';
include APPPATH . 'modules/video_call/libraries/cache-master/src/helpers.php';
include APPPATH . 'modules/video_call/libraries/call_helper.php';

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Room extends ICA_AdminCont
{
	var $page = "video_call/room/";
	var $view = "video_call/message/";
    var $icon = "chat";

    function __construct() 
	{
        parent::__construct();
		$this->tmuser 		= new Tmuser();
		$this->tmpegawai 	= new Tmpegawai();
		$this->tr_chat 		= new Tr_chat();
		$this->room_model 	= new Room_model();

		$this->load->helper('function_helper');
	}

	function updateConnectedUser( $roomId ) {
		
		$tmuser_id = $this->session->userdata('auth_id');

		$dataRoom['status']	= 1;
		$this->room_model->updateConnectedUser( $tmuser_id, $roomId, $dataRoom );

		echo json_encode(array('status' => 'ok'));
	}

	function index()
	{

		// $this->room_model->emptyTable('room_list');
		// $this->room_model->emptyTable('user_list_video');
		$tmuser_id = $this->session->userdata('auth_id');

		$checkActiveRoom = $this->room_model->getRoom( $tmuser_id );
		$scheduledCall = $this->room_model->getScheduledRoom( $tmuser_id );
		// print_r( $scheduledCall );
		// exit();

		if( count( $checkActiveRoom ) > 0 )
		{
			
			$data = array(
				'assets' => array(
					$this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.scrollTo.min.js"),
					$this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.localscroll-min.js")
				),
				'breadcrubs' => " ",
				'chatlist' => $this->query_list(),
				'title' => 'Create Room',
				'id' => null,
				'active_call' => $checkActiveRoom,
				'scheduled_call' => $scheduledCall,
				'content' => 'create-room'
			);
			$this->load->view('template_mail', $data);

		} else {			
			$data = array(
				'assets' => array(
					$this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.scrollTo.min.js"),
					$this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.localscroll-min.js")
				),
				'breadcrubs' => " ",
				'chatlist' => $this->query_list(),
				'title' => 'Create Room',
				'id' => null,
				'active_call' => null,
				'scheduled_call' => $scheduledCall,
				'content' => 'create-room'
			);
			$this->load->view('template_mail', $data);
		}

	}

	function query_list()
	{
		$tmuser_id = $this->session->userdata('auth_id');
		return $this->db
			->select('
				tmuser.id,
				tmpegawai.n_pegawai,
				tmuser.photo,
				tmuser.online
			')
			->from('truser_kirim')
			->join('tmuser', 'truser_kirim.id_kirim = tmuser.id')
			->join('tmpegawai', 'tmuser.id = tmpegawai.tmuser_id')
			->where('truser_kirim.id_user', $tmuser_id)
			->get()
			->result();
	}

	function save_room() {

		$tmuser_id = $this->session->userdata('auth_id');
		
		$usersJson 	= $this->input->post('users');
		$start 		= $this->input->post('start');
		$agenda 	= $this->input->post('agenda');

		$users = json_decode( $usersJson );

		$dataRoom['room_name'] 	= date('dmyhis') . mt_rand(10,999);
		$dataRoom['start'] 		= $start;
		$dataRoom['agenda'] 	= $agenda;
		$dataRoom['create_by']	= $tmuser_id;

		$this->room_model->createRoom( $tmuser_id, $dataRoom, $users );

		$senddata = array(
			'apikey' => SMSGATEWAY_APIKEY,  
			'callbackurl' => SMSGATEWAY_CALLBACKURL, 
			'datapacket'=> array()
		);

		$tgl = date('d F Y', strtotime( $dataRoom['start'] ));
		$jam = date('H:i', strtotime( $dataRoom['start'] ));
		foreach( $users as $user ) {

			$pegawai = $this->tmpegawai->where("tmuser_id = '". $user ."'")->get();
			$no_telp = $pegawai->telp;

			$number 			= $no_telp;
			$message			= $this->session->userdata('realname') . " Mengundang anda untuk melakukan panggilan video di aplikasi SISUMAKER tanggal $tgl pada pukul $jam";
			$sendingdatetime 	= ""; 
			array_push($senddata['datapacket'],array(
				'number' => trim($number),
				'message' => urlencode(stripslashes(utf8_encode($message))),
				'sendingdatetime' => $sendingdatetime));
			
			$sms = new api_sms_class_reguler_json();
			$sms->setIp(SMSGATEWAY_IPSERVER);
			$sms->setData($senddata);
			$responjson = $sms->send();

		}

	}

	function addUserToVideoCall() {

		$tmuser_id 	= $this->session->userdata('auth_id');
		$userToAdd 	= $this->input->post('id');
		$roomid 	= $this->input->post('roomid');
		
		$pegawai = $this->tmpegawai->where("tmuser_id = '". $userToAdd ."'")->get();
		$no_telp = $pegawai->telp;
				
		$add = $this->room_model->addUserToRoom( $userToAdd, $roomid );
		
		if( $add ) {

			$json = [
				'id'    	=> $userToAdd,
				'from_id' 	=> $this->session->userdata('auth_id'),
				'name'  	=> $this->session->userdata('realname'),
				'photo' 	=> base_url() . 'uploads/photo/' . $this->session->userdata('photo'),
				'room' 		=> $dataRoom['room_name'],
				'roomid'	=> $roomid,
				'creatorid' => 	$this->session->userdata('creatorid')
			];
			
			cache_set('grub-call-' . $json['id'], json_encode($json), 60);

			$senddata = array(
				'apikey' => SMSGATEWAY_APIKEY,  
				'callbackurl' => SMSGATEWAY_CALLBACKURL, 
				'datapacket'=> array()
			);

			$number 			= $no_telp;
			$message			= $this->session->userdata('realname') . " Mengundang anda untuk melakukan panggilan video. Buka aplikasi SISUMAKER dan ke menu video call untuk menjawab panggilan.";
			$sendingdatetime 	= ""; 
			array_push($senddata['datapacket'],array(
				'number' => trim($number),
				'message' => urlencode(stripslashes(utf8_encode($message))),
				'sendingdatetime' => $sendingdatetime));
			
			$sms = new api_sms_class_reguler_json();
			$sms->setIp(SMSGATEWAY_IPSERVER);
			$sms->setData($senddata);
			$responjson = $sms->send();

			$response = array(
				'status' 	=> 'ok',
			);
		} else {
			$response = array(
				'status' 	=> 'error',
				'msg'		=> 'Pengguna sudah ditambahkan kedalam grub panggilan'
			);
		}

		echo json_encode($response);
		
	}

	function panggilan_video($roomName) {

		$tmuser_id = $this->session->userdata('auth_id');

		$dataRoom = $this->room_model->getRoomByName( $roomName );
		$data = array(
			'assets' 		=> array(),
			'dataRoom'		=> $dataRoom,
			'roomName'		=> $roomName,
			'connectedUser'	=> $this->room_model->getUsersConnected( $dataRoom->id ),
			'breadcrubs' 	=> "",
			'chatlist' 		=> $this->query_list(),
			'userLogged'	=> $this->session->userdata,
			'title' 		=> 'Video Call',
			'content' 		=> 'video_call_on_progress',
		);

		// print_r($data);
		// exit();
		$this->load->view('template_video_call', $data);


	}

	function leave_video_call() {

		$tmuser_id 	= $this->session->userdata('auth_id');
		$roomid 	= $this->input->post('roomid');
		$creatorid 	= $this->input->post('creatorid');
		
		if( $tmuser_id == $creatorid ) {
			$this->room_model->deactiveRoom( $tmuser_id, $roomid );
		} else {
			$this->room_model->leaveGroupCall( $tmuser_id, $roomid );
		}

		$response = array(
			'status' 	=> 'ok'
		);

		echo json_encode($response);
	}

	function getInviteLists() {

		$result['user'] = $this->query_list();
		echo json_encode( $result );

	}
	
	

}