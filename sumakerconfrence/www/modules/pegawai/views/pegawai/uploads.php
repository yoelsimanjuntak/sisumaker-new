<div class="widgetbox box-inverse view_load">
    <?php echo $this->load->view($this->view . 'validate'); ?>
    <h4 class="widgettitle"></h4>
    <div class="widgetcontent wc1 stdform">
        <?php if ($result->num_rows() != 0) {
            foreach ($result->result() as $row) {
			$paraf = $row->file_paraf;
			$ttd = $row->file_ttd;
			?>
                <div class="par control-group">
					<label class="control-label" for="nip">NIP</label>
					<div class="controls"><label class="input-label"><?php echo $row->nip; ?></label></div>
				</div>
				<div class="par control-group">
					<label class="control-label" for="n_pegawai">Nama Pegawai</label>
					<div class="controls"><label class="input-label"><?php echo $row->n_pegawai; ?></label></div>
				</div>
				<div class="par control-group">
					<label class="control-label" for="telp">No Telp</label>
					<div class="controls"><label class="input-label"><?php echo $row->telp; ?></label></div>
				</div>
            <?php }
        } else { ?><p>Data Tidak Ditemukan, <a href="#" id="refresh">Segarkan Tabel</a></p><?php } ?>
    </div>
</div>
<div class="row-fluid">
	<div class="span6">
		<div class="widgetbox box-inverse">
			<h4 class="widgettitle">Paraf</h4>
			<div class="widgetcontent no-padding">
				<form action="<?php echo base_url().$this->page.'save_paraf';?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?php echo $id;?>"/>
					<table class="table">
						<tbody>
							<tr>
								<td><label>Lokasi Paraf<br/>(*.JPG, *.JPEG, *.PNG)</label></td>
								<td><input type="file" name="userfile" id="sk1"></td>
								<td><button class="btn btn-primary pull-right" id="action"><i class="iconfa-upload"></i> Upload</button></td>
							</tr>
							<?php if($paraf != ''){ ?>
							<tr>
								<td colspan="3">
									<img src="<?php echo base_url().$paraf;?>" alt=""/>
								</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
	<div class="span6">
		<div class="widgetbox box-inverse">
			<h4 class="widgettitle">Tanda Tangan</h4>
			<div class="widgetcontent no-padding">
				<form action="<?php echo base_url().$this->page.'save_ttd';?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id" value="<?php echo $id;?>"/>
					<table class="table">
						<tbody>
							<tr>
								<td><label>Lokasi TTD<br/>(*.JPG, *.JPEG, *.PNG)</label></td>
								<td><input type="file" name="userfile" id="sk1"></td>
								<td><button class="btn btn-primary pull-right" id="action"><i class="iconfa-upload"></i> Upload</button></td>
							</tr>					
							<?php if($ttd != ''){ ?>
							<tr>
								<td colspan="3">
									<img src="<?php echo base_url().$ttd;?>" alt=""/>
								</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>