<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Auth.php';

class Suratmasuk extends Auth{

	public function __construct()
	{
    	parent::__construct();
    	header("Access-Control-Allow-Origin: *"); 
		header("Access-Control-Allow-Credential: true"); 
		header("Access-Control-Max-Age: 86400"); 
		header("Access-Control-Allow-Methods:GET,POST,OPTIONS"); 
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') { 
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) 
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); 
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) 
				header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}"); 
			exit(0); 
		}
    	$this->load->model('db_dml');
    	$this->load->model('db_select');
    	$this->load->model('mymodel');
    	$this->load->model('tr_surat_penerima');
    	if($this->cektoken() == FALSE){
			header('Content-type: application/json');
    		$output = array(
				'status' => FALSE,
				'message' => 'Wrong Token'
			);
			exit(json_encode($output));
    	}
  	}

	function index_get()
	{
		$decode = $this->viewtoken();
		$pegawai_id = $decode->pegawai_id;
		$d_dari =  $this->get('d_dari',TRUE);
		$d_sampai =  $this->get('d_sampai',TRUE);
		$output = array();
		
		if(!$d_dari || !$d_sampai)
		{
			$output = array(
				'status' => FALSE,
				'message' => "Parameter request d_dari & d_sampai tidak boleh kosong."
			);
			$this->response($output,Auth::HTTP_BAD_REQUEST);
		}
		else
		{
			$query = $this->mymodel->surat_masuk($pegawai_id, $d_dari, $d_sampai);
			if($query->num_rows() == 0)
			{
				$output = array(
					'status' => TRUE,
					'message' => "Tidak ada surat."
				);
				$this->response($output,Auth::HTTP_OK);
			}
			else
			{
				foreach($query->result() as $row){
					array_push($output, array(
						'asal_surat' => $row->dari,
						'tgl_dibuat' => $row->tgl_surat,
						'tgl_diterimaTU' => $row->tgl_terima_surat,
						'tgl_dikirim' => $row->d_entry,
						'pengirim' => $row->n_pegawai,
						'perihal' => $row->prihal,
						'n_sifatsurat' => $row->n_sifatsurat,
						'status_dibaca' => $row->status,
						'id_persuratan' => $row->id_persuratan,
						'id_compose' => $row->id_compose
						//'id_compose' => 2
					));
				}
				$this->response($output,Auth::HTTP_OK);
			}
		}
	}
	
	function read_get()
	{
		$decode = $this->viewtoken();
		$pegawai_id = $decode->pegawai_id;
		$id_persuratan = $this->get('id_persuratan',TRUE);
		$output = array();
		
		if(!$id_persuratan)
		{
			$output = array(
				'status' => FALSE,
				'message' => "id_persuratan tidak boleh kosong."
			);
			$this->response($output,Auth::HTTP_BAD_REQUEST);		
		}
		else
		{
			if($this->tr_surat_penerima->cek($pegawai_id, $id_persuratan)->num_rows() == 0)
			{
				$output = array(
					'status' => FALSE,
					'message' => "Anda tidak mendapatkan disposisi surat ini."
				);
				$this->response($output,Auth::HTTP_OK);
			}
			else
			{
				foreach($this->tr_surat_penerima->read_all($pegawai_id, $id_persuratan)->result() as $row)
				{
					$data = array(
						'status' => 1,
						'd_read' => date('Y-m-d H:i:s')
					);
					$this->tr_surat_penerima->update($row->id, $data);
				}
			
				$disposisi = array();
				$row = $this->mymodel->read($id_persuratan)->row();
				foreach($this->mymodel->history_all($id_persuratan, $pegawai_id)->result() as $row_history)
				{
					$d_entry = $row_history->d_entry;
					$ke_pegawai = $row_history->ke_pegawai.'; ';
					$ke_pegawai_cc = '';
					if($row_history->num > 1){	//Jika didisposisikan lebih dari satu pegawai
						$ke_pegawai = '';
						foreach($this->mymodel->read_to($id_persuratan, $d_entry, 0)->result() as $row_to){
							$ke_pegawai .= $row_to->ke_pegawai.'; ';
						}
						
						$cc = $this->mymodel->read_to($id_persuratan, $d_entry, 1);
						if($cc->num_rows() != 0){
							foreach($cc->result() as $row_cc){
								$ke_pegawai_cc .= $row_cc->ke_pegawai.'; ';
							}
						}
					}
				
					array_push($disposisi, array(
						'dari_pegawai' => $row_history->dari_pegawai,
						'dari_photo' => $row_history->photo,
						'ke_pegawai' => $ke_pegawai,
						'ke_pegawai_cc' => $ke_pegawai_cc,
						'tgl' => $row_history->d_entry,
						'catatan' => $row_history->catatan,
						'file_surat_koreksi' => $row_history->file_surat_koreksi,
						'lampiran_surat_koreksi' => $row_history->lampiran_file_koreksi
					));	
				}
			
				$ttd = FALSE;
				if($pegawai_id == $row->id_pegawaittd){
					$ttd = 1;
				}
				$output = array(
					'no_surat' => $row->no_surat,
					'dari' => $row->dari,
					'tgl_surat' => $row->tgl_surat,
					'd_awal_kegiatan' => $row->d_awal_kegiatan,
					'd_akhir_kegiatan' => $row->d_akhir_kegiatan,
					'v_kegiatan' => $row->v_kegiatan,
					'perihal' => $row->prihal,
					'n_sifatsurat' => $row->n_sifatsurat,
					//'n_jenissurat' => $row->n_jnissurat,
					'memo' => $row->isi_surat,
					'file' => $row->file,
					'file_lampiran' => $row->file_lampiran,
					//'c_lintas_skpd' => $row->c_lintas_skpd,
					'id_compose' => $row->id_compose,
					//'id_compose' => 2,
					'ttd' => $ttd,
					//'ttd' => 1,
					'status_ttd' => $row->status_setujui,
					'disposisi' => $disposisi
				);
				$this->response($output,Auth::HTTP_OK);
			}
		}
	}
	
	// function info_get()
// 	{
// 		$decode = $this->viewtoken();
// 		$pegawai_id = $decode->pegawai_id;
// 		$id_persuratan = $this->get('id_persuratan',TRUE);
// 		$output = array();
// 		
// 		if(!$id_persuratan)
// 		{
// 			$output = array(
// 				'status' => FALSE,
// 				'message' => "id_persuratan tidak boleh kosong."
// 			);
// 			$this->response($output,Auth::HTTP_BAD_REQUEST);		
// 		}
// 		else
// 		{
// 			$output = array();
// 			$result = $this->tr_surat_penerima->info($id_persuratan);
// 			foreach($result->result() as $row_info){
// 				if($row_info->paraf != ''){
// 					$paraf = base_url().$row_info->paraf;
// 				}else{
// 					$paraf = "*) paraf belum diunggah";
// 				}
// 				
// 				if($row_info->d_read != ''){
// 					$tgl_baca = $row_info->d_read;
// 				}else{
// 					$tgl_baca = "<i class='text-warning'>* belum dibaca</i>";
// 				}
// 				
// 				array_push($output, array(){
// 					'tgl_kirim' => $row_info->d_entry,
// 					'dari' => $row_info->n_pegawai_dari.' ('.$row_info->n_jabatan_dari.' '.$row_info->n_unitkerja_dari.')',
// 					'paraf' => $paraf,
// 					'ke' => $row_info->n_pegawai_ke.' ('.$row_info->n_jabatan_ke.' '.$row_info->n_unitkerja_ke.')',
// 					'tgl_baca' => $tgl_baca
// 				});
// 			}
// 			$this->response($output,Auth::HTTP_OK);
// 		}
// 	}
	
	function listdisposisi_get()
	{
		$decode = $this->viewtoken();
		$auth_id = $decode->auth_id;
		$id_persuratan = $this->get('id_persuratan',TRUE);
		$output = array();
		
		$query = $this->mymodel->disposisi($auth_id);
		if($query->num_rows() == 0)
		{
			$output = array(
				'status' => FALSE,
				'message' => "List disposisi kosong."
			);
			$this->response($output,Auth::HTTP_BAD_REQUEST);
		}
		else
		{
			foreach($query->result() as $row)
			{
				array_push($output, array(
					'id_pegawai' => $row->id,
					'n_jabatan' => $row->n_jabatan,
					'n_unitkerja' => $row->n_unitkerja,
					'n_pegawai' => $row->n_pegawai
				));
			}
			$this->response($output,Auth::HTTP_OK);
		}	
	}
	
	function disposisi_post()
	{
		$decode = $this->viewtoken();
		$pegawai_id = $decode->pegawai_id;
		$auth_id = $decode->auth_id;
		$skpd_id = $decode->skpd_id;
		$id_persuratan = $this->post('id_persuratan',TRUE);
		$pegawai_list = $this->post('id_pegawai', TRUE);
		$output = array();
		
		if(!$id_persuratan)
		{
			$output = array(
				'status' => FALSE,
				'message' => "Id persuratan tidak boleh kosong."
			);
			$this->response($output,Auth::HTTP_BAD_REQUEST);
		}
		else
		{
			if($this->tr_surat_penerima->cek($pegawai_id, $id_persuratan)->num_rows() == 0)
			{
				$output = array(
					'status' => FALSE,
					'message' => "Anda tidak mendapatkan disposisi surat ini."
				);
				$this->response($output,Auth::HTTP_BAD_REQUEST);
			}
			else
			{
				$valid = false;
				$pegawai_list_len = count($pegawai_list);
				for($i=0; $i<$pegawai_list_len; $i++){
					$valid = $pegawai_list[$i];
				}
				if($valid == false)
				{
					//echo var_dump($_POST);
					$output = array(
						'status' => FALSE,
						'message' => "Gagal mendisposisikan surat, penerima surat tidak boleh kosong."
					);
					$this->response($output,Auth::HTTP_OK);
				}
				else
				{
					$next = true;
					$exten = '';
					if($_FILES['userfile']['name'] != '')
					{
						$name = date('YmdHis').$auth_id;
						$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
						$endof = '.'.end($explode);
						$exten = 'uploads/surat_koreksi/'.$name.$endof;
						$config['file_name'] = $name;
						$config['allowed_types'] = 'pdf|doc|docx';
						$config['upload_path'] = '../uploads/surat_koreksi/';
						$config['max_size']	= '30000000';
						$config['overwrite'] = true;
						$this->load->library('upload', $config);
						if(!$this->upload->do_upload())
						{
							$output = array(
								'status' => FALSE,
								'message' => "Error saat mengunggah : '".$this->upload->display_errors()."'"
							);
							$this->response($output,Auth::HTTP_OK);
							$next = false;
						}
					}
			
					if($next == true)
					{
						// $senddata = array(
		// 					'apikey' => SMSGATEWAY_APIKEY,  
		// 					'callbackurl' => SMSGATEWAY_CALLBACKURL, 
		// 					'datapacket'=> array()
		// 				);
						$catatan = $this->post('catatan');
						$surat = $this->mymodel->select('tmpersuratan', 'id', $id_persuratan)->row();
						$pegawai = $this->mymodel->select('tmpegawai', 'id', $pegawai_id)->row();
						$jabatan = $this->mymodel->select('tmjabatan', 'id', $pegawai->tmjabatan_id)->row()->n_jabatan;
						$unitkerja = $this->mymodel->select('tmunitkerja', 'id', $pegawai->tmunitkerja_id)->row()->initial;
						$send = "Ada disposisi surat dari ".$jabatan.' '.$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
						$true = false;
						$d_entry = date('Y-m-d H:i:s');
						for($i=0; $i<$pegawai_list_len; $i++)
						{				
							if($surat->d_awal_kegiatan != '' && $surat->d_akhir_kegiatan != '')
							{
								$data = array(
									'id_persuratan' => $id_persuratan,
									'id_pegawai_dari' => $pegawai_id,
									'id_pegawai_ke' => $pegawai_list[$i],
									'catatan' => $catatan,
									'file_surat_koreksi' => $exten,
									'lampiran_file_koreksi' => '',
									'status' => 0,
									'd_entry' => $d_entry,
									'id_skpd_dari' => $skpd_id,
									'd_awal_kegiatan' => $surat->d_awal_kegiatan,
									'd_akhir_kegiatan' => $surat->d_akhir_kegiatan,
									'v_kegiatan' => $surat->v_kegiatan
								);
							}
							else
							{
								$data = array(
									'id_persuratan' => $id_persuratan,
									'id_pegawai_dari' => $pegawai_id,
									'id_pegawai_ke' => $pegawai_list[$i],
									'catatan' => $catatan,
									'file_surat_koreksi' => $exten,
									'status' => 0,
									'd_entry' => $d_entry,
									'id_skpd_dari' => $skpd_id
								);
							}
							$this->db_dml->insert('tr_surat_penerima', $data);

							$telp = $this->db_select->select_where('tmpegawai', array('id' => $pegawai_list[$i]))->telp;
							if($telp != '-')
							{
								array_push($senddata['datapacket'],array(
									'number' => trim($telp),
									'message' => urlencode(stripslashes(utf8_encode($send))),
									'sendingdatetime' => ""));
							
									//------- Telp hanya untuk SEMENTARA
									//------- 28 April 2017, sms untuk ajudan pa wakil walikota
								if($pegawai_list[$i] == '211')
								{
									$number = "081286571031";
									$sendingdatetime = ""; 
									array_push($senddata['datapacket'],array(
										'number' => trim($number),
										'message' => urlencode(stripslashes(utf8_encode($send))),
										'sendingdatetime' => $sendingdatetime));
								}
							}
							$true = true;
						}				
						// $sms = new api_sms_class_reguler_json();
		// 				$sms->setIp(SMSGATEWAY_IPSERVER);
		// 				$sms->setData($senddata);
		// 				$responjson = $sms->send();
		// 				$this->saldo_sms->save($responjson);
				
						$output = array(
							'status' => TRUE,
							'message' => "Surat berhasil terkirim."
						);
						$this->response($output,Auth::HTTP_OK);
					}
				}
			}
		}
	}
	
	function ttd_post()
	{
		$decode = $this->viewtoken();
		$pegawai_id = $decode->pegawai_id;
		$auth_id = $decode->auth_id;
		$skpd_id = $decode->skpd_id;
		$id_persuratan = $this->post('id_persuratan',TRUE);
		$output = array();
		
		if(!$id_persuratan)
		{
			$output = array(
				'status' => FALSE,
				'message' => "Id persuratan tidak boleh kosong."
			);
			$this->response($output,Auth::HTTP_BAD_REQUEST);
		}
		else
		{
			if($this->tr_surat_penerima->cek($pegawai_id, $id_persuratan)->num_rows() == 0)
			{
				$output = array(
					'status' => FALSE,
					'message' => "Surat tidak ada atau Anda tidak mendapatkan disposisi surat ini."
				);
				$this->response($output,Auth::HTTP_OK);
			}
			else
			{
				$row = $this->db_select->select_where('tmpersuratan', array('id' => $id_persuratan));
				if($pegawai_id != $row->id_pegawaittd)
				{
					$output = array(
						'status' => FALSE,
						'message' => "Gagal melakukan TTD, pengguna tidak memiliki akses untuk menandatangani surat ini."
					);
					$this->response($output,Auth::HTTP_OK);
				}
				else
				{
					if($row->status_setujui != 0)
					{
						$output = array(
							'status' => FALSE,
							'message' => "Gagal melakukan TTD, surat sudah pernah di TTD."
						);
						$this->response($output,Auth::HTTP_OK);
					}
					else
					{
						$id_skpd_in = $row->id_skpd_in;
						$result = $this->mymodel->id_tu($id_skpd_in);
						if($result->num_rows() == 0)
						{						
							$output = array(
								'status' => FALSE,
								'message' => "Gagal melakukan TTD, sistem tidak dapat menemukan user Bag. Umum. Silahkan hubungi Admin SISUMAKER"
							);
							$this->response($output,Auth::HTTP_OK);
						}
						else
						{
							if($_FILES['userfile']['name'] == '')
							{							
								$output = array(
									'status' => FALSE,
									'message' => "File hasil TTD tidak boleh kosong."
								);
								$this->response($output,Auth::HTTP_BAD_REQUEST);
							}
							else
							{
								$name = date('YmdHis').$auth_id;
								$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
								$endof = '.'.end($explode);
								$exten = 'uploads/surat_koreksi/'.$name.$endof;
								$config['file_name'] = $name;
								$config['allowed_types'] = 'pdf|doc|docx';
								$config['upload_path'] = '../uploads/surat_koreksi/';
								$config['max_size']	= '30000000';
								$config['overwrite'] = true;
								$this->load->library('upload', $config);
								if(!$this->upload->do_upload())
								{
									$output = array(
										'status' => FALSE,
										'message' => "Error saat mengunggah : '".$this->upload->display_errors()."'"
									);
									$this->response($output,Auth::HTTP_OK);
								}
								else
								{	
							
									// Send A Message
									foreach($result->result() as $row)
									{
										$telp = $row->telp;
										$id_tu = $row->id;
									}		
									$data = array(
										'id_persuratan' => $id_persuratan,
										'id_pegawai_dari' => $pegawai_id,
										'id_pegawai_ke' => $id_tu,
										'catatan' => "",
										'file_surat_koreksi' => $exten,
										'status' => 0,
										'd_entry' => date('Y-m-d H:i:s'),
										'id_skpd_dari' => $skpd_id
									);
									$this->db_dml->insert('tr_surat_penerima', $data);

									$nomor = $this->mymodel->nomor_save($id_skpd_in, 2);
									$data = array(
										'status_setujui' => 1,
										//'no_surat' => $nomor_surat,
										'no_agenda' => 'K-'.$nomor,
										'id_pegawai_setujui' => $pegawai_id
									);
									$this->db_dml->update('tmpersuratan', $data, array('id' => $id_persuratan));
								
									if($telp != '-')
									{
	// 									$senddata = array(
	// 										'apikey' => SMSGATEWAY_APIKEY,  
	// 										'callbackurl' => SMSGATEWAY_CALLBACKURL, 
	// 										'datapacket'=> array()
	// 									);
				
										$send = "Ada surat yang harus Anda cetak dan kirimkan. Silahkan masuk ke SISUMAKER untuk melihat isi surat";
										array_push($senddata['datapacket'],array(
											'number' => trim($telp),
											'message' => urlencode(stripslashes(utf8_encode($send))),
											'sendingdatetime' => ""));
				
	// 									$sms = new api_sms_class_reguler_json();
	// 									$sms->setIp(SMSGATEWAY_IPSERVER);
	// 									$sms->setData($senddata);
	// 									$responjson = $sms->send();
									}
								
									$output = array(
										'status' => TRUE,
										'message' => "Surat berhasil di TTD."
									);
									$this->response($output,Auth::HTTP_OK);
								}
							}
						}
					}
				}
			}
		}
	}
}
