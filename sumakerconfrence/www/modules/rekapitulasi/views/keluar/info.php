<?php if ($result->num_rows() != 0) { foreach ($result->result() as $row) { ?>	
<table class="table table-bordered table-invoice">
	<colgroup>
		<col class="con1" />
		<col class="con0" />
		<col class="con1" />
		<col class="con0" />
	</colgroup>
	<tbody>
		<tr>
			<td width="120px">Surat Dari</td>
			<td><?php echo $row->dari;?></td>
			<td width="120px">Tanggal Surat</td>
			<td width="320px"><?php echo indonesian_date($row->tgl_surat);?></td>
		</tr>
		<tr>
			<td>Sifat</td>
			<td colspan="3"><?php echo $row->n_sifatsurat;?></td>
		</tr>
		<tr>
			<td>Perihal</td>
			<td colspan="3"><?php echo $row->prihal;?></td>
		</tr>
	</tbody>
</table>
<table class="table table-bordered table-invoice">
	<colgroup>
		<col class="con1" />
		<col class="con0" />
		<col class="con1" />
		<col class="con0" />
		<col class="con1" />
	</colgroup>
	<thead>
		<tr>
			<th width="5p">No.</th>
			<th width="120px">Tanggal Kirim</th>
			<th>Dari</th>
			<th>Ke</th>
			<th width="120px">Tanggal Baca</th>
		</tr>
	</thead>
	<tbody>
		<?php $a = 1;foreach($result_info->result() as $row_info){ ?>
		<tr>
			<td><?php echo $a;?></td>
			<td><center><?php echo indo_date_time($row_info->d_entry);?></center></td>
			<td><?php echo $row_info->n_pegawai_dari;?></td>
			<td><?php echo $row_info->n_pegawai_ke;?></td>
			<td><center><?php 
				if($row_info->d_read != ''){
				echo indo_date_time($row_info->d_read);
				}else{
				echo "-";
				}
			?></center></td>
		</tr>
		<?php $a++; }?>
	</tbody>
</table>
<?php } } else { ?>
<p><b>Terjadi Kesalahan!</b> Data Tidak Ditemukan</p>
<?php } ?>