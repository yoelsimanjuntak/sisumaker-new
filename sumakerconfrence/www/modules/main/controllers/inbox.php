<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Inbox extends ICA_AdminCont
{
	var $page = "main/inbox/";
	var $view = "main/inbox/";
    var $icon = "inbox";

    function __construct() 
	{
        parent::__construct();
		$this->restrict('3');
		$this->tmpersuratan = new Tmpersuratan();
		$this->tmuser = new Tmuser();
		$this->penomoran = new Penomoran();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->tmskpd = new Tmskpd();
		$this->surat = new Surat();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->tmuser_userauth = new Tmuser_userauth();
		$this->main_inbox = new Main_inbox();
		$this->tmpegawai = new Tmpegawai();
		$this->outbox = new Outbox();
        $this->load->helper('text');
        $this->load->helper('function_helper');
        $this->load->library('pagination');
		$this->load->library('api_sms_class_reguler_json');
    }

	function cek_inbox(){$return = '';$count = $this->main_inbox->newpesan($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id'));if($count != 0){$return = "(".$count.")";}$data = array('count' => $return);echo json_encode($data);}
	
	function send_mail()
	{
		$to = "muhamadyusuf0012@gmail.com";
         $subject = "Ter Dari Server SISUMAKER";
         
         $message = "<b>Hey.</b>";
         $message .= "<h1>Ini adalah email notifikasi.</h1>";
         
         $header = "From: yusuf@tangerangselatankota.go.id \r\n";
         $header .= "Reply-To: ".$to."\r\n";
		 $header .='X-Mailer: PHP/' . phpversion();
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-Type: text/html; charset=iso-8859-1\r\n";
         $header .= "Organization: tangerangselatankota.go.id\r\n";
         
         $retval = mail ($to,$subject,$message,$header);
         
         if( $retval == true ) {
            echo "Message sent successfully...";
         }else {
            echo "Message could not be sent...";
         }
	}
	
	function search_suges()
	{
		$v_search = $this->input->post('ss');
		$where = " 	AND (tmpersuratan.prihal LIKE '%".$v_search."%'
							OR tmpersuratan.dari LIKE '%".$v_search."%'
							OR tmpersuratan.no_surat LIKE '%".$v_search."%'
							OR tmpersuratan.tgl_surat LIKE '%".$v_search."%'
							OR tmpersuratan.d_awal_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.d_akhir_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.v_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.isi_surat LIKE '%".$v_search."%'
							OR tmpegawai.n_pegawai LIKE '%".$v_search."%') GROUP BY tmpersuratan.id";
		$result = $this->query(10, 0, $where);
		foreach($result->result() as $row){
			$data[] = $row->prihal;
		}
		echo json_encode($data);
	}
	
	function index($search = NULL, $offset = NULL)
	{
		$where = '';
		$filter_text = '<i class="iconfa-reorder"></i>&nbsp; Semua';
		if($offset == ''){$offset = 0;}
		if($search != NULL || $search != 0)
		{
			$exp = explode('-', $search);
			if($exp[0] == 'b')
			{
				$where = " AND tr_surat_penerima.status = ".$exp[1];
				if($exp[1] == 0){ $filter_text = '<i class="iconfa-exclamation-sign"></i>&nbsp; Belum dibaca';}
				elseif($exp[1] == 1){ $filter_text = '<i class="iconfa-ok-sign"></i>&nbsp; Sudah dibaca';}
			}
			elseif($exp[0] == 'c')
			{
				$where = " AND tmpersuratan.id_sifatsurat = ".$exp[1];
				if($exp[1] == 1){ $filter_text = '<span class="label label-important">&nbsp;</span> &nbsp;Sangat Segera';}
				elseif($exp[1] == 2){ $filter_text = '<span class="label label-warning">&nbsp;</span> &nbsp;Segera';}
				elseif($exp[1] == 3){ $filter_text = '<span class="label label-inverse">&nbsp;</span> &nbsp;Rahasia';}
				elseif($exp[1] == 4){ $filter_text = '<span class="label label-info">&nbsp;</span> &nbsp;Biasa';}
			}
			elseif($exp[0] == 's')
			{
				$v_search = substr($search, 2);
				$where = " 	AND (tmpersuratan.prihal LIKE '%".$v_search."%'
							OR tmpersuratan.dari LIKE '%".$v_search."%'
							OR tmpersuratan.no_surat LIKE '%".$v_search."%'
							OR tmpersuratan.tgl_surat LIKE '%".$v_search."%'
							OR tmpersuratan.d_awal_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.d_akhir_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.v_kegiatan LIKE '%".$v_search."%'
							OR tmpersuratan.isi_surat LIKE '%".$v_search."%'
							OR tmpegawai.n_pegawai LIKE '%".$v_search."%')";
			}
		}else{$search = 0;}
		$jml = $this->query_count($where);
		$config['base_url'] = base_url().'main/inbox/index/'.$search.'/';
		$config['total_rows'] = $jml->num_rows();
		$config['per_page'] = '25';
		$config['num_links'] = 0;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['next_link'] = '<i class="iconfa-chevron-right" title="Selanjutnya"></i>';
		$config['prev_link'] = '<i class="iconfa-chevron-left" title="Sebelumnya"></i>';
		$config['cur_tag_open'] = '';
		$config['cur_tag_close'] = '';
		$this->pagination->initialize($config);

		$data = array(
			'breadcrubs' => " Inbox",
			'assets' => array(
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.dataTables.min.js"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
			),
			'title' => 'Inbox',
			'content' => 'table',
			'paging' => $this->pagination->create_links(),
			'result' => $this->query($config['per_page'], $offset, $where),
			'num_offset' => $offset,
			'per_page' => $config['per_page'],
			'total_rows' => $config['total_rows'],
			'search' => $search,
			'filter_text' => $filter_text,
			'newpesan' => $this->main_inbox->newpesan($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
			'newsurat_masuk' => $this->main_inbox->newsurat_masuk($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
			'auth_masuk' => $this->tmuser_userauth->cek($this->session->userdata('auth_id'), 11)
		);
		$this->load->view('template_mail', $data);
	}
	
	function query($num, $offset, $where = false)
	{
		if($offset == ''){ $offset = 0; }
		$pegawai_id = $this->session->userdata('pegawai_id');
		return $this->db
			->query('
				SELECT 
				tr_surat_penerima.id,
				tr_surat_penerima.d_entry,
				tr_surat_penerima.status,
				tmpegawai.n_pegawai,
				tmpersuratan.dari,
				tmpersuratan.tgl_surat,
				tmpersuratan.tgl_terima_surat,
				tmpersuratan.prihal,
				tmpersuratan.id_sifatsurat
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_dari = tmpegawai.id
				WHERE tr_surat_penerima.id_pegawai_ke = '.$pegawai_id.$where.'
				ORDER BY tr_surat_penerima.d_entry DESC
				LIMIT '.$offset.', '.$num);
	}
	
	function query_count($where = false)
	{
		$pegawai_id = $this->session->userdata('pegawai_id');
		return $this->db
			->query('
				SELECT
				tr_surat_penerima.id
				FROM tr_surat_penerima
				JOIN tmpersuratan on tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai on tr_surat_penerima.id_pegawai_dari = tmpegawai.id
				WHERE tr_surat_penerima.id_pegawai_ke = '.$pegawai_id.$where);
	}
	
	function valid_surat($encrip)
	{
		$id = decrip($encrip);
		$id_persuratan = $this->tr_surat_penerima->where("id = '".$id."'")->get()->id_persuratan;
		$row = $this->tmpersuratan->where("id = '".$id_persuratan."'")->get();
		if($row->id_sifatsurat == 3)
		{
			$data = array(
				'id' => $id,
				'encrip' => $encrip,
				'title' => 'Passsword',
				'assets' => array(),
				'content' => 'verifikasi_pesan'
			);
			$this->load->view('view', $data);
		}
		else
		{
			redirect(base_url().$this->page.'read/'.$encrip.'/'.sha1($id.$encrip));
		}
	}
	
	function read($encrip, $valid)
	{
		$id = decrip($encrip);
		if(sha1($id.$encrip) === $valid)
		{
			$id_persuratan = $this->tr_surat_penerima->where("id = '".$id."'")->get()->id_persuratan;
			foreach($this->tr_surat_penerima->read_all($this->session->userdata('pegawai_id'), $id_persuratan)->result() as $row)
			{
				$data = array(
					'status' => 1,
					'd_read' => date('Y-m-d H:i:s')
				);
				$this->tr_surat_penerima->update($row->id, $data);
			}
		
			$data = array(
				'encrip' => $encrip,
				'valid' => $valid,
				'id_persuratan' => $id_persuratan,
				'result' => $this->main_inbox->read($id),
				'result_disposisi' => $this->main_inbox->disposisi($this->session->userdata('auth_id')),
				'result_history' => $this->main_inbox->history_all($id_persuratan, $this->session->userdata('pegawai_id')),
				'breadcrubs' => " Inbox",
				'assets' => array(
					$this->lib_load_css_js->load_css(base_url() , "assets/css/", "jquery.chosen.css"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/", "chosen.jquery.min.js"),
					$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
					),
				'title' => 'Inbox',
				'content' => 'read',
				'newpesan' => $this->main_inbox->newpesan($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'newsurat_masuk' => $this->main_inbox->newsurat_masuk($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'auth_masuk' => $this->tmuser_userauth->cek($this->session->userdata('auth_id'), 11),
				'c_read' => $this->main_inbox->auth_read($id_persuratan, $this->session->userdata('pegawai_id'))
			);
			$this->load->view('template_mail', $data);
		}
		else
		{
			$id_persuratan = 0;
			$data = array(
				'id_persuratan' => $id_persuratan,
				'result' => $this->main_inbox->read($id),
				'result_disposisi' => $this->main_inbox->disposisi($this->session->userdata('auth_id')),
				'result_history' => $this->main_inbox->history_all($id_persuratan, $this->session->userdata('pegawai_id')),
				'breadcrubs' => " Inbox",
				'assets' => array(
					$this->lib_load_css_js->load_css(base_url() , "assets/css/", "jquery.chosen.css"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/", "chosen.jquery.min.js"),
					$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
					),
				'title' => 'Inbox',
				'content' => 'read',
				'newpesan' => $this->main_inbox->newpesan($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'newsurat_masuk' => $this->main_inbox->newsurat_masuk($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'auth_masuk' => $this->tmuser_userauth->cek($this->session->userdata('auth_id'), 11),
				'c_read' => $this->main_inbox->auth_read($id_persuratan, $this->session->userdata('pegawai_id'))
			);
			$this->load->view('template_mail', $data);
		}
	}
	
	function list_skpd($c_skpd, $c_kecamatan, $c_kelurahan, $puskesmas, $c_sekolah, $c_rs, $c_uptpendidikan)
	{
		$skpd = explode('-', $c_skpd);
		$kecamatan = explode('-', $c_kecamatan);
		$kelurahan = explode('-', $c_kelurahan);
		$puskesmas = explode('-', $puskesmas);
		$sekolah = explode('-', $c_sekolah);
		$rs = explode('-', $c_rs);
		$uptpendidikan = explode('-', $c_uptpendidikan);
		$data = array(
			'id_kirim' => array($skpd[0], $kecamatan[0], $kelurahan[0], $puskesmas[0], $sekolah[0], $rs[0], $uptpendidikan[0]),
			'id_jabatan' => array($skpd[1], $kecamatan[1], $kelurahan[1], $puskesmas[1], $sekolah[1], $rs[1], $uptpendidikan[1]),
			'result' => $this->main_inbox->disposisi($this->session->userdata('auth_id')),
		);
		$this->load->view($this->view.'list_penerima', $data);
	}
	
	function varifikasi_pass()
	{
		$data = array(
			'assets' => array(),
			'content' => 'verifikasi_pass'
		);
		$this->load->view('view', $data);
	}
	
	function submit_pass()
	{
		$num = $this->tmuser->where("username = '" . $this->session->userdata('username') . "' and password = '" . md5($this->input->post('password')) . "' and c_status=1")->count();
		if($num == 0)
		{
			$result = array(
				'status' => 0
			);
			echo json_encode($result);
		}
		else
		{
			$result = array(
				'status' => 1
			);
			echo json_encode($result);
		}
	}
	
	function send_paraf()
	{
		$paraf = $this->tmpegawai->where('id', $this->session->userdata('pegawai_id'))->get()->file_paraf;
		$data = array(
			'title' => 'Sending',
			'assets' => array(),
			'content' => 'send_paraf',
			'paraf' => $paraf
		);
		$this->load->view('view', $data);
	}
	
		//----- Send SMS With Raja-sms.com
	function disposisi($id)
	{
		$valid = false;
		$pegawai_list = $this->input->post('pegawai');
		$pegawai_list_len = count($pegawai_list);
		for($i=0; $i<$pegawai_list_len; $i++)
		{
			$valid = $pegawai_list[$i];
		}
		if($valid == false)
		{
			 echo '
			<script type="text/javascript">
				alert("Sertakan penerima surat!");
				history.go(-1);
			</script>';
		}
		else
		{
			$next = true;
			$exten = '';
			if($this->input->post('file_koreksi') == 1)
			{
				if($_FILES['userfile']['name'] != '')
				{
					$name = date('YmdHis').$this->session->userdata('auth_id');
					$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
					$endof = '.'.end($explode);
					$exten = 'uploads/surat_koreksi/'.$name.$endof;
					$config['file_name'] = $name;
					$config['allowed_types'] = 'pdf|doc|docx';
					$config['upload_path'] = 'uploads/surat_koreksi/';
					$config['max_size']	= '30000';
					$config['overwrite'] = true;
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload())
					{
						echo '
							<script type="text/javascript">
								alert("'.$this->upload->display_errors().'");
								history.go(-1);
							</script>';
						$next = false;
					}
				}
				else
				{
					echo '
						<script type="text/javascript">
							alert("Surat Hasil Koreksi Harus Di-upload.");
							history.go(-1);
						</script>';
					$next = false;
				}
			}
			
			if($next == true)
			{
				$file_lampiran = '';
				if($_FILES['file_lampiran']['name'] != '')
				{
					$name_l = $name;
					$explode_l = explode(".", str_replace(' ', '', $_FILES['file_lampiran']['name']));
					$endof_l = '.'.end($explode_l);
					$file_lampiran = 'uploads/surat_intern/'.$name_l.$endof_l;
					$config_l['file_name'] = $name_l;
					$config_l['allowed_types'] = 'xls|xlsx';
					$config_l['upload_path'] = 'uploads/surat_intern/';
					$config_l['max_size']	= '30000';
					$config_l['overwrite'] = true;
					$this->load->library('upload', $config_l);
					if(!$this->upload->do_upload('file_lampiran')){
						echo '
							<script type="text/javascript">
								alert("2 '.$this->upload->display_errors().'");
								history.go(-1);
							</script>';
						$next = false;
						exit();
					}
				}
				
				$senddata = array(
					'apikey' => SMSGATEWAY_APIKEY,  
					'callbackurl' => SMSGATEWAY_CALLBACKURL, 
					'datapacket'=> array()
				);
				$dari = $this->session->userdata('pegawai_id');
				$this->tmjabatan = new Tmjabatan();
				$this->tmunitkerja = new Tmunitkerja();
				$pegawai = $this->tmpegawai->where("id = '".$dari."'")->get();
				$jabatan = $this->tmjabatan->where("id = '".$pegawai->tmjabatan_id."'")->get()->n_jabatan;
				$unitkerja = $this->tmunitkerja->where("id ='".$pegawai->tmunitkerja_id."'")->get()->initial;
				$send = "Ada disposisi surat dari ".$jabatan.' '.$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
				$true = false;
				$d_entry = date('Y-m-d H:i:s');
				for($i=0; $i<$pegawai_list_len; $i++)
				{				
					if($this->input->post('d_awal_kegiatan') != '' && $this->input->post('d_akhir_kegiatan') != '' && $this->input->post('v_kegiatan') != '')
					{
						$data = array(
							'id_persuratan' => $id,
							'id_pegawai_dari' => $dari,
							'id_pegawai_ke' => $pegawai_list[$i],
							'catatan' => $this->input->post('catatan'),
							'file_surat_koreksi' => $exten,
							'lampiran_file_koreksi' => $file_lampiran,
							'status' => 0,
							'd_entry' => $d_entry,
							'id_skpd_dari' => $this->session->userdata('skpd_id'),
							'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
							'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
							'v_kegiatan' => $this->input->post('v_kegiatan')
						);
					}
					else
					{
						$data = array(
							'id_persuratan' => $id,
							'id_pegawai_dari' => $dari,
							'id_pegawai_ke' => $pegawai_list[$i],
							'catatan' => $this->input->post('catatan'),
							'file_surat_koreksi' => $exten,
							'status' => 0,
							'd_entry' => $d_entry,
							'id_skpd_dari' => $this->session->userdata('skpd_id')
						);
					}
					$this->tr_surat_penerima->insert($data);

					$telp = $this->tmpegawai->where("id = '".$pegawai_list[$i]."'")->get()->telp;
					if($telp != '-')
					{
						array_push($senddata['datapacket'],array(
							'number' => trim($telp),
							'message' => urlencode(stripslashes(utf8_encode($send))),
							'sendingdatetime' => ""));
							
							//------- Telp hanya untuk SEMENTARA
							//------- 28 April 2017, sms untuk ajudan pa wakil walikota
						if($pegawai_list[$i] == '211')
						{
							$number = "081286571031";
							$sendingdatetime = ""; 
							array_push($senddata['datapacket'],array(
								'number' => trim($number),
								'message' => urlencode(stripslashes(utf8_encode($send))),
								'sendingdatetime' => $sendingdatetime));
						}
					}
					$true = true;
				}				
				$sms = new api_sms_class_reguler_json();
				$sms->setIp(SMSGATEWAY_IPSERVER);
				$sms->setData($senddata);
				$responjson = $sms->send();
				$this->saldo_sms->save($responjson);
				
				echo '
				<script type="text/javascript">
					alert("Surat berhasil terkirim!");
					document.location.href = "'.base_url().$this->page.'";
				</script>';
			}
		}
	}
	
		//----- Send SMS With Gammu
	/*function disposisi($id)
	{
		$valid = false;
		$pegawai_list = $this->input->post('pegawai');
		$pegawai_list_len = count($pegawai_list);
		for($i=0; $i<$pegawai_list_len; $i++)
		{
			$valid = $pegawai_list[$i];
		}
		if($valid == false)
		{
			 echo '
			<script type="text/javascript">
				alert("Sertakan penerima surat!");
				history.go(-1);
			</script>';
		}
		else
		{
			$next = true;
			$exten = '';
			if($this->input->post('file_koreksi') == 1)
			{
				if($_FILES['userfile']['name'] != '')
				{
					$name = date('YmdHis').$this->session->userdata('auth_id');
					$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
					$endof = '.'.end($explode);
					$exten = 'uploads/surat_koreksi/'.$name.$endof;
					$config['file_name'] = $name;
					$config['allowed_types'] = 'pdf|doc|docx';
					$config['upload_path'] = 'uploads/surat_koreksi/';
					$config['max_size']	= '30000';
					$config['overwrite'] = true;
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload())
					{
						echo '
							<script type="text/javascript">
								alert("'.$this->upload->display_errors().'");
								history.go(-1);
							</script>';
						$next = false;
					}
				}
				else
				{
					echo '
						<script type="text/javascript">
							alert("Surat Hasil Koreksi Harus Di-upload.");
							history.go(-1);
						</script>';
					$next = false;
				}
			}
			
			if($next == true)
			{
				$file_lampiran = '';
				if($_FILES['file_lampiran']['name'] != '')
				{
					$name_l = $name;
					$explode_l = explode(".", str_replace(' ', '', $_FILES['file_lampiran']['name']));
					$endof_l = '.'.end($explode_l);
					$file_lampiran = 'uploads/surat_intern/'.$name_l.$endof_l;
					$config_l['file_name'] = $name_l;
					$config_l['allowed_types'] = 'xls|xlsx';
					$config_l['upload_path'] = 'uploads/surat_intern/';
					$config_l['max_size']	= '30000';
					$config_l['overwrite'] = true;
					$this->load->library('upload', $config_l);
					if(!$this->upload->do_upload('file_lampiran')){
						echo '
							<script type="text/javascript">
								alert("2 '.$this->upload->display_errors().'");
								history.go(-1);
							</script>';
						$next = false;
						exit();
					}
				}
				
				$dari = $this->session->userdata('pegawai_id');
				$this->tmjabatan = new Tmjabatan();
				$this->tmunitkerja = new Tmunitkerja();
				$pegawai = $this->tmpegawai->where("id = '".$dari."'")->get();
				$jabatan = $this->tmjabatan->where("id = '".$pegawai->tmjabatan_id."'")->get()->n_jabatan;
				$unitkerja = $this->tmunitkerja->where("id ='".$pegawai->tmunitkerja_id."'")->get()->initial;
				$send = "Ada disposisi surat dari ".$jabatan.' '.$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
				$true = false;
				$d_entry = date('Y-m-d H:i:s');
				for($i=0; $i<$pegawai_list_len; $i++)
				{				
					if($this->input->post('d_awal_kegiatan') != '' && $this->input->post('d_akhir_kegiatan') != '' && $this->input->post('v_kegiatan') != '')
					{
						$data = array(
							'id_persuratan' => $id,
							'id_pegawai_dari' => $dari,
							'id_pegawai_ke' => $pegawai_list[$i],
							'catatan' => $this->input->post('catatan'),
							'file_surat_koreksi' => $exten,
							'lampiran_file_koreksi' => $file_lampiran,
							'status' => 0,
							'd_entry' => $d_entry,
							'id_skpd_dari' => $this->session->userdata('skpd_id'),
							'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
							'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
							'v_kegiatan' => $this->input->post('v_kegiatan')
						);
					}
					else
					{
						$data = array(
							'id_persuratan' => $id,
							'id_pegawai_dari' => $dari,
							'id_pegawai_ke' => $pegawai_list[$i],
							'catatan' => $this->input->post('catatan'),
							'file_surat_koreksi' => $exten,
							'status' => 0,
							'd_entry' => $d_entry,
							'id_skpd_dari' => $this->session->userdata('skpd_id')
						);
					}
					$this->tr_surat_penerima->insert($data);

					$telp = $this->tmpegawai->where("id = '".$pegawai_list[$i]."'")->get()->telp;
					if($telp != '-')
					{
						$data = array(
							'DestinationNumber' => $telp,
							'TextDecoded' => $send,
							'CreatorID' => 'Gammu',
							'DeliveryReport' => 'yes'
						);
						$this->outbox->insert($data);
						
							//------- Telp hanya untuk SEMENTARA
							//------- 28 April 2017, sms untuk ajudan pa wakil walikota
						if($pegawai_list[$i] == '211')
						{							
							$data = array(
								'DestinationNumber' => '081286571031',
								'TextDecoded' => $send,
								'CreatorID' => 'Gammu',
								'DeliveryReport' => 'yes'
							);
							$this->outbox->insert($data);
						}
					}
					$true = true;
				}
				echo '
				<script type="text/javascript">
					alert("Surat berhasil terkirim!");
					document.location.href = "'.base_url().$this->page.'";
				</script>';
			}
		}
	}*/
	
	function view_setujui($id)
	{
		$skpd_id = $this->session->userdata('skpd_id');
		$ttd = $this->tmpegawai->where('id', $this->session->userdata('pegawai_id'))->get()->file_ttd;
		$data = array(
			'title' => 'Sending',
			'id' => $id,
			'assets' => array(),
			'content' => 'send_ttd',
			'ttd' => $ttd,
			'result' => $this->id_tu($skpd_id)
		);
		$this->load->view('view', $data);
	}
	
		//----- Send SMS With Raja-sms.com
	function setujui($id)
	{
		$skpd_id = $this->session->userdata('skpd_id');
		$result = $this->id_tu($skpd_id);
		if($result->num_rows() != 0)
		{
			$nomor = $this->penomoran->nomor_save($skpd_id, 2);
		
			// No Surat Keluar
			//$row = $this->tmpersuratan->where('id', $id)->get();
			//$nomor = $this->penomoran->nomor_save($this->session->userdata('skpd_id'), 2);
			//$jenis_surat = $row->id_jnssurat;
			// $a = strlen($jenis_surat);
// 			for ($i = 2; $i > $a; $i--) {
// 				$jenis_surat = "0" . $jenis_surat;
// 			}
// 			$n_skpd = $this->tmskpd->where('id', $this->session->userdata('skpd_id'))->get()->initial;
// 			switch (date('m')){
// 				case '1' :$tgl = 'I';break;
// 				case '2' :$tgl = 'II';break;
// 				case '3' :$tgl = 'III';break;
// 				case '4' :$tgl = 'IV';break;
// 				case '5' :$tgl = 'V';break;
// 				case '6' :$tgl = 'VI';break;
// 				case '7' :$tgl = 'VII';break;
// 				case '8' :$tgl = 'VIII';break;
// 				case '9' :$tgl = 'IX';break;
// 				case '10' :$tgl = 'X';break;
// 				case '11' :$tgl = 'XI';break;
// 				case '12' :$tgl = 'XII';break;
// 				default :$tgl = '<undefined>';break;
// 			}
// 			$nomor_surat = $jenis_surat.'/'.$nomor.'/'.$n_skpd.'/'.$tgl.'/'.date('Y');
			
			$data = array(
				'status_setujui' => 1,
				'id_skpd_in' => $skpd_id,
				'id_skpd_out' => $skpd_id,
				//'no_surat' => $nomor_surat,
				'no_agenda' => 'K-'.$nomor,
				'id_pegawai_setujui' => $this->session->userdata('pegawai_id')
			);
			$this->tmpersuratan->update($id, $data);
			
			// Send A Message
			foreach($result->result() as $row)
			{
				$telp = $row->telp;
				$id_tu = $row->id;
			}
			if($telp != '-')
			{
				$senddata = array(
					'apikey' => SMSGATEWAY_APIKEY,  
					'callbackurl' => SMSGATEWAY_CALLBACKURL, 
					'datapacket'=> array()
				);
				
				$send = "Ada surat yang harus Anda cetak dan kirimkan. Silahkan masuk ke SISUMAKER untuk melihat isi surat";
				array_push($senddata['datapacket'],array(
					'number' => trim($telp),
					'message' => urlencode(stripslashes(utf8_encode($send))),
					'sendingdatetime' => ""));
				
				$sms = new api_sms_class_reguler_json();
				$sms->setIp(SMSGATEWAY_IPSERVER);
				$sms->setData($senddata);
				$responjson = $sms->send();
				$this->saldo_sms->save($responjson);
			}
			echo '
				<script type="text/javascript">
					alert("Surat berhasil disetujui!");
					document.location.href = "'.base_url().$this->page.'";
				</script>';
		}
		else
		{
			echo '
				<script type="text/javascript">
					alert("Kami tidak menemukan pegawai dengan status sebagai Pendataan. Silahkan hubungi Admin, terimakasih!");
					history.go(-1);
				</script>';
		}
	}
	
		//----- Send SMS With Gammu
	/*function setujui($id)
	{
		$result = $this->id_tu();
		if($result->num_rows() != 0)
		{
		
			// No Surat Keluar
			//$row = $this->tmpersuratan->where('id', $id)->get();
			$nomor = $this->penomoran->nomor_save($this->session->userdata('skpd_id'), 2);
			// $jenis_surat = $row->id_jnssurat;
// 			$a = strlen($jenis_surat);
// 			for ($i = 2; $i > $a; $i--) {
// 				$jenis_surat = "0" . $jenis_surat;
// 			}
// 			$n_skpd = $this->tmskpd->where('id', $this->session->userdata('skpd_id'))->get()->initial;
// 			switch (date('m')){
// 				case '1' :$tgl = 'I';break;
// 				case '2' :$tgl = 'II';break;
// 				case '3' :$tgl = 'III';break;
// 				case '4' :$tgl = 'IV';break;
// 				case '5' :$tgl = 'V';break;
// 				case '6' :$tgl = 'VI';break;
// 				case '7' :$tgl = 'VII';break;
// 				case '8' :$tgl = 'VIII';break;
// 				case '9' :$tgl = 'IX';break;
// 				case '10' :$tgl = 'X';break;
// 				case '11' :$tgl = 'XI';break;
// 				case '12' :$tgl = 'XII';break;
// 				default :$tgl = '<undefined>';break;
// 			}
// 			$nomor_surat = $jenis_surat.'/'.$nomor.'/'.$n_skpd.'/'.$tgl.'/'.date('Y');
			
			$data = array(
				'status_setujui' => 1,
				//'no_surat' => $nomor_surat,
				'no_agenda' => 'K-'.$nomor,
				'id_pegawai_setujui' => $this->session->userdata('pegawai_id')
			);
			$this->tmpersuratan->update($id, $data);
			
			// Send A Message
			foreach($result->result() as $row)
			{
				$telp = $row->telp;
				$id_tu = $row->id;
			}
			if($telp != '-')
			{
				$send = "Ada surat yang harus Anda cetak dan kirimkan. Silahkan masuk ke SISUMAKER untuk melihat isi surat";
				$data = array(
					'DestinationNumber' => $telp,
					'TextDecoded' => $send,
					'CreatorID' => 'Gammu',
					'DeliveryReport' => 'yes'
				);
				$this->outbox->insert($data);
			}
			echo '
				<script type="text/javascript">
					alert("Surat berhasil disetujui!");
					document.location.href = "'.base_url().$this->page.'";
				</script>';
		}
		else
		{
			echo '
				<script type="text/javascript">
					alert("Kami tidak menemukan pegawai dengan status sebagai Pendataan. Silahkan hubungi Admin, terimakasih!");
					history.go(-1);
				</script>';
		}
	}*/
	
	function id_tu($skpd_id)
	{
		//$skpd_id = $this->session->userdata('skpd_id');
		return $this->db
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmpegawai.telp,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
			->where('tmpegawai.tmskpd_id', $skpd_id)
			->where('tmuser_userauth.userauth_id', 5)
			->from('tmpegawai')
			->join('tmuser','tmpegawai.tmuser_id = tmuser.id')
			->join('tmuser_userauth', 'tmuser.id = tmuser_userauth.tmuser_id')
			->join('tmjabatan', 'tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja', 'tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->limit(1)
			->get();
	}
	
	function print_all($encrip, $valid)
	{
		$id = decrip($encrip);
		if(sha1($id.$encrip) === $valid)
		{
			$id_persuratan = $this->tr_surat_penerima->where("id = '".$id."'")->get()->id_persuratan;
			$data = array(
				'id_persuratan' => $id_persuratan,
				'result' => $this->main_inbox->read($id),
				'result_disposisi' => $this->main_inbox->disposisi($this->session->userdata('auth_id')),
				'result_history' => $this->main_inbox->history_all($id_persuratan, $this->session->userdata('pegawai_id')),
				'breadcrubs' => " Inbox",
				'assets' => array(
					$this->lib_load_css_js->load_css(base_url() , "assets/css/", "jquery.chosen.css"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/", "chosen.jquery.min.js"),
					$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
					),
				'title' => 'Inbox',
				'content' => 'print_all',
				'newpesan' => $this->main_inbox->newpesan($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'newsurat_masuk' => $this->main_inbox->newsurat_masuk($this->session->userdata('pegawai_id'), $this->session->userdata('skpd_id')),
				'auth_masuk' => $this->tmuser_userauth->cek($this->session->userdata('auth_id'), 11),
				'c_read' => $this->main_inbox->auth_read($id_persuratan, $this->session->userdata('pegawai_id'))
			);
			$this->load->view('print', $data);
		}
		else
		{
			echo "Error! Terdapat kesalahan pada link url.";
		}
	}
}