<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Security extends ICA_AdminCont {

    var $page = "myaccount/security/";
    var $view = "myaccount/security/";
    var $icon = "calendar";

    function __construct() {
        parent::__construct();
		$this->restrict('13');
		$this->tmpersuratan = new Tmpersuratan();
		$this->penomoran = new Penomoran();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->tmskpd = new Tmskpd();
		$this->surat = new Surat();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->tmuser_userauth = new Tmuser_userauth();
		$this->main_inbox = new Main_inbox();
		$this->tmpegawai = new Tmpegawai();
		$this->outbox = new Outbox();
        $this->load->helper('function_helper');
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
            ),
            'breadcrubs' => " Agenda",
            'title' => 'Kalender Agenda',
            'content' => 'location',
            'sort' => 1
        );
        $this->load->view('template_mail', $data);
    }
    
    function get_location() {
    	if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){
			//Send request and receive json data by latitude and longitude
			$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($_POST['latitude']).','.trim($_POST['longitude']).'&sensor=false';
			$json = @file_get_contents($url);
			$data = json_decode($json);
			$status = $data->status;
			if($status=="OK"){
				//Get address from json data
				$location = $data->results[0]->formatted_address;
			}else{
				$location =  '';
			}
			//Print address 
			echo $location.'<br/>';
			echo 'Latitude : '.trim($_POST['latitude']).'<br/>';
			echo 'Longitude : '.trim($_POST['longitude']).'<br/>';
		}
    }
    
    function geo($accuracy, $latlng, $altitude, $altitude_accuracy, $heading, $speed) {
    	$geo = 'http://maps.google.com/maps/api/geocode/xml?latlng='.htmlentities(htmlspecialchars(strip_tags($_GET['latlng']))).'&sensor=true';
		$xml = simplexml_load_file($geo);

		foreach($xml->result->address_component as $component){
			if($component->type=='street_address'){
				$geodata['precise_address'] = $component->long_name;
			}
			if($component->type=='natural_feature'){
				$geodata['natural_feature'] = $component->long_name;
			}
			if($component->type=='airport'){
				$geodata['airport'] = $component->long_name;
			}
			if($component->type=='park'){
				$geodata['park'] = $component->long_name;
			}
			if($component->type=='point_of_interest'){
				$geodata['point_of_interest'] = $component->long_name;
			}
			if($component->type=='premise'){
				$geodata['named_location'] = $component->long_name;
			}
			if($component->type=='street_number'){
				$geodata['house_number'] = $component->long_name;
			}
			if($component->type=='route'){
				$geodata['street'] = $component->long_name;
			}
			if($component->type=='locality'){
				$geodata['town_city'] = $component->long_name;
			}
			if($component->type=='administrative_area_level_3'){
				$geodata['district_region'] = $component->long_name;
			}
			if($component->type=='neighborhood'){
				$geodata['neighborhood'] = $component->long_name;
			}
			if($component->type=='colloquial_area'){
				$geodata['locally_known_as'] = $component->long_name;
			}
			if($component->type=='administrative_area_level_2'){
				$geodata['county_state'] = $component->long_name;
			}
			if($component->type=='postal_code'){
				$geodata['postcode'] = $component->long_name;
			}
			if($component->type=='country'){
				$geodata['country'] = $component->long_name;
			}
		}

		list($lat,$long) = explode(',',htmlentities(htmlspecialchars(strip_tags($_GET['latlng']))));
		$geodata['latitude'] = $lat;
		$geodata['longitude'] = $long;
		$geodata['formatted_address'] = $xml->result->formatted_address;
		$geodata['accuracy'] = htmlentities(htmlspecialchars(strip_tags($_GET['accuracy'])));
		$geodata['altitude'] = htmlentities(htmlspecialchars(strip_tags($_GET['altitude'])));
		$geodata['altitude_accuracy'] = htmlentities(htmlspecialchars(strip_tags($_GET['altitude_accuracy'])));
		$geodata['directional_heading'] = htmlentities(htmlspecialchars(strip_tags($_GET['heading'])));
		$geodata['speed'] = htmlentities(htmlspecialchars(strip_tags($_GET['speed'])));
		$geodata['google_api_src'] = $geo;
		echo '<img src="http://maps.google.com/maps/api/staticmap?center='.$lat.','.$long.'&zoom=14&size=150x150&maptype=roadmap&&sensor=true" width="150" height="150" alt="'.$geodata['formatted_address'].'" \/><br /><br />';
		echo 'Latitude: '.$lat.' Longitude: '.$long.'<br />';
		foreach($geodata as $name => $value){
			echo ''.$name.': '.str_replace('&','&amp;',$value).'<br />';
		}
    }

}