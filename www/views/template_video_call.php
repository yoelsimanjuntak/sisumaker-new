<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,no-store,must-revalidate"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="expires" content="0"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
        
        <meta name="theme-color" content="#272727">
        <meta name="msapplication-navbutton-color" content="#272727">
        <meta name="apple-mobile-web-app-status-bar-style" content="#272727">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <title>SISUMAKER | <?php echo $title; ?></title>
		<?php $base_url = base_url(); ?>
        <link rel="shortcut icon" href="<?php echo $base_url;?>assets/images/favicon.png"/>
        
        <script type="text/javascript">
            var base                = '<?php echo $base_url; ?>';
            var _USER               = <?php echo json_encode($this->session->userdata) ?>;
            var _DATAROOM           = ( ( localStorage.getItem('room') != null ) ?  JSON.parse( localStorage.getItem('room') ) : [] );
        </script> 
        
        <script>
            
            function toggleFullScreen() {
                var doc = window.document;
                var docEl = doc.documentElement;
                
                var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
                var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
                
                requestFullScreen.call(docEl);
            }
            
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                toggleFullScreen();
            }
        </script>
        <?php
        echo $this->lib_load_css_js->load_css(base_url(), "assets/video-call/plugins/materialize/css/", "materialize.min.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "font-awesome.min.css");
        echo $this->lib_load_css_js->load_css(base_url(), "assets/video-call/", "vendor.min.css?v=1.1");
        echo $this->lib_load_css_js->load_css(base_url(), "assets/video-call/", "style.min.css?v=1.2");
        
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-2.0.3.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.2.1.min.js");
        echo $this->lib_load_css_js->load_js(base_url(), "assets/video-call/plugins/materialize/js/", "materialize.min.js");
        echo $this->lib_load_css_js->load_js(base_url(), "assets/video-call/", "vendor.min.js?v=2.1");
        echo $this->lib_load_css_js->load_js(base_url(), "assets/video-call/", "app.min.js?v=1.3");
        
        foreach ($assets as $asset_js_css) {
            echo $asset_js_css;
        }
        ?>
        <!--[if lte IE 8]><?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "excanvas.min.js"); ?><![endif]-->
        <!--<?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "elements.js"); ?>-->
    </head>
    <body style="background-image:url('<?php echo $base_url.$this->session->userdata('wallpaper');?>')" class="vc-page-video-call">
        <?php echo $this->load->view($this->view.$content); ?>
    </body>
</html>
