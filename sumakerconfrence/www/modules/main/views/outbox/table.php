<style>
.dataTables_length, .dataTables_info{
	background:none !important;
}
.table .con0{
	background:rgba(255,255,255,.70);
}
.table .con1{
	background:none;
}
</style>
<div class="messagepanel">
	<div class="messagemenu">
		<ul>
			<li class="no-active"><a href="<?php echo base_url().'main/inbox';?>"><span class="iconfa-inbox"></span> Kotak Masuk <?php if($newpesan != 0){ ?><span class="new-pesan"><?php echo $newpesan;?> baru</span><?php }?></a></li>
			<li class="active"><a href="<?php echo base_url().'main/outbox';?>"><span class="iconfa-plane"></span> Pesan Terkirim</a></li>
			<!--<?php if($auth_masuk != 0){ ?>
			<li class="no-active"><a href="<?php echo base_url().'main/surat_masuk';?>"><span class="iconfa-envelope"></span> Surat Masuk <?php if($newsurat_masuk != 0){ ?><span class="new-pesan"><?php echo $newsurat_masuk;?> baru</span><?php }?></a></li>
			<?php }?>-->
		</ul>
	</div>
	<div class="messagecontent" style="border-right:1px solid #bb2f0e">
		<div class="messageleft" style="width:100%;min-height:0px;">
			<div class="row-fluid messagesearch">
				<div class="btn-group">
					<button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo $filter_text;?> <span class="caret"></span></button>
					<ul class="dropdown-menu">
						<?php if($search != '0'){ ?>
						<li><a href="<?php echo base_url().$this->page;?>"><i class="iconfa-reorder"></i> Semua</a></li>
						<li class="divider"></li>
						<?php } ?>
						<li><a href="<?php echo base_url().$this->page.'index/b-0';?>"><i class="iconfa-exclamation-sign"></i> Belum dibaca</a></li>
						<li><a href="<?php echo base_url().$this->page.'index/b-1';?>"><i class="iconfa-ok-sign"></i> Sudah dibaca</a></li>
						<li class="divider"></li>
						<li><a href="<?php echo base_url().$this->page.'index/c-1';?>"><span class="label label-important">&nbsp;</span> &nbsp;Sangat Segera</a></li>
						<li><a href="<?php echo base_url().$this->page.'index/c-2';?>"><span class="label label-warning">&nbsp;</span> &nbsp;Segera</a></li>
						<li><a href="<?php echo base_url().$this->page.'index/c-3';?>"><span class="label label-inverse">&nbsp;</span> &nbsp;Rahasia</a></li>
						<li><a href="<?php echo base_url().$this->page.'index/c-4';?>"><span class="label label-info">&nbsp;</span> &nbsp;Biasa</a></li>
					</ul>
				</div>
				<a href="" class="btn"> &nbsp; <i class="iconfa-repeat"></i> &nbsp; </a>
				<span class="pull-right">
					<span title="Menampilkan Dari - Sampai dari Jumlah Data" class="date_table_poin"><?php echo $num_offset + 1;?>-<?php $sum = $per_page + $num_offset; if($sum <= $total_rows){ echo $sum; } else{ echo $total_rows; $n_false = true; };?> dari <?php echo $total_rows;?> &nbsp;</span>
					<div class="btn-group" style="margin-right:20px;min-width:64px;">
						<?php if($num_offset == 0){ ?>
						<a class="btn-small btn disabled" rel="prev"><i class="iconfa-chevron-left"></i></a>
						<?php } echo $paging; if(isset($n_false)){ ?>
						<a class="btn-small btn disabled" rel="next"><i class="iconfa-chevron-right"></i></a>
						<?php } ?>
					</div>
					<form class="search-surat" action="<?php echo base_url().$this->page.'index/s-';?>" method="POST" id="form-search">
						<input type="text" name="search" id="search" placeholder="Cari Surat">
					</form>
				</span>
			</div>
			<div id="result"></div>
			<div id="dyntable_wrapper" class="dataTables_wrapper" role="grid" style="min-height:350px;">
				<table class="table" style="margin-bottom:0px;">
					<thead>       
						<tr>
							<th width="20px">No</th>
							<th width="140px">Asal Surat</th>
							<th width="110px">Tanggal</th>
							<th width="140px">Kepada</th>
							<th>Perihal</th>
							<th width="100px">Status</th>
							<th width="100px">Aksi</th>
						</tr>
					</thead>
					<tbody id="loading">
						<tr>
							<td valign="top" colspan="8" class="dataTables_empty">Sedang Mengambil Data <img src="<?php echo base_url().'assets/images/loaders/loader19.gif';?>"/></td>
						</tr>
					</tbody>
					<tbody id="data-loading" style="display:none">
						<?php if($result->num_rows() != 0){ $no = $num_offset;foreach($result->result() as $row){ ?>
						<tr>
							<td><center><?php $no++;echo $no;?></center></td>
							<td><span title="<?php echo $row->dari;?>" class="date_table_poin"><?php echo character_limiter($row->dari, 28);?></span></td>
							<td>
								<span title="Tanggal Surat Dibuat" class="date_table_poin">
									<i class="iconfa-envelope"></i> <?php echo p_indonesian_date($row->tgl_surat);?>
								</span>
								<?php if($row->tgl_terima_surat != '' && $row->tgl_terima_surat != '0000-00-00'){ ?>
									<br/><span class="date_table_poin" title="Tanggal Diterima Surat Oleh Bag. Umum"><i class="iconfa-signin"></i> <?php echo p_indonesian_date($row->tgl_terima_surat);?></span>
								<?php } ?>
							</td>
							<td><span title="<?php echo $row->n_pegawai;?>" class="date_table_poin"><?php echo character_limiter($row->n_pegawai, 19);?></span><br/><span class="date_silver" title="Tanggal Pengiriman Oleh <?php echo $row->n_pegawai;?>"><i class="iconfa-time"></i> <?php echo p_indonesian_date($row->d_entry);?></span></td>
							<td>
								<?php
								$view_surat = "href='".base_url().$this->page."valid_surat/".encrip($row->id)."'";
								$id_sifat = $row->id_sifatsurat;
								if($id_sifat == 1){
									$t_sifat = 'Sangat Segera';
									$c_sifat = 'important';
								}elseif($id_sifat == 2){
									$t_sifat = 'Segera';
									$c_sifat = 'warning';
									$return = "<span class='label label-'>Segera</span>";
								}elseif($id_sifat == 3){
									$view_surat = "id='view' href='#' to='".base_url().$this->page."valid_surat/".encrip($row->id)."'";
									$t_sifat = 'Rahasia';
									$c_sifat = 'inverse';
									$return = "<span class='label label-inverse'>Rahasia</span>";
								}elseif($id_sifat == 4){
									$t_sifat = 'Biasa';
									$c_sifat = 'info';
								}
								echo "<span class='label label-".$c_sifat." pull-right'>".$t_sifat."</span>".$row->prihal;?></td>
							<td>
								<?php 
								if($row->status == 1){
									echo '<em class="text-success"> <i class="iconfa-ok-sign"></i> sudah dibaca </em>';
								}else{
									$date2 = explode('-', explode(' ', $row->d_entry)[0]);
									$tgl_1 = date('d');$bln_1 = date('m');$thn_1 = date('Y');
									$tgl_2 = $date2[2];$bln_2 = $date2[1];$thn_2 = $date2[0];
									
									$dari = GregorianToJD($bln_1, $tgl_1, $thn_1);
									$hingga = GregorianToJD($bln_2, $tgl_2, $thn_2);
									
									$selisih = $dari - $hingga;
									if($selisih == 0){ $v_selisih = 'Hari ini';}
									else{ $v_selisih = $selisih.' Hari yang lalu';} ?>		
									<b class="text-warning"> <i class="iconfa-exclamation-sign"></i> belum dibaca </b>
									<span class="date_silver" title="Selisih waktu pengirim"><i class="iconfa-time"></i> <?php echo $v_selisih;?></span>
								<?php } ?>
							</td>
							<td>
								<center>
									<a id="view" href="#" to="<?php echo base_url().'main/outbox/info/'.encrip($row->id);?>"><i class="iconfa-info-sign"></i> Info</a> &nbsp;
									<a <?php echo $view_surat;?>><i class="iconfa-external-link"></i> Lihat</a>
								</center>
							</td>
						</tr>
						<?php }}else{ ?>
						<tr>
							<td valign="top" colspan="6" class="dataTables_empty">Tidak Ada Data.</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<!--<div class="row-fluid messagesearch" style="border-bottom:none;">
				<div class="btn-group">
					<button class="btn dropdown-toggle" data-toggle="dropdown"><?php echo $filter_text;?> <span class="caret"></span></button>
					<ul class="dropdown-menu">
						<?php if($search != '0'){ ?>
						<li><a href="<?php echo base_url().$this->page.'index/0/'.$num_offset;?>"><i class="iconfa-reorder"></i> Semua</a></li>
						<li class="divider"></li>
						<?php } ?>
						<li><a href="<?php echo base_url().$this->page.'index/b-0/'.$num_offset;?>"><i class="iconfa-exclamation-sign"></i> Belum dibaca</a></li>
						<li><a href="<?php echo base_url().$this->page.'index/b-1/'.$num_offset;?>"><i class="iconfa-ok-sign"></i> Sudah dibaca</a></li>
						<li class="divider"></li>
						<li><a href="<?php echo base_url().$this->page.'index/c-1/'.$num_offset;?>"><span class="label label-important">&nbsp;</span> &nbsp;Sangat Segera</a></li>
						<li><a href="<?php echo base_url().$this->page.'index/c-2/'.$num_offset;?>"><span class="label label-warning">&nbsp;</span> &nbsp;Segera</a></li>
						<li><a href="<?php echo base_url().$this->page.'index/c-3/'.$num_offset;?>"><span class="label label-inverse">&nbsp;</span> &nbsp;Rahasia</a></li>
						<li><a href="<?php echo base_url().$this->page.'index/c-4/'.$num_offset;?>"><span class="label label-info">&nbsp;</span> &nbsp;Biasa</a></li>
					</ul>
				</div>
				<a href="" class="btn"> &nbsp; <i class="iconfa-repeat"></i> &nbsp; </a>
				<span class="pull-right">
					<span title="Menampilkan Dari - Sampai dari Jumlah Data" class="date_table_poin"><?php echo $num_offset + 1;?>-<?php $sum = $per_page + $num_offset; if($sum <= $total_rows){ echo $sum; } else{ echo $total_rows; $n_false = true; };?> dari <?php echo $total_rows;?> &nbsp;</span>
					<div class="btn-group" style="margin-right:20px;min-width:64px;">
						<?php if($num_offset == 0){ ?>
						<a class="btn-small btn disabled" rel="prev"><i class="iconfa-chevron-left"></i></a>
						<?php } echo $paging; if(isset($n_false)){ ?>
						<a class="btn-small btn disabled" rel="next"><i class="iconfa-chevron-right"></i></a>
						<?php } ?>
					</div>
					<form class="search-surat" action="<?php echo base_url().$this->page.'index/s-';?>" method="POST" id="form-search2">
						<input type="text" name="search2" id="search2" placeholder="Cari Surat">
					</form>
				</span>
			</div>-->
		</div>
	</div>
</div>
<script type="text/javascript">
	fancybox();
	jQuery('#data-loading').fadeIn();
	jQuery('#loading').remove();
	jQuery(document).ready(function(){
		v_search = '<?php echo $search;?>';
		split = v_search.split('-');
		if(split[0] == 's'){
			jQuery('#search').val(v_search.substr(2));
			jQuery('#search2').val(v_search.substr(2));
		}
		jQuery("#form-search")
			.validate({
				rules: {
					search: "required",
				},
				submitHandler:function(form){
					jQuery('#result').fadeOut('fast');
					document.location.href = jQuery(form).attr('action') + encodeURIComponent(jQuery('#search').val());
				}
		});
		jQuery("#form-search2")
			.validate({
				rules: {
					search2: "required",
				},
				submitHandler:function(form){
					jQuery('#result').fadeOut('fast');
					document.location.href = jQuery(form).attr('action') + encodeURIComponent(jQuery('#search2').val());
				}
		});
		jQuery("#search").autocomplete({
			source: function(request, response) {
				jQuery.ajax({ 
					url: "<?php echo base_url().$this->page.'search_suges'; ?>",
					data: { ss: jQuery("#search").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);
					}    
				});
			},
			minLength: 1,
			select: function( event, ui ) {
				document.location.href = '<?php echo base_url().$this->page;?>index/s-' + encodeURIComponent(ui.item.value);
		  	}
		});

	});
</script>