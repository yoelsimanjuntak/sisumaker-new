jQuery.noConflict();
jQuery(document).ready(function(){
    // dropdown in leftmenu
    jQuery('.leftmenu .dropdown > a').click(function(){
        if(!jQuery(this).next().is(':visible'))
            jQuery(this).next().slideDown('fast');
        else
            jQuery(this).next().slideUp('fast');	
        return false;
    });
    
    jQuery('.leftmenu .dropdown > ul li.submenu > a').click(function(){
        if(!jQuery(this).next().is(':visible'))
            jQuery(this).next().slideDown('fast');
        else
            jQuery(this).next().slideUp('fast');
        return false;
    });
	
    if(jQuery.uniform) 
        jQuery('input:checkbox, input:radio, .uniform-file').uniform();
		
    if(jQuery('.widgettitle .close').length > 0) {
        jQuery('.widgettitle .close').click(function(){
            jQuery(this).parents('.widgetbox').fadeOut(function(){
                jQuery(this).remove();
            });
        });
    }
	// add menu bar for phones and tablet
    jQuery('.topbar').remove();
   jQuery('<div class="topbar"><a class="barmenu">'+
		    '</a><div class="chatmenu"></a></div>').insertBefore('.mainwrapper');
    jQuery('.topbar .barmenu').click(function() {
		  
        var lwidth = '200px';
        if(jQuery(window).width() < 340) {
            lwidth = '200px';
        }
		  
        if(!jQuery(this).hasClass('open')) {
            jQuery('.rightpanel, .headerinner, .topbar').css({
                marginLeft: lwidth
            },'fast');
            jQuery('.logo, .leftpanel').css({
                marginLeft: 0
            },'fast');
            jQuery(this).addClass('open');
        } else {
            jQuery('.rightpanel, .headerinner, .topbar').css({
                marginLeft: 0
            },'fast');
            jQuery('.logo, .leftpanel').css({
                marginLeft: '-'+lwidth
            },'fast');
            jQuery(this).removeClass('open');
        }
    });
	jQuery('.topbar .chatmenu').click(function(){
		if(!jQuery('.onlineuserpanel').is(':visible')) {
			jQuery('.onlineuserpanel,#chatwindows').show();
			jQuery('.topbar .chatmenu').css({right: '210px'});
		} else {
			jQuery('.onlineuserpanel, #chatwindows').hide();
			jQuery('.topbar .chatmenu').css({right: '10px'});
		}
	});
    // show/hide left menu
    jQuery(window).resize(function(){
        if(!jQuery('.topbar').is(':visible')) {
            jQuery('.rightpanel, .headerinner').css({
                marginLeft: '200px'
            });
            jQuery('.logo, .leftpanel').css({
                marginLeft: 0
            });
        } else {
            jQuery('.rightpanel, .headerinner').css({
                marginLeft: 0
            });
            jQuery('.logo, .leftpanel').css({
                marginLeft: '-200px'
            });
        }
    });	
    // dropdown menu for profile image
    jQuery('.userloggedinfo img').click(function(){
        if(jQuery(window).width() < 480) {
            var dm = jQuery('.userloggedinfo .userinfo');
            if(dm.is(':visible')) {
                dm.hide();
            } else {
                dm.show();
            }
        }
    });
	// expand/collapse boxes
	if(jQuery('.minimize').length > 0) {
		  
		  jQuery('.minimize').click(function(){
					 if(!jQuery(this).hasClass('collapsed')) {
								jQuery(this).addClass('collapsed');
								jQuery(this).html("&#43;");
								jQuery(this).parents('.widgetbox')
										      .css({marginBottom: '20px'})
												.find('.widgetcontent')
												.hide();
					 } else {
								jQuery(this).removeClass('collapsed');
								jQuery(this).html("&#8211;");
								jQuery(this).parents('.widgetbox')
										      .css({marginBottom: '0'})
												.find('.widgetcontent')
												.show();
					 }
					 return false;
		  });
			  
	}
	// fixed right panel
	var winSize = jQuery(window).height();
	if(jQuery('.rightpanel').height() < winSize) {
		jQuery('.rightpanel').height(winSize);
	}
	// if facebook like chat is enabled
	if(jQuery.cookie('enable-chat')) {
		
		jQuery('body').addClass('chatenabled');
		jQuery.get(base + 'chat/list_online',function(data){
			jQuery('body').append(data);
		});
		
	} else {
		
		if(jQuery('.chatmenu').length > 0) {
			jQuery('.chatmenu').remove();
		}
		
	}
});