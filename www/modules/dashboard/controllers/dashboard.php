<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Dashboard extends ICA_AdminCont {

    var $page = "dashboard/dashboard/";
    var $view = "dashboard/dashboard/";
    var $icon = "briefcase";

    function __construct() {
        parent::__construct();
        $this->restrict('21');
		$this->tmpersuratan = new Tmpersuratan();
		$this->penomoran = new Penomoran();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->tmskpd = new Tmskpd();
		$this->surat = new Surat();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->tmuser_userauth = new Tmuser_userauth();
		$this->main_inbox = new Main_inbox();
		$this->tmpegawai = new Tmpegawai();
		$this->outbox = new Outbox();
        $this->load->helper('function_helper');
    }

    function index($id_skpd = false)
	{
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
				$this->lib_load_css_js->load_css(base_url() , "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
				$this->lib_load_css_js->load_js(base_url() , "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5"),
                $this->lib_load_css_js->load_css(base_url(), "assets/js/fullcalendar/", "fullcalendar.min.css"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/lib/", "moment.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/", "fullcalendar.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fullcalendar/", "lang-all.js"),
				$this->lib_load_css_js->load_js(base_url(), "assets/js/highchart/", "highcharts.js"),
				$this->lib_load_css_js->load_js(base_url(), "assets/js/highchart/modules/", "exporting.js")
            ),
            'breadcrubs' => "",
            'title' => 'SISUMAKER :: Dashboard',
            'content' => 'calendar',
            'sort' => 3,
			'id_skpd' => $id_skpd,
			'result_skpd' => $this->tmskpd->select()->result(),
			's_masuk' => $this->tmpersuratan->where('id_jnssurat', 1)->count(),
			's_keluar' => $this->tmpersuratan->where('id_jnssurat', 2)->count(),
        );
        $this->load->view($this->page.'main', $data);
    }
	
	function get_data()
	{
		$jsonevents = array();
		$pegawai_id = $this->session->userdata('pegawai_id');
		$json = $this->query($pegawai_id);
		foreach($json->result() as $row)
		{
			if($row->d_awal_kegiatan != '' || $row->d_akhir_kegiatan != '' || $row->v_kegiatan != '')
			{	
				$start = explode(' ', $row->d_awal_kegiatan);
				$end = explode(' ', $row->d_akhir_kegiatan);
				$jsonevents[] = array(
					'id' => $row->id,
					'title' => $row->v_kegiatan,
					'start' => $start[0].'T'.$start[1],
					'end' => $end[0].'T'.$end[1],
					'url' => base_url().$this->page.'v_surat/'.$row->id
				);
			}
		}
		echo json_encode($jsonevents);
    }
	
	function query($pegawai_id)
	{
		return $this->db
				->select('
					tmpersuratan.id,
					tmpersuratan.d_awal_kegiatan,
					tmpersuratan.d_akhir_kegiatan,
					tmpersuratan.v_kegiatan
				')
				->where('tmpersuratan.id_jnssurat', 1)
				->from('tmpersuratan')
				->get();
	}
	
	function v_surat($id)
	{
		$id_persuratan = $this->tr_surat_penerima->where("id = '".$id."'")->get()->id_persuratan;
		$data = array(
			'id' => $id,
			'id_persuratan' => $id_persuratan,
			'result' => $this->main_inbox->read($id),
			'assets' => array(),
			'title' => 'Read Inbox',
			'content' => 'v_surat'
		);
		$this->load->view('view', $data);
	}
	
	function absen_submit($id)
	{
		$data = array(
			'c_absen' => '1',
			'd_absen' => date('Y-m-d H:i:s')
		);
		$this->tr_surat_penerima->update($id, $data);
		redirect(base_url().$this->page.'v_surat/'.$id);
	}
	
	function data($id_skpd = false)
	{		
		function cek_absen($id)
		{
			$return = '';
			$query = mysql_query("SELECT tmpegawai.id, tmpegawai.n_pegawai, tr_surat_penerima.d_absen FROM tr_surat_penerima JOIN tmpegawai ON tr_surat_penerima.id_pegawai_ke = tmpegawai.id WHERE tr_surat_penerima.id_persuratan = '".$id."' AND tr_surat_penerima.c_absen = 1 ORDER BY tmpegawai.n_pegawai ASC;");
			$count = mysql_num_rows($query);
			if($count != 0)
			{
				$return = "<ol class='list-ordered'>";
				while($row = mysql_fetch_array($query))
				{
					$return .= "<li title='Absen Pada : ".indo_date_time($row['d_absen'])."'>".$row['n_pegawai']."</li>";
				}
				$return .= "</ol>";
			}
			else
			{
				$return = "<span class='text-warning'>* Belum Ada Yang Hadir.</span>";
			}
			return $return;
		}
		$this->load->library('datatables');
		if($id_skpd == false)	//Semua SKPD
		{
			echo $this->datatables
				->select('
					tmpersuratan.id,
					tmpersuratan.prihal,
					tmpersuratan.dari,
					tmpersuratan.d_awal_kegiatan,
					tmpersuratan.d_akhir_kegiatan,
					tmpersuratan.v_kegiatan,
					tmskpd.n_skpd,
					tmpersuratan.no_surat
				')
				->where('tmpersuratan.id_jnssurat', 1)
				->where('tmpersuratan.d_awal_kegiatan <> ', '')
				->where('tmpersuratan.d_akhir_kegiatan <> ', '')
				->where('tmpersuratan.v_kegiatan <> ', '')
				->from('tmpersuratan')
				->join('tmskpd', 'tmpersuratan.id_skpd_in = tmskpd.id')
				->edit_column('tmpersuratan.d_awal_kegiatan',
					'<center>$1 <br/><sup>s</sup>/<sub>d</sub><br/>$2</center>',
					'indo_date_time(tmpersuratan.d_awal_kegiatan), indo_date_time(tmpersuratan.d_akhir_kegiatan)'
				)
				->edit_column('tmpersuratan.no_surat',
					'$1',
					'cek_absen(tmpersuratan.id)'
				)
				->unset_column('tmpersuratan.d_akhir_kegiatan')
				->generate();
		}
		else
		{
			echo $this->datatables
				->select('
					tmpersuratan.id,
					tmpersuratan.prihal,
					tmpersuratan.dari,
					tmpersuratan.d_awal_kegiatan,
					tmpersuratan.d_akhir_kegiatan,
					tmpersuratan.v_kegiatan,
					tmskpd.n_skpd,
					tmpersuratan.no_surat
				')
				->where('tmpersuratan.id_jnssurat', 1)
				->where('tmpersuratan.d_awal_kegiatan <> ', '')
				->where('tmpersuratan.d_akhir_kegiatan <> ', '')
				->where('tmpersuratan.v_kegiatan <> ', '')
				->where('tmpersuratan.id_skpd_in', $id_skpd)
				->from('tmpersuratan')
				->join('tmskpd', 'tmpersuratan.id_skpd_in = tmskpd.id')
				->edit_column('tmpersuratan.d_awal_kegiatan',
					'<center>$1 <br/><sup>s</sup>/<sub>d</sub><br/>$2</center>',
					'indo_date_time(tmpersuratan.d_awal_kegiatan), indo_date_time(tmpersuratan.d_akhir_kegiatan)'
				)
				->edit_column('tmpersuratan.no_surat',
					'$1',
					'cek_absen(tmpersuratan.id)'
				)
				->unset_column('tmpersuratan.d_akhir_kegiatan')
				->generate();
		}
		
	}
}