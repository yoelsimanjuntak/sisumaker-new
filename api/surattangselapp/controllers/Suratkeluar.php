<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'controllers/Auth.php';

class Suratkeluar extends Auth
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credential: true');
        header('Access-Control-Max-Age: 86400');
        header('Access-Control-Allow-Methods:GET,POST,OPTIONS');
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $this->load->model('db_dml');
        $this->load->model('db_select');
        $this->load->model('mymodel');
        $this->load->model('tr_surat_penerima');
        $this->load->helper('function_helper');
        if ($this->cektoken() == false) {
            header('Content-type: application/json');
            $output = [
                'status' => 0,
                'message' => 'Wrong Token',
                'token' => $this->input->get_request_header('Authorization')
            ];
            exit(json_encode($output));
        }
    }

    public function index_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $per_page = $this->get('per_page', true);
        $page = $this->get('page', true);
        $output = [];

        if (!$per_page || !$page) {
            $output = [
                'status' => 0,
                'message' => 'Parameter request per_page & page tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            if ($page == 1) {
                $offset = 0;
            } else {
                $page = $page - 1;
                $offset = $page * $per_page;
            }

            $query = $this->mymodel->surat_keluar($pegawai_id, $per_page, $offset);
            if ($query->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Tidak ada surat.'
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                foreach ($query->result() as $row) {
                    array_push($output, [
                        'asal_surat' => $row->dari,
                        'tgl_dibuat' => $row->tgl_surat,
                        'tgl_diterimaTU' => $row->tgl_terima_surat,
                        'tgl_dikirim' => simple_date($row->d_entry),
                        'kepada' => $row->n_pegawai,
                        'perihal' => $row->prihal,
                        'n_sifatsurat' => $row->n_sifatsurat,
                        'status_dibaca' => $row->status,
                        'id_persuratan' => $row->id_persuratan,
                        'id_compose' => $row->id_compose,
                        'id_sifatsurat' => $row->id_sifatsurat,
                        'photo' => $row->photo
                        //'id_compose' => 2
                    ]);
                }

                $o = [
                    'status' => 1,
                    'page' => $page,
                    'total_page' => $this->mymodel->c_all_surat_keluar($pegawai_id) / $per_page,
                    'data' => $output
                ];
                $this->response($o, Auth::HTTP_OK);
            }
        }
    }

    public function filter_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $per_page = $this->get('per_page', true);
        $page = $this->get('page', true);
        $search = $this->get('search', true);
        $output = [];
        $filter_by = '';

        if (!$per_page || !$page) {
            $output = [
                'status' => 0,
                'message' => 'Parameter request per_page & page tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            $exp = explode('-', $search);
            if ($exp[0] == 'b') {
                $where = ' AND tr_surat_penerima.status = ' . $exp[1];
                if ($exp[1] == 0) {
                    $filter_by = 'Belum dibaca';
                } elseif ($exp[1] == 1) {
                    $filter_by = 'Sudah dibaca';
                }
            } elseif ($exp[0] == 'c') {
                $where = ' AND tmpersuratan.id_sifatsurat = ' . $exp[1];
                if ($exp[1] == 1) {
                    $filter_by = 'Sangat Segera';
                } elseif ($exp[1] == 2) {
                    $filter_by = 'Segera';
                } elseif ($exp[1] == 3) {
                    $filter_by = 'Rahasia';
                } elseif ($exp[1] == 4) {
                    $filter_by = 'Biasa';
                }
            } elseif ($exp[0] == 's') {
                $v_search = substr($search, 2);
                $where = " 	AND (tmpersuratan.prihal LIKE '%" . $v_search . "%'
							OR tmpersuratan.dari LIKE '%" . $v_search . "%'
							OR tmpersuratan.no_surat LIKE '%" . $v_search . "%'
							OR tmpersuratan.tgl_surat LIKE '%" . $v_search . "%'
							OR tmpersuratan.d_awal_kegiatan LIKE '%" . $v_search . "%'
							OR tmpersuratan.d_akhir_kegiatan LIKE '%" . $v_search . "%'
							OR tmpersuratan.v_kegiatan LIKE '%" . $v_search . "%'
							OR tmpersuratan.isi_surat LIKE '%" . $v_search . "%'
							OR tmpegawai.n_pegawai LIKE '%" . $v_search . "%')";
            }

            if ($page == 1) {
                $offset = 0;
            } else {
                $page = $page - 1;
                $offset = $page * $per_page;
            }

            $query = $this->mymodel->surat_keluar($pegawai_id, $per_page, $offset, $where);
            if ($query->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Tidak ada surat.'
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                foreach ($query->result() as $row) {
                    array_push($output, [
                        'asal_surat' => $row->dari,
                        'tgl_dibuat' => $row->tgl_surat,
                        'tgl_diterimaTU' => $row->tgl_terima_surat,
                        'tgl_dikirim' => simple_date($row->d_entry),
                        'pengirim' => $row->n_pegawai,
                        'perihal' => $row->prihal,
                        'n_sifatsurat' => $row->n_sifatsurat,
                        'status_dibaca' => $row->status,
                        'id_persuratan' => $row->id_persuratan,
                        'id_compose' => $row->id_compose,
                        'id_sifatsurat' => $row->id_sifatsurat,
                        'photo' => $row->photo,
                        'filter_by' => $filter_by
                        //'id_compose' => 2
                    ]);
                }

                $o = [
                    'status' => 1,
                    'page' => $page,
                    'total_page' => $this->mymodel->c_all_surat_masuk($pegawai_id) / $per_page,
                    'data' => $output
                ];
                $this->response($o, Auth::HTTP_OK);
            }
        }
    }

    public function count_all_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $o = [
            'status' => 1,
            'count' => $this->mymodel->c_all_surat_keluar($pegawai_id)
        ];
        $this->response($o, Auth::HTTP_OK);
    }

    public function count_new_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $o = [
            'status' => 1,
            'count' => $this->mymodel->c_new_surat_keluar($pegawai_id)
        ];
        $this->response($o, Auth::HTTP_OK);
    }

    public function read_get()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $id_persuratan = $this->get('id_persuratan', true);
        $output = [];

        if (!$id_persuratan) {
            $output = [
                'status' => 0,
                'message' => 'id_persuratan tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            if ($this->tr_surat_penerima->cek2($pegawai_id, $id_persuratan)->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Anda tidak mendapatkan disposisi surat ini.',
                    'id_persuratan' => $id_persuratan,
                    'pegawai' => $pegawai_id
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                foreach ($this->tr_surat_penerima->read_all($pegawai_id, $id_persuratan)->result() as $row) {
                    $data = [
                        'status' => 1,
                        'd_read' => date('Y-m-d H:i:s')
                    ];
                    $this->tr_surat_penerima->update($row->id, $data);
                }

                $disposisi = [];
                $row = $this->mymodel->read($id_persuratan)->row();
                foreach ($this->mymodel->history_all($id_persuratan, $pegawai_id)->result() as $row_history) {
                    $d_entry = $row_history->d_entry;
                    $ke_pegawai = $row_history->ke_pegawai . '; ';
                    $ke_pegawai_cc = '';
                    if ($row_history->num > 1) {	//Jika didisposisikan lebih dari satu pegawai
                        $ke_pegawai = '';
                        foreach ($this->mymodel->read_to($id_persuratan, $d_entry, 0)->result() as $row_to) {
                            $ke_pegawai .= $row_to->ke_pegawai . '; ';
                        }

                        $cc = $this->mymodel->read_to($id_persuratan, $d_entry, 1);
                        if ($cc->num_rows() != 0) {
                            foreach ($cc->result() as $row_cc) {
                                $ke_pegawai_cc .= $row_cc->ke_pegawai . '; ';
                            }
                        }
                    }
                    $date_d = date_create($row_history->d_entry);
                    $date_f = date_format($date_d, "Y-m-d");


                    array_push($disposisi, [
                        'dari_pegawai' => $row_history->dari_pegawai,
                        'dari_photo' => $row_history->photo,
                        'file_coretan' => $row_history->file_coretan,
                        'ke_pegawai' => $ke_pegawai,
                        'ke_pegawai_cc' => $ke_pegawai_cc,
                        'tgl' => $date_f,
                        'catatan' => $row_history->catatan,
                        'file_surat_koreksi' => $row_history->file_surat_koreksi,
                        'lampiran_surat_koreksi' => $row_history->lampiran_file_koreksi
                    ]);
                }
                $info = $this->mymodel->info($id_persuratan)->result();
                $ttd = false;
                if ($pegawai_id == $row->id_pegawaittd) {
                    $ttd = 1;
                }
                $output = [
                    'status' => 1,
                    'no_surat' => $row->no_surat,
                    'dari' => $row->dari,
                    'tgl_surat' => $row->tgl_surat,
                    'd_awal_kegiatan' => $row->d_awal_kegiatan,
                    'd_akhir_kegiatan' => $row->d_akhir_kegiatan,
                    'v_kegiatan' => $row->v_kegiatan,
                    'perihal' => $row->prihal,
                    'n_sifatsurat' => $row->n_sifatsurat,
                    //'n_jenissurat' => $row->n_jnissurat,
                    'memo' => $row->isi_surat,
                    'file' => $row->file,
                    'file_lampiran' => $row->file_lampiran,
                    //'c_lintas_skpd' => $row->c_lintas_skpd,
                    'id_compose' => $row->id_compose,
                    //'id_compose' => 2,
                    'ttd' => $ttd,
                    //'ttd' => 1,
                    'status_ttd' => $row->status_setujui,
                    'disposisi' => $disposisi,
                    'info' => $info
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        }
    }

    // function info_get()
    // 	{
    // 		$decode = $this->viewtoken();
    // 		$pegawai_id = $decode->pegawai_id;
    // 		$id_persuratan = $this->get('id_persuratan',TRUE);
    // 		$output = array();
//
    // 		if(!$id_persuratan)
    // 		{
    // 			$output = array(
    // 				'status' => FALSE,
    // 				'message' => "id_persuratan tidak boleh kosong."
    // 			);
    // 			$this->response($output,Auth::HTTP_BAD_REQUEST);
    // 		}
    // 		else
    // 		{
    // 			$output = array();
    // 			$result = $this->tr_surat_penerima->info($id_persuratan);
    // 			foreach($result->result() as $row_info){
    // 				if($row_info->paraf != ''){
    // 					$paraf = base_url().$row_info->paraf;
    // 				}else{
    // 					$paraf = "*) paraf belum diunggah";
    // 				}
//
    // 				if($row_info->d_read != ''){
    // 					$tgl_baca = $row_info->d_read;
    // 				}else{
    // 					$tgl_baca = "<i class='text-warning'>* belum dibaca</i>";
    // 				}
//
    // 				array_push($output, array(){
    // 					'tgl_kirim' => $row_info->d_entry,
    // 					'dari' => $row_info->n_pegawai_dari.' ('.$row_info->n_jabatan_dari.' '.$row_info->n_unitkerja_dari.')',
    // 					'paraf' => $paraf,
    // 					'ke' => $row_info->n_pegawai_ke.' ('.$row_info->n_jabatan_ke.' '.$row_info->n_unitkerja_ke.')',
    // 					'tgl_baca' => $tgl_baca
    // 				});
    // 			}
    // 			$this->response($output,Auth::HTTP_OK);
    // 		}
    // 	}

    public function listdisposisi_get()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $id_persuratan = $this->get('id_persuratan', true);
        $output = [];

        $query = $this->mymodel->disposisi($auth_id);
        if ($query->num_rows() == 0) {
            $output = [
                'status' => false,
                'message' => 'List disposisi kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            foreach ($query->result() as $row) {
                array_push($output, [
                    'status' => 1,
                    'id_pegawai' => $row->id,
                    'n_jabatan' => $row->n_jabatan,
                    'n_unitkerja' => $row->n_unitkerja,
                    'n_pegawai' => $row->n_pegawai,
                    'telp' => $row->telp
                ]);
            }
            $this->response($output, Auth::HTTP_OK);
        }
    }
}
