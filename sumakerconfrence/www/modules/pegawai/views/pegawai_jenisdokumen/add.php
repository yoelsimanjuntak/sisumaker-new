<div class="widgetbox box-inverse view_load">
	<?php echo $this->load->view($this->view.'validate');?>
	<h4 class="widgettitle"></h4>
	<div class="widgetcontent wc1">
		<form id="form1" class="stdform" method="post" action="<?php echo site_url().$this->page.'save';?>" novalidate="novalidate">
			<input type="hidden" name="tmpegawai_id" value="<?php echo $tmpegawai_id;?>"/>
			<div class="par control-group">
				<label class="control-label" for="tmjenisdokumen_id">Jenis Dokumen</label>
				<div class="controls">
					<select name="tmjenisdokumen_id">
						<option value="">Pilih</option>
						<?php foreach($result_tmjenisdokumen->result() as $row_tmjenisdokumen){ ?>
						<option value="<?php echo $row_tmjenisdokumen->id;?>"><?php echo $row_tmjenisdokumen->n_jenisdokumen;?></option>
						<?php } ?>
					</select>
				</div>
			</div>			
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action">Simpan</button>
				<a href="#" id="close_add" class="btn btn-default">Batal</a>
			</p>
		</form>
	</div>
</div>