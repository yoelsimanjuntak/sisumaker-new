<div class="widgetbox box-inverse">
	<?php echo $this->load->view($this->view.'validate');?>
	<h5 class="widgettitle"></h5>
	<div class="widgetcontent wc1">
		<form id="form1" class="stdform" action="<?php echo site_url().$this->page.'filter';?>" method="post" enctype="multipart/form-data">
			<div class="par control-group">
				<label class="control-label" for="tgl_surat">Dari Tanggal</label>
				<div class="controls"><input type="text" name="d_dari" id="d_dari" class="datepicker_eng"></div>
			</div>
			<div class="par control-group">
				<label class="control-label" for="d_sampai">Sampai Tanggal</label>
				<div class="controls"><input type="text" name="d_sampai" id="d_sampai" class="datepicker_eng"></div>
			</div>
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action">Filter</button>
			</p>
		</form>
	</div>
</div>