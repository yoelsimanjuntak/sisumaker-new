<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,no-store,must-revalidate"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="expires" content="0"/>
        <title><?php echo $title; ?></title>
		<?php $base_url = base_url(); ?>
		<link rel="shortcut icon" href="<?php echo $base_url;?>assets/images/favicon.png"/>
        <?php
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.red.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.mail.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "jquery.ui.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "responsive-tables.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/prettify/", "prettify.css");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-2.0.3.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.2.1.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.slimscroll.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "function.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "chat.js");
        foreach ($assets as $asset_js_css) {
            echo $asset_js_css;
        }
        ?>
        <script type="text/javascript">base='<?php echo $base_url; ?>';</script> 
        <!--[if lte IE 8]><?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "excanvas.min.js"); ?><![endif]-->
        <!--<?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "elements.js"); ?>-->
		<style>
			.header{
				background: url('<?php echo $base_url;?>assets/images/header_dashboard.png');
				background-repeat: no-repeat;
				background-position: center;
				height: 200px;
			}
		</style>
    </head>
    <body style="background-image:url('<?php echo $base_url.'assets/images/dashboard.png';?>')">
        <div class="mainwrapper">
            <div class="header">
				<a href="<?php echo $base_url;?>"><div class="logo"></div></a>
				<div class="headerinner"></div>
			</div>            
			<div class="maincontent">
				<div class="maincontentinner">
					<div class="row-fluid">
						<div class="span8">
							<span id="addVar">
								<div class="navbar" style="margin-bottom:10px;">
									<div class="navbar-inner">
										<a href="#" class="brand">Filter Data</a>
										<div class="navbar-form">
											<select name="type_search" class="span6" id="search">
												<option value="">Semua</option>
												<?php foreach($result_skpd as $row){ ?>
												<option<?php if($id_skpd == $row->id){ ?> selected="selected"<?php } ?>  value="<?php echo $row->id;?>"><?php echo $row->n_skpd;?></option>
												<?php } ?>
											</select>
											<a href="#" id="reload" class="btn pull-right"><i class="iconfa-refresh"></i> Refresh</a>
											
										</div>
									</div>
								</div>
							</span>
							<table id="dyntable" class="table table-bordered">
								<colgroup>
									<col class="con0" style="align: center; width: 4%" />
									<col class="con1" />
									<col class="con0" />
									<col class="con1" />
									<col class="con0" />
									<col class="con1" />
									<col class="con0" />
								</colgroup>
								<thead>
									<tr>
										<th width="15px">No</th>
										<th width="140px">Perihal</th>
										<th width="100px">Asal Surat</th>
										<th width="100px">Tgl Kegiatan</th>
										<th width="200px">Lokasi</th>
										<th width="200px">SKPD</th>
										<th width="200px">Kehadiran</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td valign="top" colspan="7" class="dataTables_empty">Loading <img src="<?php echo base_url() . 'assets/images/loaders/loader19.gif'; ?>"/></td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<div class="span4">
							<div class="widgetcontent">
								<div style="width:100%">
									<div id="chartplace" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
								</div>
							</div>	
							<div id='script-warning'>
								<code>php/get-events.php</code> must be running.
							</div>
							<div class="widgetbox box-inverse">	
								<h4 class="widgettitle nopadding"></h4>
								<div class="widgetcontent wc1">
									<div id='loading'>loading...</div>
									<div id='calendar'></div>
								</div>
							</div>
						</div>
					</div><br/>
					<!--
					<h5 class="subtitle">Data Detail</h5>
					<div class="row-fluid">
						<div class="span4">
							<div class="widgetbox box-inverse">	
								<h4 class="widgettitle">Penggunaan Aplikasi</h4>
								<div class="widgetcontent nopadding">
									<table class="table table-detail">
										<tr>
											<td width="200"><i class="iconfa-user"></i> &nbsp;User</td>
											<td>1</td>
										</tr>
										<tr>
											<td><i class="iconfa-inbox"></i> &nbsp;Surat Masuk</td>
											<td>1</td>
										</tr>
										<tr>
											<td><i class="iconfa-plane"></i> &nbsp;Surat Keluar</td>
											<td>1</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="span8">
							<div class="widgetbox box-inverse">	
								<h4 class="widgettitle nopadding"></h4>
								<div class="widgetcontent" style="min-height:135px;">
									<div class="row-fluid">
										<div class="span4">
											<table class="table table-detail" border="1">
												<tr>
													<td rowspan="4" width="120">Profil</td>
													<td>NAMA</td>
												</tr>
												<tr>
													<td>NIP</td>
												</tr>
												<tr>
													<td>TELP</td>
												</tr>
											</table>
										</div>
										<div class="span4">
											<center>Inih</center>
										</div>
										<div class="span4">
											<center>Inih</center>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>-->
					<div class="footer">
						<div class="footer-left">
							<span>Copyright &copy; 2015. SISUMAKER (Sistem Informasi Surat Masuk dan Keluar)</span>
						</div>
						<div class="footer-right">
							<span>Copyright &copy; 2015. Kota Tangerang Selatan</span>
						</div>
					</div>
				</div>
			</div>
        </div>
		<style>
		#dyntable{
			min-height:800px;
		}
		.navbar-inner, .dataTables_length, .dataTables_info, #search, .dataTables_length select, .dataTables_filter input{
			background: rgba(255,255,255,.70)
		}
		.table .con0{
			background: rgba(255,255,255,.80)
		}
		.table .con1{
			background: rgba(255,255,255,.70)
		}
		.subtitle{
			font-size: 14px;
			color:  #ccc;
			margin-bottom: 15px;
		}
		.box-inverse .widgetcontent{
			border-color: rgba(255,255,255,.70)
		}
		.highcharts-background
        {
            fill: rgba(255,255,255,.10);
            color: #ffffff;
        }
		#script-warning {
			display: none;
			background: #eee;
			border-bottom: 1px solid #ddd;
			padding: 0 5px;
			line-height: 40px;
			text-align: center;
			font-weight: bold;
			font-size: 12px;
			color: red;
		}
		#loading {
			display: none;
			position: absolute;
			top: 10px;
			right: 10px;
		}
		#calendar {
			max-width: 900px;
			margin: 20px auto;
			padding: 0 10px;
		}
		.fc-event-container a{
			color:#ccc;
		}
		.table-detail{
			margin:5px 10px 10px 5px;
		}
		.table-detail td{
			border-top:none;
		}
		</style>
		<script type="text/javascript">
			var oTable;
			datatables_ps("<?php echo site_url() . $this->page . 'data/'.$id_skpd; ?>", <?php echo $sort; ?>);
			jQuery(document).ready(function(){
				jQuery('body').removeClass('chatenabled');
				Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
					return {
						radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
						stops: [
							[0, color],
							[1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
						]
					};
				});
				jQuery('#chartplace').highcharts({
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Surat Masuk dan Keluar, 2015'
					},
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								format: '<b>{point.name}</b>: {point.percentage:.1f} %',
								style: {
									color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								}
							}
						}
					},
					series: [{
						type: 'pie',
						name: 'Jumlah',
						data: [
							['Surat Keluar',  <?php echo $s_keluar;?>],
							{
								name: 'Surat Masuk',
								y: <?php echo $s_masuk;?>,
								sliced: true,
								selected: true
							}
						]
					}]
				});

				jQuery('#search').on('change', function(){
					document.location.href = base + 'dashboard/index/' + jQuery(this).val();
				});
				jQuery('#reload').on('click', function(){
					oTable.fnDraw();
				});
				
				jQuery('#calendar').fullCalendar({
					//theme: true,
					lang: 'id',
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,agendaDay'
					},
					//defaultDate: '<?php echo date('Y-m-d');?>',
					editable: false,
					eventLimit: true, // allow "more" link when too many events
					events: {
						//url: base + '<?php echo $this->page;?>get_data',
						error: function() {
							jQuery('#script-warning').show();
						}
					},
					loading: function(bool) {
						jQuery('#loading').toggle(bool);
					}
				});
				jQuery(".fc-h-event").live('click',function(event){
					event.preventDefault();
						jQuery.fancybox.open({
							href 			: jQuery(this).attr("href"),
							type 			: 'iframe',
							padding 		: 2,
							opacity			: true,
							titlePosition	: 'over',
							openEffect 		: 'elastic',
							openSpeed  		: 150,
							closeEffect 	: 'elastic',
							closeSpeed  	: 150,
							width			: 900,
							helpers : {
								title : {
									type : 'inside'
								},
								overlay : {
									css : {
										'background' : 'transparent',
										'background' : 'rgba(0, 0, 0, 0.6)'
									}
								}
							}
					});
				});
			});
		</script>
    </body>
</html>