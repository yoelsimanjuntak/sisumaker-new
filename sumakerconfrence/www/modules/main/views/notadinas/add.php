<div class="widgetbox box-inverse">
	<?php echo $this->load->view($this->view.'validate');?>
	<h4 class="widgettitle nopadding">
		<span style="margin:10px;" id="compose_caption">Nota Dinas</span>
		<a href="<?php echo base_url().'main/inbox';?>" class="pull-right btn"><i class="icon-remove nopadding"></i></a>
		<div style="clear:both"></div>
	</h4>
	<div class="widgetcontent wc1">
		<form id="form1" class="stdform" action="<?php echo site_url().$this->page.'save';?>" method="post" enctype="multipart/form-data">
			<!--<div class="control-group">
				<label class="control-label" for="id_jnssurat">Jenis Surat</label>
				<div class="controls">
					<select id="id_jnssurat" name="id_jnssurat" class="span5" onchange="dn_file()">
						<option value="">Pilih</option>
						<?php foreach($result_jnssurat->result() as $row_jnssurat){?>
						<option value="<?php echo $row_jnssurat->id;?>"><?php echo $row_jnssurat->n_jnissurat;?></option>
						<?php }?>
					</select>
					<span id="v_dn_file"></span>
				</div>
			</div>-->
			<input type="hidden" id="c_skpd" name="c_skpd" value="0-0"/> 
			<input type="hidden" id="c_kecamatan" name="c_kecamatan" value="0-0"/> 
			<input type="hidden" id="c_kelurahan" name="c_kelurahan" value="0-0"/> 
			<input type="hidden" id="c_puskesmas" name="c_puskermas" value="0-0"/> 
			<input type="hidden" id="c_sekolah" name="c_sekolah" value="0-0"/> 
			<input type="hidden" id="c_rs" name="c_rs" value="0-0"/> 
			<input type="hidden" id="c_uptpendidikan" name="c_uptpendidikan" value="0-0"/>
			<div class="control-group">
				<label class="control-label" for="id_compose">Jenis Surat</label>
				<div class="controls" style="padding:5px 2px;">
					<input type="radio" name="id_compose" value="1" checked="checked" id="id_compose" tt="Nota Dinas"> Nota Dinas &nbsp; &nbsp;
					<input type="radio" name="id_compose" value="2" id="id_compose" tt="Surat Keluar"> Surat Keluar
				</div>
			</div>			
			<div class="control-group" id="no_surat">
				<label class="control-label" for="no_surat">Nomor Surat</label>
				<div class="controls"><input type="text" name="no_surat" style="width:295px;"></div>
			</div>
			<div class="control-group" id="v_pegawaittd" style="margin-bottom:9px">
				<label class="control-label" for="id_pegawaittd">Akan Di TTD Oleh</label>
				<div class="controls">
					<select id="id_pegawaittd" name="id_pegawaittd" required="" data-placeholder="Jabatan - Nama Pegawai" class="span5 chzn-select" style="z-index:99999;" tabindex="3">
						<?php foreach($result_ttdsekda->result() as $row_ttdsekda){?>
						<option value="<?php echo $row_ttdsekda->id;?>"><?php echo $row_ttdsekda->n_unitkerja.' - '.$row_ttdsekda->n_pegawai;?></option>
						<?php }foreach($result_ttdskpd->result() as $result_ttdskpd){?>
						<option value="<?php echo $result_ttdskpd->id;?>"><?php echo $result_ttdskpd->n_unitkerja.' - '.$result_ttdskpd->n_pegawai;?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="pegawai">Kepada</label>
				<div class="controls" id="view_list_pegawai">
					<select id="pegawai" name="pegawai[]" required="" data-placeholder="Jabatan - Nama Pegawai" class="span7 chzn-select" multiple="multiple" style="z-index:99999;" tabindex="3">
						<?php foreach($result_disposisi->result() as $row_disposisi){?>
						<option value="<?php echo $row_disposisi->id;?>"><?php echo $row_disposisi->n_jabatan.' '.$row_disposisi->n_unitkerja.' - '.$row_disposisi->n_pegawai;?></option>
						<?php }?>
					</select>
				</div>
				<?php if($this->session->userdata('auth_id') == '359' || $this->session->userdata('auth_id') == '360'){ ?>
				<small class="desc">
					Semua Kepala&nbsp;
					<a style="cursor:pointer" id="check_skpd">SKPD</a> | 
					<a style="cursor:pointer" id="check_kecamatan">Kecamatan</a> | 
					<a style="cursor:pointer" id="check_kelurahan">Kelurahan</a> | 
					<a style="cursor:pointer" id="check_puskesmas">Puskesmas</a> | 
					<!--<a style="cursor:pointer" id="check_sekolah">Sekolah</a> | -->
					<a style="cursor:pointer" id="check_rs">Rumah Sakit</a> | 
					<a style="cursor:pointer" id="check_uptpendidikan">UPT Pendidikan</a>
				</small>
				<?php } ?>
			</div>
			<div class="row-fluid">
				<div class="span7">
					<div class="control-group">
						<label class="control-label" for="pegawai_cc">CC</label>
						<div class="controls">
							<select id="pegawai_cc" name="pegawai_cc[]" required="" data-placeholder="Jabatan - Nama Pegawai" class="span6 chzn-select" multiple="multiple" style="z-index:99999;" tabindex="3">
								<?php foreach($result_disposisi->result() as $row_disposisi){?>
								<option value="<?php echo $row_disposisi->id;?>"><?php echo $row_disposisi->n_jabatan.' '.$row_disposisi->n_unitkerja.' - '.$row_disposisi->n_pegawai;?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="tgl_surat">Tanggal Surat</label>
						<div class="controls"><input type="text" name="tgl_surat" id="tgl_surat" class="input-medium" placeholder="0000-00-00"></div>
					</div>
					<div class="control-group" id="cek_prihal">
						<label class="control-label" for="prihal">Perihal</label>
						<div class="controls"><input type="text" name="prihal" id="prihal" class="span6"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="id_sifatsurat">Sifat Surat</label>
						<div class="controls">
							<select name="id_sifatsurat" class="input-medium" id="id_sifatsurat">
								<option value="">Pilih</option>
								<?php foreach($result_tmsifat_surat->result() as $row_tmsifat_surat){ ?>
								<option value="<?php echo $row_tmsifat_surat->id;?>"><?php echo $row_tmsifat_surat->n_sifatsurat;?></option>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
				<div class="span5">
					<b>Diisi jika perlu ada kegiatan:</b>
					<div class="control-group">
						<label class="control-label" for="d_awal_kegiatan">Tanggal Mulai</label>
						<div class="controls"><input type="text" name="d_awal_kegiatan" id="d_awal_kegiatan" class="input-medium datepicker_eng_time" placeholder="0000-00-00 00:00:00"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="d_akhir_kegiatan">Tanggal Akhir</label>
						<div class="controls"><input type="text" name="d_akhir_kegiatan" id="d_akhir_kegiatan" class="input-medium datepicker_eng_time" placeholder="0000-00-00 00:00:00"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="v_kegiatan">Lokasi</label>
						<div class="controls"><textarea name="v_kegiatan" id="v_kegiatan" class="input-medium" placeholder="exp: Lokasi"></textarea></div>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="isi_surat">Memo Surat</label>
				<div class="controls" style="margin-left:140px;">
					<textarea name="isi_surat" rows="5" cols="60" class="tinymce"></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="userfile">File Surat</label>
				<div class="controls">
					<span class="fileupload fileupload-new" data-provides="fileupload" id="fileupload1" style="padding:0px;margin-bottom:-10px;">
						<div class="input-append">
							<div class="uneditable-input span2">
								<i class="iconfa-file fileupload-exists"></i>
								<span class="fileupload-preview"></span>
							</div>
							<span class="btn btn-file">
								<span class="fileupload-new">Pilih file</span>
								<span class="fileupload-exists" name="file_upload_a">Ubah</span>
								<input type="file" name="userfile" id="userfile"/>
							</span>
							<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" name="file_upload_a">Hapus</a>
						</div>
					</span>
					&nbsp; <sup class="text-info label label-info" style="font-weight:bold"> &nbsp; .doc&nbsp;</sup>
					&nbsp; <sup class="text-info label label-warning" style="font-weight:bold"> &nbsp; .ppt&nbsp;</sup>
					&nbsp; <sup class="text-info label label-warning" style="font-weight:bold"> &nbsp; .pdf&nbsp;</sup>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="file_lampiran">File Lampiran</label>
				<div class="controls">
					<span class="fileupload fileupload-new" data-provides="fileupload" id="fileupload1" style="padding:0px;margin-bottom:-10px;">
						<div class="input-append">
							<div class="uneditable-input span2">
								<i class="iconfa-file fileupload-exists"></i>
								<span class="fileupload-preview"></span>
							</div>
							<span class="btn btn-file">
								<span class="fileupload-new">Pilih file</span>
								<span class="fileupload-exists" name="file_upload_a">Ubah</span>
								<input type="file" name="file_lampiran" id="file_lampiran"/>
							</span>
							<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" name="file_upload_a">Hapus</a>
						</div>
					</span>
					&nbsp; <sup class="text-info label label-success" style="font-weight:bold"> &nbsp; .xls&nbsp;</sup>
				</div>
			</div>
			<p class="stdformbutton">
				<a id="check_paraf" to="<?php echo base_url().$this->page.'varifikasi_pass';?>" style="cursor:pointer" class="btn"><i class="iconfa-check"></i> Paraf</a>
				<a id="send" to="<?php echo base_url().$this->page.'send_paraf';?>" class="btn btn-primary" style="display:none"><i class="iconfa-plane"></i> Kirim</a>
				<!--<a href="<?php echo base_url().'main/inbox';?>" class="btn btn-default">Batal</a>-->
			</p>
		</form>
	</div>
</div>