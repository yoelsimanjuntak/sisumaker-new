<?php
class onesignal
{
    protected $data;
    protected $smsserverip;
    public function setData($data)
    {
        $this->data = $data;
    }

    public function sendMessage()
    {
        $this->android();
    }



    public function android()
    {
        // $dt=json_encode($this->data);
        $data = $this->data;
        $content = array(
            "en" => $data['perihal']
        );

        $fields = array(
            'app_id' => "1b786f8c-4e62-4d96-bffc-97a27e5b87d0",
            'filters' => array(
                array("field" => "tag", "key" => "pegawai_id", "relation" => "=", "value" => $data['ke']),
                array("operator" => "AND"),
                array("field" => "tag", "key" => "domain", "relation" => "=", "value" => 'http://103.219.112.2/sisumakertebingtinggi')
            ),
            // 'include_player_ids' => array("02dedbee-4528-4d7e-8f6e-4089f6face3f"),
            // 'included_segments' => array('All'),
            'data' => array("type" => $data['type'], "id_surat" => $data['id_surat']),
            'headings' => ['en' => $data['dari']],
            'contents' => $content
        );

        $fields = json_encode($fields);
        // print("\nJSON sent:\n");
        // print($fields);


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic MGRiNzhhNGEtMDJhOC00NTk4LTlhZTEtNGZkOGVlZDNmNjdj'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            $output = [
                'status' => 0,
                'message' => curl_error($ch)
            ];
            // print "Error: " . curl_error($ch);
        } else {
            $output = [
                'status' => 1,
                'message' => 'success'
            ];
        }
        curl_close($ch);

        $f = [
            's' => $output,
            'r' => $response
        ];

        return $f;
    }
}
