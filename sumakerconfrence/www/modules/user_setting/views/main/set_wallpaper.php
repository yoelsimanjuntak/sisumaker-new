<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#change-wallpaper').live('click', function(){
			jQuery('body').css({"background-image" : "url(" + jQuery(this).attr("link-file") + ")"});
			jQuery.ajax({
				"type":"post",
				"cache":false,
				"url":jQuery(this).attr("to"),
				"success":function(){
					//jQuery('body').css({"background" : "url(" + jQuery(this).attr("link-file") + ")"});
				}
			});
			return false;
		});
	});
</script>
<div class="widgetbox box-inverse">
    <h4 class="widgettitle">Setting Wallpaper</h4>
    <div class="widgetcontent wc1">
		<div class="mediamgr_content">
			<div class="row-fluid">
				<div class="span10">
					<ul id="medialist" class="listfile">
						<?php foreach($result as $row){ ?>
						<li class="image">
						  <a href="#" to="<?php echo base_url().$this->page.'change_wallpaper/'.$row->id;?>" link-file="<?php echo base_url().$row->link;?>" id="change-wallpaper"><img src="<?php echo base_url().$row->link;?>" alt="<?php echo $row->n_wallpaper;?>"/></a>
							<span class="filename"><?php echo $row->n_wallpaper;?></span>
					   </li>
						<?php } ?>
					</ul>
				</div>
				<div class="span2">
					<div style="border-bottom: 1px dashed #000">Upload File</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.image a img{
	max-width:100px;
}
</style>