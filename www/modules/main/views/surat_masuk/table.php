<div class="messagepanel">
	<div class="messagemenu">
		<ul>
			<li class="no-active"><a href="<?php echo base_url().'main/inbox';?>"><span class="iconfa-inbox"></span> Inbox <?php if($newpesan != 0){ ?><span class="new-pesan"><?php echo $newpesan;?> baru</span><?php }?></a></li>
			<li class="no-active"><a href="<?php echo base_url().'main/outbox';?>"><span class="iconfa-plane"></span> Outbox</a></li>
			<?php if($auth_masuk != 0){ ?>
			<li class="active"><a href="<?php echo base_url().'main/surat_masuk';?>"><span class="iconfa-envelope"></span> Surat Masuk <?php if($newsurat_masuk != 0){ ?><span class="new-pesan"><?php echo $newsurat_masuk;?> baru</span><?php }?></a></li>
			<?php }?>
		</ul>
	</div>
	<div class="messagecontent" style="border-right:1px solid #bb2f0e">
		<div class="messageleft" style="width:100%;">
			<div class="row-fluid messagesearch">
				<div class="span9">
					<button class="btn" id="refresh"><i class="icon-repeat"></i> Reload</button>
					<div class="pull-right"> </div>
				</div>
				<div class="span3">
				</div>
			</div>
			<script type="text/javascript">
				var oTable;
				datatables_ps("<?php echo site_url().$this->page.'data/'.$this->session->userdata('pegawai_id');?>", 0);
				fancybox();
				jQuery(document).ready(function(){
					jQuery('#refresh').live('click',function(){
						oTable.fnDraw();
					});
				});
			</script>
			<div id="result"></div>
			<table id="dyntable" class="table table-bordered" style="margin-top:1px;">
				<colgroup>
					<col class="con0" style="align: center; width: 4%" />
					<col class="con1" />
					<col class="con0" />
					<col class="con1" />
					<col class="con0" />
					<col class="con1" />
					<col class="con0" />
					<col class="con1" />
				</colgroup>
				<thead>        
					<tr>
						<th width="15px">No</th>
						<th width="140px">Dari</th>
						<th width="120px">Tanggal Surat</th>
						<th>Perihal</th>
						<th width="100px">Sifat Surat</th>
						<th width="100px">Status</th>
						<th width="100px">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td valign="top" colspan="8" class="dataTables_empty">Loading <img src="<?php echo base_url().'assets/images/loaders/loader19.gif';?>"/></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>