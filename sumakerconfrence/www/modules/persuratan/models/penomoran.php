<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Penomoran extends DataMapper 
{

    var $table = 'penomoran';
    var $key = 'id';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here
	function nomor($id_skpd, $id_jenissurat)
	{
		$thn = date('y');
		$query = $this->db
			->select('nomor')
			->from($this->table)
			->where('id_jenissurat', $id_jenissurat)
			->where('id_skpd', $id_skpd)
			->where('tahun', $thn)
			->get();
		
		if($query->num_rows() != 0)
		{
			foreach($query->result() as $row)
			{
				$nomor = $row->nomor + 1;
			}
			$return = $nomor;
		}
		else
		{
			$return = 1;
		}
		$a = strlen($return);
		for ($i = 4; $i > $a; $i--) {
			$return = "0" . $return;
		}
		return $return;
	}
	
	function nomor_save($id_skpd, $id_jenissurat)
	{
		$thn = date('y');
		$query = $this->db
			->select('id, nomor')
			->from($this->table)
			->where('id_jenissurat', $id_jenissurat)
			->where('id_skpd', $id_skpd)
			->where('tahun', $thn)
			->get();
		
		if($query->num_rows() != 0)
		{
			foreach($query->result() as $row)
			{
				$nomor = $row->nomor + 1;
				$id = $row->id;
			}
			$form = array('nomor' => $nomor);
			$this->update($id, $form);
			$return = $nomor;
		}
		else
		{
			$form = array(
				'id_jenissurat' => $id_jenissurat,
				'id_skpd' => $id_skpd,
				'tahun' => $thn,
				'nomor' => 1
			);
			$this->insert($form);
			$return = 1;
		}
		$a = strlen($return);
		for ($i = 4; $i > $a; $i--) {
			$return = "0" . $return;
		}
		return $return;
	}
	
    function insert($form) 
	{
        return $this->db
			->insert($this->table, $form);
    }

    function update($id, $form) 
	{
        return $this->db
			->where($this->key, $id)
			->update($this->table, $form);
    }

}

?>
