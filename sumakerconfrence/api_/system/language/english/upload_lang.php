<?php
/*
$lang['upload_userfile_not_set'] = "Unable to find a post variable called userfile.";
$lang['upload_file_exceeds_limit'] = "The uploaded file exceeds the maximum allowed size in your PHP configuration file.";
$lang['upload_file_exceeds_form_limit'] = "The uploaded file exceeds the maximum size allowed by the submission form.";
$lang['upload_file_partial'] = "The file was only partially uploaded.";
$lang['upload_no_temp_directory'] = "The temporary folder is missing.";
$lang['upload_unable_to_write_file'] = "The file could not be written to disk.";
$lang['upload_stopped_by_extension'] = "The file upload was stopped by extension.";
$lang['upload_no_file_selected'] = "You did not select a file to upload.";
$lang['upload_invalid_filetype'] = "The filetype you are attempting to upload is not allowed.";
$lang['upload_invalid_filesize'] = "The file you are attempting to upload is larger than the permitted size.";
$lang['upload_invalid_dimensions'] = "The image you are attempting to upload exceedes the maximum height or width.";
$lang['upload_destination_error'] = "A problem was encountered while attempting to move the uploaded file to the final destination.";
$lang['upload_no_filepath'] = "The upload path does not appear to be valid.";
$lang['upload_no_file_types'] = "You have not specified any allowed file types.";
$lang['upload_bad_filename'] = "The file name you submitted already exists on the server.";
$lang['upload_not_writable'] = "The upload destination folder does not appear to be writable.";
*/

$lang ['upload_userfile_not_set'] = "Tidak dapat menemukan variabel posting disebut userfile.";
$lang ['upload_file_exceeds_limit'] = "File upload melebihi ukuran maksimum yang diperbolehkan ";
$lang ['upload_file_exceeds_form_limit'] = "File upload melebihi ukuran maksimum yang diizinkan .";
$lang ['upload_file_partial'] = "Berkas itu hanya sebagian upload.";
$lang ['upload_no_temp_directory'] = "Folder sementara hilang.";
$lang ['upload_unable_to_write_file'] = "File tidak dapat ditulis ke disk.";
$lang ['upload_stopped_by_extension'] = "File Upload dihentikan oleh ekstensi.";
$lang ['upload_no_file_selected'] = "Anda tidak memilih file untuk meng-upload.";
$lang ['upload_invalid_filetype'] = "Tipe file yang Anda upload tidak diperbolehkan.";
$lang ['upload_invalid_filesize'] = "Berkas yang Anda sedang di-upload lebih besar dari ukuran yang diizinkan.";
$lang ['upload_invalid_dimensions'] = "Gambar yang Anda upload exceedes ketinggian maksimum atau lebar.";
$lang ['upload_destination_error'] = "Masalah ditemui ketika mencoba untuk memindahkan file upload ke tujuan akhir.";
$lang ['upload_no_filepath'] = "Path meng-upload  tidak valid.";
$lang ['upload_no_file_types'] = "Anda belum menentukan jenis file yang diperbolehkan.";
$lang ['upload_bad_filename'] = "Nama file yang Anda kirimkan sudah ada pada server.";
$lang ['upload_not_writable'] = "Folder tujuan meng-upload tidak dapat diproses.";

/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */