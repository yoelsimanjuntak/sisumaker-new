<?php if ($result->num_rows() != 0) { foreach ($result->result() as $row) { 
	$one = 1;
	if($result_history->num_rows() != 1)
	{
	
			// 1. Jika surat sudah pernah didisposisikan		=> lihat tr_surat_terima
		$pegawai_id = $this->session->userdata('pegawai_id');
		foreach($result_history->result() as $row_history)
		{
			// $disposisi = 0;
			// if($row_history->id_pegawai_ke === $pegawai_id){$disposisi = 1;}
			// elseif($row_history->id_pegawai_dari === $pegawai_id){$disposisi = 1;}
			$disposisi = 1;
			if($disposisi == true or $one == 1)
			{
				$d_entry = $row_history->d_entry;
				$catatan = $row_history->catatan;
				$photo = $row_history->photo;
				$d_entry_temp = $row_history->d_entry;
				$dari_pegawai = $row_history->dari_pegawai;
				$ke_pegawai = $row_history->ke_pegawai.'; '; ?>
				
				<div class="msgauthor">
					<div class="thumb"><img src="<?php echo base_url().'uploads/photo/'.$photo; ?>" alt="" /></div>
					<div class="authorinfo">
						<span class="date pull-right"><?php echo indo_date_time($d_entry);?></span>
						<h5><strong><?php echo $dari_pegawai;?></strong></h5>
						<span class="to">to <?php echo $ke_pegawai;?></span>
						<?php if($catatan != ''){ ?>
						<blockquote>
							<p><?php echo $catatan;?></p>
							<small><?php echo $dari_pegawai;?></small>
						</blockquote>
						<?php } ?>
						<?php if($row_history->file_surat_koreksi != '') {?>
							<b><a href="<?php echo base_url().$row_history->file_surat_koreksi;?>" class="btn"><i class="iconfa-paper-clip"></i> File Koreksi Surat</a></b>
						<?php }?>
					</div>
				</div>
				<?php if($one == 1){ ?>
				<div class="msgbody">
					<table class="table table-bordered table-invoice">
						<colgroup>
							<col class="con1" />
							<col class="con0" />
							<col class="con1" />
							<col class="con0" />
						</colgroup>
						<tbody>
							<tr>
								<td width="120px">Surat Dari</td>
								<td><?php echo $row->dari;?></td>
								<td width="120px">Tanggal Surat</td>
								<td width="320px"><?php echo indonesian_date($row->tgl_surat);?></td>
							</tr>
							<tr>
								<td>Sifat</td>
								<td colspan="3"><?php echo $row->n_sifatsurat;?></td>
							</tr>
							<tr>
								<td>Perihal</td>
								<td colspan="3"><?php echo $row->prihal;?></td>
							</tr>
						</tbody>
					</table>
					<?php echo $row->isi_surat; ?>
					<b><a href="<?php echo base_url().$row->file;?>" class="btn"><i class="iconfa-paper-clip"></i> File Surat</a></b>
				</div>
				<?php
					$one = 2;
				}
			}
		} 
	}else{ 
		
			// 2. Jika Belum Pernah Didisposisikan ?>
	<div class="msgauthor">
		<div class="thumb"><img src="<?php echo base_url().'uploads/photo/'.$row->photo; ?>" alt="" /></div>
		<div class="authorinfo">
			<span class="date pull-right"><?php echo indonesian_date($row->d_entry);?></span>
			<h5><strong><?php echo $row->n_pegawai;?></strong></h5>
			<span class="to">to <?php echo $this->session->userdata('realname');?></span>
		</div>
	</div>
	<div class="msgbody">
		<table class="table table-bordered table-invoice">
			<colgroup>
				<col class="con1" />
				<col class="con0" />
				<col class="con1" />
				<col class="con0" />
			</colgroup>
			<tbody>
				<tr>
					<td width="120px">Surat Dari</td>
					<td><?php echo $row->dari;?></td>
					<td width="120px">Tanggal Surat</td>
					<td width="320px"><?php echo indonesian_date($row->tgl_surat);?></td>
				</tr>
				<tr>
					<td>Sifat</td>
					<td colspan="3"><?php echo $row->n_sifatsurat;?></td>
				</tr>
				<tr>
					<td>Perihal</td>
					<td colspan="3"><?php echo $row->prihal;?></td>
				</tr>
			</tbody>
		</table>
		<?php echo $row->isi_surat; ?>
		<b><a href="<?php echo base_url().$row->file;?>" class="btn"><i class="iconfa-paper-clip"></i> File Surat</a></b>
	</div>
	<?php } 
	} } else { ?>
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page;?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
		</div>
	</div>
	<div class="msgauthor">
		<p><b>Terjadi Kesalahan!</b> Data Tidak Ditemukan</p>
	</div>
</div>
<?php } ?>