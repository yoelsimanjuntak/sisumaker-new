<div class="widgetbox box-inverse view_load">
	<?php echo $this->load->view($this->view.'validate');?>
	<h4 class="widgettitle"></h4>
	<div class="widgetcontent wc1">
		<?php if($result->num_rows() != 0){foreach($result->result() as $row){ ?>
		<form id="form1" class="stdform" method="post" action="<?php echo site_url().$this->page.'save/'.$row->id;?>" novalidate="novalidate">
			<input type="hidden" name="n_unitkerja_temp" value="<?php echo $row->n_unitkerja;?>"/>
			<div class="par control-group">
				<label class="control-label" for="id_skpd">SKPD</label>
				<div class="controls">
					<?php 
					$id_skpd = $this->session->userdata('skpd_id');
					if($id_skpd != 0){
						$n_skpd = $this->tmskpd->where("id = '".$id_skpd."'")->get()->n_skpd; ?>
						<input type="text" name="id_skpd" value="<?php echo $id_skpd;?>" style="display:none">
						<label class="input-label"><?php echo $n_skpd;?></label>
					<?php
					}else{ ?>
						<select name="id_skpd">
							<option value="">Pilih</option>
							<?php foreach($result_tmskpd->result() as $row_tmskpd){ ?>
							<option value="<?php echo $row_tmskpd->id;?>"<?php if ($row->id_skpd == $row_tmskpd->id) { ?> selected="selected"<?php } ?>><?php echo $row_tmskpd->n_skpd;?></option>
							<?php }?>	
						</select>
					<?php } ?>
				</div>
			</div>
			<div class="par control-group">
				<label class="control-label" for="n_unitkerja">Nama Unit Kerja</label>
				<div class="controls"><input type="text" name="n_unitkerja" id="n_unitkerja" class="input-large" value="<?php echo $row->n_unitkerja;?>"/></div>
			</div>
			<div class="par control-group">
				<label class="control-label" for="initial">Inisial</label>
				<div class="controls"><input type="text" name="initial" id="initial" class="input-large" value="<?php echo $row->initial;?>"></div>
			</div>
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action">Simpan Perubahan</button>
				<a href="#" id="close_add" class="btn btn-default">Batal</a>
			</p>
		</form>
		<?php }}else{?><p>Data Tidak Ditemukan, <a href="#" id="refresh">Segarkan Tabel</a></p><?php }?>
	</div>
</div>