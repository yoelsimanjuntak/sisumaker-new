<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Tmuser extends CI_Model
{
    public $table = 'tmuser';
    public $key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    //put your code here
    public function select($key = false, $id = false)
    {
        if ($id == false) {
            return $this->db
                ->get($this->table);
        } else {
            return $this->db
                ->where($key, $id)
                ->get($this->table);
        }
    }

    public function select_skpd($id, $skpd_id = false)
    {
        if ($skpd_id != false) {
            return $this->db
                ->select('
					tmuser.id,
					tmuser.realname
				')
                ->from($this->table)
                ->join('tmpegawai', 'tmuser.id = tmpegawai.tmuser_id')
                ->where('tmpegawai.tmskpd_id', $skpd_id)
                ->where('tmuser.id !=', $id)
                ->get();
        } else {
            return $this->db
                ->select('
					id,
					realname
				')
                ->from($this->table)
                ->where('id !=', $id)
                ->get();
        }
    }

    public function insert($form)
    {
        $this->db
            ->insert($this->table, $form);

        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    public function update($id, $form)
    {
        return $this->db
            ->where($this->key, $id)
            ->update($this->table, $form);
    }

    public function delete($id)
    {
        return $this->db
            ->where($this->key, $id)
            ->delete($this->table);
    }
}
