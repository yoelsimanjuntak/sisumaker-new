<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Pegawai extends Controller
{
	
	var $base = "https://sisumaker.tangerangselatankota.go.id/";
	
	function __construct() 
	{
        parent::__construct();
    }

	function get_photo()
	{
		$jsonStr = file_get_contents("php://input");
		$json = json_decode($jsonStr);
		$nip = $json->nip;
		if($nip == "")
		{
			$output = array(
				'status' => false,
				'nip' => $nip
			);
			$this->response($output);
		}
		else
		{
			$query = $this->db
						->query("
							SELECT
								photo
							FROM
								tmpegawai
							JOIN tmuser ON tmpegawai.tmuser_id = tmuser.id
							WHERE tmpegawai.nip = '".$nip."'
						");
			if($query->num_rows() == 0)
			{
				$output = array(
					'status' => false
				);
				echo json_encode($output);
			}
			else
			{
				$output = array(
					'status' => true,
					'photo' => $this->base."uploads/photo/".$query->row()->photo
				);
				echo json_encode($output);
			}
		}
	}
}