<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery(".chzn-select").chosen();
		fancybox();
		
		function check()
		{
			c_skpd = jQuery('#c_skpd').val();
			c_kecamatan = jQuery('#c_kecamatan').val();
			c_kelurahan = jQuery('#c_kelurahan').val();
			c_puskesmas = jQuery('#c_puskesmas').val();
			c_sekolah = jQuery('#c_sekolah').val();
			c_rs = jQuery('#c_rs').val();
			c_uptpendidikan = jQuery('#c_uptpendidikan').val();
			jQuery('#view_list_pegawai').load(base + '<?php echo $this->page;?>list_skpd/' + c_skpd + '/' + c_kecamatan + '/' + c_kelurahan + '/' + c_puskesmas + '/' + c_sekolah + '/' + c_rs + '/' + c_uptpendidikan);
		}
		
		jQuery('#check_skpd').on('click', function(){
			jQuery('#c_skpd').val('1-3');
			check();
		});
		
		jQuery('#check_kecamatan').on('click', function(){
			jQuery('#c_kecamatan').val('2-12');
			check();
		});
		
		jQuery('#check_kelurahan').on('click', function(){
			jQuery('#c_kelurahan').val('3-13');
			check();
		});
		
		jQuery('#check_puskesmas').on('click', function(){
			jQuery('#c_puskesmas').val('4-13');
			check();
		});
		
		jQuery('#check_sekolah').on('click', function(){
			jQuery('#c_sekolah').val('5-13');
			check();
		});
		
		jQuery('#check_rs').on('click', function(){
			jQuery('#c_rs').val('6-3');
			check();
		});
		
		jQuery('#check_uptpendidikan').on('click', function(){
			jQuery('#c_uptpendidikan').val('7-12');
			check();
		});
		jQuery("#check_paraf").live('click', function(){
			if(!jQuery("#check_paraf").hasClass('disabled'))
			{
				ret = false;
				jQuery('#pegawai option:selected').each(function(i, selected){ 
					ret = true; 
				});
				if(ret == false)
				{
					alert('Anda harus menyertakan penerima disposisi.');
					jQuery('#pegawai').focus();
				}
				else
				{
					jQuery.fancybox.open({
						href 			: jQuery(this).attr("to"),
						type 			: 'iframe',
						padding 		: 2,
						opacity			: true,
						titlePosition	: 'over',
						openEffect 		: 'elastic',
						openSpeed  		: 150,
						closeEffect 	: 'elastic',
						closeSpeed  	: 150,
						width			: 900,
						helpers : {
							title : {
								type : 'inside'
							},
							overlay : {
								css : {
									'background' : 'transparent',
									'background' : 'rgba(0, 0, 0, 0.6)'
								}
							}
						}
					});
				}
			}
			return false;
		});
	});
</script>
<div class="messagepanel">
	<div class="messagemenu">
		<ul>
			<li class="active"><a href="<?php echo base_url().'main/inbox';?>"><span class="iconfa-inbox"></span> Kotak Masuk <?php if($newpesan != 0){ ?><span class="new-pesan"><?php echo $newpesan;?> baru</span><?php }?></a></li>
			<li class="no-active"><a href="<?php echo base_url().'main/outbox';?>"><span class="iconfa-plane"></span> Pesan Terkirim</a></li>
			<!--<?php if($auth_masuk != 0){ ?>
			<li class="no-active"><a href="<?php echo base_url().'main/surat_masuk';?>"><span class="iconfa-envelope"></span> Surat Masuk <?php if($newsurat_masuk != 0){ ?><span class="new-pesan"><?php echo $newsurat_masuk;?> baru</span><?php }?></a></li>
			<?php }?>-->
		</ul>
	</div>
	<div class="messagecontent" style="border-right:1px solid #bb2f0e">
		<div class="messageleft" style="width:100%;">
			<?php $this->load->view($this->view.'type1');?>
		</div>
	</div>
</div>