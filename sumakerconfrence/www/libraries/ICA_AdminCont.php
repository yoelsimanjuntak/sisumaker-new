<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of ICA_AdminCont class
 *
 * @author  Muhamad Yusuf
 * @since   1.0
 *
 * 
 */
class ICA_AdminCont extends MY_Controller 
{

    public function __construct() {
        parent::__construct();
        if(!$this->session->userdata('auth_id'))
		{
			redirect('');
		}
		$this->load->library('Menu_loader');
    }
	
	function restrict($to)
	{
		if(!$this->session->userdata('auth_id'))
		{
			redirect();
		}
		else
		{
			$query = $this->db
				->from('tmuser_userauth')
				->where('tmuser_id', $this->session->userdata('auth_id'))
				->where('userauth_id', $to)
				->get();
			if($query->num_rows() == 0)
			{
				redirect('error');
			}
		}
	}

}

// This is the end of ICA_AdminCont class