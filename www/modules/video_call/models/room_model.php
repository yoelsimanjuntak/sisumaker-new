<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Vive Vio
 */
class Room_model extends DataMapper 
{

    var $table  = 'room_list';
    var $key    = 'id';

    function __construct() 
	{

        parent::__construct();
    }

    function emptyTable($tableName){
        $this->db->empty_table($tableName);
    }

    function getRoom( $idUser ) {
        
        $where = "now() > start OR call_on_progress=1";
        // $where = "user_list_video.user_id = $idUser AND ( now() > start OR call_on_progress=1 )";

        $result = array();
        $q  = $this->db
                ->select('room_name, call_on_progress, room_list.id, room_list.create_by, room_list.agenda, count(user_list_video.user_id) as total_user')
                ->from('room_list')
                ->join('user_list_video', 'room_list.id = user_list_video.room_id')
                ->where($where)
                ->where_in('user_list_video.user_id', $idUser)
                ->get()->row();
        // $result = $this->db->get('user_list_video')->result();

        
        if( $q->total_user > 1 ) {
            $result = $q;
        } elseif( $q->total_user === 1 ) {

            if( $q->create_by == $idUser ) {
                $this->deactiveRoom($idUser, $q->id);
            } else {
                $this->leaveGroupCall($idUser, $q->id);
            }
        }
        
        return $result;
    }

    function getRoomByName($roomname) {

        $q = $this->db
            ->select('room_name, call_on_progress, room_list.id, room_list.create_by, room_list.agenda, count(user_list_video.user_id) as total_user')
            ->from('room_list')
            ->join('user_list_video', 'room_list.id = user_list_video.room_id')
            ->where('room_name', $roomname)
            ->get()->row();
        
        return $q;
    }

    function getScheduledRoom( $idUser ) {

        $where = "user_list_video.user_id = $idUser";

        return $this->db
                ->select('room_list.*')
                ->from('room_list')
                ->join('user_list_video', 'room_list.id = user_list_video.room_id')
                ->where( $where )
                ->order_by('start', 'ASC')
                ->get()->result();
    }

    function getUsersConnected( $idRoom ) {
        return $this->db
            ->select('user_list_video.status, tmuser.realname, tmuser.photo, tmuser.id')
            ->from('user_list_video')
            ->join('tmuser', 'user_list_video.user_id = tmuser.id')
            ->where(array(
                'room_id' => $idRoom
            ))
            ->get()->result();
    }

    function createRoom( $idUser, $dataRoom, $dataUser = array() ) {

        $this->db->insert( 'room_list', $dataRoom);
        $idRoom = $this->db->insert_id();

        $this->db->insert( 'user_list_video', array(
            'room_id' => $idRoom,
            'user_id' => $idUser
        ));

        foreach( $dataUser as $user ) {

            $dataUserRoom['user_id']  = $user;
            $dataUserRoom['room_id']  = $idRoom;

            $this->db->insert( 'user_list_video', $dataUserRoom);
            
        }

        echo 'ok';

    }

    function updateConnectedUser($idUser, $roomId, $dataRoom ) {

        return $this->db
			->where(array(
                'user_id'     => $idUser,
                'room_id'     => $roomId
            ))
			->update('user_list_video', $dataRoom);

    }

    function createSingleRoom( $idUser, $dataRoom, $user ) {

        
        
        if( count( $this->getRoom($idUser) ) > 0 ) {
            
            return false;

        } else {
            
            $this->db->insert( 'room_list', $dataRoom);
            $idRoom = $this->db->insert_id();
    
            $this->db->insert( 'user_list_video', array(
                'room_id' => $idRoom,
                'user_id' => $idUser
            ));
    
            $dataUserRoom['user_id']  = $user;
            $dataUserRoom['room_id']  = $idRoom;
            $this->db->insert( 'user_list_video', $dataUserRoom);

            return true;

        }

    }

    function leaveGroupCall( $tmuser_id, $roomid ) {

        return $this->db
			->where(array(
                'room_id'   => $roomid,
                'user_id'   => $tmuser_id
            ))
			->delete('user_list_video');
    }

    function deactiveRoom( $tmuser_id, $roomid ) {

        $this->db
			->where(array(
                'id'       => $roomid,
                'create_by'     => $tmuser_id
            ))
			->update('room_list', array(
                'call_on_progress' => 0
            ));

        return $this->db
			->where(array(
                'room_id'   => $roomid
            ))
			->delete('user_list_video');
    }

    function addUserToRoom( $userToAdd, $roomid ) {
        
        if( count( $this->getRoom($userToAdd) ) > 0 ) {

            return false;
        } else {

            $dataUserRoom['user_id']  = $userToAdd;
            $dataUserRoom['room_id']  = $roomid;
            $this->db->insert( 'user_list_video', $dataUserRoom);

            return true;
        }

    }
    

}

?>
