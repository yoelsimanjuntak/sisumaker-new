<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Panel extends Controller {

    var $page = "panel/";
    var $view = "panel/";
    var $icon = "laptop";

    function __construct() {
        parent::__construct();
        $this->log_verifikasi = new Log_verifikasi();
        $this->tmuser = new Tmuser();
        $this->tmpegawai = new Tmpegawai();
        $this->tmskpd = new Tmskpd();
        $this->surat = new Surat();
        $this->tmuser_userauth = new Tmuser_userauth();
        $this->tr_wallpaper = new Tr_wallpaper();
        $this->set_wallpaper = new Set_wallpaper();
		$this->outbox = new Outbox();
        $this->load->helper('function_helper');
		$this->load->library('api_sms_class_reguler_json');
    }

    function index() {
        if ($this->session->userdata('login') == TRUE)
		{
			$email = 0;
			if($this->session->userdata('pegawai_id') != '0')
			{
				$inbox = $this->tmuser_userauth->where("tmuser_id = '".$this->session->userdata('auth_id')."' and userauth_id = 3")->count();
				if($inbox != 0)
				{
					$data = array(
						'email' => 1
					);
					$this->session->set_userdata($data);
					$email = 1;
				}
			}
			if($email == 1)
			{
				redirect(base_url().'main/inbox');
			}
			else
			{
				$this->tmpersuratan = new Tmpersuratan;
				$this->load->library('Menu_loader');
				$data = array(
					'count_masuk' => $this->tmpersuratan->where("id_jnssurat = '1' and id_skpd_in = '".$this->session->userdata('skpd_id')."' and id_skpd_out <> '".$this->session->userdata('skpd_id')."'")->count(),
					'count_keluar' => $this->tmpersuratan->where("id_jnssurat = '2' and id_skpd_in = '".$this->session->userdata('skpd_id')."' and id_skpd_out = '".$this->session->userdata('skpd_id')."' and status_setujui = '1'")->count(),
					'assets' => array(
						$this->lib_load_css_js->load_js(base_url(), "assets/js/highchart/", "highcharts.js"),
						$this->lib_load_css_js->load_js(base_url(), "assets/js/highchart/modules/", "exporting.js"),
					),
					'breadcrubs' => 'Selamat Datang....',
					'title' => 'Beranda',
					'content' => 'dashboard'
				);
				$this->load->view('template', $data);
			}
        }
		else
		{
            $this->load->view('form_login');
        }
    }

    function login() {
        if ($this->session->userdata('login')) {
            redirect('');
        }
        $this->load->view('form_login');
    }


    	//----- Send SMS With Raja-sms.com
    function login_submit() {
        if ($this->session->userdata('login')) {
            redirect('');
        }
        if ($this->input->post('username') == "" or $this->input->post('password') == "") {
            $output = array(
                'status' => 0
            );
            echo json_encode($output);
        } else {
            if ($this->tmuser->where("username = '" . $this->input->post('username') . "' and password = '" . md5($this->input->post('password')) . "' and c_status=1")->count() == 0) {
                $output = array(
                    'status' => 0
                );
                echo json_encode($output);
            } else {
            $result = $this->tmuser->where("username = '" . $this->input->post('username') . "' and password = '" . md5($this->input->post('password')) . "' and c_status=1")->get();
            $iduser = $result->id;
            if ($iduser == 1) {
					$pegawai = $this->tmpegawai->where("tmuser_id = '".$iduser."'")->get();
					if ($pegawai->id != 0) {
						$pegawaiid = $pegawai->id;
						$nip = $pegawai->nip;
						$jabatan_id = $pegawai->tmjabatan_id;
						$unitkerja_id = $pegawai->tmunitkerja_id;
						$skpd_id = $pegawai->tmskpd_id;
						$n_skpd = $this->tmskpd->where('id', $pegawai->tmskpd_id)->get()->initial;
					} else {
						$pegawaiid = '0';
						$nip = '-';
						$jabatan_id = '0';
						$unitkerja_id = '0';
						$skpd_id = '0';
						$n_skpd = '';
					}
					$wallpaper = 'uploads/wallpaper/Beautiful_Take_See150605.png';
					$num_wallpaper = $this->tr_wallpaper->where('tmuser_id', $iduser)->count();
					if($num_wallpaper != 0)
					{
						$id_wallpaper = $this->tr_wallpaper->where('tmuser_id', $iduser)->get()->tr_wallpaper_id;
						$wallpaper = $this->set_wallpaper->where('id', $id_wallpaper)->get()->link;
					}
					$data = array(
						'last_login' => now()
					);
					$this->tmuser->update($result->id, $data);
					$data = array(
						'auth_id' => $iduser,
						'realname' => $result->realname,
						'username' => $result->username,
						'password' => $result->password,
						'n_skpd' => $n_skpd,
						'photo' => $result->photo,
						'nip' => $nip,
						'pegawai_id' => $pegawaiid,
						'jabatan_id' => $jabatan_id,
						'unitkerja_id' => $unitkerja_id,
						'skpd_id' => $skpd_id,
						'email' => 0,
						'wallpaper' => $wallpaper,
						'login' => TRUE
					);
					$this->session->set_userdata($data);
					$output = array(
						'status' => 2
					);
					echo json_encode($output);
				} else {
            
                $result = $this->tmuser->where("username = '" . $this->input->post('username') . "' and password = '" . md5($this->input->post('password')) . "' and c_status=1")->get();
                $userid = $result->id;
                $pegawai = $this->tmpegawai->where("tmuser_id = '".$userid."'")->get();
                $no_telp = $pegawai->telp;
                $digits = 4;
				$kode_verifikasi = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
                
                $this->log_verifikasi->del_log($userid);
                $data_log = array(
                	'verifikasi' => $kode_verifikasi,
                	'tmuser_id' => $userid,
                	'd_entry' => date('Y-m-d')
                );
                $this->log_verifikasi->insert($data_log);
                
                $senddata = array(
					'apikey' => SMSGATEWAY_APIKEY,  
					'callbackurl' => SMSGATEWAY_CALLBACKURL, 
					'datapacket'=> array()
				);
				$number 			= $no_telp;
				$message			= "Kode verifikasi SISUMAKER Anda adalah ".$kode_verifikasi;
				$sendingdatetime 	= ""; 
				array_push($senddata['datapacket'],array(
					'number' => trim($number),
					'message' => urlencode(stripslashes(utf8_encode($message))),
					'sendingdatetime' => $sendingdatetime));
				
				$sms = new api_sms_class_reguler_json();
				$sms->setIp(SMSGATEWAY_IPSERVER);
				$sms->setData($senddata);
				// $responjson = $sms->send();
				//$status_sms = $this->saldo_sms->save($responjson);
				
				$encrip = encrip($userid);
                $output = array(
                    'status' => 1,
                    'link' => base_url().$this->page.'form_verifikasi/'.$encrip.'/'.sha1(md5($encrip))
                );
                
                echo json_encode($output);
                }
            }
        }
    }

    // function login_submit() {
//         if ($this->session->userdata('login')) {
//             redirect('');
//         }
//         if ($this->input->post('username') == "" or $this->input->post('password') == "") {
//             $output = array(
//                 'status' => 0
//             );
//             echo json_encode($output);
//         } else {
//             if ($this->tmuser->where("username = '" . $this->input->post('username') . "' and password = '" . md5($this->input->post('password')) . "' and c_status=1")->count() == 0) {
//                 $output = array(
//                     'status' => 0
//                 );
//                 echo json_encode($output);
//             } else {
//                 $result = $this->tmuser->where("username = '" . $this->input->post('username') . "' and password = '" . md5($this->input->post('password')) . "' and c_status=1")->get();
//                 $userid = $result->id;
//                 $pegawai = $this->tmpegawai->where("tmuser_id = '".$userid."'")->get();
//                 $no_telp = $pegawai->telp;
//                 $digits = 4;
// 				$kode_verifikasi = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
//                 
//                 $this->log_verifikasi->del_log($userid);
//                 $data_log = array(
//                 	'verifikasi' => $kode_verifikasi,
//                 	'tmuser_id' => $userid,
//                 	'd_entry' => date('Y-m-d')
//                 );
//                 $this->log_verifikasi->insert($data_log);
//                 
//                 $send = "Kode verifikasi SISUMAKER Anda adalah ".$kode_verifikasi;
//                 $data_outbox = array(
// 					'DestinationNumber' => $no_telp,
// 					'TextDecoded' => $send,
// 					'CreatorID' => 'Gammu',
// 					'DeliveryReport' => 'yes'
// 				);
// 				$this->outbox->insert($data_outbox);
// 				
// 				$encrip = encrip($userid);
//                 $output = array(
//                     'status' => 1,
//                     'link' => base_url().$this->page.'form_verifikasi/'.$encrip.'/'.sha1(md5($encrip))
//                 );
//                 echo json_encode($output);
//             }
//         }
//     }
    
    function form_verifikasi($encrip, $secure){
    	$userid = decrip($encrip);
    	if($secure === sha1(md5($encrip)))
    	{
    		$pegawai = $this->tmpegawai->where("tmuser_id = '".$userid."'")->get();
        	$no_telp = $pegawai->telp;
    		$dash_no_telp = "";
    		$b = 0;
    		for($a = 2; $a<= strlen($no_telp); $a++)
    		{
    			if($b == 4){
    				$dash_no_telp .= "-";
    				$b = 0;
    			}
    			$dash_no_telp .= "&#9679;";
    			$b++;
    		}
    		$dash_no_telp .= substr($no_telp, -2);
    		$data = array(
    			'userid' => $userid,
    			'dash_no_telp' => $dash_no_telp,
    			'encrip' => $encrip,
    			'secure' => $secure
    		);
    		$this->load->view('log_verifikasi', $data);
    	}
    	else
    	{
    		echo "Terdapat kesalahan saat pembuatan link url.";
    	}
    }
    
    function submit_verifikasi(){
    	$encrip = $this->input->post('encrip');
    	$secure = $this->input->post('secure');
    	$microtime = $this->input->post('microtime');
    	$token = $this->input->post('token');
    	$verifikasi = $this->input->post('verifikasi') + 0;
    	if($token === sha1($secure.$encrip.$microtime))
    	{
    		$userid = decrip($encrip);
    		$kode_valid = 1;
    		// $kode_valid = $this->log_verifikasi->where("tmuser_id = ".$userid." and verifikasi = ".$verifikasi)->count();
    		if($kode_valid == 1)
    		{
    			$result = $this->tmuser->where("id = ".$userid)->get();
				$pegawai = $this->tmpegawai->where("tmuser_id = '".$result->id."'")->get();
				if ($pegawai->id != 0) {
					$pegawaiid = $pegawai->id;
					$nip = $pegawai->nip;
					$jabatan_id = $pegawai->tmjabatan_id;
					$unitkerja_id = $pegawai->tmunitkerja_id;
					$skpd_id = $pegawai->tmskpd_id;
					$n_skpd = $this->tmskpd->where('id', $pegawai->tmskpd_id)->get()->n_skpd;
				} else {
					$pegawaiid = '0';
					$nip = '-';
					$jabatan_id = '0';
					$unitkerja_id = '0';
					$skpd_id = '0';
					$n_skpd = '';
				}
				$data = array(
					'last_login' => now(),
					'online' => now()
				);
				$this->tmuser->update($result->id, $data);
				$wallpaper = 'uploads/wallpaper/Beautiful_Take_See150605.png';
				$num_wallpaper = $this->tr_wallpaper->where('tmuser_id', $result->id)->count();
				if($num_wallpaper != 0)
				{
					$id_wallpaper = $this->tr_wallpaper->where('tmuser_id', $result->id)->get()->tr_wallpaper_id;
					$wallpaper = $this->set_wallpaper->where('id', $id_wallpaper)->get()->link;
				}
				$data = array(
					'auth_id' => $result->id,
					'realname' => $result->realname,
					'username' => $result->username,
					'password' => $result->password,
					'n_skpd' => $n_skpd,
					'photo' => $result->photo,
					'nip' => $nip,
					'pegawai_id' => $pegawaiid,
					'jabatan_id' => $jabatan_id,
					'unitkerja_id' => $unitkerja_id,
					'skpd_id' => $skpd_id,
					'email' => 0,
					'wallpaper' => $wallpaper,
					'login' => TRUE
				);
				$this->session->set_userdata($data);
				$output = array(
					'status' => 1
				);
				echo json_encode($output);
			}
			else
			{
				$output = array(
					'status' => 0
				);
				echo json_encode($output);
			}
    	}
    	else
    	{
    		$output = array(
				'status' => 0
			);
			echo json_encode($output);
    	}
    }

    function edit() {
        if (!$this->session->userdata('login')) {
            redirect('');
        }
		$this->load->library('Menu_loader');
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "bootstrap-fileupload.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "bootstrap-fileupload.min.css")),
            'breadcrubs' => 'Edit Profile',
            'title' => 'Edit Profile',
            'content' => 'edit'
        );
		$template = 'template';
		if($this->session->userdata('email') == 1)
		{
			$template = 'template_mail';
		}
		$this->load->view($template, $data);
    }

    function edit_submit_pass()
	{
		if(!$this->session->userdata('login')){ redirect(''); }
		if($this->session->userdata('password') == md5($this->input->post('passwordlama')))
		{
			$this->session->sess_destroy();
			$data = array(
				'password' => md5($this->input->post('password')),
				'd_update' => date('Y-m-d H:i:s')
			);
			$this->tmuser->update($this->session->userdata('auth_id'), $data);
			$data = array(
				'password' => md5($this->input->post('password'))
			);
			$this->session->set_userdata($data);
			$output = array(
				'status' => 1
			);
			echo json_encode($output);
		}
		else
		{
			$output = array(
				'status' => 11
			);
			echo json_encode($output);
		}
	}
	
	function edit_submit_nama()
	{
		if(!$this->session->userdata('login')){ redirect(''); }
		$this->session->sess_destroy();
		$data = array(
			'realname' => $this->input->post('realname'),
			'd_update' => date('Y-m-d H:i:s')
		);
		$this->tmuser->update($this->session->userdata('auth_id'), $data);
		$data = array(
			'realname' => $this->input->post('realname')
		);
		$this->session->set_userdata($data);
		$output = array(
			'status' => 1,
			'user' => "<h5>".$this->input->post('realname')."</h5>NIP. ".$this->session->userdata('nip')."<ul><li><a href='".base_url()."panel/edit/'>Edit Profile<small> - ".$this->input->post('n_user')."</small></a></li><li><a href='".base_url()."panel/logout'>Sign Out</a></li></ul>"
		);
		echo json_encode($output);
	}

    function submit_photo() {
        if ($_FILES['userfile']['name'] != "") {
            $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.' . end($explode);
            $exten = $this->session->userdata('username') . $endof;
            $config['file_name'] = $this->session->userdata('username');
            $config['upload_path'] = './uploads/photo/';
            $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config['max_size'] = '1000000';
            $config['overwrite'] = true;
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                echo '
					<script type="text/javascript">
						alert("' . $this->upload->display_errors() . '");
						document.location.href = "' . base_url() . $this->page . 'edit";	
					</script>
				';
            } else {
                $data = array(
                    'photo' => $exten
                );
                $this->tmuser->update($this->session->userdata('auth_id'), $data);
                $this->session->set_userdata($data);
                redirect(base_url() . $this->page . 'edit');
            }
        } else {
            echo '
			<script type="text/javascript">
				alert("File Gambar Harus Diisi");
				document.location.href = "' . base_url() . $this->page . 'edit";
			</script>';
        }
    }

    function delete_photo() {
        $data = array(
            'photo' => "default.png"
        );
        $this->tmuser->update($this->session->userdata('userid'), $data);
        $this->session->set_userdata($data);
    }

    function logout() {		
		$data = array(
			'online' => 0
		);
		$this->tmuser->update($this->session->userdata('auth_id'), $data);
        $data = array(
			'auth_id' => '',
			'realname' => '',
			'username' => '',
			'password' => '',
			'n_skpd' => '',
			'photo' => '',
			'nip' => '',
			'pegawai_id' => '',
			'jabatan_id' => '',
			'unitkerja_id' => '',
			'skpd_id' => '',
			'email' => '',
			'wallpaper' => '',
			'login' => FALSE
        );
        $this->session->set_userdata($data);
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect('');
    }

}

?>