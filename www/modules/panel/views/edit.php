<style>
	.error label.error{display:none !important;}
	.success label.valid{display:none !important;}
</style>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#load_pass").click(function(){
			if(!jQuery(this).hasClass('active')) {
				jQuery("#pass").slideDown('fast');
				jQuery(this).addClass('active');
			}else{
				jQuery("#pass").slideUp('fast');
				jQuery(this).removeClass('active');
			}
		});
		jQuery('#delete').live('click', function(){
			if(confirm("Yakin Ingin Menghapus photo ini?")){
				jQuery.ajax({
					"type":"post",
					"cache":false,
					"url":"<?php echo base_url().$this->page.'delete_photo';?>",
					"success":function(){
						jQuery("#delete").remove();
						jQuery(".profilethumb").html('<img src="<?php echo base_url().'uploads/photo/default.png';?>" alt="Photo Profile" class="img-polaroid"/>');
						jQuery(".userphoto").html('<img src="<?php echo base_url().'uploads/photo/default.png';?>" alt="Photo Profile" class="img-polaroid"/>');
					}
				});
			}
			return false;
		});
		jQuery("#form1")
			.validate({
				rules: {
					passwordlama: "required",
					password: "required",
					cpassword:{
						required:true,
						equalTo:"#password"
					}
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 1)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Diperbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								jQuery('#action').button('reset');
								jQuery('#result').fadeIn('fast');
							}
							else
							{
								jQuery('#result').html('<p class="pesan gagal"><span class="iconfa-remove"></span> Password Lama Salah<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								jQuery('#action').button('reset');
								jQuery('#result').fadeIn('fast');
							}
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
		jQuery("#form2")
			.validate({
				rules: {
					n_user: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 1)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Diperbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								jQuery('#action').button('reset');
								jQuery('#result').fadeIn('fast');
								jQuery('.userinfo').html(data.user);
							}							
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
	});
</script>
<div id="result"></div>
<div class="row-fluid editprofileform">
	<div class="span8 profile-left">
		<div class="widgetbox login-information">
			<h4 class="widgettitle">Login Information</h4>
			<div class="widgetcontent">
				<p class="par control-group">
					<label style="padding:0">Username</label>
					<?php echo $this->session->userdata('username');?>
				</p>
				<button type="button" class="btn btn-primary" id="load_pass">
					<span class="iconfa-key"></span> Ubah Password?
				</button>
				<div id="pass" style="display:none;">
					<form id="form1" action="<?php echo base_url().$this->page.'edit_submit_pass';?>" method="post">
						<p class="par control-group">
							<label>Password Lama</label>
							<input type="password" name="passwordlama" id="passwordlama" class="input-xlarge"> 
						</p>
						<p class="par control-group">
							<label>Password Baru</label>
							<input type="password" name="password" id="password" class="input-xlarge"> 
						</p>
						<p class="par control-group">
							<label>Konfirmasi Password</label>
							<input type="password" name="cpassword" id="cpassword" class="input-xlarge">  
						</p>
						<p>
							<button type="submit" class="btn btn-primary" id="action">Update Password</button>
						</p>
					</form>
				</div>
			</div>
		</div>
		<form id="form2" action="<?php echo base_url().$this->page.'edit_submit_nama';?>" method="post">
			<div class="widgetbox personal-information">
				<h4 class="widgettitle">Personal Information</h4>
				<div class="widgetcontent">
					<p class="par control-group">
						<label>Nama Lengkap</label>
						<input type="text" name="realname" class="input-xlarge" value="<?php echo $this->session->userdata('realname');?>" required="">
					</p>
				</div>
			</div>
			<p>
				<button type="submit" class="btn btn-primary" id="action">Update Profile</button>
			</p>
		</form>
	</div>
	<div class="span4">
		<div class="widgetbox profile-photo">
			<div class="headtitle">
				<div class="btn-group">
					<button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
					<ul class="dropdown-menu">
						<li><a style="cursor:pointer;" data-toggle="collapse" data-target="#photoubah"><span class="iconfa-edit"></span> Change Photo</a></li>
						<?php if($this->session->userdata('photo') != "default.png"){?><li><a href="#" id="delete"><span class="iconfa-remove"></span> Remove Photo</a></li><?php }?>
					</ul>
				</div>
				<h4 class="widgettitle">Profile Photo</h4>
			</div>
			<div class="widgetcontent">
				<div class="profilethumb">
					 <img src="<?=base_url().'uploads/photo/'.$this->session->userdata('photo');?>" alt="Photo Profile" class="img-polaroid"/>
				</div>
				<div id="photoubah" class="collapse" style="text-align:center;">
					<div class="input-append" style="margin-top:3px;">
						<form action="<?php echo base_url().$this->page.'submit_photo';?>" method="post" enctype="multipart/form-data">
							<div class="fileupload fileupload-new" data-provides="fileupload" id="fileupload1">
								<div class="input-append">
									<div class="uneditable-input span2">
										<i class="iconfa-file fileupload-exists"></i>
										<span class="fileupload-preview"></span>
									</div>
									<span class="btn btn-file">
										<span class="fileupload-new">Pilih file</span>
										<span class="fileupload-exists" name="file_upload_a">Ubah</span>
										<input type="file" name="userfile"/>
									</span>
									<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" name="file_upload_a">Hapus</a>
									<button type="submit" class="btn fileupload-exists" style="height:30px;"><i class="icon-ok-circle"></i></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>