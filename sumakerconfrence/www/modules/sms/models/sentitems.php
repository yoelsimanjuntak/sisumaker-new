<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sentitems
 *
 * @author Yusuf
 */
class sentitems extends DataMapper 
{

    var $table = 'sentitems';
    var $key = 'ID';

    function __construct() 
	{
        parent::__construct();
    }

//put your code here
	function select($key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($this->table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($this->table);
		}
	}
	
	function delete($id) 
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }
	
}

?>
