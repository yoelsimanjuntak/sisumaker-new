<div id="chatwindows" class="chatwindows"></div><!--chatwindows-->
<div class="onlineuserpanel">
    <div id="chatscroll" class="onlineusers">
        <ul>
			<?php foreach($chatlist as $row_chat){
			$class = '';
			if($row_chat->online != 0 || $row_chat->online != '')
			{
				$on = now() - $row_chat->online;
				if($on <= 20)
				{
					$class = 'on';
				}
			}
			?>
            <li id="<?php echo $row_chat->id;?>" class="<?php echo $class;?>"><img src="<?php echo base_url().'uploads/photo/'.$row_chat->photo;?>" /> <span><?php echo $row_chat->n_pegawai;?></span></li>
            <?php }?>
        </ul>
    </div><!--onlineusers-->
</div><!--onlineuserpanel-->

<script type="text/javascript">
    
    jQuery(document).ready(function(){

        // chat slim scroll
        if(jQuery('#chatscroll').length > 0) {
           
            jQuery('#chatscroll').slimscroll({
                color: '#999',
                size: '5px',
                width: '200px',
                height: '100%'
            });
           
            jQuery('#chatscroll li').click(function() {
                
                // store user ids in a cookie
                var i = jQuery(this).attr('id');
                if(jQuery.cookie('user-selected')) {
                    var c = jQuery.cookie('user-selected').split(',');
                    
                    // we make sure that what we inserted is not yet existing
                    var ue = false;
                    for(a = 0; a < c.length; a++) {
                        if(c[a] == i) ue = true;
                    }
                    
                    if(!ue) {
                        c.push(i);
                        jQuery.cookie('user-selected', c.join(','), { path: '/' });
                    }
                    
                } else {
                    jQuery.cookie('user-selected', i, { path: '/' });
                }
            
                var n = jQuery(this).find('span').text();
                var id = jQuery(this).attr('id');
                var userxist = checkUser(id);
                  
                var wincount = jQuery('#chatwindows .chatwin').length;
                if(wincount < 4) {
                    
                    // check if user already opened a window
                    if(userxist == false) {
                        var chatwin = appendUser(n,id);
                        jQuery('#chatwindows').append(chatwin);
						jQuery('#chatmsg_' + id).load(base + 'chat/get_data/' + id);
						var gg = jQuery(document).height() * 3;
						jQuery('#chatmsg_' + id).animate({ scrollTop: gg }, "slow");
					}
                } else {
                     
                    if(jQuery('#chatwinlist').length > 0) {
                        
                        if(!userxist) {
                            jQuery('#chatwinlist ul').append('<li id="c_'+ id +'"><h4>'+ n +'</h4></li>').show();
                            jQuery('#chatwinlist i').text(jQuery('#chatwinlist li').length);
                        }
                        
                    } else {
                        
                        if(!userxist) {
                            jQuery('#chatwindows').append('<div id="chatwinlist"><span class="iconfa-comment"></span><i>1</i></div>');
                            jQuery('#chatwinlist').append('<ul><li id="'+ id +'"><h4>'+ n +'</h4></li></ul>');
                        }
                        
                    }
                     
                }
            }); // end of #chatscroll li click
            
               
            jQuery('.chatwin .close').live('click', function(){
                  
                var p = jQuery(this).parent().remove();
                    
                if(jQuery('#chatwinlist li').length > 0) {
                    
                  var n = jQuery('#chatwinlist li:last-child h4').text();
                  var id = jQuery('#chatwinlist li:last-child').attr('id');
                  var chatwin = appendUser(n,id, false);
                  
                  jQuery(chatwin).insertBefore('#chatwinlist');
                  jQuery('#chatwinlist li:last-child').remove();
                  
                  jQuery('#chatwinlist i').text(jQuery('#chatwinlist li').length);
                  
                } 
                
                if(jQuery('#chatwinlist li').length == 0)
                    jQuery('#chatwinlist').remove();
                    
                // remove from cookies
                var md = jQuery(this).parent().attr('id').split('_');
                if(jQuery.cookie('user-selected')) {
                    
                    var nc = [];
                    var oc = jQuery.cookie('user-selected').split(',');
                    for(a = 0; a < oc.length; a++) {
                        if(oc[a] != md[1]) nc.push(oc[a]);
                    }
                    jQuery.cookie('user-selected', nc.join(','), { path: '/' });
                }
                
                
            });
            
            jQuery('#chatwinlist span').live('click', function(){
                var p = jQuery(this).parent().find('ul');
                if(p.is(':visible')) p.hide(); else p.show();
            });
            
            
            jQuery('#chatwinlist li').live('click', function(){
                
                var n = jQuery(this).find('h4').text();
                var id = jQuery(this).attr('id');
                var chatwin = appendUser(n,id);
                var cu = jQuery('.chatwin:nth-child(4) h4').text();
				var id_user = id.split('-');
				
                jQuery(this).remove();
                jQuery('#chatwinlist ul').append('<li><h4>'+ cu +'</h4></li>');
                				
                jQuery(chatwin).insertBefore('#chatwinlist');
				jQuery('.chatwin:nth-child(4)').remove();
				jQuery('#chatmsg_' + id).load(base + 'chat/get_data/' + id_user[1]);
            });
            
            
            if(jQuery.cookie('user-selected')) {
                var uc = jQuery.cookie('user-selected').split(',');
                
                var maxwin = 4;
                if(jQuery(document).width() < 769) maxwin = 2;
                if(jQuery(document).width() < 641) maxwin = 1;
                //alert(jQuery(document).width());
                
                for(a = 0; a < uc.length; a++) {
                    var n = jQuery('#'+uc[a]).find('span').text();
                    var id = jQuery('#'+uc[a]).attr('id');
                    var userxist = checkUser(id);
                    if(a < maxwin) {
                        if(!userxist) {
                            var chatwin = appendUser(n,id,false);
                            jQuery('#chatwindows').append(chatwin);
							jQuery('#chatmsg_' + id).load(base + 'chat/get_data/' + id);
							jQuery('#chatmsg_' + id).animate({ scrollTop: jQuery(document).height() }, "slow");
                        }
                        
                    } else {
                        
                        if(jQuery('#chatwinlist').length > 0) {
                        
                            if(!userxist) {
                                jQuery('#chatwinlist ul').append('<li id="'+ id +'"><h4>'+ n +'</h4></li>');
                                jQuery('#chatwinlist').find('i').text(jQuery('#chatwinlist ul li').length);
                            }
                        
                        } else {
                        
                            if(!userxist) {
                                jQuery('#chatwindows').append('<div id="chatwinlist"><span class="iconfa-comment"></span><i>1</i></div>');
                                jQuery('#chatwinlist').append('<ul><li id="cu-'+ id +'"><h4>'+ n +'</h4></li></ul>');
                            }
                        
                        }
                    }
                }
            }
            
            jQuery('.chatinput').live('keypress', function(e){
				if(e.which == 13) {
					var t = jQuery(this);
					var v = t.val();
					var id = jQuery(this).attr('id');
					jQuery.ajax({
						type:'post',
						url: base + "chat/save_msg/" + id,
						cache: false,
						data: t,
						dataType: 'json',
						success: function(data) {
							if(data.status == 1)
							{
								t.val('');
								t.parents('.chatwin').find('.chatmsg').append('<li><span class="right">'+ v +'</span></li>');
							}
							else
							{
								t.parents('.chatwin').find('.chatmsg').append('<li><b>Error! </b></li>');	
							}
						}
					});
                }
            });
            
           
        } // end of slimscroll length
    });
	
	function checkUser(id) {
		userxist = false;
		jQuery('#chatwindows h4').each(function(){
			var i = jQuery(this).parent().attr('id').split('_');
			if(id == i[1]) {
				userxist = true;
				var p = jQuery(this).parent();
				p.removeClass('animate0 bounceInUp')
				.addClass('animate0 bounce')
				.on('webkitAnimationEnd oanimationend oAnimationEnd msAnimationEnd animationend',function(){
					p.removeClass('animate0 bounce');	
				});
			}
		});
		return userxist;
	}
		
	function appendUser(n,id,animate) {
		var a = (animate == null || animate == true)? 'animate0 bounceInUp' : '';
			
		var chatwin = '<div id="c_'+id+'" class="chatwin '+ a +'">'
					+ '<a class="video-call" onclick="return NatievaVideoCall.inviteUserToVideoCall(this, '+ id +', \''+ n +'\')"><span class="iconfa-facetime-video"></span></a>'
					+ '<a class="close">&times;</a>'
					+ '<h4>'+ n +'</h4>'
					+ '<ul class="chatmsg" id="chatmsg_' + id + '"></ul>'
					+ '<div class="chattext">'
					+ '<input name="text" class="chatinput input-block-level" placeholder="Type message and hit enter..." id='+ id +'/>'
					+ '</div>'
					+ '</div>';
		return chatwin;    
	}

	function newMessage() {
		jQuery.ajax({
			type:'post',
			url: base + "chat/new_message/",
			cache: false,
			dataType: 'json',
			success: function(data) {
				if(data.status != 0)
				{
					if(data.status == 1)
					{
						var n = data.n_pegawai;
						var id = data.from;
						tabMessage(n, id);
					}
					else
					{
						var n_pegawai = data.n_pegawai.split('{}');
						var from = data.from.split('{}');
						num = n_pegawai.length;
						for(i=0;i<num; i++)
						{
							var n = n_pegawai[i];
							var id = from[i];
							tabMessage(n, id);
						}
					}
				}
				var waktu = setTimeout("newMessage()", 1000);
			}
		});
		
		function tabMessage(n, id)
		{
			var userxist = checkUser(id);
			if(userxist == false)
			{
				var chatwin = appendUser(n,id);
				jQuery.cookie('user-selected', id, { path: '/' });
				jQuery('#chatwindows').append(chatwin);
				jQuery('#chatmsg_' + id).load(base + 'chat/get_data/' + id);
				jQuery('#chatmsg_' + id).animate({ scrollTop: jQuery(document).height() }, "slow");
			}
			else
			{
				jQuery('#chatmsg_' + id).last().load(base + 'chat/get_data/' + id);
				jQuery('#chatmsg_' + id).animate({ scrollTop: jQuery(document).height() }, "slow");
			}
		}
	}
	
	function listOnline()
	{
		jQuery.ajax({
			type:'post',
			url: base + "chat/user_online/",
			cache: false,
			dataType: 'json',
			success: function(data) {
				if(data.status != 0)
				{
					var user_id = data.user_id.split('|');
					var active = data.active.split('|');
					num = user_id.length;
					for(i=0;i<num; i++)
					{
						if(active[i] == 1){
							jQuery('#' + user_id[i]).addClass('on');
						}
						else
						{
							jQuery('#' + user_id[i]).removeClass('on');
						}
					}
				}
				var waktu = setTimeout("listOnline()", 10000);
			}
		});
	}
	
	newMessage();
	listOnline();
</script>