<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#form1")
			.validate({
				rules: {
					id_surat: "required",
					dari: "required",
					no_surat: "required",
					tgl_surat: "required",
					userfile: "required",
					tgl_terima_surat: "required",
					no_agenda: "required",
					id_sifatsurat: "required",
					prihal: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				}
			});
	});
</script>