<div id="result"></div>
<div class="widgetbox box-inverse">
	<?php echo $this->load->view($this->view.'validate_user');?>
	<h4 class="widgettitle"></h4>
	<div class="widgetcontent wc1">
		<div class="tabs-top">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#rA">Ganti Nama</a></li>
				<li><a data-toggle="tab" href="#rB">Ganti Username</a></li>
				<li><a data-toggle="tab" href="#rC">Ganti Password</a></li>
				<li><a data-toggle="tab" href="#rD">List Peran</a></li>
				<li><a data-toggle="tab" href="#rE">User Kirim Surat</a></li>
			</ul>
			<?php if($result->num_rows() != 0){foreach($result->result() as $row){ ?>
			<div class="tab-content" style="overflow:inherit !important">
				<div id="rA" class="tab-pane active">
					<form id="form1" class="stdform" method="post" action="<?php echo site_url().$this->page.'save_name/'.$row->id;?>" novalidate="novalidate">
						<div class="par control-group">
							<label class="control-label" for="realname">Nama user</label>
							<div class="controls"><input type="text" name="realname" id="realname"  class="input-large" value="<?php echo $row->realname;?>"/></div>
						</div>
						<p class="stdformbutton">
							<button class="btn btn-primary" id="action">Simpan</button>
							<a href="<?php echo base_url().$this->page;?>" class="btn btn-default">Batal</a>
						</p>
					</form>
				</div>
				<div id="rB" class="tab-pane">
					<form id="form2" class="stdform" method="post" action="<?php echo site_url().$this->page.'save_username/'.$row->id;?>" novalidate="novalidate">
						<input type="hidden" name="username_temp" value="<?php echo $row->username;?>"/>
						<div class="par control-group">
							<label class="control-label" for="username">Username</label>
							<div class="controls"><input type="text" name="username" id="username" class="input-large"  onkeyup="nospaces(this)" value="<?php echo $row->username;?>"/></div>
						</div>
						<p class="stdformbutton">
							<button class="btn btn-primary" id="action">Simpan</button>
							<a href="<?php echo base_url().$this->page;?>" class="btn btn-default">Batal</a>
						</p>
					</form>
				</div>
				<div id="rC" class="tab-pane">
					<form id="form3" class="stdform" method="post" action="<?php echo site_url().$this->page.'save_password/'.$row->id;?>" novalidate="novalidate">						
						<div class="par control-group">
							<label class="control-label" for="password">Password</label>
							<div class="controls"><input type="password" name="password" id="password" class="input-large"/></div>
						</div>
						<div class="par control-group">
							<label class="control-label" for="cpassword">Ulangi Password</label>
							<div class="controls"><input type="password" name="cpassword" id="cpassword" class="input-large"/></div>
						</div>						
						<p class="stdformbutton">
							<button class="btn btn-primary" id="action">Simpan</button>
							<a href="<?php echo base_url().$this->page;?>" class="btn btn-default">Batal</a>
						</p>
					</form>
				</div>
				<div id="rD" class="tab-pane">
					<script type="text/javascript">
					var oTable;
					datatables_ps("<?php echo site_url().$this->page.'data_userauth/'.$row->id;?>", 1);
					</script>
					<div class="row-fluid">
						<div class="span7">
							<table id="dyntable" class="table table-bordered">
								<colgroup>
									<col class="con0" style="align: center; width: 4%" />
									<col class="con1" />
									<col class="con0" />
								</colgroup>
								<thead>
									<tr>
										<th width="15px">No</th>
										<th>Nama Role</th>
										<th width="20px">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td valign="top" colspan="3" class="dataTables_empty">Loading <img src="<?php echo base_url().'assets/images/loaders/loader19.gif';?>"/></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="span5">
							<div class="widgetbox box-inverse">
								<h4 class="widgettitle"></h4>
								<div class="widgetcontent wc1">
									<form id="form4" class="stdform" method="post" action="<?php echo site_url().$this->page.'save_userauth/'.$row->id;?>" novalidate="novalidate">
										<div class="control-group">
											<label class="control" for="userauth_id">List Peran</label>
											<select id="userauth_id" name="userauth_id[]" required="" data-placeholder="List Peran" multiple="multiple" style="width:340px;" tabindex="3">
												<?php foreach($result_userauth->result() as $row_userauth){?>
												<option value="<?php echo $row_userauth->id_role;?>"><?php echo $row_userauth->description;?></option>
												<?php }?>
											</select>
										</div>
										<p class="stdformbutton">
											<button class="btn btn-primary" id="action">Simpan</button>
										</p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="rE" class="tab-pane">
					<script type="text/javascript">
					var oTable1;
					datatables_ps1("<?php echo site_url().$this->page.'data_userkirim/'.$row->id;?>", 1);
					</script>
					<div class="row-fluid">
						<div class="span7">
							<table id="dyntable1" class="table table-bordered">
								<colgroup>
									<col class="con0" style="align: center; width: 4%" />
									<col class="con1" />
									<col class="con0" />
								</colgroup>
								<thead>
									<tr>
										<th width="15px">No</th>
										<th>Nama Pegawai</th>
										<th width="20px">Aksi</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td valign="top" colspan="3" class="dataTables_empty">Loading <img src="<?php echo base_url().'assets/images/loaders/loader19.gif';?>"/></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="span5">
							<div class="widgetbox box-inverse">
								<h4 class="widgettitle"></h4>
								<div class="widgetcontent wc1">
									<form id="form5" class="stdform" method="post" action="<?php echo site_url().$this->page.'save_userkirim/'.$row->id;?>" novalidate="novalidate">
										<div class="control-group">
											<label class="control" for="userkirim_id">Pegawai/ User</label>
											<select id="userkirim_id" name="userkirim_id[]" required="" data-placeholder="List Pegawai/ User" class="chzn-select" multiple="multiple" style="width:340px;" tabindex="3">
												<?php foreach($result_user->result() as $row_user){?>
												<option value="<?php echo $row_user->id;?>"><?php echo $row_user->realname;?></option>
												<?php }?>
											</select>
										</div>
										<p class="stdformbutton">
											<button class="btn btn-primary" id="action">Simpan</button>
										</p>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }}else{?><p>Data Tidak Ditemukan, Segera hubungi admin untuk memperbaiki ini. <a href="<?php echo base_url().$this->page;?>">Kembali Ketabel</a></p><?php }?>
		</div>
	</div>
</div>