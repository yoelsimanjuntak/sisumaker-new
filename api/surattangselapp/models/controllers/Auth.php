<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

class Auth extends REST_Controller
{
    public $secretkey = ')(*&^%5u8h4n4ll0H!@#$%';

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credential: true');
        header('Access-Control-Max-Age: 86400');
        header('Access-Control-Allow-Methods:GET,POST,OPTIONS');
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $this->load->library('form_validation');
    }

    public function cektoken()
    {
        $this->load->model('loginmodel');
        $jwt = $this->input->get_request_header('Authorization', true);
        try {
            $decode = JWT::decode($jwt, $this->secretkey, ['HS256']);
            $cek_p = sha1('%U' . $decode->auth_id . ')' . $decode->username . '*&' . $decode->pegawai_id);
            if ($this->loginmodel->is_valid_num($decode->username) > 0 && $cek_p === $decode->username_p) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function viewtoken()
    {
        $jwt = $this->input->get_request_header('Authorization', true);
        return ($jwt) ? JWT::decode($jwt, $this->secretkey, ['HS256']) : false;
    }

    public function cekApi_get()
    {
        $output = [
            'status' => 1
        ];
        $this->response($output, REST_Controller::HTTP_OK);
    }

    public function index_get()
    {
        $this->response([
            'status' => 0,
            'message' => 'Akses ditolak.'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }

    public function index_post()
    {
        $this->load->model('loginmodel');

        $date = new DateTime();

        $user = $this->post('username', true);
        $pass = $this->post('password', true);
        // $telp = $this->post('telp', true);

        $row = $this->loginmodel->login($user, md5($pass));

        if ($row) {
            $payload['auth_id'] = $row->id;
            $payload['pegawai_id'] = $row->pegawai_id;
            $payload['username'] = $row->username;
            $payload['username_p'] = sha1('%U' . $payload['auth_id'] . ')' . $payload['username'] . '*&' . $payload['pegawai_id']);
            $payload['skpd_id'] = $row->tmskpd_id;
            $payload['tmunitkerja_id'] = $row->tmunitkerja_id;
            $payload['realname'] = $row->realname;
            $payload['iat'] = $date->getTimestamp(); //waktu di buat
            $payload['exp'] = $date->getTimestamp() + 2629746; //satu bulan

            $output = [
                'status' => 1,
                'id_token' => JWT::encode($payload, $this->secretkey),
                'realname' => $row->realname,
                'username' => $row->username,
                'n_skpd' => $row->n_skpd,
                'photo' => $row->photo,
                'nip' => $row->nip,
                'id_skpd' => $row->tmskpd_id,
                'pegawai_id' => $row->pegawai_id
                
            ];
            $this->response($output, REST_Controller::HTTP_OK);
        } else {
            $this->loginfail($user, $pass);
        }
    }

    public function loginfail($user, $pass)
    {
        $this->response([
            'status' => 0,
            'username' => $user,
            'password' => $pass,
            'message' => 'Invalid Credentials!'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }

    public function berita_get()
    {
        $output = [
            'link' => 'https://berita.tangerangselatankota.go.id/api/news/terbaru/10'
        ];
        $this->response($output, REST_Controller::HTTP_OK);
    }

    public function regis_post()
    {
        $this->load->model('tmpegawai');
        $this->load->model('tmuser');

        $nama = $this->post('nama', true);
        $telp = $this->post('telp', true);
        $alamat = $this->post('alamat', true);
        $email = $this->post('email', true);
        $username = $this->post('username', true);
        $password = $this->post('password', true);

        $data = [
            'username' => $username,
            'password' => md5($password),
            'realname' => $nama,
            'c_status' => 0,
            'd_entry' => date('Y-m-d H:i:s')
        ];

        

        $id_user =  $this->tmuser->insert($data);
        if (!$id_user) {
            $this->response([
                'status' => 0,
                'error' => 'errr insert user'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }

        $data2 = [
            'n_pegawai' => $nama,
            'telp' => $telp,
            'alamat' => $alamat,
            'email' => $email,
            'tmuser_id' => $id_user
        ];
        $pegawai = $this->tmpegawai->insert($data2);
        if (!$pegawai) {
            $this->response([
                'status' => 0,
                'error' => 'tmpegawai '
            ], REST_Controller::HTTP_BAD_REQUEST);
        }

        $this->response([
            'status' => 1,
            'berhasil' => 'Berhasil ',
            'id_user' => $id_user
        ], REST_Controller::HTTP_OK);
    }

    //method untuk not found 404
    public function notfound($pesan)
    {
        $this->response([
            'status' => 0,
            'message' => $pesan
        ], REST_Controller::HTTP_NOT_FOUND);
    }

    //method untuk bad request 400
    public function badreq($pesan)
    {
        $this->response([
            'status' => 0,
            'message' => $pesan
        ], REST_Controller::HTTP_BAD_REQUEST);
    }
}
