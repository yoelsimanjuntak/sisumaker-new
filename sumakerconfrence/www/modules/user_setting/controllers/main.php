<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Main extends ICA_AdminCont {

    var $page = "user_setting/main/";
    var $view = "user_setting/main/";
    var $icon = "picture";

    function __construct() {
        parent::__construct();
        $this->set_wallpaper = new Set_wallpaper();
        $this->tr_wallpaper = new Tr_wallpaper();
    }

    function wallpaper() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Setting Wallpaper",
            'title' => 'Set Wallpaper',
            'content' => 'set_wallpaper',
			'result' => $this->set_wallpaper->select('c_status', 1)->result()
        );
        $this->load->view('template_mail', $data);
    }
	
	function change_wallpaper($id)
	{
		$user_id = $this->session->userdata('auth_id');
		$row = $this->set_wallpaper->where('id', $id)->get();	
		$data = array(
			'wallpaper' => $row->link
		);
		$this->session->set_userdata($data);
		$data = array(
			'tmuser_id' => $user_id,
			'tr_wallpaper_id' => $row->id
		);
		$num = $this->tr_wallpaper->where('tmuser_id', $user_id)->count();
		if($num == 0)
		{
			$this->tr_wallpaper->insert($data);
		}
		else
		{
			$num_id = $this->tr_wallpaper->where('tmuser_id', $user_id)->get()->id;
			$this->tr_wallpaper->update($num_id, $data);
		}
		$data = array(
			'status' => 1
		);
		echo json_encode($data);
		//$data = array();
	}
}
