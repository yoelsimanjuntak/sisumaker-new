<?php

function p_indonesian_date($date)
{
    if(empty($date)) return "";
    $tgl = substr($date,8,2);
    $thn = substr($date,0,4);
    $bulan = get_nama_bulan($date);
    $return = '';
    if($thn == date('Y'))
    {
    	$return = $tgl." ".$bulan;
    }
    else
    {
    	$arrtgl = explode("-",$date);
    	$return = $tgl."/".$arrtgl[1]."/".$thn;
    }
    return $return;
}

function indo_date_time($datetime)
{
    $arr_date = explode(" ", $datetime);
    $ind_date = indonesian_date($arr_date[0]);
    return $ind_date.", ".substr($arr_date[1],0,8);
}

function indonesian_date($date)
{
    if(empty($date)) return "";
    $tgl = substr($date,8,2);
    $thn = substr($date,0,4);
    $bulan = get_nama_bulan($date);
    return $tgl." ".$bulan." ".$thn;
}

function anti_sql_injection_int($str_id) {
	$str_id = preg_replace('/[^0-9]/i','',$str_id);
	$str_id = htmlspecialchars((string)$str_id,ENT_QUOTES,'UTF-8');
	$str_id = htmlentities((string)$str_id,ENT_QUOTES,'UTF-8');
	$str_id = htmlentities((string)$str_id,ENT_QUOTES,'UTF-8');
	$str_id = strip_tags($str_id);
	$str_id = stripcslashes($str_id);
	$str_id = trim($str_id);
	return intval($str_id);
}

function anti_sql_injection_char($str_char) {
	//$str_char = preg_replace('/[^a-z|A-Z|0-9]/i','',$str_char);
	$str_char = htmlspecialchars((string)$str_char,ENT_QUOTES,'UTF-8');
	$str_char = htmlentities((string)$str_char,ENT_QUOTES,'UTF-8');
	$str_char = htmlentities((string)$str_char,ENT_QUOTES,'UTF-8');
	$str_char = strip_tags($str_char);
	$str_char = stripcslashes($str_char);
	$str_char = trim($str_char);
	return strval($str_char);
}

function get_nama_bulan($tgl)
{
	$arrtgl = explode("-",$tgl);
	switch($arrtgl[1])
	{
		case "01" : $bulan = "Januari"; break;
		case "02" : $bulan = "Februari"; break;
		case "03" : $bulan = "Maret"; break;
		case "04" : $bulan = "April"; break;
		case "05" : $bulan = "Mei"; break;
		case "06" : $bulan = "Juni"; break;
		case "07" : $bulan = "Juli"; break;
		case "08" : $bulan = "Agustus"; break;
		case "09" : $bulan = "September"; break;
		case "10" : $bulan = "Oktober"; break;
		case "11" : $bulan = "November"; break;
		default : $bulan = "Desember"; break;
	}
	return $bulan;
}

function encrip($z)
{
	$a = base_convert($z, 10, 2);
	$b = dechex($a);
	return $b;
}

function decrip($a)
{
	$b = hexdec($a);
	$z = base_convert($b, 2, 10);
	return $z;
}