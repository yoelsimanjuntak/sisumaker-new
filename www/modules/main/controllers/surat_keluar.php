<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Surat_Keluar extends ICA_AdminCont {

    var $page = "main/surat_keluar/";
    var $view = "main/surat_keluar/";
    var $icon = "check";

    function __construct() {
        parent::__construct();
		$this->restrict('11');
        $this->tmpersuratan = new Tmpersuratan();
		$this->penomoran = new Penomoran();
		$this->tmjenis_surat = new Tmjenis_surat();
		$this->tmsifat_surat = new Tmsifat_surat();
		$this->tmskpd = new Tmskpd();
		$this->surat = new Surat();
		$this->tr_surat_penerima = new Tr_surat_penerima();
		$this->main_inbox = new Main_inbox();
		$this->tmpegawai = new Tmpegawai();
		$this->outbox = new Outbox();
        $this->load->helper('function_helper');
        $this->load->library('encrypt');
		$this->load->library('api_sms_class_masking_json');
    }
	
	function cek_surat_keluar(){$return = '';$skpd_id = $this->session->userdata('skpd_id');$count = $this->tmpersuratan->where("tmpersuratan.id_skpd_in = '".$skpd_id."' and tmpersuratan.id_skpd_out = '".$skpd_id."' and tmpersuratan.status_setujui = 1 and tmpersuratan.status_disposisi = 0")->count();if($count != 0){$return = "(".$count.")";}$data = array('count' => $return);echo json_encode($data);}

    function index()
	{
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Surat Keluar",
            'title' => 'Surat Keluar',
            'content' => 'table',
            'sort' => 4
        );
       $this->load->view('template', $data);
    }
	
	function list_skpd($skpd, $kecamatan, $keluarahan)
	{
		$data = array(
			'cc' => array($skpd, $kecamatan, $keluarahan),
			'result' => $this->dinas_disposisi($this->session->userdata('skpd_id'))
		);
		$this->load->view($this->view.'list_penerima', $data);
	}

    function disposisi($id)
	{
		$row = $this->tmpersuratan->where('id', $id)->get();
		if($row->id_skpd_in == $this->session->userdata('skpd_id'))
		{
			$file_surat = '';
			foreach($this->tr_surat_penerima->select('id_persuratan', $id)->result() as $row)
			{
				if($row->file_surat_koreksi != '')
				{
					$file_surat = $row->file_surat_koreksi;
				}
			}
			$data = array(
				'result_jnssurat' => $this->tmjenis_surat->select(),
				'result_tmsifat_surat' => $this->tmsifat_surat->select(),
				'result_disposisi' => $this->dinas_disposisi($this->session->userdata('skpd_id')),
				'result_disposisi_internal' => $this->main_inbox->disposisi_pendataan($this->session->userdata('skpd_id')),
				'file_surat' => $file_surat,
				'assets' => array(
					$this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
					$this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.ui.timepicker.js"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/", "tinymce/jquery.tinymce.js"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/", "wysiwyg.js"),
					$this->lib_load_css_js->load_css(base_url() , "assets/css/", "jquery.chosen.css"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/", "chosen.jquery.min.js"),
					$this->lib_load_css_js->load_js(base_url() , "assets/js/", "bootstrap-fileupload.min.js"),
					$this->lib_load_css_js->load_css(base_url() , "assets/css/", "bootstrap-fileupload.min.css")
				),
				'breadcrubs' => ' <a href="'.base_url().$this->page.'">Surat Keluar</a> <span class="separator"></span> Kirim Surat',
				'title' => 'Kirim Surat',
				'result' => $this->tmpersuratan->select('id', $id),
				'content' => 'disposisi'
			);
			$this->load->view('template', $data);
		}
		else
		{
			echo "Error! Anda tidak memiliki Hak Akses untuk mendisposisikan File Ini.";
		}
    }
	
	function dinas_disposisi($skpd_id)
	{
		return $this->db
				->select('
					tmpegawai.id,
					tmpegawai.n_pegawai,
					tmjabatan.n_jabatan,
					tmunitkerja.n_unitkerja,
					tmskpd.n_skpd,
					tmskpd.id_kirim
				')
				->where('tmpegawai.tmskpd_id != ', $skpd_id)
				->where('tmuser_userauth.userauth_id', 5)
				->from('tmpegawai')
				->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
				->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
				->join('tmskpd','tmpegawai.tmskpd_id = tmskpd.id')
				->join('tmuser','tmpegawai.tmuser_id = tmuser.id')
				->join('tmuser_userauth', 'tmuser.id = tmuser_userauth.tmuser_id')
				->get();
	}

    	//----- Send SMS With Raja-sms.com
    function disposisi_submit($id)
	{
        if($this->session->userdata('pegawai_id') != 0)
		{	
			$valid = false;
			$pegawai_list = $this->input->post('pegawai');
			$pegawai_list_len = count($pegawai_list);
			for($i=0; $i<$pegawai_list_len; $i++)
			{
				$valid = $pegawai_list[$i];
			}
			$pegawai_list_i = $this->input->post('pegawai_internal');
			$pegawai_list_len_i = count($pegawai_list_i);
			for($i=0; $i<$pegawai_list_len_i; $i++)
			{
				$valid = $pegawai_list_i[$i];
			}
			if($valid != false)
			{
				if(count($this->input->post('pegawai')) != 0 && count($this->input->post('pegawai_internal')) != 0)
				{
					if($_FILES['userfile']['name'] != '')
					{
						$name = date('YmdHis').$this->session->userdata('auth_id');
						$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
						$endof = '.'.end($explode);
						$exten = 'uploads/surat_extern/'.$name.$endof;
						$config['file_name'] = $name;
						$config['allowed_types'] = 'pdf';
						$config['upload_path'] = 'uploads/surat_extern/';
						$config['max_size']	= '30000';
						$config['overwrite'] = true;
						$this->load->library('upload', $config);
						if(!$this->upload->do_upload())
						{
							echo '
								<script type="text/javascript">
									alert("'.$this->upload->display_errors().'");
									history.go(-1);
								</script>';
						}
						else
						{
							$data = array(
								'no_surat' => $this->input->post('no_surat'),
								'tgl_surat' => $this->input->post('tgl_surat'),
								'id_sifatsurat' => $this->input->post('id_sifatsurat'),
								'prihal' => $this->input->post('prihal'),
								'isi_surat' => $this->input->post('isi_surat'),
								'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
								'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
								'v_kegiatan' => $this->input->post('v_kegiatan'),
								'status_disposisi' => '1'
							);
							$this->tmpersuratan->update($id, $data);
							
							$senddata = array(
								'apikey' => SMSGATEWAY_APIKEY,  
								'callbackurl' => SMSGATEWAY_CALLBACKURL, 
								'datapacket'=> array()
							);
				
								//Send To Eksternal:
							$valid = false;
							for($i=0; $i<$pegawai_list_len; $i++)
							{
								$valid = $pegawai_list[$i];
							}
							if($valid != false)
							{
								$id_pegawai = $this->session->userdata('pegawai_id');
								$id_skpd_out = $this->session->userdata('skpd_id');
								$dari = $this->tmskpd->where('id', $id_skpd_out)->get()->n_skpd;;
								$send = "Ada surat masuk dari ".$dari.". Silahkan masuk ke pendataan SISUMAKER untuk melihat isi surat.";
								for($i=0; $i<$pegawai_list_len; $i++)
								{
									$id_skpd_in = $this->tmpegawai->where('id', $pegawai_list[$i])->get()->tmskpd_id;
									$data = array(
										'dari' => $dari,
										'no_surat' => $this->input->post('no_surat'),
										'tgl_surat' => $this->input->post('tgl_surat'),
										'id_jnssurat' => '1',
										'id_surat' => '1',
										'id_sifatsurat' => $this->input->post('id_sifatsurat'),
										'prihal' => $this->input->post('prihal'),
										'file' => $exten,
										'isi_surat' => $this->input->post('isi_surat'),
										'id_pegawai' => $pegawai_list[$i],
										'id_skpd_in' => $id_skpd_in,
										'id_skpd_out' => $id_skpd_out,
										'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
										'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
										'v_kegiatan' => $this->input->post('v_kegiatan'),
										'c_lintas_skpd' => 1,
										'd_entry' => date('Y-m-d')
									);
									$this->tmpersuratan->insert($data);
									$id_persuratan = $this->tmpersuratan->select_max();
													
									$data = array(
										'id_persuratan' => $id_persuratan,
										'id_pegawai_dari' => $id_pegawai,
										'id_pegawai_ke' => $pegawai_list[$i],
										'id_skpd_dari' => $id_skpd_out,
										'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
										'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
										'v_kegiatan' => $this->input->post('v_kegiatan'),
										'status' => 0,
										'cc' => 0,
										'd_entry' => date('Y-m-d H:i:s')
									);
									$this->tr_surat_penerima->insert($data);
								}
							}
							
								//Send To Internal
							$valid = false;
							for($i=0; $i<$pegawai_list_len_i; $i++)
							{
								$valid = $pegawai_list_i[$i];
							}
							if($valid != false)
							{
								$dari = $this->session->userdata('pegawai_id');
								$this->tmjabatan = new Tmjabatan();
								$this->tmunitkerja = new Tmunitkerja();
								$pegawai = $this->tmpegawai->where("id = '".$dari."'")->get();
								$jabatan = $this->tmjabatan->where("id = '".$pegawai->tmjabatan_id."'")->get()->n_jabatan;
								$unitkerja = $this->tmunitkerja->where("id ='".$pegawai->tmunitkerja_id."'")->get()->initial;
								$send = "Ada disposisi surat dari ".$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
								$true = false;
								$d_entry = date('Y-m-d H:i:s');
								for($i=0; $i<$pegawai_list_len_i; $i++)
								{
									$data = array(
										'id_persuratan' => $id,
										'id_pegawai_dari' => $dari,
										'id_pegawai_ke' => $pegawai_list_i[$i],
										'file_surat_koreksi' => $exten,
										'status' => 0,
										'd_entry' => $d_entry,
										'id_skpd_dari' => $this->session->userdata('skpd_id'),
										'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
										'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
										'v_kegiatan' => $this->input->post('v_kegiatan')
									);
									$this->tr_surat_penerima->insert($data);

									$telp = $this->tmpegawai->where("id = '".$pegawai_list_i[$i]."'")->get()->telp;
									if($telp != '-')
									{
										array_push($senddata['datapacket'],array(
											'number' => trim($telp),
											'message' => urlencode(stripslashes(utf8_encode($send))),
											'sendingdatetime' => ""));
									}
								}
							}
				
							$sms = new api_sms_class_masking_json();
							$sms->setIp(SMSGATEWAY_IPSERVER);
							$sms->setData($senddata);
							$responjson = $sms->send();
// 							$this->saldo_sms->save($responjson);
													
							redirect(base_url().'main/surat_keluar');
						}
					}
					else
					{
						echo '
							<script type="text/javascript">
								alert("Berkas yang di-upload Error!");
								history.go(-1);
							</script>';
					}
				}
				else
				{
					echo '
						<script type="text/javascript">
							alert("Inputan kepada harus di isi.");
							history.go(-1);
						</script>';
				}
			}
			else
			{
				 echo '
				<script type="text/javascript">
					alert("Sertakan penerima surat!");
					history.go(-1);
				</script>';
			}
		}
		else
		{
			echo '
				<script type="text/javascript">
					alert("Auth Id pegawai Tidak ada.");
					history.go(-1);
				</script>';
		}
    }

		//----- Send SMS With Gammu
    /*function disposisi_submit($id)
	{
        if($this->session->userdata('pegawai_id') != 0)
		{	
			$valid = false;
			$pegawai_list = $this->input->post('pegawai');
			$pegawai_list_len = count($pegawai_list);
			for($i=0; $i<$pegawai_list_len; $i++)
			{
				$valid = $pegawai_list[$i];
			}
			$pegawai_list_i = $this->input->post('pegawai_internal');
			$pegawai_list_len_i = count($pegawai_list_i);
			for($i=0; $i<$pegawai_list_len_i; $i++)
			{
				$valid = $pegawai_list_i[$i];
			}
			if($valid != false)
			{
				if(count($this->input->post('pegawai')) != 0 && count($this->input->post('pegawai_internal')) != 0)
				{
					if($_FILES['userfile']['name'] != '')
					{
						$name = date('YmdHis').$this->session->userdata('auth_id');
						$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
						$endof = '.'.end($explode);
						$exten = 'uploads/surat_extern/'.$name.$endof;
						$config['file_name'] = $name;
						$config['allowed_types'] = 'pdf';
						$config['upload_path'] = 'uploads/surat_extern/';
						$config['max_size']	= '30000';
						$config['overwrite'] = true;
						$this->load->library('upload', $config);
						if(!$this->upload->do_upload())
						{
							echo '
								<script type="text/javascript">
									alert("'.$this->upload->display_errors().'");
									history.go(-1);
								</script>';
						}
						else
						{
							$data = array(
								'no_surat' => $this->input->post('no_surat'),
								'tgl_surat' => $this->input->post('tgl_surat'),
								'id_sifatsurat' => $this->input->post('id_sifatsurat'),
								'prihal' => $this->input->post('prihal'),
								'isi_surat' => $this->input->post('isi_surat'),
								'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
								'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
								'v_kegiatan' => $this->input->post('v_kegiatan'),
								'status_disposisi' => '1'
							);
							$this->tmpersuratan->update($id, $data);
							
								//Send To Eksternal:
							$valid = false;
							for($i=0; $i<$pegawai_list_len; $i++)
							{
								$valid = $pegawai_list[$i];
							}
							if($valid != false)
							{
								$id_pegawai = $this->session->userdata('pegawai_id');
								$id_skpd_out = $this->session->userdata('skpd_id');
								$dari = $this->tmskpd->where('id', $id_skpd_out)->get()->n_skpd;;
								$send = "Ada surat masuk dari ".$dari.". Silahkan masuk ke pendataan SISUMAKER untuk melihat isi surat.";
								for($i=0; $i<$pegawai_list_len; $i++)
								{
									$id_skpd_in = $this->tmpegawai->where('id', $pegawai_list[$i])->get()->tmskpd_id;
									$data = array(
										'dari' => $dari,
										'no_surat' => $this->input->post('no_surat'),
										'tgl_surat' => $this->input->post('tgl_surat'),
										'id_jnssurat' => '1',
										'id_surat' => '1',
										'id_sifatsurat' => $this->input->post('id_sifatsurat'),
										'prihal' => $this->input->post('prihal'),
										'file' => $exten,
										'isi_surat' => $this->input->post('isi_surat'),
										'id_pegawai' => $pegawai_list[$i],
										'id_skpd_in' => $id_skpd_in,
										'id_skpd_out' => $id_skpd_out,
										'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
										'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
										'v_kegiatan' => $this->input->post('v_kegiatan'),
										'c_lintas_skpd' => 1,
										'd_entry' => date('Y-m-d')
									);
									$this->tmpersuratan->insert($data);
									$id_persuratan = $this->tmpersuratan->select_max();
													
									$data = array(
										'id_persuratan' => $id_persuratan,
										'id_pegawai_dari' => $id_pegawai,
										'id_pegawai_ke' => $pegawai_list[$i],
										'id_skpd_dari' => $id_skpd_out,
										'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
										'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
										'v_kegiatan' => $this->input->post('v_kegiatan'),
										'status' => 0,
										'cc' => 0,
										'd_entry' => date('Y-m-d H:i:s')
									);
									$this->tr_surat_penerima->insert($data);
									
									$telp = $this->tmpegawai->where("id = '".$pegawai_list[$i]."'")->get()->telp;
									if($telp != '-')
									{
										$data = array(
											'DestinationNumber' => $telp,
											'TextDecoded' => $send,
											'CreatorID' => 'Gammu',
											'DeliveryReport' => 'yes'
										);
										$this->outbox->insert($data);
									}
								}
							}
							
								//Send To Internal
							$valid = false;
							for($i=0; $i<$pegawai_list_len_i; $i++)
							{
								$valid = $pegawai_list_i[$i];
							}
							if($valid != false)
							{
								$dari = $this->session->userdata('pegawai_id');
								$this->tmjabatan = new Tmjabatan();
								$this->tmunitkerja = new Tmunitkerja();
								$pegawai = $this->tmpegawai->where("id = '".$dari."'")->get();
								$jabatan = $this->tmjabatan->where("id = '".$pegawai->tmjabatan_id."'")->get()->n_jabatan;
								$unitkerja = $this->tmunitkerja->where("id ='".$pegawai->tmunitkerja_id."'")->get()->initial;
								$send = "Ada disposisi surat dari ".$jabatan." ".$unitkerja.". Silahkan masuk ke SISUMAKER untuk melihat isi surat";
								$true = false;
								$d_entry = date('Y-m-d H:i:s');
								for($i=0; $i<$pegawai_list_len_i; $i++)
								{
									$data = array(
										'id_persuratan' => $id,
										'id_pegawai_dari' => $dari,
										'id_pegawai_ke' => $pegawai_list_i[$i],
										'file_surat_koreksi' => $exten,
										'status' => 0,
										'd_entry' => $d_entry,
										'id_skpd_dari' => $this->session->userdata('skpd_id'),
										'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
										'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
										'v_kegiatan' => $this->input->post('v_kegiatan')
									);
									$this->tr_surat_penerima->insert($data);

									$telp = $this->tmpegawai->where("id = '".$pegawai_list_i[$i]."'")->get()->telp;
									if($telp != '-')
									{
										$data = array(
											'DestinationNumber' => $telp,
											'TextDecoded' => $send,
											'CreatorID' => 'Gammu',
											'DeliveryReport' => 'yes'
										);
										$this->outbox->insert($data);
									}
								}
							}							
							redirect(base_url().'main/surat_keluar');
						}
					}
					else
					{
						echo '
							<script type="text/javascript">
								alert("Berkas yang di-upload Error!");
								history.go(-1);
							</script>';
					}
				}
				else
				{
					echo '
						<script type="text/javascript">
							alert("Inputan kepada harus di isi.");
							history.go(-1);
						</script>';
				}
			}
			else
			{
				 echo '
				<script type="text/javascript">
					alert("Sertakan penerima surat!");
					history.go(-1);
				</script>';
			}
		}
		else
		{
			echo '
				<script type="text/javascript">
					alert("Auth Id pegawai Tidak ada.");
					history.go(-1);
				</script>';
		}
    }*/

	function download_surat($id)
	{
		$row = $this->tmpersuratan->where('id', $id)->get();
		if($row->id_skpd_in == $this->session->userdata('skpd_id'))
		{
			$file_surat = $row->file;
			foreach($this->tr_surat_penerima->select('id_persuratan', $id)->result() as $row)
			{
				if($row->file_surat_koreksi != '')
				{
					$file_surat = $row->file_surat_koreksi;
				}
			}
			$download = file_get_contents('./'.$file_surat);
			$exp = explode('.', $file_surat);
			$endof = '.'.end($exp);
			$this->load->helper('download');
			force_download('surat_'.$id.$endof, $download);
		}
		else
		{
			echo "Error! Anda tidak memiliki Hak Akses untuk mendownload File Ini.";
		}
	}

	function download_lampiran($id)
	{
		$row = $this->tmpersuratan->where('id', $id)->get();
		if($row->id_skpd_in == $this->session->userdata('skpd_id'))
		{
			$file_surat = $row->file_lampiran;
			foreach($this->tr_surat_penerima->select('id_persuratan', $id)->result() as $row)
			{
				if($row->lampiran_file_koreksi != '')
				{
					$file_surat = $row->lampiran_file_koreksi;
				}
			}
			if($file_surat != '')
			{
				$download = file_get_contents('./'.$file_surat);
				$exp = explode('.', $file_surat);
				$endof = '.'.end($exp);
				$this->load->helper('download');
				force_download('lampiran_'.$id.$endof, $download);
			}
			else
			{
				echo '
					<script type="text/javascript">
						alert("Tidak ada lampiran.");
						history.go(-1);
					</script>';
			}
		}
		else
		{
			echo "Error! Anda tidak memiliki Hak Akses untuk mendownload File Ini.";
		}
	}
	
	function download_qrcode($id)
	{
		$row = $this->tmpersuratan->where('id', $id)->get();
		if($row->id_skpd_in == $this->session->userdata('skpd_id'))
		{
			$name = sha1('D7e'.$id.'Qo');
			$file = 'uploads/qrcode/'.$name.'.png';
			$this->load->library('ciqrcode');
			$uniq = sha1($id.',*&R34D^%'.$this->session->userdata('skpd_id'));
			$params = array(
				'data' => base_url().'qrcode/read/s/'.$this->session->userdata('skpd_id').'/'.$uniq.'/'.$id,
				'level' => 'H',
				'size' => 2,
				'savename' => $file
			);
			$this->ciqrcode->generate($params);
			$this->load->helper('download');
			$download = file_get_contents('./'.$file);
			force_download('qrcode_'.$id.'.png', $download);
		}
		else
		{
			echo "Error! Anda tidak memiliki Hak Akses untuk mendownload File Ini.";
		}
	}
	
	function download_ttd($id)
	{
		$row = $this->tmpersuratan->where('id', $id)->get();
		if($row->id_skpd_in == $this->session->userdata('skpd_id'))
		{
			$ttd = $this->tmpegawai->where('id', $row->id_pegawai_setujui)->get()->file_ttd;
			if($ttd != '')
			{
				if(file_exists('./'.$ttd))
				{
					$explode = explode(".", str_replace(' ', '', $ttd));
					$endof = '.' . end($explode);
					$download = file_get_contents('./'.$ttd);
					$this->load->helper('download');
					force_download('ttd_'.$id.'.'.$endof, $download);
				}
				else
				{					
					echo '
					<script type="text/javascript">
						alert("File tanda tangan tidak bisa diunduh! Silahkan hubungi admin SISUMAKER.");
						history.go(-1);
					</script>';
				}
			}
			else
			{
				echo '
				<script type="text/javascript">
					alert("File tanda tangan belum diunggah!");
					history.go(-1);
				</script>';
			}
		}
		else
		{
			echo "Error! Anda tidak memiliki Hak Akses untuk mendownload File Ini.";
		}
	}
	
    function data() {
		$skpd_id = $this->session->userdata('skpd_id');
		function cek_penyetujui($n){
			$return = $n;
			if($n == '' || $n == 'null')
			{
				$return = '<span class="text-error"><b>Error!</b><br/>Silahkan hubungi Admin SISUMAKER</span>';
			}
			return $return;
		}
		function cek_status($a){
			if($a == 1){
				$return = '<em class="text-success"> <i class="iconfa-ok-sign"></i> sudah didisposisi </em>';
			}else{
				$return = '<b class="text-warning"> <i class="iconfa-exclamation-sign"></i> belum didisposisi </b>';
			}
			return $return;
		}
		function download($id)
		{
			$return = '	<a href="'.base_url().'main/surat_keluar/download_surat/'.$id.'" target="_blank"><i class="iconfa-file"></i> Surat</a><br/>
						<a href="'.base_url().'main/surat_keluar/download_lampiran/'.$id.'" target="_blank"><i class="iconfa-paper-clip"></i> Lampiran</a><br/>
						<a href="'.base_url().'main/surat_keluar/download_qrcode/'.$id.'" target="_blank"><i class="iconfa-qrcode"></i> QRCode</a><br/>
						<a href="'.base_url().'main/surat_keluar/download_ttd/'.$id.'" target="_blank"><i class="iconfa-user-md"></i> TTD</a>';
			return $return;
		}
		function cek_jenis($id){
			if($id == '1'){
				$return = "<span class='label label-important'>Sangat Segera</span>";
			}elseif($id == '2'){
				$return = "<span class='label label-warning'>Segera</span>";
			}elseif($id == '3'){
				$return = "<span class='label label-inverse'>Rahasia</span>";
			}elseif($id == '4'){
				$return = "<span class='label label-info'>Biasa</span>";
			}
			return $return;
		}
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				tmpersuratan.id,
				tmpersuratan.dari,
				(SELECT tmpegawai.n_pegawai from tmpegawai where tmpegawai.id = tmpersuratan.id_pegawai_setujui) as n_penyetujui,
				tmpersuratan.no_surat,
				tmpersuratan.tgl_surat,
				tmpersuratan.prihal,
				tmpersuratan.id_sifatsurat,
				tmpersuratan.status_disposisi,
				tmpersuratan.file
			')
			->from('tmpersuratan')
			->where('tmpersuratan.id_skpd_in', $skpd_id)
			->where('tmpersuratan.id_skpd_out', $skpd_id)
			->where('tmpersuratan.status_setujui', 1)
			->edit_column('n_penyetujui',
				'$1',
				'cek_penyetujui(n_penyetujui)'
			)
			->edit_column('tmpersuratan.tgl_surat',
				'$1',
				'indonesian_date(tmpersuratan.tgl_surat)')
			->edit_column('tmpersuratan.id_sifatsurat',
				'$1',
				'cek_jenis(tmpersuratan.id_sifatsurat)')
			->edit_column('tmpersuratan.status_disposisi',
				'$1',
				'cek_status(tmpersuratan.status_disposisi)')
			->edit_column('tmpersuratan.file',
				'<center>$1</center>',
				'download(tmpersuratan.id)')
			->add_column('aksi',
				'<center>
					<a href="'.base_url().$this->page.'disposisi/$1">Disposisi <i class="iconfa-signout"></i></a>
				</center>',
				'tmpersuratan.id')
			->generate();
    }
	
	function history_disposisi($pegawai, $id_persuratan)
	{
		function v_surat($b)
		{
			$return = '';
			if($b != '')
			{
				$return = '<a href="'.base_url().$b.'" target="_blank">Download Surat</a>';
			}
			return $return;
		}
		function cek_terima($a)
		{
			$return = '<b class="text-warning"> <i class="iconfa-exclamation-sign"></i> belum diterima </b>';
			if($a != '')
			{
				$return = '<em class="text-success"> <i class="iconfa-ok-sign"></i> sudah diterima </em>';
			}
			return $return;
		}
		$no_surat = $this->tmpersuratan->where('id', $id_persuratan)->get()->no_surat;
		$skpd_id = $this->session->userdata('skpd_id');
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				tmpegawai.id,
				tmskpd.n_skpd,
				tmpersuratan.file,
				tmpersuratan.no_agenda,
				tr_surat_penerima.d_entry
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_pegawai_dari', $pegawai)
			->where('tr_surat_penerima.id_skpd_dari', $skpd_id)
			->where('tmpersuratan.id_skpd_in <> ', $skpd_id)
			->where('tmpersuratan.no_surat', $no_surat)
			->join('tmpegawai','tr_surat_penerima.id_pegawai_ke = tmpegawai.id')
			->join('tmpersuratan','tr_surat_penerima.id_persuratan = tmpersuratan.id')
			->join('tmskpd','tmpegawai.tmskpd_id = tmskpd.id')
			->edit_column('tmpersuratan.file',
				'$1',
				'v_surat(tmpersuratan.file)')
			->edit_column('tmpersuratan.no_agenda',
				'$1',
				'cek_terima(tmpersuratan.no_agenda)')
			->edit_column('tr_surat_penerima.d_entry',
				'$1',
				'indo_date_time(tr_surat_penerima.d_entry)')
			->generate();
	}
	
	function history_disposisi_i($pegawai, $id_persuratan)
	{
		function cek_baca($a)
		{
			$return = '<b class="text-warning"> <i class="iconfa-exclamation-sign"></i> belum dibaca </b>';
			if($a == 1)
			{
				$return = '<em class="text-success"> <i class="iconfa-ok-sign"></i> sudah dibaca </em>';
			}
			return $return;
		}
		function name($n, $j, $u)
		{
			return '<b>'.$n.'</b><br/>('.$j.' '.$u.')';
		}
		$no_surat = $this->tmpersuratan->where('id', $id_persuratan)->get()->no_surat;
		$skpd_id = $this->session->userdata('skpd_id');
		$this->load->library('datatables');
		echo $this->datatables
			->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tr_surat_penerima.status,
				tr_surat_penerima.d_entry,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
			->from('tr_surat_penerima')
			->where('tr_surat_penerima.id_pegawai_dari', $pegawai)
			->where('tr_surat_penerima.id_skpd_dari', $skpd_id)
			->where('tmpersuratan.id_skpd_in', $skpd_id)
			->where('tmpersuratan.no_surat', $no_surat)
			->join('tmpegawai','tr_surat_penerima.id_pegawai_ke = tmpegawai.id')
			->join('tmpersuratan','tr_surat_penerima.id_persuratan = tmpersuratan.id')
			->join('tmjabatan','tmpegawai.tmjabatan_id = tmjabatan.id')
			->join('tmunitkerja','tmpegawai.tmunitkerja_id = tmunitkerja.id')
			->edit_column('tmpegawai.n_pegawai',
				'$1',
				'name(tmpegawai.n_pegawai, tmjabatan.n_jabatan, tmunitkerja.n_unitkerja)'
			)
			->edit_column('tr_surat_penerima.status',
				'$1',
				'cek_baca(tr_surat_penerima.status)')
			->edit_column('tr_surat_penerima.d_entry',
				'$1',
				'indo_date_time(tr_surat_penerima.d_entry)')
			->unset_column('tmjabatan.n_jabatan')
			->unset_column('tmunitkerja.n_unitkerja')
			->generate();
	}

}