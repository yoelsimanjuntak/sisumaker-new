<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title></title>
        <link rel="stylesheet" href=<?php echo site_url("assets/theme/css/style.default.css"); ?> type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
    <body class="errorpage">
        <div class="mainwrapper">
            <div class="errortitle">
                <h4 class="animate0 fadeInUp">Ups.. Halaman yang ada cari tidak tersedia...</h4>
                <span class="animate1 bounceIn">4</span>
                <span class="animate2 bounceIn">0</span>
                <span class="animate3 bounceIn">4</span>
                <div class="errorbtns animate4 fadeInUp">
                    <a onclick="history.back()" class="btn btn-primary btn-large">Go to Previous Page</a>
                    <!--                    <a href="dashboard.html" class="btn btn-large">Go to Dashboard</a>-->
                </div>
            </div>
        </div><!--mainwrapper-->
    </body>
</html>
