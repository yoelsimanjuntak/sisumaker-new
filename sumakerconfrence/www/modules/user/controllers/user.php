<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class User extends ICA_AdminCont {

    var $page = "user/user/";
    var $view = "user/user/";
    var $icon = "cogs";

    function __construct() {
        parent::__construct();
		$this->restrict('7');
        $this->tmuser = new Tmuser();
        $this->userauth = new Userauth();
        $this->tmuser_userauth = new Tmuser_userauth();
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> User",
            'title' => 'Master User',
            'content' => 'table',
            'sort' => 1
        );
        $this->load->view('template', $data);
    }

    function add() {
        $this->load->view($this->view . 'add');
    }

    function edit($id) {
        $data = array(
            'result_userauth' => $this->userauth->select(),
            'result' => $this->tmuser->select('id', $id),
            'assets' => array(
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "jquery.chosen.css"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "chosen.jquery.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> User",
            'title' => 'Setting User',
            'content' => 'edit'
        );
        $this->load->view('template', $data);
    }

    function save() {
        if ($this->tmuser->select('username', $this->input->post('username'))->num_rows() == 0) {
            $data = array(
                'realname' => $this->input->post('realname'),
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password')),
                'c_status' => $this->input->post('c_status'),
                'd_entry' => date('Y-m-d H:i:s')
            );
            $this->tmuser->insert($data);
            $output = array(
                'status' => 1
            );
            echo json_encode($output);
        } else {
            $output = array(
                'status' => 11
            );
            echo json_encode($output);
        }
    }

    function save_name($id) {
        $data = array(
            'realname' => $this->input->post('realname'),
            'd_update' => date('Y-m-d H:i:s')
        );
        $this->tmuser->update($id, $data);
        $output = array(
            'status' => 2
        );
        echo json_encode($output);
    }

    function save_username($id) {
        if ($this->tmuser->select('username', $this->input->post('username'))->num_rows() == 0) {
            $data = array(
                'username' => $this->input->post('username'),
                'd_update' => date('Y-m-d H:i:s')
            );
            $this->tmuser->update($id, $data);
            $output = array(
                'status' => 2
            );
            echo json_encode($output);
        } else {
            $output = array(
                'status' => 22
            );
            echo json_encode($output);
        }
    }

    function save_password($id) {
        $data = array(
            'password' => md5($this->input->post('password')),
            'd_update' => date('Y-m-d H:i:s')
        );
        $this->tmuser->update($id, $data);
        $output = array(
            'status' => 2
        );
        echo json_encode($output);
    }

    function save_userauth($id) {
        $true = false;
        $userauth_list = $this->input->post('userauth_id');
        $userauth_list_len = count($userauth_list);
        for ($i = 0; $i < $userauth_list_len; $i++) {
            if ($this->tmuser_userauth->where("tmuser_id = '" . $id . "' and userauth_id = '" . $userauth_list[$i] . "'")->count() == 0) {
                $data = array(
                    'tmuser_id' => $id,
                    'userauth_id' => $userauth_list[$i]
                );
                $this->tmuser_userauth->insert($data);
                $true = true;
            }
        }
        if ($true == true) {
            $output = array(
                'status' => 2
            );
            echo json_encode($output);
        } else {
            $output = array(
                'status' => 22
            );
            echo json_encode($output);
        }
    }

    function delete($id) {
        $this->tmuser->delete($id);
    }

    function delete_userauth($id) {
        $this->tmuser_userauth->delete($id);
    }

    function delete_perizinan($id) {
        $this->tmuser_tmperizinan->delete($id);
    }

    function data_userauth($id) {
        $this->load->library('datatables');
        echo $this->datatables
                ->select('
					tmuser_userauth.id,
					userauth.description
				')
                ->from('tmuser_userauth')
                ->where('tmuser_userauth.tmuser_id', $id)
                ->join('userauth', 'tmuser_userauth.userauth_id = userauth.id_role')
                ->add_column('aksi', '<center>
					<a id="delete_userauth" href="#" to="' . base_url() . $this->page . 'delete_userauth/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>', 'tmuser_userauth.id')
                ->generate();
    }

    function data() 
	{
        function cek_lastlogin($time) {
            if ($time != '') {
                $return = timespan($time);
            } else {
                $return = "-";
            }
            return $return;
        }

        function cek_pegawai($id) {
            $result = mysql_query("SELECT count(id) as num FROM tmpegawai where tmuser_id = '" . $id . "'");
            while ($row = mysql_fetch_array($result)) {
                $count = $row['num'];
            }
            if ($count == 0) {
                $return = "<span class='label'>Tidak</span>";
            } else {
                $return = "<span class='label label-success'>Ya</span>";
            }
            return $return;
        }

        $this->load->library('datatables');
        echo $this->datatables
				->select('
					tmuser.id,
					tmuser.username,
					tmuser.realname,
					tmuser.last_login
					')
				->from('tmuser')
				->edit_column('tmuser.last_login', '$1', 'cek_lastlogin(tmuser.last_login)')
				->add_column('pegawai', '<center>$1</center>', 'cek_pegawai(tmuser.id)')
				->add_column('aksi', '<center>
					<a href="' . base_url() . $this->page . 'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
					<a id="delete" href="#" to="' . base_url() . $this->page . 'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>', 'tmuser.id')
				->generate();
    }

}
