<div id="result"></div>
<?php if ($result->num_rows() != 0) { foreach ($result->result() as $row) { ?>
<div class="widgetbox box-inverse">
    <?php echo $this->load->view($this->view.'validate_accept'); ?>
    <h4 class="widgettitle"></h4>
    <div class="widgetcontent wc1">
		<form id="form1" class="stdform" method="post" action="<?php echo site_url() . $this->page . 'save_accept/' . $row->id; ?>" novalidate="novalidate">
			<input type="hidden" name="no_surat_temp" value="<?php echo $row->no_surat;?>"/>
			<div class="row-fluid">
				<div class="span6">
					<div class="control-group">
						<label class="control-label" for="id_surat">Jenis Surat</label>
						<div class="controls">
							<select name="id_surat" id="id_surat" class="input-medium" disabled="disabled">
								<option value="">Pilih</option>
								<?php foreach($result_surat->result() as $row_surat){ ?>
								<option value="<?php echo $row_surat->id;?>"<?php if($row->id_surat == $row_surat->id){?> selected="selected"<?php }?>><?php echo $row_surat->n_surat;?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="dari">Surat Dari</label>
						<div class="controls"><input type="text" name="dari" id="dari" class="span8" value="<?php echo $row->dari;?>" disabled="disabled"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="no_surat">No. Surat</label>
						<div class="controls"><input type="text" name="no_surat" id="no_surat" class="input-medium" value="<?php echo $row->no_surat;?>" disabled="disabled"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="tgl_surat">Tanggal Surat</label>
						<div class="controls"><input type="text" name="tgl_surat" id="tgl_surat" class="input-medium" value="<?php echo $row->tgl_surat;?>" disabled="disabled"></div>
					</div>					
					<div class="control-group">
						<label class="control-label" for="prihal">Perihal</label>
						<div class="controls">
							<textarea name="prihal" id="prihal" class="span8" style="height:120px" disabled="disabled"><?php echo $row->prihal;?></textarea>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="control-group">
						<label class="control-label" for="tgl_terima_surat">Diterima Tanggal</label>
						<div class="controls"><input type="text" name="tgl_terima_surat" id="tgl_terima_surat" class="input-medium datepicker_eng" value="<?php echo date('Y-m-d');?>" disabled="disabled"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="no_agenda">No. Agenda</label>
						<div class="controls"><input type="text" name="no_agenda" id="no_agenda" class="input-small" disabled="disabled" value="<?php echo $no_agenda;?>"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="id_sifatsurat">Sifat</label>
						<div class="controls">
							<select name="id_sifatsurat" class="input-medium">
								<option value="">Pilih</option>
								<?php foreach($result_tmsifat_surat->result() as $row_tmsifat_surat){ ?>
								<option value="<?php echo $row_tmsifat_surat->id;?>"<?php if($row->id_sifatsurat == $row_tmsifat_surat->id){?> selected="selected"<?php }?>><?php echo $row_tmsifat_surat->n_sifatsurat;?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<b>Diisi jika perlu ada kegiatan:</b>
					<div class="control-group">
						<label class="control-label" for="d_awal_kegiatan">Tanggal Mulai</label>
						<div class="controls"><input type="text" name="d_awal_kegiatan" id="d_awal_kegiatan" class="input-medium datepicker_eng_time" placeholder="0000-00-00 00:00:00" value="<?php echo $row->d_awal_kegiatan;?>" disabled="disabled"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="d_akhir_kegiatan">Tanggal Akhir</label>
						<div class="controls"><input type="text" name="d_akhir_kegiatan" id="d_akhir_kegiatan" class="input-medium datepicker_eng_time" placeholder="0000-00-00 00:00:00" value="<?php echo $row->d_akhir_kegiatan;?>" disabled="disabled"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="v_kegiatan">Lokasi</label>
						<div class="controls"><textarea name="v_kegiatan" id="v_kegiatan" class="input-xlarge" placeholder="exp: Lokasi" disabled="disabled"><?php echo $row->v_kegiatan;?></textarea></div>
					</div>
				</div>
			</div>
			<p class="stdformbutton">
				<button class="btn btn-primary" id="action"><i class="iconfa-check"></i> Terima Surat</button>
				<a href="<?php echo base_url().$this->page;?>" id="close_add" class="btn btn-default">Batal</a>
			</p>
		</form>
    </div>
</div>
<div class="widgetbox box-inverse">
    <h4 class="widgettitle">File Surat</h4>
    <div class="widgetcontent wc1">
		<form class="stdform" method="post" action="<?php echo site_url().$this->page . 'save_file/' . $row->id; ?>" enctype="multipart/form-data">
			<object type="application/pdf" data="<?php echo base_url().$row->file;?>" width="100%" height="500px"/>
		</form>
    </div>
</div>
<?php } } else { ?><p>Data Tidak Ditemukan, <a href="#" id="refresh">Segarkan Tabel</a></p><?php } ?>