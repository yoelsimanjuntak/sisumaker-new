<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Loginmodel extends CI_Model
{
    public $table = 'tmuser';
    public $key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function login($user, $pass)
    {
        return $this->db
            ->select('
    					tmuser.id,
    					tmuser.username,
    					tmuser.realname,
    					tmuser.photo,
    					tmskpd.n_skpd,
    					tmpegawai.nip,
    					tmpegawai.id as pegawai_id,
                        tmpegawai.tmskpd_id,
                        tmpegawai.tmunitkerja_id

    				')
            ->from($this->table)
            ->join('tmpegawai', 'tmuser.id = tmpegawai.tmuser_id')
            ->join('tmskpd', 'tmpegawai.tmskpd_id = tmskpd.id')
            ->where('tmuser.username', $user)
            ->where('tmuser.password', $pass)

            ->where('tmpegawai.c_status', 1)
            ->get()
            ->row();
    }

    public function is_valid_num($username)
    {
        return $this->db
            ->select('id')
            ->from($this->table)
            ->where('username', $username)
            ->get()
            ->num_rows();
    }

    public function update($id, $form)
    {
        return $this->db
            ->where($this->key, $id)
            ->update($this->table, $form);
    }
}
