jQuery.noConflict();
var tabledata;
jQuery(document).ready(function() {

	l = window.location.href.slice();
	if(l != base + 'main/notadinas' && l != base + 'pendataan'){
		jQuery('.leftmenu ul.nav-o li a[href="' + window.location.href.slice() + '"]').addClass('active');
    }
	
	jQuery(".datepicker_eng").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true
	});
	
	jQuery(".datepicker_ind").datepicker({
		dateFormat: 'dd-mm-yy',
		changeMonth: true,
		changeYear: true
	});

    // change skin color
    jQuery('.skin-color a').click(function() {
        return false;
    });
    jQuery('.skin-color a').hover(function() {
        var s = jQuery(this).attr('href');
        if (jQuery('#skinstyle').length > 0) {
            if (s != 'default') {
                jQuery('#skinstyle').attr('href', base + 'assets/css/style.' + s + '.css');
                jQuery.cookie('skin-color', s, {
                    path: '/'
                });
            } else {
                jQuery('#skinstyle').remove();
                jQuery.cookie("skin-color", '', {
                    path: '/'
                });
            }
        } else {
            if (s != 'default') {
                jQuery('head').append('<link id="skinstyle" rel="stylesheet" href="' + base + 'assets/css/style.' + s + '.css" type="text/css" />');
                jQuery.cookie("skin-color", s, {
                    path: '/'
                });
            }
        }
        return false;
    });

    // load selected skin color from cookie
    if (jQuery.cookie('skin-color')) {
        var c = jQuery.cookie('skin-color');
        if (c) {
            jQuery('head').append('<link id="skinstyle" rel="stylesheet" href="' + base + 'assets/css/style.' + c + '.css" type="text/css" />');
            jQuery.cookie("skin-color", c, {
                path: '/'
            });
        }
    }
});

function datatables(sort) {
	jQuery(document).ready(function(){
		oTable = jQuery('#dyntable').dataTable({
			"bStateSave":false,
			"aaSorting":[[sort, "desc" ]],
			"iDisplayLength":10,
			"sPaginationType":"full_numbers"
		});
	});
}

function datatables_ps(url, sort){
	jQuery(document).ready(function(){
		oTable = jQuery('#dyntable').dataTable({
			"bStateSave":false,
			"aaSorting":[[sort, "desc" ]],
			"iDisplayLength":10,
			"sPaginationType":"full_numbers",
			"bProcessing":true,
			"bServerSide":true,
			"deferRender":true,
			"sAjaxSource":url,
			"fnServerData":function(sSource, aoData, fnCallback){
				jQuery.ajax({
					"dataType":"json",
					"type":"post",
					"cache":false,
					"url":sSource,
					"data":aoData,
					"success":fnCallback
				})
			},
			"fnDrawCallback": function ( oSettings ) {
				if ( oSettings.bSorted || oSettings.bFiltered )
				{
					for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
					{
						jQuery('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
					}
				}
			}
		});
	});
}

function datatables_ps1(url, sort){
	jQuery(document).ready(function(){
		oTable1 = jQuery('#dyntable1').dataTable({
			"bStateSave":false,
			"aaSorting":[[sort, "desc" ]],
			"iDisplayLength":10,
			"sPaginationType":"full_numbers",
			"bProcessing":true,
			"bServerSide":true,
			"sAjaxSource":url,
			"fnServerData":function(sSource, aoData, fnCallback){
				jQuery.ajax({
					"dataType":"json",
					"type":"post",
					"cache":false,
					"url":sSource,
					"data":aoData,
					"success":fnCallback
				})
			},
			"fnDrawCallback": function ( oSettings ) {
					if ( oSettings.bSorted || oSettings.bFiltered )
					{
						for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
						{
							jQuery('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
						}
					}
				}
		});
	});
}

function fancybox(){
	jQuery("#view").live('click',function(event){
		event.preventDefault();
			jQuery.fancybox.open({
				href 			: jQuery(this).attr("to"),
				type 			: 'iframe',
				padding 		: 2,
				opacity			: true,
				titlePosition	: 'over',
				openEffect 		: 'elastic',
				openSpeed  		: 150,
				closeEffect 	: 'elastic',
				closeSpeed  	: 150,
				width			: 900,
				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'transparent',
							'background' : 'rgba(0, 0, 0, 0.6)'
						}
					}
				}
		});
	});
}

function fancybox_image(){
	jQuery("a#fancybox_image").fancybox({
		'titleShow'     : false,
		'transitionIn'	: 'elastic',
		'transitionOut'	: 'elastic',
		'easingIn'      : 'easeOutBack',
		'easingOut'     : 'easeInBack',
		helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'transparent',
							'background' : 'rgba(0, 0, 0, 0.6)'
						}
					}
				}
	});
}

function titleScroller(text) {
    document.title = text;
    setTimeout(function () {
        titleScroller(text.substr(1) + text.substr(0, 1));
    }, 500);
}

function cek_inbox() {
    jQuery.ajax({
		type:'post',
        url: base + "main/inbox/cek_inbox",
        cache: false,
        dataType: 'json',
        success: function(data) {
            jQuery("#count_new_inbox").html(data.count);
			if(data.count != 0)
			{
				text = data.count + ' Surat Masuk';
				titleScroller(text);
			}
        }
    });
    //var waktu = setTimeout("cek_inbox()", 30000);
}

function cek_pendataan() {
    jQuery.ajax({
		type:'post',
        url: base + "pendataan/cek_pendataan",
        cache: false,
        dataType: 'json',
        success: function(data) {
            jQuery("#count_new_pendataan").html(data.count);
        }
    });
    //var waktu = setTimeout("cek_pendataan()", 30000);
}

function cek_surat_keluar() {
    jQuery.ajax({
		type:'post',
        url: base + "main/surat_keluar/cek_surat_keluar",
        cache: false,
        dataType: 'json',
        success: function(data) {
            jQuery("#count_new_surat_keluar").html(data.count);
        }
    });
    //var waktu = setTimeout("cek_surat_keluar()", 30000);
}

function cek_new_message() {
    jQuery.ajax({
		type:'post',
        url: base + "chat/message/cek_new_message",
        cache: false,
        dataType: 'json',
        success: function(data) {
            jQuery("#count_new_message").html(data.count);
        }
    });
    //var waktu = setTimeout("cek_new_message()", 30000);
}

/*function cek_pascatinjauan() {
    jQuery.ajax({
		type:'post',
        url: base + "skpd/pascatinjauan/cek_pascatinjauan",
        cache: false,
        dataType: 'json',
        success: function(data) {
            jQuery("#count_pascatinjauan").html(data.count_pascatinjauan);
            jQuery("#new_pascatinjauan").html(data.new_pascatinjauan);
        }
    });
    var waktu = setTimeout("cek_pascatinjauan()", 30000);
}

function cek_hasilrekomendasi() {
    jQuery.ajax({
		type:'post',
        url: base + "skpd/hasilrekomendasi/cek_hasilrekomendasi",
        cache: false,
        dataType: 'json',
        success: function(data) {
            jQuery("#count_hasilrekomendasi").html(data.count_hasilrekomendasi);
            jQuery("#new_hasilrekomendasi").html(data.new_hasilrekomendasi);
        }
    });
    var waktu = setTimeout("cek_hasilrekomendasi()", 30000);
}

function cek_arsip() {
    jQuery.ajax({
		type:'post',
        url: base + "arsip/arsip/cek_arsip",
        cache: false,
        dataType: 'json',
        success: function(data) {
            jQuery("#count_arsip").html(data.count_arsip);
            jQuery("#new_arsip").html(data.new_arsip);
        }
    });
    var waktu = setTimeout("cek_arsip()", 30000);
}*/