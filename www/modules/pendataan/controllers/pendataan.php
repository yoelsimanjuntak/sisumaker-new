<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Pendataan extends ICA_AdminCont
{
    public $page = "pendataan/pendataan/";
    public $view = "pendataan/pendataan/";
    public $icon = "pencil";

    public function __construct()
    {
        parent::__construct();
        $this->restrict('5');
        $this->tmpersuratan = new Tmpersuratan();
        $this->penomoran = new Penomoran();
        $this->tmsifat_surat = new Tmsifat_surat();
        $this->surat = new Surat();
        $this->tr_surat_penerima = new Tr_surat_penerima();
        $this->tmpegawai = new Tmpegawai();
        $this->outbox = new Outbox();
        $this->main_inbox = new Main_inbox();
        $this->load->helper('function_helper');
        $this->load->library('api_sms_class_reguler_json');
        $this->load->library('onesignal');
    }

    public function index()
    {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/js/fancybox/", "jquery.fancybox.css?v=2.1.5"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/fancybox/", "jquery.fancybox.js?v=2.1.5")
                ),
            'breadcrubs' => " Surat Masuk",
            'title' => 'Surat Masuk',
            'content' => 'table',
            'sort' => 0
        );
        $this->load->view('template', $data);
    }
    
    public function notif($data)
    {
        $notif = new onesignal();
        $notif->setData($data);
        
        $responjson2 = $notif->android();
    }

    public function notifTest()
    {
        $Onesignal = [
            'dari' => 'Aldi Ardianto',
            'ke' => 3171,
            'perihal' =>  'Ajakan untuk bermain game'
        ];

        $notif = $this->notif($Onesignal);
        // $this->response($notif, Auth::HTTP_OK);
    }
    
    public function cek_pendataan()
    {
        $return = ' ';
        $count = $this->tmpersuratan->where("tgl_terima_surat IS NULL and no_agenda IS NULL and id_skpd_in = '".$this->session->userdata('skpd_id')."' and c_lintas_skpd = 1")->count();
        if ($count != 0) {
            $return = "(".$count.")";
        }
        $data = array('count' => $return);
        echo json_encode($data);
    }
    
    public function add()
    {
        $data = array(
            'no_agenda' => 'M-'.$this->penomoran->nomor($this->session->userdata('skpd_id'), 1),
            'result_surat' => $this->surat->select('id <', 3),
            'result_tmsifat_surat' => $this->tmsifat_surat->select(),
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.ui.timepicker.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "bootstrap-fileupload.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "bootstrap-fileupload.min.css")
                ),
            'breadcrubs' => " <a href='".base_url().$this->page."'>Pendataan Surat</a> <span class='separator'></span> Tambah Pendataan Surat",
            'title' => 'Pendataan Surat',
            'content' => 'add'
        );
        $this->load->view('template', $data);
    }
    
    public function edit($id)
    {
        $c_lintas_skpd = $this->tmpersuratan->where("id = '".$id."'")->get()->c_lintas_skpd;
        if ($c_lintas_skpd == 1) {
            redirect($this->page.'accept/'.$id);
        }
        $data = array(
            'result_surat' => $this->surat->select('id <', 3),
            'result_tmsifat_surat' => $this->tmsifat_surat->select(),
            'result' => $this->tmpersuratan->select('id', $id),
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.ui.timepicker.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "bootstrap-fileupload.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "bootstrap-fileupload.min.css")
                ),
            'breadcrubs' => " <a href='".base_url().$this->page."'>Pendataan Surat</a> <span class='separator'></span> Edit Pendataan Surat",
            'title' => 'Pendataan Surat',
            'content' => 'edit'
        );
        $this->load->view('template', $data);
    }
    
    public function accept($id)
    {
        $row = $this->tmpersuratan->where("id = '".$id."'")->get();
        $c_lintas_skpd = $row->c_lintas_skpd;
        if ($c_lintas_skpd == 0) {
            redirect($this->page.'edit/'.$id);
        }
        $no_agenda = $row->no_agenda;
        if ($no_agenda == '') {
            $id_surat_penerima = $this->tr_surat_penerima->where("id_persuratan = '".$id."' and id_pegawai_ke = '".$this->session->userdata('pegawai_id')."'")->get()->id;
            $data = array(
                'status' => 1,
                'd_read' => date('Y-m-d H:i:s')
            );
            $this->tr_surat_penerima->update($id_surat_penerima, $data);
        }
        $data = array(
            'no_agenda' => 'M-'.$this->penomoran->nomor($this->session->userdata('skpd_id'), 1),
            'result_surat' => $this->surat->select('id <', 3),
            'result_tmsifat_surat' => $this->tmsifat_surat->select(),
            'result' => $this->tmpersuratan->select('id', $id),
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.ui.timepicker.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "bootstrap-fileupload.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "bootstrap-fileupload.min.css")
                ),
            'breadcrubs' => " <a href='".base_url().$this->page."'>Pendataan Surat</a> <span class='separator'></span> Terima Surat",
            'title' => 'Terima Surat',
            'content' => 'accept'
        );
        $this->load->view('template', $data);
    }
    
    public function save_accept($id)
    {
        $no_agenda = $this->tmpersuratan->where("id = '".$id."'")->get()->no_agenda;
        if ($no_agenda == '') {
            $data = array(
                'no_agenda' => 'M-'.$this->penomoran->nomor_save($this->session->userdata('skpd_id'), 1),
                'tgl_terima_surat' => date('Y-m-d'),
                'id_sifatsurat' => $this->input->post('id_sifatsurat')
            );
            $this->tmpersuratan->update($id, $data);
            $output = array(
                'status' => 2
            );
            echo json_encode($output);
        } else {
            $output = array(
                'status' => 22
            );
            echo json_encode($output);
        }
    }
    
    public function save($id = false)
    {
        if ($id == false) {
            $num_rows = $this->tmpersuratan->where("no_surat = '".$this->input->post('no_surat')."' and id_skpd_in = '".$this->session->userdata('skpd_id')."'")->count();
            if ($num_rows == 0) {
                $name = date('YmdHis').$this->session->userdata('auth_id');
                $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
                $endof = '.'.end($explode);
                $exten = 'uploads/surat_masuk/'.$name.$endof;
                $config['file_name'] = $name;
                $config['allowed_types'] = 'pdf';
                $config['upload_path'] = 'uploads/surat_masuk/';
                $config['max_size']	= '300000';
                $config['overwrite'] = true;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload()) {
                    echo '
						<script type="text/javascript">
							alert("'.$this->upload->display_errors().'");
							history.go(-1);
						</script>';
                } else {
                    if ($this->input->post('d_awal_kegiatan') != '' && $this->input->post('d_akhir_kegiatan') != '' && $this->input->post('v_kegiatan') != '') {
                        $data = array(
                            'no_surat' => $this->input->post('no_surat'),
                            'no_agenda' => 'M-'.$this->penomoran->nomor_save($this->session->userdata('skpd_id'), 1),
                            'dari' => $this->input->post('dari'),
                            'tgl_terima_surat' => $this->input->post('tgl_terima_surat'),
                            'tgl_surat' => $this->input->post('tgl_surat'),
                            'id_sifatsurat' => $this->input->post('id_sifatsurat'),
                            'id_surat' => $this->input->post('id_surat'),
                            'prihal' => $this->input->post('prihal'),
                            'file' => $exten,
                            'id_pegawai' => $this->session->userdata('pegawai_id'),
                            'id_skpd_in' => $this->session->userdata('skpd_id'),
                            'id_skpd_out' => '0',
                            'd_entry' => date('Y-m-d'),
                            'id_jnssurat' => 1,
                            'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
                            'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
                            'v_kegiatan' => $this->input->post('v_kegiatan')
                        );
                    } else {
                        $data = array(
                            'no_surat' => $this->input->post('no_surat'),
                            'no_agenda' => 'M-'.$this->penomoran->nomor_save($this->session->userdata('skpd_id'), 1),
                            'dari' => $this->input->post('dari'),
                            'tgl_terima_surat' => $this->input->post('tgl_terima_surat'),
                            'tgl_surat' => $this->input->post('tgl_surat'),
                            'id_sifatsurat' => $this->input->post('id_sifatsurat'),
                            'id_surat' => $this->input->post('id_surat'),
                            'prihal' => $this->input->post('prihal'),
                            'file' => $exten,
                            'id_pegawai' => $this->session->userdata('pegawai_id'),
                            'id_skpd_in' => $this->session->userdata('skpd_id'),
                            'id_skpd_out' => '0',
                            'd_entry' => date('Y-m-d'),
                            'id_jnssurat' => 1
                        );
                    }
                    $this->tmpersuratan->insert($data);
                    redirect(base_url().$this->page);
                }
            } else {
                echo '
					<script type="text/javascript">
						alert("No. Surat sudah ada!");
						history.go(-1);
					</script>';
            }
        } else {
            if ($this->input->post('no_surat_temp') == $this->input->post('no_surat')) {
                $data = array(
                    'dari' => $this->input->post('dari'),
                    'tgl_terima_surat' => $this->input->post('tgl_terima_surat'),
                    'tgl_surat' => $this->input->post('tgl_surat'),
                    'id_sifatsurat' => $this->input->post('id_sifatsurat'),
                    'id_surat' => $this->input->post('id_surat'),
                    'prihal' => $this->input->post('prihal'),
                    'id_pegawai' => $this->session->userdata('pegawai_id'),
                    'id_jnssurat' => 1
                );
                $this->tmpersuratan->update($id, $data);
                $output = array(
                    'status' => 2
                );
                echo json_encode($output);
            } else {
                $num_rows = $this->tmpersuratan->where("no_surat = '".$this->input->post('no_surat')."' and id_skpd_in = '".$this->session->userdata('skpd_id')."'")->count();
                if ($num_rows == 0) {
                    $data = array(
                        'no_surat' => $this->input->post('no_surat'),
                        'dari' => $this->input->post('dari'),
                        'tgl_terima_surat' => $this->input->post('tgl_terima_surat'),
                        'tgl_surat' => $this->input->post('tgl_surat'),
                        'id_sifatsurat' => $this->input->post('id_sifatsurat'),
                        'id_surat' => $this->input->post('id_surat'),
                        'prihal' => $this->input->post('prihal'),
                        'id_pegawai' => $this->session->userdata('pegawai_id'),
                        'id_jnssurat' => 1
                    );
                    $this->tmpersuratan->update($id, $data);
                    $output = array(
                        'status' => 2
                    );
                    echo json_encode($output);
                } else {
                    $output = array(
                        'status' => 22
                    );
                    echo json_encode($output);
                }
            }
        }
    }
    
    public function save_file($id)
    {
        if ($_FILES['userfile']['name'] != "") {
            $name = date('YmdHis').$this->session->userdata('auth_id');
            $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.'.end($explode);
            $exten = 'uploads/surat_masuk/'.$name.$endof;
            $config['file_name'] = $name;
            $config['allowed_types'] = 'pdf';
            $config['upload_path'] = 'uploads/surat_masuk/';
            $config['max_size']	= '3000000';
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                echo '
					<script type="text/javascript">
						alert("'.$this->upload->display_errors().'");
						history.go(-1);
					</script>';
            } else {
                $temp_file = $this->tmpersuratan->where("id = '$id'")->get()->file;
                if (file_exists('./'.$temp_file)) {
                    unlink('./'.$temp_file);
                }
                $data = array(
                    'file' => $exten
                );
                $this->tmpersuratan->update($id, $data);
                redirect(base_url().$this->page.'edit/'.$id);
            }
        } else {
            echo '
			<script type="text/javascript">
				alert("File Surat Harus Diisi");
				history.go(-1);
			</script>';
        }
    }
    
    public function preview($id)
    {
        $data = array(
            'result' => $this->tmpersuratan->select('id', $id),
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "bootstrap-fileupload.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "bootstrap-fileupload.min.css")
                ),
            'title' => 'Preview Surat',
            'content' => 'preview'
        );
        $this->load->view('view', $data);
    }
    
    public function disposisi($id)
    {
        $data = array(
            'result' => $this->tmpersuratan->select('id', $id),
            'result_disposisi' => $this->main_inbox->disposisi_pendataan($this->session->userdata('skpd_id')),
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js"),
                $this->lib_load_css_js->load_css(base_url(), "assets/css/", "jquery.chosen.css"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "chosen.jquery.min.js")
                ),
            'title' => 'Disposisi Surat',
            'content' => 'disposisi'
        );
        $this->load->view('view', $data);
    }
    
    //----- Send SMS With Raja-sms.com
    public function disposisi_submit($id)
    {
        $valid = false;
        $pegawai_list = $this->input->post('pegawai');
        $pegawai_list_len = count($pegawai_list);
        for ($i=0; $i<$pegawai_list_len; $i++) {
            $valid = $pegawai_list[$i];
        }
        if ($valid == false) {
            echo '
			<script type="text/javascript">
				alert("Sertakan penerima surat!");
				history.go(-1);
			</script>';
        } else {
            $data = array(
                'status' => 1
            );
            $this->tmpersuratan->update($id, $data);
            $dari = $this->session->userdata('pegawai_id');
            $this->tmjabatan = new Tmjabatan();
            $this->tmunitkerja = new Tmunitkerja();
            $pegawai = $this->tmpegawai->where("id = '".$dari."'")->get();
            $jabatan = $this->tmjabatan->where("id = '".$pegawai->tmjabatan_id."'")->get()->n_jabatan;
            $unitkerja = $this->tmunitkerja->where("id ='".$pegawai->tmunitkerja_id."'")->get()->initial;
            $send = "Ada disposisi surat masuk dari bagian umum Silahkan masuk ke SISUMAKER untuk melihat isi surat";
            $true = false;
            $prihal = $this->tmpersuratan->where('id', $id)->get();
                
            $senddata = array(
                'apikey' => SMSGATEWAY_APIKEY,
                'callbackurl' => SMSGATEWAY_CALLBACKURL,
                'datapacket'=> array()
            );
            for ($i=0; $i<$pegawai_list_len; $i++) {
                if ($this->input->post('d_awal_kegiatan') != '' && $this->input->post('d_akhir_kegiatan') != '' && $this->input->post('v_kegiatan') != '') {
                    $data = array(
                        'id_persuratan' => $id,
                        'id_pegawai_dari' => $dari,
                        'id_pegawai_ke' => $pegawai_list[$i],
                        'catatan' => '',
                        'status' => 0,
                        'd_entry' => date('Y-m-d H:i:s'),
                        'id_skpd_dari' => $this->session->userdata('skpd_id'),
                        'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
                        'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
                        'v_kegiatan' => $this->input->post('v_kegiatan')
                    );
                } else {
                    $data = array(
                        'id_persuratan' => $id,
                        'id_pegawai_dari' => $dari,
                        'id_pegawai_ke' => $pegawai_list[$i],
                        'catatan' => '',
                        'status' => 0,
                        'd_entry' => date('Y-m-d H:i:s'),
                        'id_skpd_dari' => $this->session->userdata('skpd_id')
                    );
                }
                $this->tr_surat_penerima->insert($data);
                //$Onesignal = [
                //    'type' => 1,
                //    'id_surat' => $id,
                //   'dari' => $this->session->userdata('realname').$unitkerja,
                //   'ke' => $pegawai_list[$i],
                //   'perihal' =>  $prihal->prihal
                //];

                //$this->notif($Onesignal);
                
                $telp = $this->tmpegawai->where("id = '".$pegawai_list[$i]."'")->get()->telp;
                array_push($senddata['datapacket'], array(
                    'number' => trim($telp),
                    'message' => urlencode(stripslashes(utf8_encode($send))),
                    'sendingdatetime' => ""));

                                        
                //------- Telp hanya untuk SEMENTARA
                //------- 28 April 2017, sms untuk ajudan pa wakil walikota
                if ($pegawai_list[$i] == '211') {
                    $number = "081286571031";
                    $sendingdatetime = "";
                    array_push($senddata['datapacket'], array(
                        'number' => trim($number),
                        'message' => urlencode(stripslashes(utf8_encode($send))),
                        'sendingdatetime' => $sendingdatetime));
                }
                $true = true;
            }
                
            $sms = new api_sms_class_reguler_json();
            $sms->setIp(SMSGATEWAY_IPSERVER);
            $sms->setData($senddata);
            $responjson = $sms->send();
            $this->saldo_sms->save($responjson);
            
            echo '
			<script type="text/javascript">
				alert("Surat berhasil terkirim!");
				parent.jQuery.fancybox.close();
				parent.oTable.fnDraw();
			</script>';
        }
    }
    
    //----- Send SMS With Gammu
    // function disposisi_submit($id)
    // 	{
    // 		$valid = false;
    // 		$pegawai_list = $this->input->post('pegawai');
    // 		$pegawai_list_len = count($pegawai_list);
    // 		for($i=0; $i<$pegawai_list_len; $i++)
    // 		{
    // 			$valid = $pegawai_list[$i];
    // 		}
    // 		if($valid == false)
    // 		{
    // 			 echo '
    // 			<script type="text/javascript">
    // 				alert("Sertakan penerima surat!");
    // 				history.go(-1);
    // 			</script>';
    // 		}
    // 		else
    // 		{
    // 			$data = array(
    // 				'status' => 1
    // 			);
    // 			$this->tmpersuratan->update($id, $data);
    // 			$dari = $this->session->userdata('pegawai_id');
    // 			$this->tmjabatan = new Tmjabatan();
    // 			$this->tmunitkerja = new Tmunitkerja();
    // 			$pegawai = $this->tmpegawai->where("id = '".$dari."'")->get();
    // 			$jabatan = $this->tmjabatan->where("id = '".$pegawai->tmjabatan_id."'")->get()->n_jabatan;
    // 			$unitkerja = $this->tmunitkerja->where("id ='".$pegawai->tmunitkerja_id."'")->get()->initial;
    // 			$send = "Ada disposisi surat masuk dari Bagian Umum. Silahkan masuk ke SISUMAKER untuk melihat isi surat";
    // 			$true = false;
    // 			for($i=0; $i<$pegawai_list_len; $i++)
    // 			{
    // 				if($this->input->post('d_awal_kegiatan') != '' && $this->input->post('d_akhir_kegiatan') != '' && $this->input->post('v_kegiatan') != '')
    // 				{
    // 					$data = array(
    // 						'id_persuratan' => $id,
    // 						'id_pegawai_dari' => $dari,
    // 						'id_pegawai_ke' => $pegawai_list[$i],
    // 						'catatan' => '',
    // 						'status' => 0,
    // 						'd_entry' => date('Y-m-d H:i:s'),
    // 						'id_skpd_dari' => $this->session->userdata('skpd_id'),
    // 						'd_awal_kegiatan' => $this->input->post('d_awal_kegiatan'),
    // 						'd_akhir_kegiatan' => $this->input->post('d_akhir_kegiatan'),
    // 						'v_kegiatan' => $this->input->post('v_kegiatan')
    // 					);
    // 				}
    // 				else
    // 				{
    // 					$data = array(
    // 						'id_persuratan' => $id,
    // 						'id_pegawai_dari' => $dari,
    // 						'id_pegawai_ke' => $pegawai_list[$i],
    // 						'catatan' => '',
    // 						'status' => 0,
    // 						'd_entry' => date('Y-m-d H:i:s'),
    // 						'id_skpd_dari' => $this->session->userdata('skpd_id')
    // 					);
    // 				}
    // 				$this->tr_surat_penerima->insert($data);
    // 				$telp = $this->tmpegawai->where("id = '".$pegawai_list[$i]."'")->get()->telp;
    // 				$data = array(
    // 					'DestinationNumber' => $telp,
    // 					'TextDecoded' => $send,
    // 					'DeliveryReport' => 'yes',
    // 					'CreatorID' => 'Gammu'
//
    // 				);
    // 				$this->outbox->insert($data);
//
//
    // 				//------- Telp hanya untuk SEMENTARA
    // 					//------- 28 April 2017, sms untuk ajudan pa wakil walikota
//
    // 				if($pegawai->id == '211')
    // 				{
    // 					$data = array(
    // 						'DestinationNumber' => '081286571031',
    // 						'TextDecoded' => $send,
    // 						'CreatorID' => 'Gammu',
    // 						'DeliveryReport' => 'yes'
    // 					);
    // 					$this->outbox->insert($data);
    // 				}
//
    // 				$true = true;
    // 			}
    // 			 echo '
    // 			<script type="text/javascript">
    // 				alert("Surat berhasil terkirim!");
    // 				parent.jQuery.fancybox.close();
    // 				parent.oTable.fnDraw();
    // 			</script>';
    // 		}
    // 	}
    
    public function info($id_persuratan)
    {
        $data = array(
            'result_info' => $this->tr_surat_penerima->info($id_persuratan),
            'content' => 'info'
        );
        $this->load->view('view', $data);
    }
    
    public function print_disposisi($id)
    {
        $nama_surat = "Disposisi $id";
        $this->load->plugin('odf');
        $odf = new odf('assets/odt/disposisi.odt');
        $result = $this->db
            ->select('n_jabatan, n_unitkerja, catatan')
            ->from('tr_surat_penerima')
            ->where('id_persuratan', $id)
            ->where('id_pegawai_dari', 6)
            ->join('tmpegawai', 'tr_surat_penerima.id_pegawai_ke = tmpegawai.id')
            ->join('tmjabatan', 'tmpegawai.tmjabatan_id = tmjabatan.id')
            ->join('tmunitkerja', 'tmpegawai.tmunitkerja_id = tmunitkerja.id')
            ->get();
        $article = $odf->setSegment('property');
        foreach ($result->result() as $row) {
            $catatan = $row->catatan;
            $article->n_pegawai($row->n_jabatan.' '.$row->n_unitkerja);
            $article->merge();
        }
        $odf->mergeSegment($article);
        foreach ($this->tmpersuratan->select('id', $id)->result() as $row) {
            $n_sifat = $this->tmsifat_surat->where("id = '".$row->id_sifatsurat."'")->get()->n_sifatsurat;
            $odf->setVars('dari', $row->dari);
            $odf->setVars('no_surat', $row->no_surat);
            $odf->setVars('tgl_surat', $row->tgl_surat);
            $odf->setVars('tgl_terima_surat', $row->tgl_terima_surat);
            $odf->setVars('no_agenda', $row->no_agenda);
            $odf->setVars('n_sifatsurat', $n_sifat);
            $odf->setVars('prihal', $row->prihal);
            $odf->setVars('catatan', $catatan);
        }
        $odf->setVars('tglskr', $this->lib_date->mysql_to_human(date('Y-m-d')));
        $odf->exportAsAttachedFile($nama_surat.'.odt');
    }
    
    public function delete($id)
    {
        $this->tmpersuratan->delete($id);
    }
    
    public function data()
    {
        function cek_jenis($id)
        {
            if ($id == '1') {
                $return = "<span class='label label-important'>Sangat Segera</span>";
            } elseif ($id == '2') {
                $return = "<span class='label label-warning'>Segera</span>";
            } elseif ($id == '3') {
                $return = "<span class='label label-inverse'>Rahasia</span>";
            } elseif ($id == '4') {
                $return = "<span class='label label-info'>BIASA</span>";
            }
            return $return;
        }
        function cek_status($id, $status, $no_agenda)
        {
            if ($no_agenda != '') {
                if ($status == '1') {
                    $return = '<a id="view" href="#" to="'.base_url().'pendataan/info/'.$id.'"><i class="iconfa-info-sign"></i> Info Disposisi</a>';
                } else {
                    $return = '<span class="muted">[ Belum Didisposi ]</span>';
                }
            } else {
                $return = '<span class="muted">[ Belum Diterima ]</span>';
            }
            return $return;
        }
        function cek_del($id, $no_del)
        {
            if ($no_del == '0') {
                $return = '<a id="delete" href="#" to="'.base_url().'pendataan/pendataan/delete/'.$id.'"><i class="icon-remove" title="Hapus data"></i></a>';
            } else {
                $return = '<a href="#"><i class="icon-remove icon-white" title="Hapus data"></i></a>';
            }
            return $return;
        }
        function cek_disposisi($status, $id, $no_agenda)
        {
            if ($no_agenda != '') {
                if ($status == '1') {
                    $return = '<i class="icon-ok icon-white" title="Surat sudah didisposisikan"></i>';
                } else {
                    $return = '<a id="view" href="#" to="'.base_url().'pendataan/pendataan/disposisi/'.$id.'"><i class="icon-ok" title="Disposisikan surat"></i></a>';
                }
            } else {
                $return = '<i class="icon-ok icon-white" title="Silahkan terima surat"></i>';
            }
            return $return;
        }
        function cek_edit($id, $no_agenda, $c_lintas_skpd)
        {
            if ($c_lintas_skpd == 1) {
                if ($no_agenda == '') {
                    $return = '<a href="'.base_url().'pendataan/accept/'.$id.'"><i class="icon-envelope" title="Terima surat"></i></a>';
                } else {
                    $return = '<a href="#"><i class="icon-envelope icon-white" title="Surat sudah diterima"></i></a>';
                }
            } else {
                $return = '<a href="'.base_url().'pendataan/edit/'.$id.'"><i class="icon-pencil" title="Edit data"></i></a>';
            }
            return $return;
        }
        function cek_tglterima($date, $c_lintas_skpd)
        {
            if ($date != '' && $date != '0000-00-00') {
                $return = indonesian_date($date);
            } else {
                if ($c_lintas_skpd == 1) {
                    $return = '<span class="text-warning">*Belum diterima.</span>';
                } else {
                    $return = 'Error! Silahkan hubungan Admin.';
                }
            }
            return $return;
        }
        $pegawai_id = $this->session->userdata('pegawai_id');
        $this->load->library('datatables');
        echo $this->datatables
            ->select('
				tmpersuratan.id,
				tmpersuratan.no_agenda,
				tmpersuratan.no_surat,
				tmpersuratan.dari,
				tmpersuratan.tgl_surat,
				tmpersuratan.tgl_terima_surat,
				tmpersuratan.id_sifatsurat,
				tmpersuratan.status,
				tmpersuratan.c_lintas_skpd
			')
            ->from('tmpersuratan')
            ->where('tmpersuratan.id_jnssurat = 1')
            ->where('tmpersuratan.id_pegawai = '.$pegawai_id)
            ->edit_column(
                'tmpersuratan.tgl_surat',
                '$1',
                'indonesian_date(tmpersuratan.tgl_surat)'
            )
            ->edit_column(
                'tmpersuratan.status',
                '<center>$1</center>',
                'cek_status(tmpersuratan.id, tmpersuratan.status, tmpersuratan.no_agenda)'
            )
            ->edit_column(
                'tmpersuratan.tgl_terima_surat',
                '$1',
                'cek_tglterima(tmpersuratan.tgl_terima_surat, tmpersuratan.c_lintas_skpd)'
            )
            ->edit_column(
                'tmpersuratan.id_sifatsurat',
                '$1',
                'cek_jenis(tmpersuratan.id_sifatsurat)'
            )
            ->add_column(
                'aksi',
                '<center>
					<a id="view" href="#" to="'.base_url().$this->page.'preview/$1"><i class="iconsweets-pdf2" title="Preview"></i></a> &nbsp; 
					$2 &nbsp; 
					$3 &nbsp; 
					$4
				</center>',
                'tmpersuratan.id, cek_disposisi(tmpersuratan.status, tmpersuratan.id, tmpersuratan.no_agenda), cek_edit(tmpersuratan.id, tmpersuratan.no_agenda, tmpersuratan.c_lintas_skpd), cek_del(tmpersuratan.id, tmpersuratan.c_lintas_skpd)'
            )
            ->unset_column('tmpersuratan.c_lintas_skpd')
            ->generate();
    }
}
