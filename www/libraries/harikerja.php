<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Harikerja
{
	var $CI = false;

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->helper('url');
	}

	function harikerjaa($d_dari, $d_sampai)
	{
		$query = $this->CI->db
			->from('tmholiday')
			->where('tmholiday.d_holiday between "'.$d_dari.'" and "'.$d_sampai.'"')
			->get();
		return $query->num_rows();
	}
}