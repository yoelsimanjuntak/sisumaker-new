<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Chat extends ICA_AdminCont
{
	var $page = "chat/chat/";
	var $view = "chat/chat/";
    var $icon = "chat";

    function __construct() 
	{
        parent::__construct();
		$this->tmuser = new Tmuser();
		$this->tmpegawai = new Tmpegawai();
		$this->tr_chat = new Tr_chat();
        $this->load->helper('function_helper');
    }

	function list_online()
	{
		$data = array(
			'chatlist' => $this->query_list()
		);
		$this->load->view($this->view.'list-online', $data);
	}
	
	function query_list()
	{
		$tmuser_id = $this->session->userdata('auth_id');
		return $this->db
			->select('
				tmuser.id,
				tmpegawai.n_pegawai,
				tmuser.photo,
				tmuser.online
			')
			->from('truser_kirim')
			->join('tmuser', 'truser_kirim.id_kirim = tmuser.id')
			->join('tmpegawai', 'tmuser.id = tmpegawai.tmuser_id')
			->where('truser_kirim.id_user', $tmuser_id)
			->order_by('tmuser.online', 'desc')
			->get()
			->result();
	}
	
	function new_message()
	{
		$id = $this->session->userdata('auth_id');
		$result = $this->tr_chat->new_message($id);
		$from = '';
		$n_pegawai = '';
		
		$data = array(
			'online' => now()
		);
		$this->tmuser->update($id, $data);
		if($result->num_rows() != 0)
		{
			foreach($result->result() as $row)
			{
				if($from != '')
				{
					$from .= '{}';
					$n_pegawai .= '{}';
				}
				$from .= $row->from;
				$n_pegawai .= $row->n_pegawai;
			}
		}
		$data = array(
			'status' => $result->num_rows(),
			'from' => $from,
			'n_pegawai' => $n_pegawai
		);
		echo json_encode($data);
	}
	
	function user_online()
	{
		$status = 0;
		$user_id = '';
		$active = '';
		foreach($this->query_list() as $row)
		{
			$status = 1;
			if($user_id != '')
			{
				$user_id .= '|';
				$active .= '|';
			}
			$user_id .= $row->id;
			if($row->online != 0 || $row->online != '')
			{
				$on = now() - $row->online;
				if($on <= 20)
				{
					$active .= '1';
				}
				else
				{
					$active .= '0';
				}
			}
		}
		$data = array(
			'status' => $status,
			'user_id' => $user_id,
			'active' => $active
		);
		echo json_encode($data);
	}
	
	function get_data($id)
	{
		$photo = base_url().'uploads/photo/'.$this->tmuser->where('id', $id)->get()->photo;
		$result = $this->tr_chat->read_message($this->session->userdata('auth_id'), $id);
		$this->tr_chat->up_message($this->session->userdata('auth_id'), $id);
		if($result->num_rows() != 0)
		{
			$return = '';
			foreach($result->result() as $row)
			{
				$c = 'left';
				//$img = '<img src="'.$photo.'" alt="Photo"/>';
				$img = '';
				if($row->from == $this->session->userdata('auth_id'))
				{
					$c = 'right';
					$img = '';
				}
				$return .= '<li>'.$img.'<span class="'.$c.'">'.$row->message.'</span></li>';
			}
			echo $return;
		}
	}
	
	function save_msg($id)
	{
		$user_id = $this->session->userdata('auth_id');
		$data = array(
			'from' => $user_id,
			'to' => $id,
			'message' => $this->input->post('text'),
			'send' => date('Y-m-d H:i:s')
		);
		if($this->tr_chat->insert($data)){
			$status = 1;
		}
		else{
			$status = 99;
		}
		$data = array(
			'status' => $status
		);
		echo json_encode($data);
	}
}