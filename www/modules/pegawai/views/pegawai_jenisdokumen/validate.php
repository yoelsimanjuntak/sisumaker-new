<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#form1")
			.validate({
				rules: {
					tmjenisdokumen_id: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 1)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Simpan<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								jQuery('#form1').trigger('reset');
								if(jQuery('#form1 .control-group').hasClass('success')) {
									jQuery('#form1 .control-group').removeClass('success').removeClass('error');
									jQuery('label.success').hide();
									jQuery('label.valid').hide();
								}
								oTable.fnDraw();
							}
							else if(data.status == 2)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Perbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								oTable.fnDraw();
							}
							else if(data.status == 11)
							{
								jQuery('#result').html('<p class="pesan gagal"><span class="iconfa-check"></span> Data Gagal Disimpan<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							else if(data.status == 22)
							{
								jQuery('#result').html('<p class="pesan gagal"><span class="iconfa-check"></span> Data Gagal Diperbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							jQuery('#action').button('reset');
							jQuery('#result').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
	});
</script>