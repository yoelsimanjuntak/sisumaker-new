<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#select-all').click(function () {
			var selected = this.checked;
			jQuery('#rekomendasiid :checkbox').each(function () {this.checked = selected;});
		});
		jQuery(".chzn-select").chosen();
		jQuery("#form1")
			.validate({
				rules: {
					n_user: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 2)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Perbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							jQuery('#action').button('reset');
							jQuery('#result').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
		jQuery("#form2")
			.validate({
				rules: {
					username: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 2)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Perbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							else if(data.status == 22)
							{
								jQuery('#result').html('<p class="pesan gagal"><span class="iconfa-check"></span> Data Gagal Diperbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							jQuery('#action').button('reset');
							jQuery('#result').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
		
		jQuery("#form3")
			.validate({
				rules: {
					password: "required",
					cpassword:{
						required:true,
						equalTo:"#password"
					}
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 2)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Perbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							jQuery('#action').button('reset');
							jQuery('#result').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
		jQuery("#form4")
			.validate({
				rules: {
					userauth_id: "required"
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 2)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Perbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								oTable.fnDraw();
							}
							else if(data.status == 22)
							{
								jQuery('#result').html('<p class="pesan gagal"><span class="iconfa-check"></span> Data Gagal Disimpan, Role Sudah Pernah Disimpan<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							jQuery('#action').button('reset');
							jQuery('#result').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
		
		jQuery("#form5")
			.validate({
				rules: {
					perizinan_id: "required"
				},
				submitHandler:function(form){
					jQuery('#action').button('loading');
					jQuery('#result').fadeOut('fast');
					jQuery.ajax({
						type:'post',
						url:jQuery(form).attr('action'),
						data:jQuery(form).serialize(),
						dataType:'json',
						success:function(data){
							if(data.status == 2)
							{
								jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Perbaharui<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
								oTable1.fnDraw();
							}
							else if(data.status == 22)
							{
								jQuery('#result').html('<p class="pesan gagal"><span class="iconfa-check"></span> Data Gagal Disimpan, Perizinan Sudah Pernah Disimpan<a class="close" data-dismiss="alert" href="#">&times;</a></p>');
							}
							jQuery('#action').button('reset');
							jQuery('#result').fadeIn('fast');
							jQuery('body').animate({
								scrollTop: 200
							}, 600);
						}
					});
					return false;
				}
			});
			
		jQuery('#delete_userauth').live('click', function(){
			if(confirm("Yakin Ingin Menghapus data ini?")){
				jQuery.ajax({
					"type":"post",
					"cache":false,
					"url":jQuery(this).attr("to"),
					"success":function(){
						jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Hapus<a class="close" data-dismiss="alert" href="#">&times;</a></p>').fadeIn('slow');
						oTable.fnDraw();
					}
				});
			}
			return false;
		});
		jQuery('#delete_perizinan').live('click', function(){
			if(confirm("Yakin Ingin Menghapus data ini?")){
				jQuery.ajax({
					"type":"post",
					"cache":false,
					"url":jQuery(this).attr("to"),
					"success":function(){
						jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Hapus<a class="close" data-dismiss="alert" href="#">&times;</a></p>').fadeIn('slow');
						oTable1.fnDraw();
					}
				});
			}
			return false;
		});
		
	});
</script>