<script type="text/javascript">
    var oTable;
    datatables_ps("<?php echo site_url() . $this->page . 'data'; ?>", <?php echo $sort; ?>);
    jQuery(document).ready(function(){
        jQuery('#refresh').live('click',function(){
            oTable.fnDraw();
        });
    });
</script>
<table id="dyntable" class="table table-bordered">
    <colgroup>
        <col class="con0" style="align: center; width: 4%" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
    </colgroup>
    <thead>        
        <tr>
            <th width="15px">No</th>
            <th width="140px">Dari</th>
            <th width="140px">Penyetuju</th>
            <th width="140px">No Surat</th>
            <th width="120px">Tanggal Surat</th>
            <th>Perihal</th>
            <th width="100px">Sifat Surat</th>
            <th width="120px">Status</th>
            <th width="100px">Unduh</th>
            <th width="100px">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td valign="top" colspan="10" class="dataTables_empty">Loading <img src="<?php echo base_url() . 'assets/images/loaders/loader19.gif'; ?>"/></td>
        </tr>
    </tbody>
</table>