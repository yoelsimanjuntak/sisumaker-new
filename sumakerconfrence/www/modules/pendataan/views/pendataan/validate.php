<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#tgl_surat").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_awal_kegiatan").datepicker("option", "minDate", selectedDate);
				if(jQuery("#d_awal_kegiatan").val() == ''){
					jQuery("#d_akhir_kegiatan").datepicker("option", "minDate", selectedDate);
				}
			}
		});
		jQuery("#d_awal_kegiatan").datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm:ss',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_akhir_kegiatan").datepicker("option", "minDate", selectedDate);
			}
		});
		jQuery("#d_akhir_kegiatan").datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm:ss',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_awal_kegiatan").datepicker("option", "maxDate", selectedDate);
			}
		});
		jQuery("#form1")
			.validate({
				rules: {
					id_surat: "required",
					dari: "required",
					no_surat: "required",
					tgl_surat: "required",
					userfile: "required",
					tgl_terima_surat: "required",
					no_agenda: "required",
					id_sifatsurat: "required",
					prihal: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				}
			});
	});
</script>