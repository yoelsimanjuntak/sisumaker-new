<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'controllers/Auth.php';

class Suratbaru extends Auth
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credential: true');
        header('Access-Control-Max-Age: 86400');
        header('Access-Control-Allow-Methods:GET,POST,OPTIONS');
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $this->load->model('db_dml');
        $this->load->model('db_select');
        $this->load->model('mymodel');
        $this->load->model('tr_surat_penerima');
        $this->load->model('tmuser');
        $this->load->model('tmpersuratan');
        $this->load->model('penomoran');
        $this->load->model('tmsifat_surat');
        $this->load->model('surat');
        $this->load->model('tmjenis_surat');
        $this->load->model('tr_surat_penerima');
        $this->load->model('tmpegawai');
        $this->load->model('tmunitkerja');
        $this->load->model('tmjabatan');
        $this->load->model('main_inbox');

        $this->load->library('onesignal');
        $this->load->helper('function_helper');
        // if ($this->cektoken() == false) {
        //     header('Content-type: application/json');
        //     $output = [
        //         'status' => 0,
        //         'message' => 'Wrong Token',
        //         'token' => $this->input->get_request_header('Authorization')
        //     ];
        //     exit(json_encode($output));
        // }
    }

    public function index_get()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $skpd_id = $decode->skpd_id;
        $id_persuratan = $this->get('id_persuratan', true);
        $output = [];

        $query = $this->mymodel->disposisi($auth_id);
        if ($query->num_rows() == 0) {
            $output = [
                'status' => false,
                'message' => 'List disposisi kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            foreach ($query->result() as $row) {
                $output['disposisi'][] = [
                    'status' => 1,
                    'id_pegawai' => $row->id,
                    'n_jabatan' => $row->n_jabatan,
                    'n_unitkerja' => $row->n_unitkerja,
                    'n_pegawai' => $row->n_pegawai.' | '. $row->n_jabatan,
                    'telp' => $row->telp
                ]   ;
            }
        }

        $query = $this->mymodel->penandatangan_sekda();
        $query2 = $this->mymodel->penandatangan_skpd($skpd_id);
        if ($query->num_rows() == 0 && $query2->num_rows() == 0) {
            $output = [
                'status' => false,
                'message' => 'List disposisi kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            foreach ($query->result() as $row) {
                $output['ttd'][] = [
                    'status' => 1,
                    'id_pegawai' => $row->id,
                    'n_jabatan' => $row->n_jabatan,
                    'n_unitkerja' => $row->n_unitkerja,
                    'n_pegawai' => $row->n_pegawai. ' | '. $row->n_jabatan,
                ];
            }

            foreach ($query2->result() as $row) {
                $output['ttd'][] = [
                    'status' => 1,
                    'id_pegawai' => $row->id,
                    'n_jabatan' => $row->n_jabatan,
                    'n_unitkerja' => $row->n_unitkerja,
                    'n_pegawai' => $row->n_pegawai.' | '. $row->n_jabatan,
                ];
            }

            foreach ($this->mymodel->sifat_surat()->result() as $row) {
                $output['sifatsurat'][] = [
                    'id' => $row->id,
                    'n_sifatsurat' => $row->n_sifatsurat,
                ];
            }

            $this->response($output, Auth::HTTP_OK);
        }
    }

    public function notif($data)
    {
        $notif = new onesignal();
        $notif->setData($data);
        $responjson1 = $notif->ios();
        $responjson2 = $notif->android();



        return $responjson1;
    }

    public function notifTest_get()
    {
        $Onesignal = [
            'dari' => 'Aldi Ardianto',
            'ke' => 3171,
            'perihal' =>  'Ajakan untuk bermain game'
        ];

        $notif = $this->notif($Onesignal);
        $this->response($notif, Auth::HTTP_OK);
    }

    public function kirim_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $realname = $decode->realname;
        $pegawai_id = $decode->pegawai_id;
        $tmunitkerja_id = $decode->tmunitkerja_id;

        $i_pegawai = $this->post('pegawai', true);
        $i_d_awal_kegiatan = $this->post('d_awal_kegiatan', true);
        $i_d_akhir_kegiatan = $this->post('d_akhir_kegiatan', true);
        $i_v_kegiatan = $this->post('v_kegiatan', true);
        $i_id_compose = $this->post('id_compose', true);
        $i_no_surat = $this->post('no_surat', true);
        $i_id_pegawaittd = $this->post('id_pegawaittd', true);
        //Validasi Disposisi
        if (count($i_pegawai != 0)) {
            $next = true;
            if ($i_d_awal_kegiatan != '' || $i_d_akhir_kegiatan != '' || $i_v_kegiatan != '') {
                if ($i_d_awal_kegiatan == '' || $i_d_akhir_kegiatan == '' || $i_v_kegiatan == '') {
                    $output = [
                        'status' => false,
                        'message' => 'Inputan kegiatan harus diisi lengkap.'
                    ];
                    $this->response($output, Auth::HTTP_BAD_REQUEST);
                    $next = false;
                }
            }
            if ($next == true) {
                $dari = $pegawai_id;
                $unitkerja = '';
                if ($tmunitkerja_id != 0) {
                    $unitkerja = ' (' . $this->tmunitkerja->select('id', $tmunitkerja_id)->row()->n_unitkerja . ')';
                }

                $id_compose = $i_id_compose;
                $no_surat = $i_no_surat;
                $id_pegawaittd = $i_id_pegawaittd;
                if ($id_compose == 1) {
                    $id_pegawaittd = 0;
                } elseif ($id_compose == 2) {
                    $no_surat = '';
                }

                //$nomor = $this->penomoran->nomor_save($decode->skpd_id, 2);
                $data = [
                    'dari' => $realname . $unitkerja,
                    'id_compose' => $id_compose,
                    'no_surat' => $no_surat,
                    'id_pegawaittd' => $id_pegawaittd,
                    'tgl_surat' => $this->post('tgl_surat', true),
                    'id_surat' => 1,
                    'id_jnssurat' => 2,
                    'id_sifatsurat' => $this->post('id_sifatsurat', true),
                    'prihal' => $this->post('perihal', true),
                    'file' => $this->post('userfile', true),
                    'file_lampiran' => $this->post('file_lampiran', true),
                    'isi_surat' => $this->post('isi_surat', true),
                    'id_pegawai' => $dari,
                    'id_skpd_in' => $decode->skpd_id,
                    'id_skpd_out' => $decode->skpd_id,
                    'd_awal_kegiatan' => $i_d_awal_kegiatan,
                    'd_akhir_kegiatan' => $i_d_akhir_kegiatan,
                    'v_kegiatan' => $i_v_kegiatan,
                    'd_entry' => date('Y-m-d')
                ];
                if (!$this->tmpersuratan->insert($data)) {
                    $output = [
                        'status' => false,
                        'message' => 'error'
                    ];
                    $this->response($output, Auth::HTTP_BAD_REQUEST);
                }
                
                $id = $this->tmpersuratan->select_max();

                $pegawai = $this->tmpegawai->select('id', $dari)->row();
                $jabatan = $this->tmjabatan->select('id', $pegawai->tmjabatan_id)->row()->n_jabatan;
                $unitkerja = $this->tmunitkerja->select('id', $pegawai->tmunitkerja_id)->row()->initial;



                //Kepada:
                $d_entry = date('Y-m-d H:i:s');
                $send = 'Ada disposisi surat dari ' . $jabatan . ' ' . $unitkerja . '. Silahkan masuk ke SISUMAKER untuk melihat isi surat';
                $pegawai_list = $i_pegawai;
                $pegawai_list_len = count($pegawai_list);
                for ($i = 0; $i < $pegawai_list_len; $i++) {
                    $data = [
                        'id_persuratan' => $id,
                        'id_pegawai_dari' => $dari,
                        'id_pegawai_ke' => $pegawai_list[$i],
                        'status' => 0,
                        'cc' => 0,
                        'd_entry' => $d_entry,
                        'id_skpd_dari' => $decode->skpd_id,
                        'd_awal_kegiatan' => $i_d_awal_kegiatan,
                        'd_akhir_kegiatan' => $i_d_akhir_kegiatan,
                        'v_kegiatan' => $i_v_kegiatan
                    ];
                    if (!$this->tr_surat_penerima->insert($data)) {
                        $output = [
                            'status' => false,
                            'message' => 'ada yg error'
                        ];
                        $this->response($output, Auth::HTTP_BAD_REQUEST);
                    }
                    

                    $Onesignal = [
                        'dari' => $realname . $unitkerja,
                        'ke' => $pegawai_list[$i],
                        'perihal' =>  $this->post('perihal', true)
                    ];

                    $this->notif($Onesignal);

                    $telp = $this->tmpegawai->select('id', $pegawai_list[$i])->row()->telp;
                }

                //CC:
                $valid_cc = false;
                $send = 'Ada CC surat dari ' . $unitkerja . '. Silahkan masuk ke SISUMAKER untuk melihat isi surat';
                $pegawai_cc_list = $this->post('pegawai_cc', true);
                $pegawai_cc_list_len = count($pegawai_cc_list);
                for ($i = 0; $i < $pegawai_cc_list_len; $i++) {
                    $valid_cc = $pegawai_cc_list[$i];
                }
                if ($valid_cc != false) {
                    for ($i = 0; $i < $pegawai_cc_list_len; $i++) {
                        $data = [
                            'id_persuratan' => $id,
                            'id_pegawai_dari' => $dari,
                            'id_pegawai_ke' => $pegawai_cc_list[$i],
                            'status' => 0,
                            'cc' => 1,
                            'd_entry' => $d_entry,
                            'id_skpd_dari' => $decode->skpd_id,
                            'd_awal_kegiatan' => $i_d_awal_kegiatan,
                            'd_akhir_kegiatan' => $i_d_akhir_kegiatan,
                            'v_kegiatan' => $i_v_kegiatan
                        ];
                        $this->tr_surat_penerima->insert($data);

                        $Onesignal = [
                            'dari' => $realname . $unitkerja,
                            'ke' => $pegawai_cc_list[$i],
                            'perihal' =>  $this->post('perihal', true)
                        ];

                        $this->notif($Onesignal);

                        $telp = $this->tmpegawai->select('id', $pegawai_cc_list[$i])->row()->telp;
                        // if ($telp != '-') {
                                //     array_push($senddata['datapacket'], [
                                //         'number' => trim($telp),
                                //         'message' => urlencode(stripslashes(utf8_encode($send))),
                                //         'sendingdatetime' => '']);

                                //     //------- Telp hanya untuk SEMENTARA
                                //     //------- 28 April 2017, sms untuk ajudan pa wakil walikota
                                //     if ($pegawai_cc_list[$i] == '211') {
                                //         $number = '081286571031';
                                //         $sendingdatetime = '';
                                //         array_push($senddata['datapacket'], [
                                //             'number' => trim($number),
                                //             'message' => urlencode(stripslashes(utf8_encode($send))),
                                //             'sendingdatetime' => $sendingdatetime]);
                                //     }
                                // }
                    }
                }
                // $sms = new api_sms_class_reguler_json();
                // $sms->setIp(SMSGATEWAY_IPSERVER);
                // $sms->setData($senddata);
                // $responjson = $sms->send();
                // $this->saldo_sms->save($responjson);

                $output = [
                    'status' => true,
                    'message' => 'Surat Berhasil'
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        } else {
            $output = [
                'status' => false,
                'message' => 'Inputan kepada harus di isi.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }

    public function fileSurat_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;

        if ($_FILES['userfile']['name'] != '') {
            $name = date('YmdHis') . $auth_id;
            $explode = explode('.', str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.' . end($explode);
            $exten = 'uploads/surat_intern/' . $name . $endof;
            $config['file_name'] = $name;
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
            $config['upload_path'] = '../uploads/surat_intern/';
            $config['max_size'] = '30000';
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $output = [
                    'status' => false,
                    'message' => 'error ' . $this->upload->display_errors()
                ];
                $this->response($output, Auth::HTTP_BAD_REQUEST);
            } else {
                $output = [
                    'status' => true,
                    'message' => 'berhasil di upload',
                    'path' => $exten,
                    'filename' => $name . $endof
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        } else {
            $output = [
                'status' => false,
                'message' => 'Berkas yang di-upload Error!'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }

    public function fileLampiran_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $name = date('YmdHis') . $auth_id;
        $file_lampiran = '';
        if ($_FILES['file_lampiran']['name'] != '') {
            $name_l = $name . '_l';
            $explode_l = explode('.', str_replace(' ', '', $_FILES['file_lampiran']['name']));
            $endof_l = '.' . end($explode_l);
            $file_lampiran = 'uploads/surat_intern/' . $name_l . $endof_l;
            $config_l['file_name'] = $name_l;
            $config_l['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
            $config_l['upload_path'] = '../uploads/surat_intern/';
            $config_l['max_size'] = '30000';
            $config_l['overwrite'] = true;
            $this->load->library('upload', $config_l);
            if (!$this->upload->do_upload('file_lampiran')) {
                $output = [
                    'status' => false,
                    'message' => '2 ' . $this->upload->display_errors()
                ];
                $this->response($output, Auth::HTTP_BAD_REQUEST);
                $next = false;
                exit();
            } else {
                $output = [
                    'status' => true,
                    'message' => 'berhasil di upload',
                    'path' => $file_lampiran,
                    'filename' => $name_l . $endof_l
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        } else {
            $output = [
                'status' => false,
                'message' => 'Berkas yang di-upload Error!'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }

    public function test_post()
    {
        $auth_id = 3;
        if ($_FILES['userfile']['name'] != '') {
            $name = date('YmdHis') . $auth_id;
            $explode = explode('.', str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.' . end($explode);
            $exten = 'uploads/' . $name . $endof;
            $config['file_name'] = $name;
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx|';
            $config['upload_path'] = '../uploads/';
            $config['max_size'] = '30000';
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile');
            $output = [
                'status' => true,
                'message' => 'Behasil!',
                'nameFile' => $exten
            ];
            $this->response($output, Auth::HTTP_OK);
        } else {
            $output = [
                'status' => false,
                'message' => 'Berkas yang di-upload Error!'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }

    public function ttd_post()
    {
        $decode = $this->viewtoken();
        $pegawai_id = $decode->pegawai_id;
        $auth_id = $decode->auth_id;
        $skpd_id = $decode->skpd_id;
        $id_persuratan = $this->post('id_persuratan', true);
        $output = [];

        if (!$id_persuratan) {
            $output = [
                'status' => 0,
                'message' => 'Id persuratan tidak boleh kosong.'
            ];
            $this->response($output, Auth::HTTP_BAD_REQUEST);
        } else {
            if ($this->tr_surat_penerima->cek($pegawai_id, $id_persuratan)->num_rows() == 0) {
                $output = [
                    'status' => 0,
                    'message' => 'Surat tidak ada atau Anda tidak mendapatkan disposisi surat ini.'
                ];
                $this->response($output, Auth::HTTP_OK);
            } else {
                $row = $this->db_select->select_where('tmpersuratan', ['id' => $id_persuratan]);
                if ($pegawai_id != $row->id_pegawaittd) {
                    $output = [
                        'status' => 0,
                        'message' => 'Gagal melakukan TTD, pengguna tidak memiliki akses untuk menandatangani surat ini.'
                    ];
                    $this->response($output, Auth::HTTP_OK);
                } else {
                    if ($row->status_setujui != 0) {
                        $output = [
                            'status' => 0,
                            'message' => 'Gagal melakukan TTD, surat sudah pernah di TTD.'
                        ];
                        $this->response($output, Auth::HTTP_OK);
                    } else {
                        $id_skpd_in = $row->id_skpd_in;
                        $result = $this->mymodel->id_tu($id_skpd_in);
                        if ($result->num_rows() == 0) {
                            $output = [
                                'status' => 0,
                                'message' => 'Gagal melakukan TTD, sistem tidak dapat menemukan user Bag. Umum. Silahkan hubungi Admin SISUMAKER'
                            ];
                            $this->response($output, Auth::HTTP_OK);
                        } else {
                            if ($_FILES['userfile']['name'] == '') {
                                $output = [
                                    'status' => 0,
                                    'message' => 'File hasil TTD tidak boleh kosong.'
                                ];
                                $this->response($output, Auth::HTTP_BAD_REQUEST);
                            } else {
                                $name = date('YmdHis') . $auth_id;
                                $explode = explode('.', str_replace(' ', '', $_FILES['userfile']['name']));
                                $endof = '.' . end($explode);
                                $exten = 'uploads/surat_koreksi/' . $name . $endof;
                                $config['file_name'] = $name;
                                $config['allowed_types'] = 'pdf|doc|docx';
                                $config['upload_path'] = '../uploads/surat_koreksi/';
                                $config['max_size'] = '30000000';
                                $config['overwrite'] = true;
                                $this->load->library('upload', $config);
                                if (!$this->upload->do_upload()) {
                                    $output = [
                                        'status' => 0,
                                        'message' => "Error saat mengunggah : '" . $this->upload->display_errors() . "'"
                                    ];
                                    $this->response($output, Auth::HTTP_OK);
                                } else {
                                    // Send A Message
                                    foreach ($result->result() as $row) {
                                        $telp = $row->telp;
                                        $id_tu = $row->id;
                                    }
                                    $data = [
                                        'id_persuratan' => $id_persuratan,
                                        'id_pegawai_dari' => $pegawai_id,
                                        'id_pegawai_ke' => $id_tu,
                                        'catatan' => '',
                                        'file_surat_koreksi' => $exten,
                                        'status' => 0,
                                        'd_entry' => date('Y-m-d H:i:s'),
                                        'id_skpd_dari' => $skpd_id
                                    ];
                                    $this->db_dml->insert('tr_surat_penerima', $data);

                                    $nomor = $this->mymodel->nomor_save($id_skpd_in, 2);
                                    $data = [
                                        'status_setujui' => 1,
                                        //'no_surat' => $nomor_surat,
                                        'no_agenda' => 'K-' . $nomor,
                                        'id_pegawai_setujui' => $pegawai_id
                                    ];
                                    $this->db_dml->update('tmpersuratan', $data, ['id' => $id_persuratan]);

                                    if ($telp != '-') {
                                        // 									$senddata = array(
                                        // 										'apikey' => SMSGATEWAY_APIKEY,
                                        // 										'callbackurl' => SMSGATEWAY_CALLBACKURL,
                                        // 										'datapacket'=> array()
                                        // 									);

                                        // $send = 'Ada surat yang harus Anda cetak dan kirimkan. Silahkan masuk ke SISUMAKER untuk melihat isi surat';
                                        // array_push($senddata['datapacket'], [
                                        //     'number' => trim($telp),
                                        //     'message' => urlencode(stripslashes(utf8_encode($send))),
                                        //     'sendingdatetime' => '']);

                                        // 									$sms = new api_sms_class_reguler_json();
    // 									$sms->setIp(SMSGATEWAY_IPSERVER);
    // 									$sms->setData($senddata);
    // 									$responjson = $sms->send();
                                    }

                                    $output = [
                                        'status' => 1,
                                        'message' => 'Surat berhasil di TTD.'
                                    ];
                                    $this->response($output, Auth::HTTP_OK);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
