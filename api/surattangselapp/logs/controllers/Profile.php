<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'controllers/Auth.php';

class Profile extends Auth
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credential: true');
        header('Access-Control-Max-Age: 86400');
        header('Access-Control-Allow-Methods:GET,POST,OPTIONS');
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $this->load->model('db_dml');
        $this->load->model('db_select');
        $this->load->model('mymodel');
        $this->load->model('tr_surat_penerima');
        $this->load->model('tmuser');
        $this->load->model('tmpersuratan');
        $this->load->model('penomoran');
        $this->load->model('tmsifat_surat');
        $this->load->model('surat');
        $this->load->model('tmjenis_surat');
        $this->load->model('tr_surat_penerima');
        $this->load->model('tmpegawai');
        $this->load->model('tmunitkerja');
        $this->load->model('tmjabatan');
        $this->load->model('main_inbox');

        // $this->load->library('api_sms_class_reguler_json');
        $this->load->helper('function_helper');
        if ($this->cektoken() == false) {
            header('Content-type: application/json');
            $output = [
                'status' => 0,
                'message' => 'Wrong Token',
                'token' => $this->input->get_request_header('Authorization')
            ];
            exit(json_encode($output));
        }
    }

    public function index_get()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;



        $query = $this->mymodel->profile($auth_id);
        if ($query->num_rows() == 0) {
            $output = [
                'status' => false,
                'message' => 'List disposisi kosong.'
            ];
        } else {
            $output = [
                    'username' => $query->row()->username,
                    'realname' => $query->row()->realname,
                    'nip' => $query->row()->nip,
                    'photo' => $query->row()->photo,
                    'telp' => $query->row()->telp

                ]   ;
            $this->response($output, Auth::HTTP_OK);
        }
    }

    public function setProfile_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $pegawai_id = $decode->pegawai_id;

        $nama = $this->post('realname', true);
        $nip = $this->post('nip', true);
        $telp = $this->post('telp', true);
        $username = $this->post('username', true);
        $password = $this->post('password', true);

        if ($this->mymodel->profile_edit($auth_id, $pegawai_id, $nama, $nip, $telp, $username, $password)) {
            $output = [
                'status' => true,
                'message' => 'Profile berhasil di ganti'
            ];

            $this->response($output, Auth::HTTP_OK);
        } else {
            $output = [
                'status' => false,
                'message' => 'Error!'
            ];

            $this->response($output, Auth::HTTP_OK);
        }
    }

    public function password_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $pwdlama = $this->post('passwordlama', true);
        $pwdbaru = $this->post('password', true);


        // $nama = $this->post('realname', true);
        if ($this->mymodel->chck_pwd($auth_id, $pwdlama)) {
            if ($this->mymodel->password_edit($auth_id, $pwdbaru)) {
                $output = [
                    'status' => true,
                    'message' => 'password berhasil di ganti'
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        } else {
            $output = [
                'status' => false,
                'message' => 'Password lama berbeda!'
            ];

            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }

    public function photo_post()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $username = $decode->username;

        if ($_FILES['userfile']['name'] != "") {
            $explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
            $endof = '.' . end($explode);
            $exten = $username . $endof;
            $config['file_name'] = $username;
            $config['upload_path'] = '../uploads/photo/';
            $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
            $config['max_size'] = '1000000';
            $config['overwrite'] = true;
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                $output = [
                    'status' => false,
                    'message' => 'Photo gagal di ganti'.$this->upload->display_errors()
                ];
    
                $this->response($output, Auth::HTTP_BAD_REQUEST);
            } else {
                $data = array(
                    'photo' => $exten
                );
                $this->tmuser->update($auth_id, $data);

                $output = [
                    'status' => true,
                    'message' => 'Photo berhasil di ganti'
                ];
                $this->response($output, Auth::HTTP_OK);
            }
        } else {
            $output = [
                'status' => false,
                'message' => 'File gambar tidak boleh kosong'
            ];

            $this->response($output, Auth::HTTP_BAD_REQUEST);
        }
    }
}
