<script type="text/javascript">
	jQuery(document).ready(function(){
		ret = '';
		parent.jQuery('#pegawai option:selected').each(function(i, selected){ 
			ret = ret + '<li>' + jQuery(selected).text() + '</li>'; 
		});
		if(ret == '')
		{
			alert('Anda harus menyertakan penerima disposisi.');
			parent.jQuery.fancybox.close();
		}
		koreksi = 'Tidak';
		if(parent.jQuery('input[type="radio"]:checked').val() != 0)
		{
			koreksi = 'Ya';
		}
		else
		{
			jQuery('#f_koreksi_t').remove();
			jQuery('#koreksi_t').remove();
		}
		if(parent.jQuery('#i_koreksi').length == 0)
		{
			jQuery('#f_koreksi_t').remove();
			jQuery('#koreksi_t').remove();
		}
		else
		{
			jQuery('#f_koreksi').html(parent.jQuery('#userfile').val().split('\\').pop());
			jQuery('#f_lampiran').html(parent.jQuery('#file_lampiran').val().split('\\').pop());
		}
		jQuery('#list_penerima').html(ret);
		jQuery('#catatan').html(parent.jQuery('#catatan').val());
		jQuery('#koreksi').html(koreksi);
		jQuery('#send').live('click', function(){
			parent.jQuery.fancybox.close();
			parent.jQuery('#form4').submit();
		})
	});
</script>
<table class="table table-bordered table-invoice">
	<colgroup>
		<col class="con1" />
		<col class="con0" />
	</colgroup>
	<tbody>
		<tr>
			<td width="140px">Paraf</td>
			<td>Dikirim Ke</td>
		</tr>
		<tr>
			<td>
				<?php if($paraf != ''){ ?>
				<img src="<?php echo base_url().$paraf;?>" alt=""/>
				<?php } ?>
			</td>
			<td>
				<ol id="list_penerima" class="list-ordered"></ol>
			</td>
		</tr>
	</tbody>
</table>
<table class="table table-bordered table-invoice">
	<tbody>
		<tr>
			<td width="140px">Catatan</td>
			<td id="catatan"></td>
		</tr>
		<tr id="koreksi_t">
			<td>Perlu Dikoreksi</td>
			<td id="koreksi"></td>
		</tr>
		<tr id="f_koreksi_t">
			<td>File Koreksi</td>
			<td id="f_koreksi"></td>
		</tr>
		<tr id="f_lampiran_t">
			<td>Lampiran Koreksi</td>
			<td id="f_lampiran"></td>
		</tr>
	</tbody>
</table>

<a href="#" class="btn btn-primary pull-right" id="send"><i class="iconfa-plane"></i> Kirim Disposisi</a>
<div style="clear:both"></div>