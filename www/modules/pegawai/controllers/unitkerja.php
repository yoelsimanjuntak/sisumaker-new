<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Unitkerja extends ICA_AdminCont {

    var $page = "pegawai/unitkerja/";
    var $view = "pegawai/unitkerja/";
    var $icon = "cogs";

    function __construct() {
        parent::__construct();
		$this->restrict('10');
        $this->tmunitkerja = new Tmunitkerja();
        $this->tmskpd = new Tmskpd();
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Setting Umum <span class='separator'></span> Pegawai <span class='separator'></span> Unit Kerja",
            'title' => 'Master Unit Kerja',
            'content' => 'table',
            'sort' => 1
        );
        $this->load->view('template', $data);
    }

    function add() {
        $data = array(
            'result_tmskpd' => $this->tmskpd->select()
        );
        $this->load->view($this->view . 'add', $data);
    }

    function edit($id) {
        $data = array(
            'result_tmskpd' => $this->tmskpd->select(),
            'result' => $this->tmunitkerja->select('id', $id)
        );
        $this->load->view($this->view . 'edit', $data);
    }

    function save($id = false) {
        if ($id == false) {
            if ($this->tmunitkerja->where("n_unitkerja = '".$this->input->post('n_unitkerja')."' and id_skpd = '".$this->input->post('id_skpd')."'")->count() == 0) {
                $data = array(
                    'n_unitkerja' => $this->input->post('n_unitkerja'),
                    'id_skpd' => $this->input->post('id_skpd'),
                    'initial' => $this->input->post('initial')
                );
                $this->tmunitkerja->insert($data);
                $output = array(
                    'status' => 1
                );
                echo json_encode($output);
            } else {
                $output = array(
                    'status' => 11
                );
                echo json_encode($output);
            }
        } else {
            if ($this->input->post('n_unitkerja_temp') == $this->input->post('n_unitkerja')) {
                $data = array(
                    'n_unitkerja' => $this->input->post('n_unitkerja'),
                    'id_skpd' => $this->input->post('id_skpd'),
                    'initial' => $this->input->post('initial')
                );
                $this->tmunitkerja->update($id, $data);
                $output = array(
                    'status' => 2
                );
                echo json_encode($output);
            } else {
				if ($this->tmunitkerja->where("n_unitkerja = '".$this->input->post('n_unitkerja')."' and id_skpd = '".$this->input->post('id_skpd')."'")->count() == 0) {
                    $data = array(
                        'n_unitkerja' => $this->input->post('n_unitkerja'),
						'id_skpd' => $this->input->post('id_skpd'),
						'initial' => $this->input->post('initial')
                    );
                    $this->tmunitkerja->update($id, $data);
                    $output = array(
                        'status' => 2
                    );
                    echo json_encode($output);
                } else {
                    $output = array(
                        'status' => 22
                    );
                    echo json_encode($output);
                }
            }
        }
    }

    function delete($id) {
        $this->tmunitkerja->delete($id);
    }

    function data() {
        $this->load->library('datatables');
		$skpd_id = $this->session->userdata('skpd_id');
		if($skpd_id != 0)
		{
			echo $this->datatables
					->select('
						tmunitkerja.id,
						tmunitkerja.n_unitkerja,
						tmskpd.n_skpd
					')
					->from('tmunitkerja')
					->join('tmskpd','tmunitkerja.id_skpd = tmskpd.id')
					->where('id_skpd', $skpd_id)
					->add_column('aksi', '<center>
						<a id="edit" href="#" to="' . base_url() . $this->page . 'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
						<a id="delete" href="#" to="' . base_url() . $this->page . 'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
					</center>', 'tmunitkerja.id')
					->generate();
		}
		else
		{
			echo $this->datatables
					->select('
						tmunitkerja.id,
						tmunitkerja.n_unitkerja,
						tmskpd.n_skpd
					')
					->from('tmunitkerja')
					->join('tmskpd','tmunitkerja.id_skpd = tmskpd.id')
					->add_column('aksi', '<center>
						<a id="edit" href="#" to="' . base_url() . $this->page . 'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
						<a id="delete" href="#" to="' . base_url() . $this->page . 'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
					</center>', 'tmunitkerja.id')
					->generate();
		}
    }

}
