<?php if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Saldo_sms
{
	var $CI = false;

	function __construct()
	{
		$this->CI =& get_instance();
	}

	function save($json_encode)
	{
		$json = json_decode($json_encode);
		foreach($json as $obj){
    		$date_entry = date('Y-m-d H:i:s');
    		$globalstatus = $obj[0]->globalstatus;
			$globalstatustext = $obj[0]->globalstatustext;
		   
		   	foreach($obj[0]->datapacket as $obj1){
				$data_saldo = array(
					'globalstatus' => $globalstatus,
					'globalstatustext' => $globalstatustext,
					'number' => $obj1->packet->number,
					'sendingid' => $obj1->packet->sendingid,
					'sendingstatus' => $obj1->packet->sendingstatus,
					'sendingstatustext' => $obj1->packet->sendingstatustext,
					'price' => $obj1->packet->price,
					'date_entry' => $date_entry
				);
				$this->CI->db
					->insert('tmsaldo', $data_saldo);
			}
		}
		$output = array(
			'globalstatus' => $globalstatus,
			'globalstatustext' => $globalstatustext
		);
		return $output;
	}
}