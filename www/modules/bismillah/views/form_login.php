<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Login | Aplikasi Pelayanan Terpadu Integrasi</title>
			<?php 
			$base_url = base_url();
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.red.css");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-1.9.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.1.1.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
			echo $this->lib_load_css_js->load_js(base_url() , "assets/js/", "jquery.validate.min.js");
			echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "jquery.chosen.css");
			echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "chosen.jquery.min.js"); 
			?>
		<link rel="shortcut icon" href="<?php echo $base_url.'assets/images/favicon.png';?>">
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery(".chzn-select").chosen();
				jQuery("form")
				.validate({
					submitHandler:function(form){
						jQuery('#action').button('loading');
						jQuery('.login-alert').fadeOut('fast');
						jQuery.ajax({
							type:'post',
							url:jQuery(form).attr('action'),
							data:jQuery(form).serialize(),
							dataType:'json',
							success:function(data){
								if(data.status == 1)
								{
									jQuery('.login-alert').html("<div class='alert alert-success'>Login Berhasil...<a href='<?php echo $base_url.$this->page;?>' style='font-weight:bold;' title='Klik, jika tidak dipindahkan oleh browser.'>Klik <span class='iconfa-info-sign'></span></a></div>");
									document.location.href = "<?php echo $base_url.$this->page;?>";
								}
								else if(data.status == 0)
								{
									jQuery('.login-alert').html("<div class='alert alert-error'><strong>Username atau Password belum terdaftar</strong></div>");
								}
								jQuery('#action').button('reset');
								jQuery('.login-alert').fadeIn('fast');
							}
						});
						return false;
					}
				});
			});
		</script>
	</head>
	<body class="loginpage" style="background:url(<?php echo base_url().'assets/images/bg4_1440x900.jpg';?>);">
		<div class="loginpanel">
			<div class="loginpanelinner" style="background:rgba(255,255,255,0.2);">
				<div class="logo animate0 bounceIn"><img src="<?php echo $base_url.'assets/images/logo.png';?>" alt="Logo" width="90%"/></div>
				<form id="form" action="<?php echo $base_url.$this->page.'login_submit';?>" method="post">
					<div class="inputwrapper login-alert"></div>
					<div class="inputwrapper animate1 bounceIn">
						<input type="text" name="username" id="username" placeholder="Masukkan username" />
					</div>
					<div class="inputwrapper animate2 bounceIn">
						<input type="password" name="password" id="password" placeholder="Masukkan password" />
					</div>
					<div class="inputwrapper animate2 bounceIn">
						<select name="userid" style="width: 100%;" class="chzn-select">
							<?php foreach($user->result() as $row){ ?>
							<option value="<?php echo $row->id;?>"><?php echo $row->username;?></option>
							<?php }?>
						</select>
					</div>
					<div class="inputwrapper animate3 bounceIn">
						<button name="submit" id="action">Sign In</button>
					</div>
				</form>
			</div>
		</div>
		<div class="loginfooter">
			<p>Copyright &copy; 2014. Aplikasi Persuratan.</p>
		</div>
	</body>
</html>