<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,no-store,must-revalidate"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="expires" content="0"/>

        <meta name="theme-color" content="#272727">
        <meta name="msapplication-navbutton-color" content="#272727">
        <meta name="apple-mobile-web-app-status-bar-style" content="#272727">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

        <title>SISUMAKER | <?php echo $title; ?></title>
		<?php $base_url = base_url(); ?>
        <link rel="shortcut icon" href="<?php echo $base_url;?>assets/images/favicon.png"/>
        
        <script type="text/javascript">
            var _USER = <?php echo json_encode($this->session->userdata) ?>;
            var base='<?php echo $base_url; ?>';
        </script> 
        
        <?php
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.red.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.mail.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "jquery.ui.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "responsive-tables.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/prettify/", "prettify.css");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-2.0.3.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.2.1.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.slimscroll.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "function.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "chat.js");
        echo $this->lib_load_css_js->load_js(base_url(), "assets/video-call/", "vendor.min.js?v=2.1");
        echo $this->lib_load_css_js->load_css(base_url(), "assets/video-call/", "vendor.min.css?v=1.1");
        echo $this->lib_load_css_js->load_js(base_url(), "assets/video-call/", "app.min.js?v=1.3");
        echo $this->lib_load_css_js->load_css(base_url(), "assets/video-call/", "style.min.css?v=1.2");
        
        
        foreach ($assets as $asset_js_css) {
            echo $asset_js_css;
        }
        ?>
        <!--[if lte IE 8]><?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "excanvas.min.js"); ?><![endif]-->
        <!--<?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "elements.js"); ?>-->
    </head>
    <body style="background-image:url('<?php echo $base_url.$this->session->userdata('wallpaper');?>')">
        <div class="mainwrapper">
            <div class="header">
				<a href="<?php echo $base_url;?>"><div class="logo"></div></a>
				<div class="headerinner">
					<ul class="headmenu">
						<li class="title-skpd" style="border:none">
                            <h2><?php echo $this->session->userdata('n_skpd');?></h2>
                            <h3>Kota Tebing Tinggi</h3>
                        </li>
						<li class="right">
                            <div class="userloggedinfo">
                                <span class="userphoto"><img alt="Photo Profile" style="max-height:90px" src="<?php echo $base_url.'uploads/photo/'.$this->session->userdata('photo'); ?>" /></span>
                                <div class="userinfo">
                                    <h5><?php echo $this->session->userdata('realname'); ?></h5>
                                    NIP. <?php echo $this->session->userdata('nip'); ?>
                                    <ul>
                                        <li><a href="<?php echo $base_url.'panel/edit/'.$this->session->userdata('userid'); ?>">Edit Profile<small> - <?php echo $this->session->userdata('username'); ?></small></a></li>
                                        <li><a href="<?php echo $base_url.'panel/logout'; ?>">Sign Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
					</ul>
				</div>
			</div>
            <div class="leftpanel">
                <div class="leftmenu">        
                    <ul class="nav-o nav nav-tabs nav-stacked">
						<?php echo $this->menu_loader->create_menu($this->session->userdata('auth_id'));?>
						<!--<li class='dropdown'><a href='#'><span class='iconfa-credit-card'></span>Template</a>
							<ul>
								<?php
								$this->tmjenissurat = new Tmjenis_surat();
								foreach($this->tmjenissurat->select("file_surat != ", '')->result() as $row){ ?>
								<li><a href="<?php echo $base_url.$row->file_surat;?>"><?php echo $row->n_jnissurat;?></a></li>
								<?php }?>
							<ul>
						</li>-->
						<li><a href="<?php echo $base_url.'chat/message';?>"><span class="iconfa-comments"></span>Pesan <span id='count_new_message'></span></span><script type='text/javascript'>cek_new_message();</script></a></li>
                        <li>
                            <a href="<?php echo $base_url.'video_call/room';?>">
                                <span class="iconfa-facetime-video"></span>Video Call
                                <span id='count_new_message'></span>
                                <script type='text/javascript'>cek_new_message();</script>
                            </a>
                        </li>
                    </ul>
					<div class="p-bottom">
						<ul class="nav nav-tabs nav-stacked">
							<li>
								<a href="<?php echo $base_url.'user_setting/main/wallpaper';?>" title="Setting Wallpaper"><span class='iconfa-picture'></span></a>
								<a href="" title="Send A Message" id="enablechat"><span class='iconfa-comments-alt'></span></a>
								<a href="https://docs.google.com/viewer?url=<?php echo $base_url;?>user_manual.pdf" title="Help?" target="_blank"><span class='iconfa-road'></span></a>
							</li>
						</ul>
					</div>
                </div>
            </div>
            <div class="rightpanel">
                <div class="maincontent">
                    <div class="maincontentinner">  
                        <div class="row-fluid">
                            <?php echo $this->load->view($this->view.$content); ?>
                        </div>
                        <div class="footer">
                            <div class="footer-left">
                                <span>Copyright &copy; 2019. SISUMAKER (Sistem Informasi Surat Masuk dan Keluar)</span>
                            </div>
                            <div class="footer-right">
								<span>Copyright &copy; 2019. Kota Tebing Tinggi</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <img src="<?php echo base_url().'assets/video-call/loader.gif'?>" id="imageLoader" style="display: none">
        
    </body>
</html>
