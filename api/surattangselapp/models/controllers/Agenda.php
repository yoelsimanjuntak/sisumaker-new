<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'controllers/Auth.php';

class Agenda extends Auth
{
    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credential: true');
        header('Access-Control-Max-Age: 86400');
        header('Access-Control-Allow-Methods:GET,POST,OPTIONS');
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $this->load->model('db_dml');
        $this->load->model('db_select');
        $this->load->model('mymodel');
        $this->load->model('tr_surat_penerima');
        $this->load->model('tmuser');
        $this->load->model('tmpersuratan');
        $this->load->model('penomoran');
        $this->load->model('tmsifat_surat');
        $this->load->model('surat');
        $this->load->model('tmjenis_surat');
        $this->load->model('tr_surat_penerima');
        $this->load->model('tmpegawai');
        $this->load->model('tmunitkerja');
        $this->load->model('tmjabatan');
        $this->load->model('main_inbox');

        // $this->load->library('api_sms_class_reguler_json');
        $this->load->helper('function_helper');
        if ($this->cektoken() == false) {
            header('Content-type: application/json');
            $output = [
                'status' => 0,
                'message' => 'Wrong Token',
                'token' => $this->input->get_request_header('Authorization')
            ];
            exit(json_encode($output));
        }
    }

    public function index_get()
    {
        $decode = $this->viewtoken();
        $auth_id = $decode->auth_id;
        $pegawai_id = $decode->pegawai_id;
        $jsonevents = [];

        $smasuk = $this->query_smasuk($pegawai_id);
        foreach ($smasuk->result() as $row) {
            if ($row->d_awal_kegiatan != '' && $row->d_akhir_kegiatan != '' && $row->v_kegiatan != '') {
                $start = explode(' ', $row->d_awal_kegiatan);
                $end = explode(' ', $row->d_akhir_kegiatan);
                $jsonevents[] = [
                    'id' => $row->id,
                    'title' => $row->prihal,
                    'lokasi' => $row->v_kegiatan,
                    'startTime' => $row->d_awal_kegiatan,
                    'endTime' => $row->d_akhir_kegiatan,
                    'url' => base_url() . $this->page . 'v_surat/' . $row->id
                ];
            }
        }
        $skeluar = $this->query_skeluar($pegawai_id);
        foreach ($skeluar->result() as $row) {
            if ($row->d_awal_kegiatan != '' && $row->d_akhir_kegiatan != '' && $row->v_kegiatan != '') {
                $start = explode(' ', $row->d_awal_kegiatan);
                $end = explode(' ', $row->d_akhir_kegiatan);
                $jsonevents[] = [
                    'id' => $row->id,
                    'title' => $row->prihal,
                    'lokasi' => $row->v_kegiatan,
                    'startTime' => $row->d_awal_kegiatan,
                    'endTime' => $row->d_akhir_kegiatan,
                    'url' => base_url() . $this->page . 'v_surat/' . $row->id
                ];
            }
        }
        $this->response($jsonevents, Auth::HTTP_OK);
        // echo json_encode($jsonevents);
    }

    public function query_smasuk($pegawai_id)
    {
        return $this->db
                ->select('
					tr_surat_penerima.id,
					tr_surat_penerima.d_awal_kegiatan,
					tr_surat_penerima.d_akhir_kegiatan,
					tr_surat_penerima.v_kegiatan,
                    tmpersuratan.prihal,
                    tmpersuratan.v_kegiatan
				')
                ->where('tr_surat_penerima.id_pegawai_ke', $pegawai_id)
                ->where('tmpersuratan.id_jnssurat', 1)
                ->from('tr_surat_penerima')
                ->join('tmpersuratan', 'tr_surat_penerima.id_persuratan = tmpersuratan.id')
                ->group_by('tr_surat_penerima.id_persuratan')
                ->get();
    }

    public function query_skeluar($pegawai_id)
    {
        return $this->db
                ->select('
					tr_surat_penerima.id,
					tr_surat_penerima.d_awal_kegiatan,
					tr_surat_penerima.d_akhir_kegiatan,
					tr_surat_penerima.v_kegiatan,
                    tmpersuratan.prihal,
                    tmpersuratan.v_kegiatan
				')
                ->where('tr_surat_penerima.id_pegawai_ke', $pegawai_id)
                ->where('tmpersuratan.id_jnssurat', 2)
                ->where('tmpersuratan.status_setujui', 1)
                ->from('tr_surat_penerima')
                ->join('tmpersuratan', 'tr_surat_penerima.id_persuratan = tmpersuratan.id')
                ->group_by('tr_surat_penerima.id_persuratan')
                ->get();
    }
}
