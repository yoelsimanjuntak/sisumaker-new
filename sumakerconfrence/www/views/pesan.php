<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,no-store,must-revalidate"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="expires" content="0"/>
        <title><?php echo $title; ?> | Aplikasi Persuratan BP2T</title>
        <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'; ?>"/>
        <?php
        $base_url = base_url();
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.default.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "style.red.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "jquery.ui.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/css/", "responsive-tables.css");
        echo $this->lib_load_css_js->load_css($base_url, "assets/prettify/", "prettify.css");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-2.0.3.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-migrate-1.2.1.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery-ui-1.9.2.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "modernizr.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "bootstrap.min.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "jquery.cookie.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "custom.js");
        echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "function.js");
        foreach ($assets as $asset_js_css) {
            echo $asset_js_css;
        }
        ?>
        <script type="text/javascript">base='<?php echo $base_url; ?>';</script> 
        <!--[if lte IE 8]><?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "excanvas.min.js"); ?><![endif]-->
        <!--<?php echo $this->lib_load_css_js->load_js($base_url, "assets/js/", "elements.js"); ?>-->
    </head>
    <body class="main_pesan">
        <div class="mainwrapper">
            <div class="header">
                <div class="logo" style="position: absolute; width: 100%; height: 110px; padding: 0px; display: block !important; margin-left: 0px; background-image: url(<?php echo $base_url.'assets/images/header.png'; ?>); background-repeat: no-repeat;">&nbsp;</div>
                <div class="headerinner">
                    <ul class="headmenu pull-right">
                        <li class="">
                            <div class="userloggedinfo">
                                <span class="userphoto"><img alt="Photo Profile" width="80px" height="80px" src="<?php echo $base_url.'uploads/photo/'.$this->session->userdata('photo'); ?>" /></span>
                                <div class="userinfo">
                                    <h5><?php echo $this->session->userdata('realname'); ?></h5>
                                    NIP. <?php echo $this->session->userdata('nip'); ?>
                                    <ul>
                                        <li><a href="<?php echo $base_url.'panel/edit/'.$this->session->userdata('userid'); ?>">Edit Profile<small> - <?php echo $this->session->userdata('username'); ?></small></a></li>
                                        <li><a href="<?php echo $base_url.'panel/logout'; ?>">Sign Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
			<div class="maincontent">
				<div class="maincontentinner">
					<div class="row-fluid">
						<div id="result"></div>
						<div class="messagepanel">
							<div class="messagehead">
								<button class="btn btn-success btn-large">Compose Nota Dinas</button>
							</div>
							<div class="messagemenu">
								<ul>
									<li<?php if($this->active == 'inbox'){?> class="active"<?php }?>><a href=""><span class="iconfa-inbox"></span> Inbox</a></li>
									<li<?php if($this->active == 'send'){?> class="active"<?php }?>><a href=""><span class="iconfa-plane"></span> Sent</a></li>
								</ul>
							</div>
							<div class="messagecontent" style="border-right:1px solid #bb2f0e;border-bottom:1px solid #bb2f0e;">
								<?php echo $this->load->view($this->view.$content); ?>
							</div>
						</div>
					</div>
					<div class="footer">
						<div class="footer-left">
							<span>Copyright &copy; 2014. Aplikasi Persuratan BP2T.</span>
						</div>
						<div class="footer-right">
							<span>&copy; 2014. Developer by Badan Pelayanan Perijinan Terpadu (BP2T)<br/> KOTA TANGERANG SELATAN.</span>
						</div>
					</div>
				</div>
			</div>
        </div>
    </body>
</html>