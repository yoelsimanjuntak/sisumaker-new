<script type="text/javascript">
	var oTable;
	datatables_ps("<?php echo site_url().$this->page.'data';?>", <?php echo $sort;?>);
	fancybox();
	jQuery(document).ready(function(){
		jQuery('#delete').live('click', function(){
			if(confirm("Yakin Ingin Menghapus data ini?")){
				jQuery.ajax({
					"type":"post",
					"cache":false,
					"url":jQuery(this).attr("to"),
					"success":function(){
						jQuery('#result').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button><strong style="font-weight:bold">Sukses!</strong> Data Berhasil Dihapus.</div>').fadeIn('slow');
						oTable.fnDraw();
					}
				});
			}
			return false;
		});
	});
	function cek_outbox() {
		jQuery("#count_outbox").html('...');
		jQuery.ajax({
			type:'post',
			url: "<?php echo base_url();?>sms/s_outbox/cek_min",
			cache: false,
			dataType: 'json',
			success: function(data) {
				if(data.count != 0)
				{
					jQuery("#count_outbox").html(data.count);
				}
				else
				{
					jQuery("#count_outbox").html('');
				}
			}
		});
		var waktu = setTimeout("cek_outbox()", 40000);
	}

	function cek_sentitems() {
		jQuery("#count_sentitems").html('...');
		jQuery.ajax({
			type:'post',
			url: "<?php echo base_url();?>sms/s_senditem/cek_min",
			cache: false,
			dataType: 'json',
			success: function(data) {
				if(data.count != 0)
				{
					jQuery("#count_sentitems").html(data.count);
				}
				else
				{
					jQuery("#count_sentitems").html('');
				}
			}
		});
		var waktu = setTimeout("cek_sentitems()", 50000);
	}
</script>
<div id="result"></div>
<a href="<?php echo base_url().'sms/s_outbox';?>">
	<button class="btn btn-large">Pending
		<sup class="label label-important" id="count_outbox">
			<script type="text/javascript">cek_outbox();</script>
		</sup>
	</button>
</a>
<button class="btn btn-primary btn-large">Gagal Terkirim
<sup class="label label-important" id="count_sentitems">
		<script type="text/javascript">cek_sentitems();</script>
	</sup>
</button>
<div style="clear:both;"></div>
<div class="load" style="display:none;"></div>
<table id="dyntable" class="table table-bordered">
    <colgroup>
        <col class="con0" style="align: center; width: 4%" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
        <col class="con1" />
        <col class="con0" />
    </colgroup>
    <thead>        
		<tr>
			<th width="15px">No</th>
			<th width="80px">Number</th>
			<th width="180px">Nama SKPD</th>
			<th width="180px">Nama</th>
			<th>Text Decoded</th>
			<th width="160px">Sending Date Time</th>
			<th width="80px">Aksi</th>
		</tr>
    </thead>
	<tbody>
		<tr>
			<td valign="top" colspan="7" class="dataTables_empty">Loading <img src="<?php echo base_url().'assets/images/loaders/loader19.gif';?>"/></td>
		</tr>
	</tbody>
	</tbody>
</table>