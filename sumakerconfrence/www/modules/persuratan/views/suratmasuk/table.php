<script type="text/javascript">
    var oTable;
    datatables_ps("<?php echo site_url() . $this->page . 'data'; ?>", <?php echo $sort; ?>);
    jQuery(document).ready(function(){
        jQuery('#delete').live('click', function(){
            if(confirm("Yakin Ingin Menghapus data ini?")){
                jQuery.ajax({
                    "type":"post",
                    "cache":false,
                    "url":jQuery(this).attr("to"),
                    "success":function(){
                        jQuery(".view_load").slideUp('fast');
                        jQuery("#open_add").removeClass('open');
                        jQuery("#open_add i").removeClass('icon-minus-sign').addClass('icon-plus-sign');
                        jQuery('#result').html('<p class="pesan berhasil"><span class="iconfa-check"></span> Data Berhasil Di Hapus<a class="close" data-dismiss="alert" href="#">&times;</a></p>').fadeIn('slow');
                        oTable.fnDraw();
                    }
                });
            }
            return false;
        });
        jQuery("#open_add").live('click',function(){
            if(!jQuery(this).hasClass('open')) {
                jQuery(".load").load(jQuery(this).attr('to')).slideDown('fast');
                jQuery(this).addClass('open');
                jQuery("#open_add i").removeClass('icon-plus-sign').addClass('icon-minus-sign');
            } else {
                jQuery(".view_load").slideUp('fast');
                jQuery(this).removeClass('open');
                jQuery("#open_add i").removeClass('icon-minus-sign').addClass('icon-plus-sign');
            }
        });
        jQuery("#close_add").live('click',function(){	
            jQuery(".view_load").slideUp('fast');
            jQuery("#open_add").removeClass('open');
            jQuery("#open_add i").removeClass('icon-minus-sign').addClass('icon-plus-sign');
        });
        jQuery('#edit').live('click',function(){
            jQuery(".load").load(jQuery(this).attr("to")).slideDown('fast');
            jQuery("#open_add").removeClass('open');
            jQuery("#open_add i").removeClass('icon-minus-sign').addClass('icon-plus-sign');
        });
        jQuery('#load').live('click',function(){
            jQuery(".load").load(jQuery(this).attr("to")).slideDown('fast');
        });
        jQuery('#refresh').live('click',function(){
            oTable.fnDraw();
        });
    });
</script>
<div id="result"></div>
<span id="addVar">
    <button id="open_add" class="add btn btn-primary" to="<?php echo site_url() . $this->page . 'add'; ?>"><i class="icon-plus-sign icon-white"></i> Tambah Sifat Surat</button>
    <button id="refresh" class="btn pull-right">Segarkan Tabel <i class="icon-refresh"></i></button>
</span>
<div class="load" style="display:none;"></div>
<table id="dyntable" class="table table-bordered">
    <colgroup>
        <col class="con0" style="align: center; width: 4%" />
        <col class="con1" />
        <col class="con0" />
    </colgroup>
    <thead>        
        <tr>
            <th width="15px">No</th>
            <th>No Surat</th>
            <th>Surat Dari</th>
            <th>Tanggal Surat</th>
            <th>Tanggal Terima Surat</th>
            <th>No Agenda</th>
            <th>Sifat Surat</th>
            <th>Prihal</th>
            <th>Status</th>
            <th width="40px">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td valign="top" colspan="7" class="dataTables_empty">Loading <img src="<?php echo base_url() . 'assets/images/loaders/loader19.gif'; ?>"/></td>
        </tr>
    </tbody>
</table>