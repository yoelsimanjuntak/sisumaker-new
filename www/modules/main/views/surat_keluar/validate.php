<script type="text/javascript">
	function file_koreksi_show(a){
		if(a == '1'){
			jQuery('#file_koreksi').show();
			jQuery('#file_default').hide();
		}else{
			jQuery('#file_default').show();
			jQuery('#file_koreksi').hide();
		}
		return false;
	}
	jQuery(document).ready(function(){
		jQuery('#check_skpd').on('click', function(){
			jQuery('#c_skpd').val('1');
			skpd = jQuery('#c_skpd').val();
			kecamatan = jQuery('#c_kecamatan').val();
			kelurahan = jQuery('#c_keluarahan').val();
			jQuery('#view_list_pegawai').load(base + 'main/surat_keluar/list_skpd/' + skpd + '/' + kecamatan + '/' + kelurahan);
		});
		
		jQuery('#check_kecamatan').on('click', function(){
			jQuery('#c_kecamatan').val('2');
			skpd = jQuery('#c_skpd').val();
			kecamatan = jQuery('#c_kecamatan').val();
			kelurahan = jQuery('#c_keluarahan').val();
			jQuery('#view_list_pegawai').load(base + 'main/surat_keluar/list_skpd/' + skpd + '/' + kecamatan + '/' + kelurahan);
		});
		
		jQuery('#check_kelurahan').on('click', function(){
			jQuery('#c_keluarahan').val('3');
			skpd = jQuery('#c_skpd').val();
			kecamatan = jQuery('#c_kecamatan').val();
			kelurahan = jQuery('#c_keluarahan').val();
			jQuery('#view_list_pegawai').load(base + 'main/surat_keluar/list_skpd/' + skpd + '/' + kecamatan + '/' + kelurahan);
		});
		
		jQuery("#tgl_surat").datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_awal_kegiatan").datepicker("option", "minDate", selectedDate);
				if(jQuery("#d_awal_kegiatan").val() == ''){
					jQuery("#d_akhir_kegiatan").datepicker("option", "minDate", selectedDate);
				}
			}
		});
		jQuery("#d_awal_kegiatan").datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm:ss',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_akhir_kegiatan").datepicker("option", "minDate", selectedDate);
			}
		});
		jQuery("#d_akhir_kegiatan").datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm:ss',
			changeMonth: true,
			changeYear: true,
			onClose: function(selectedDate){
				jQuery("#d_awal_kegiatan").datepicker("option", "maxDate", selectedDate);
			}
		});
		jQuery(".chzn-select").chosen();
		jQuery("#form1")
			.validate({
				rules: {
					id_jnssurat: "required",
					id_surat: "required",
					pegawai: "required",
					no_surat: "required",
					tgl_surat: "required",
					id_sifatsurat: "required",
					prihal: "required",
					isi_surat: "required",
					userfile: "required"
				},
				highlight: function(label) {
					jQuery(label).closest('.control-group').addClass('error');
				},
				success: function(label) {
					label
						.text('Ok!').addClass('valid')
						.closest('.control-group').addClass('success');
				}
			});
	});
</script>