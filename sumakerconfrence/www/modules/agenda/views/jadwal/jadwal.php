<script type="text/javascript">
    var oTable;
    var oTable1;
    datatables_ps("<?php echo site_url() . $this->page . 'data'; ?>", 3);
    datatables_ps1("<?php echo site_url() . $this->page . 'data_keluar'; ?>", 2);
    jQuery(document).ready(function(){
		jQuery('#search').on('change', function(){
			document.location.href = base + 'agenda/jadwal/index/' + jQuery(this).val();
		});
    });
</script>
<!--<span id="addVar">
	<div class="navbar" style="margin-bottom:10px;">
		<div class="navbar-inner">
			<div class="row-fluid">
				<div class="span6">
					<a href="#" class="brand">Filter Data</a>
					<div class="navbar-form">
						<form id="form1" method="post" action="<?php echo base_url().$this->page.'filter';?>" class="navbar-form">
							<input type="text" name="value_search1" value="<?php echo $value_search1;?>" class="span2"/>
							<input type="text" name="value_search2" value="<?php echo $value_search2;?>" class="span2"/>
							<button class="btn" type="submit">Cari</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</span>-->
<div class="row-fluid">
	<div class="span6">
		<div style="padding-bottom:10px;color:#fff;"><h4><i class="iconfa-inbox"></i> Surat Masuk</h4></div>
		<table id="dyntable" class="table table-bordered">
			<colgroup>
				<col class="con0" style="align: center; width: 4%" />
				<col class="con1" />
				<col class="con0" />
				<col class="con1" />
				<col class="con0" />
				<col class="con1" />
			</colgroup>
			<thead>
				<tr>
					<th width="15px">No</th>
					<th width="140px">Perihal</th>
					<th width="100px">Asal Surat</th>
					<th width="100px">Tanggal</th>
					<th width="200px">Lokasi</th>
					<th width="200px">Kehadiran</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td valign="top" colspan="7" class="dataTables_empty">Loading <img src="<?php echo base_url() . 'assets/images/loaders/loader19.gif'; ?>"/></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="span6">
		<div style="padding-bottom:10px;color:#fff;"><h4><i class="iconfa-plane"></i> Surat Keluar</h4></div>
		<table id="dyntable1" class="table table-bordered">
			<colgroup>
				<col class="con0" style="align: center; width: 4%" />
				<col class="con1" />
				<col class="con0" />
				<col class="con1" />
				<col class="con0" />
			</colgroup>
			<thead>
				<tr>
					<th width="15px">No</th>
					<th width="140px">Perihal</th>
					<th width="100px">Tanggal</th>
					<th width="200px">Lokasi</th>
					<th width="200px">Kehadiran</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td valign="top" colspan="7" class="dataTables_empty">Loading <img src="<?php echo base_url() . 'assets/images/loaders/loader19.gif'; ?>"/></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
                        