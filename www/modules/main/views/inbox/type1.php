<?php if ($result->num_rows() != 0) {
    if ($c_read != 0) {
        foreach ($result->result() as $row) {
            ?>	
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page; ?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a> &nbsp;<b>
			<?php
            if ($row->id_compose != 0 && $row->id_compose != 1) {
                if ($row->status_setujui == 0) {
                    if ($row->id_pegawaittd == $this->session->userdata('pegawai_id')) {
                        ?>
						<a href="#"  id="view" to="<?php echo base_url().$this->page.'view_setujui/'.$row->id; ?>" class="btn btn-success"><i class="iconfa-check"></i> TANDA TANGANI SURAT</a>
				<?php
                    } else {
                        $n_ttd = $this->tmpegawai->where('id', $row->id_pegawaittd)->get()->n_pegawai;
                        echo "<span style='font-weight:normal'>Akan Di TTD Oleh : ".$n_ttd."</span>";
                    }
                } else {
                    echo "SURAT SUDAH DITANDA TANGANI";
                }
            } ?></b>
		</div>
		<div class="span3">
			<a href="<?php echo base_url().$this->page.'print_all/'.$encrip.'/'.$valid; ?>" target="_blank" class="pull-right" style="margin-right:20px;padding:7px 12px 5px"><span class="iconfa-print"></span> Cetak</a>
		</div>
	</div>
	
	<?php
    $one = 1;
            $view_surat = $row->file;
            $pegawai_id = $this->session->userdata('pegawai_id');
            foreach ($result_history->result() as $row_history) {
                // $disposisi = 0;
                // if($row_history->id_pegawai_ke === $pegawai_id){$disposisi = 1;}
                // elseif($row_history->id_pegawai_dari === $pegawai_id){$disposisi = 1;}
                $disposisi = 1;
                if ($disposisi == true or $one == 1) {
                    $d_entry = $row_history->d_entry;
                    $catatan = $row_history->catatan;
                    $file_coretan = $row_history->file_coretan;
                    $photo = $row_history->photo;
                    $d_entry_temp = $row_history->d_entry;
                    $dari_pegawai = $row_history->dari_pegawai;
                    $ke_pegawai = $row_history->ke_pegawai.'; ';
                    if ($row_history->num > 1) {	//Jika didisposisikan lebih dari satu pegawai
                        $ke_pegawai = '';
                        $to = $this->main_inbox->read_to($id_persuratan, $d_entry, 0)->result();
                        foreach ($to as $row_to) {
                            $ke_pegawai .= $row_to->ke_pegawai.'; ';
                        }
                
                        $cc = $this->main_inbox->read_to($id_persuratan, $d_entry, 1);
                        if ($cc->num_rows() != 0) {
                            $ke_pegawai .= "</span><span class='to'>cc ";
                            foreach ($cc->result() as $row_cc) {
                                $ke_pegawai .= $row_cc->ke_pegawai.'; ';
                            }
                        }
                    } ?>
			<div class="msgauthor">
				<div class="thumb"><img src="<?php echo base_url().'uploads/photo/'.$photo; ?>" alt="Photo" /></div>
				<div class="authorinfo">
					<span class="date pull-right"><?php echo indo_date_time($d_entry); ?></span>
					<h5><strong><?php echo $dari_pegawai; ?></strong></h5>
					<span class="to">to <?php echo $ke_pegawai; ?></span>
					<?php if ($catatan != '') {
                        ?>
					<blockquote>
						<p><?php echo $catatan; ?></p>
						<small><?php echo $dari_pegawai; ?></small>
					</blockquote>
					<?php
                    } elseif ($file_coretan != '') {
                        echo '<img src="'.base_url().$file_coretan.'" />';
                    } ?>
					<?php if ($row_history->file_surat_koreksi != '') {
                        $view_surat = $row_history->file_surat_koreksi; ?>
						<div class="btn-group">
						<a href="https://docs.google.com/viewer?url=<?php echo base_url().$row_history->file_surat_koreksi; ?>" class="btn btn-large text-link" target="_blank" title="View"><i class="iconfa-paper-clip"></i> File Koreksi Surat</a>
						<a href="<?php echo base_url().$row_history->file_surat_koreksi; ?>" class="btn btn-large text-link" target="_blank" title="Download"><i class="iconfa-download-alt"></i></a>
					</div>
					<?php
                    }
                    if ($row_history->lampiran_file_koreksi != '') {
                        ?>
					<div class="btn-group">
						<a href="https://docs.google.com/viewer?url=<?php echo base_url().$row_history->file_lampiran_koreksi; ?>" class="btn btn-large text-link" target="_blank" title="View"><i class="iconfa-paper-clip"></i> File Koreksi Lampiran</a>
						<a href="<?php echo base_url().$row_history->file_lampiran_koreksi; ?>" class="btn btn-large text-link" target="_blank" title="Download"><i class="iconfa-download-alt"></i></a>
					</div>
					<?php
                    } ?>
				</div>
			</div>
			<?php if ($one == 1) {
                        ?>
			<div class="msgbody">
				<div class="row-fluid">
					<div class="span6">
						<table class="table table-bordered table-invoice">
							<colgroup>
								<col class="con1" />
								<col class="con0" />
								<col class="con1" />
								<col class="con0" />
							</colgroup>
							<tbody>
								<tr>
									<td width="120px">Surat Dari</td>
									<td><?php echo $row->dari; ?></td>
								</tr>
								<tr>
									<td>Tanggal Surat</td>
									<td><?php echo indonesian_date($row->tgl_surat); ?></td>
								</tr>
								<tr>
									<td>Sifat</td>
									<td>
										<?php
                                        $id_sifat = $row->id_sifatsurat;
                        $t_sifat = $row->n_sifatsurat;
                        if ($id_sifat == 1) {
                            $c_sifat = 'important';
                        } elseif ($id_sifat == 2) {
                            $c_sifat = 'warning';
                        } elseif ($id_sifat == 3) {
                            $c_sifat = 'inverse';
                        } elseif ($id_sifat == 4) {
                            $c_sifat = 'info';
                        }
                        echo "<span class='label label-".$c_sifat."'>".$t_sifat."</span>"; ?>
									</td>
								</tr>
								<tr>
									<td>Perihal</td>
									<td><?php echo $row->prihal; ?></td>
								</tr>
							</tbody>
						</table>
						<p><?php echo $row->isi_surat; ?></p>
						<?php if ($row->d_awal_kegiatan != '' && $row->d_akhir_kegiatan != '' && $row->v_kegiatan != '') {
                            ?>
						<table class="table table-bordered pull-right" style="width:300px">
							<colgroup>
								<col class="con1" />
								<col class="con0" />
							</colgroup>
							<thead>
								<tr>
									<th colspan="2"><i class="iconfa-calendar"></i> &nbsp;Jadwal Kegiatan: </th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td width="80px">Mulai</td>
									<td><?php echo indo_date_time($row->d_awal_kegiatan); ?></td>
								</tr>
								<tr>
									<td>Akhir</td>
									<td><?php echo indo_date_time($row->d_akhir_kegiatan); ?></td>
								</tr>
								<tr>
									<td>Keterangan</td>
									<td><?php echo $row->v_kegiatan; ?></td>
								</tr>
							</tbody>
						</table>
						<div style="clear:both"></div>
						<?php
                        } ?>
						<div class="btn-group">
							<a href="https://docs.google.com/viewer?url=<?php echo base_url().$row->file; ?>" class="btn btn-large text-link" target="_blank" title="View"><i class="iconfa-paper-clip"></i> File Surat</a>
							<a href="<?php echo base_url().$row->file; ?>" class="btn btn-large text-link" target="_blank" title="Download"><i class="iconfa-download-alt"></i></a>
						</div>
						<?php if ($row->file_lampiran != '') {
                            ?>
						<div class="btn-group">
							<a href="https://docs.google.com/viewer?url=<?php echo base_url().$row->file_lampiran; ?>" class="btn btn-large text-link" target="_blank" title="View"><i class="iconfa-paper-clip"></i> File Lampiran</a>
							<a href="<?php echo base_url().$row->file_lampiran; ?>" class="btn btn-large text-link" target="_blank" title="Download"><i class="iconfa-download-alt"></i></a>
						</div>
						<?php
                        } ?>
					</div>
					<div class="span6" id="view_surat">&nbsp;</div>
				</div>
				<div style="clear:both"></div>
			</div>
			<?php
                $one = $one + 1;
                    }
                }
            } ?>

	<script type="text/javascript">
	function file_koreksi_show(a){
		if(a == '1'){
			jQuery('#file_koreksi').show();
		}else{
			jQuery('#file_koreksi').hide();
		}
		return false;
	}
	jQuery('#view_surat').html('<iframe src="https://docs.google.com/viewer?url=<?php echo base_url().$view_surat; ?>&embedded=true" width="100%" height="500px" style="background:#FFFFFF;border:1px #ccc solid" allowtransparency="true" frameborder="0" border="0">File Surat...</iframe>');
	</script>
	<div class="msgreply">
		<div class="thumb"><img src="<?php echo base_url().'uploads/photo/'.$this->session->userdata('photo'); ?>" alt="Photo" /></div>
		<div class="reply">
			<strong style="font-weight:bold;">Kirim Surat Ke:</strong>
			<?php if ($this->session->userdata('auth_id') == '359' || $this->session->userdata('auth_id') == '360') {
                ?>
			<small class="desc">
				Semua Kepala&nbsp;
				<a style="cursor:pointer" id="check_skpd">SKPD</a> | 
				<a style="cursor:pointer" id="check_kecamatan">Kecamatan</a> | 
				<a style="cursor:pointer" id="check_kelurahan">Kelurahan</a> | 
				<a style="cursor:pointer" id="check_puskesmas">Puskesmas</a> | 
				<!--<a style="cursor:pointer" id="check_sekolah">Sekolah</a> | -->
				<a style="cursor:pointer" id="check_rs">Rumah Sakit</a> | 
				<a style="cursor:pointer" id="check_uptpendidikan">UPT Pendidikan</a>
			</small>
			<?php
            } ?>
			<form id="form4" class="stdform" method="post" action="<?php echo site_url().$this->page.'disposisi/'.$row->id; ?>" novalidate="novalidate"  enctype="multipart/form-data">				
				<input type="hidden" id="c_skpd" name="c_skpd" value="0-0"/> 
				<input type="hidden" id="c_kecamatan" name="c_kecamatan" value="0-0"/> 
				<input type="hidden" id="c_kelurahan" name="c_kelurahan" value="0-0"/> 
				<input type="hidden" id="c_puskesmas" name="c_puskermas" value="0-0"/> 
				<input type="hidden" id="c_sekolah" name="c_sekolah" value="0-0"/> 
				<input type="hidden" id="c_rs" name="c_rs" value="0-0"/> 
				<input type="hidden" id="c_uptpendidikan" name="c_uptpendidikan" value="0-0"/>
				<input type="hidden" name="d_awal_kegiatan" value="<?php echo $row->d_awal_kegiatan; ?>"/>
				<input type="hidden" name="d_akhir_kegiatan" value="<?php echo $row->d_akhir_kegiatan; ?>"/>
				<input type="hidden" name="v_kegiatan" value="<?php echo $row->v_kegiatan; ?>"/>
				<span id="view_list_pegawai">
					<select id="pegawai" name="pegawai[]" required="" data-placeholder="Nama Pegawai - Jabatan" class="chzn-select" multiple="multiple" style="width:100%;z-index:99999;" tabindex="3">
						<?php foreach ($result_disposisi->result() as $row_disposisi) {
                ?>
						<option value="<?php echo $row_disposisi->id; ?>"><?php echo $row_disposisi->n_jabatan.' '.$row_disposisi->n_unitkerja.' - '.$row_disposisi->n_pegawai; ?></option>
						<?php
            } ?>
					</select>
				</span>
				<textarea placeholder="Catatan:" style="margin-top: 0px; margin-bottom: 10px; height: 155px;" name="catatan" id="catatan"></textarea>
				
				<?php if ($row->id_skpd_in == $row->id_skpd_out) { //Upload File Koreksi Jika surat keluar?>
				<b>
					<div class="row-fluid">
						<span class="span2">Surat Perlu Dikoreksi</span> 
						<span class="span10">
							<input id="i_koreksi" type="radio" name="file_koreksi" value="1" onclick="file_koreksi_show(1)"> &nbsp;Ya &nbsp; &nbsp; &nbsp;
							<input id="i_koreksi" type="radio" name="file_koreksi" value="0" onclick="file_koreksi_show(0)" checked="checked"> &nbsp;Tidak
						</span>
					</div>
				</b>
				<span id="file_koreksi" style="display:none">
					<div class="row-fluid">
						<span class="span2">File Surat Koreksi</span> 
						<span class="span10">
							<input id="userfile" type="file" name="userfile" style="margin-bottom:10px;"/>
							&nbsp; <sup class="text-info label label-info" style="font-weight:bold"> &nbsp; .doc&nbsp;</sup>
							&nbsp; <sup class="text-info label label-warning" style="font-weight:bold"> &nbsp; .pdf&nbsp;</sup>
						</span>
					</div>
					<div class="row-fluid">
						<span class="span2">Lapiran Koreksi</span> 
						<span class="span10">
							<input id="file_lampiran" type="file" name="file_lampiran" style="margin-bottom:10px;"/>
							&nbsp; <sup class="text-info label label-success" style="font-weight:bold"> &nbsp; .xls&nbsp;</sup>
						</span>
					</div>
				</span>
				<?php
            } ?>
				<p class="stdformbutton">
					<a id="check_paraf" to="<?php echo base_url().$this->page.'varifikasi_pass'; ?>" style="cursor:pointer" class="btn"><i class="iconfa-check"></i> Paraf</a>
					<a id="view" to="<?php echo base_url().$this->page.'send_paraf'; ?>" class="btn btn-primary action_send" style="display:none"><i class="iconfa-plane"></i> Kirim</a>
				</p>
			</form>
		</div>
	</div>
</div>
<?php
        }
    } else {
        ?>
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page; ?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
		</div>
	</div>
	<div class="msgauthor">
		<p><b>Terjadi Kesalahan!</b> Anda tidak mendapatkan disposisi surat ini.</p>
	</div>
</div>
<?php
    }
} else {
    ?>
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page; ?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
		</div>
	</div>
	<div class="msgauthor">
		<p><b>Terjadi Kesalahan!</b> Data Tidak Ditemukan</p>
	</div>
</div>
<?php
} ?>