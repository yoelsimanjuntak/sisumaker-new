<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of manipulasi
 *
 * @author Yusuf
 */
class Tmunitkerja extends CI_Model
{

    var $table = 'tmunitkerja';
    var $key = 'id';

    function __construct()
	{
		parent::__construct();
		$this->load->database();
    }

//put your code here
	function select($key = false, $id = false)
	{
		if($id == false)
		{
			return $this->db
				->get($this->table);
		}
		else{
			return $this->db
				->where($key, $id)
				->get($this->table);
		}
	}





	function select_skpd($id_skpd = false)
	{
		if($id_skpd != 0)
		{
			return $this->db
				->select('
					id,
					n_unitkerja
				')
				->where('id_skpd', $id_skpd)
				->from($this->table)
				->get();
		}
		else
		{
			return $this->db
				->select('
					id,
					n_unitkerja
				')
				->from($this->table)
				->get();
		}
	}

    function insert($form)
	{
        return $this->db
			->insert($this->table, $form);
    }

    function update($id, $form)
	{
        return $this->db
			->where($this->key, $id)
			->update($this->table, $form);
    }

    function delete($id)
	{
        return $this->db
			->where($this->key, $id)
			->delete($this->table);
    }

}

?>
