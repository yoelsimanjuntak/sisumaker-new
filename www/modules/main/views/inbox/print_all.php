<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr height="14px">
			<td width="143"><img src="<?php echo base_url().'assets/images/logo_print.png';?>" alt="Sisumaker" class="logo"></td>
			<td align="right">
				<font size="-1" color="#777">
					<b><?php echo $this->session->userdata('realname');?> &lt;<?php echo $this->session->userdata('username');?>&gt;</b>
				</font>
			</td>
		</tr>
	</tbody>
</table>
<?php if ($result->num_rows() != 0) { if($c_read != 0){ foreach ($result->result() as $row) { ?>	
<div class="messageview">
	<?php 
	$one = 1;
	if($result_history->num_rows() > 1)
	{
	
			// 1. Jika surat sudah pernah didisposisikan		=> lihat tr_surat_terima
		$pegawai_id = $this->session->userdata('pegawai_id');
		foreach($result_history->result() as $row_history)
		{
			// $disposisi = 0;
			// if($row_history->id_pegawai_ke === $pegawai_id){$disposisi = 1;}
			// elseif($row_history->id_pegawai_dari === $pegawai_id){$disposisi = 1;}
			$disposisi = 1;
			if($disposisi == true or $one == 1)
			{
				$d_entry = $row_history->d_entry;
				$catatan = $row_history->catatan;
				$photo = $row_history->photo;
				$d_entry_temp = $row_history->d_entry;
				$dari_pegawai = $row_history->dari_pegawai;
				$ke_pegawai = $row_history->ke_pegawai.'; ';
				if($row_history->num > 1){	//Jika didisposisikan lebih dari satu pegawai
					$ke_pegawai = '';
					$to = $this->main_inbox->read_to($id_persuratan, $d_entry, 0)->result();
					foreach($to as $row_to){
						$ke_pegawai .= $row_to->ke_pegawai.'; ';
					}
					
					$cc = $this->main_inbox->read_to($id_persuratan, $d_entry, 1);
					if($cc->num_rows() != 0){
						$ke_pegawai .= "</span><span class='to'>cc ";
						foreach($cc->result() as $row_cc){
							$ke_pegawai .= $row_cc->ke_pegawai.'; ';
						}
					}
				} ?>
				
				<hr/>
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="message">
					<tbody>
						<tr>
							<td><font size="-1"><b><?php echo $dari_pegawai;?> </b></font></td>
							<td align="right"><font size="-1"><?php echo indo_date_time($d_entry);?></font></td>
						</tr>
						<tr>
							<td colspan="2"><font size="-1" class="recipient"><div>Kepada: <?php echo $ke_pegawai;?></div></font></td>
						</tr>
						<tr>
							<td colspan="2">
								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:6px 0px 0px 7px;">
									<tbody>
										<tr>
											<td>
												<div style="overflow: hidden;">
													<font size="-1">
														<div dir="ltr">
															<?php if($catatan != ''){ ?> 
															<table width="100%" cellpadding="0px" style="margin:6px 0px 0px 7px;">
																<tr>
																	<td><?php  echo $catatan;?></td>
																</tr>
															</table>
															<?php } if($row_history->file_surat_koreksi != '' || $row_history->lampiran_file_koreksi != ''){ ?>
															<table>
																<tr>
																	<td><br/><br/><br/></td>
																	<td>
																		<?php if($row_history->file_surat_koreksi != '') {?>
																		<em>File Koreksi Surat Terlampir</em>
																		<?php }if($row_history->lampiran_file_koreksi != ''){ ?>
																		<em>File Koreksi Lampiran Terlampir</em>
																		<?php } ?>
																	</td>
																</tr>
															</table>
															<?php } ?>
														</div>
													</font>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
				<?php if($one == 1){ ?>
					<table width="100%" cellpadding="0px" style="margin:6px 0px 0px 7px;">
						<colgroup>
							<col class="con1" />
							<col class="con0" />
							<col class="con1" />
							<col class="con0" />
						</colgroup>
						<tbody>
							<tr>
								<td width="120px">Surat Dari</td>
								<td><?php echo $row->dari;?></td>
								<td width="120px">Tanggal Surat</td>
								<td width="320px"><?php echo indonesian_date($row->tgl_surat);?></td>
							</tr>
							<tr>
								<td>Sifat</td>
								<td colspan="3"><?php echo $row->n_sifatsurat;?></td>
							</tr>
							<tr>
								<td>Perihal</td>
								<td colspan="3"><?php echo $row->prihal;?></td>
							</tr>
						</tbody>
					</table>
					<?php if($row->d_awal_kegiatan != '' && $row->d_akhir_kegiatan != '' && $row->v_kegiatan != ''){ ?>
					<table cellpadding="3px" style="width:300px;float:right;">
						<colgroup>
							<col class="con1" />
							<col class="con0" />
						</colgroup>
						<thead>
							<tr>
								<th colspan="2"><i class="iconfa-calendar"></i> &nbsp;Jadwal Kegiatan: </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="80px">Mulai</td>
								<td><?php echo indo_date_time($row->d_awal_kegiatan);?></td>
							</tr>
							<tr>
								<td>Akhir</td>
								<td><?php echo indo_date_time($row->d_akhir_kegiatan);?></td>
							</tr>
							<tr>
								<td>Lokasi</td>
								<td><?php echo $row->v_kegiatan;?></td>
							</tr>
						</tbody>
					</table>
					<?php } ?>
					<table>
						<tr>
							<td><br/><br/><br/></td>
							<td>
								<?php echo $row->isi_surat; ?>
								<em>File Surat Terlampir</em>
								<?php if($row->file_lampiran != ''){ ?>
								<em>File Koreksi Terlampir</em>
								<?php } ?>
							</td>
						</tr>
					</table>
					<div style="clear:both"></div>
				<?php
					$one = $one + 1;
				}
			}
		} 
	}else{ 
		
			// 2. Jika Belum Pernah Didisposisikan ?>
	<hr/>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" class="message">
		<tbody>
			<tr>
				<td><font size="-1"><b><?php echo $row->n_pegawai;?></b></font></td>
				<td align="right"><font size="-1"><?php echo indo_date_time($row->d_entry);?></font></td>
			</tr>
			<?php
			$d_entry = $row->d_entry;
			$ke_pegawai = $this->session->userdata('realname');
			$to = $this->main_inbox->read_to($id_persuratan, $d_entry, 0);
			if($to->num_rows() != 0){
				$ke_pegawai = '';
				foreach($to->result() as $row_to){
					$ke_pegawai .= $row_to->ke_pegawai.'; ';
				}
			}	
			$cc = $this->main_inbox->read_to($id_persuratan, $d_entry, 1);
			if($cc->num_rows() != 0){
				$ke_pegawai .= "</span><span class='to'>cc ";
				foreach($cc->result() as $row_cc){
					$ke_pegawai .= $row_cc->ke_pegawai.'; ';
				}
			}
			?>
			<tr>
				<td colspan="2"><font size="-1" class="recipient"><div>Kepada: <?php echo $ke_pegawai;?></div></font></td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin:6px 0px 0px 7px;">
						<tbody>
							<tr>
								<td>
									<div style="overflow: hidden;">
										<font size="-1">
											<div dir="ltr">
											</div>
										</font>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		<tbody>
	</table>
	<table width="100%" cellpadding="1" cellspacing="0" border="0" style="margin:6px 0px 0px 7px;">
		<colgroup>
			<col class="con1" />
			<col class="con0" />
			<col class="con1" />
			<col class="con0" />
		</colgroup>
		<tbody>
			<tr>
				<td width="120px">Surat Dari</td>
				<td><?php echo $row->dari;?></td>
				<td width="120px">Tanggal Surat</td>
				<td width="320px"><?php echo indonesian_date($row->tgl_surat);?></td>
			</tr>
			<tr>
				<td>Sifat</td>
				<td colspan="3"><?php echo $row->n_sifatsurat;?></td>
			</tr>
			<tr>
				<td>Perihal</td>
				<td colspan="3"><?php echo $row->prihal;?></td>
			</tr>
		</tbody>
	</table>
	<?php if($row->d_awal_kegiatan != '' && $row->d_akhir_kegiatan != '' && $row->v_kegiatan != ''){ ?>
	<table cellpadding="0px" style="width:300px;float:right;">
		<colgroup>
			<col class="con1" />
			<col class="con0" />
		</colgroup>
		<thead>
			<tr>
				<th colspan="2"><i class="iconfa-calendar"></i> &nbsp;Jadwal Kegiatan: </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td width="80px">Mulai</td>
				<td><?php echo indo_date_time($row->d_awal_kegiatan);?></td>
			</tr>
			<tr>
				<td>Akhir</td>
				<td><?php echo indo_date_time($row->d_akhir_kegiatan);?></td>
			</tr>
			<tr>
				<td>Lokasi</td>
				<td><?php echo $row->v_kegiatan;?></td>
			</tr>
		</tbody>
	</table>
	<?php } ?>
	<table>
		<tr>
			<td><br/><br/><br/></td>
			<td>
				<?php echo $row->isi_surat; ?>
				<em>File Surat Terlampir</em>
				<?php if($row->file_lampiran != ''){ ?>
				<em>File Koreksi Terlampir</em>
				<?php } ?>
			</td>
		</tr>
	</table>
	<div style="clear:both"></div>
	<?php } ?>
</div>
<?php } }  else { ?>
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page;?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
		</div>
	</div>
	<div class="msgauthor">
		<p><b>Terjadi Kesalahan!</b> Anda tidak mendapatkan disposisi surat ini.</p>
	</div>
</div>
<?php }} else { ?>
<div class="messageview">
	<div class="row-fluid messagesearch">
		<div class="span9">
			<a href="<?php echo base_url().$this->page;?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
		</div>
	</div>
	<div class="msgauthor">
		<p><b>Terjadi Kesalahan!</b> Data Tidak Ditemukan</p>
	</div>
</div>
<?php } ?>