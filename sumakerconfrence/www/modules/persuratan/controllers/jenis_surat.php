<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of welcome
 *
 * @author Yusuf
 */
class Jenis_surat extends ICA_AdminCont {

    var $page = "persuratan/jenis_surat/";
    var $view = "persuratan/jenissurat/";
    var $icon = "cogs";

    function __construct() {
        parent::__construct();
		$this->restrict('9');
        $this->tmjenissurat = new Tmjenis_surat();
        $this->load->helper('function_helper');
    }

    function index() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.dataTables.min.js"),
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Surat <span class='separator'></span> Jenis Surat",
            'title' => 'Master Jenis Surat',
            'content' => 'table',
            'sort' => 1
        );
        $this->load->view('template', $data);
    }

    function add() {
        $data = array(
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Surat <span class='separator'></span> <a href='".base_url().$this->page."'>Jenis Surat</a> <span class='separator'></span> Tambah",
            'title' => 'Master Jenis Surat',
            'content' => 'add',
            'sort' => 1
        );
        $this->load->view('template', $data);
    }

    function edit($id) {
		$data = array(
			'result' => $this->tmjenissurat->select('id', $id),
            'assets' => array(
                $this->lib_load_css_js->load_js(base_url(), "assets/js/", "jquery.validate.min.js")
            ),
            'breadcrubs' => " Konfigurasi <span class='separator'></span> Surat <span class='separator'></span> <a href='".base_url().$this->page."'>Jenis Surat</a> <span class='separator'></span> Edit",
            'title' => 'Master Jenis Surat',
            'content' => 'edit'
        );
        $this->load->view('template', $data);
    }

    function save($id = false) {
        if ($id == false) {
            if ($this->tmjenissurat->select('n_jnissurat', $this->input->post('n_jnissurat'))->num_rows() == 0) {
				
				//Upload template berkas
				if($_FILES['userfile']['name'] != '')
				{
					$name = str_replace(" ", "_", $this->input->post('n_jnissurat'));
					$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
					$endof = '.'.end($explode);
					$exten = 'uploads/template/'.$name.$endof;
					$config['file_name'] = $name;
					$config['allowed_types'] = 'doc|docx';
					$config['upload_path'] = 'uploads/template/';
					$config['max_size']	= '30000';
					$config['overwrite'] = true;
					$this->load->library('upload', $config);
					if(!$this->upload->do_upload())
					{
						echo '
							<script type="text/javascript">
								alert("'.$this->upload->display_errors().'");
								history.go(-1);
							</script>';
						$next = false;
					}
					else
					{
						$data = array(
							'n_jnissurat' => $this->input->post('n_jnissurat'),
							'id_surat' => $this->input->post('id_surat'),
							'file_surat' => $exten
						);
						$this->tmjenissurat->insert($data);
						redirect(base_url().$this->page);
					}
				}
				else
				{
					echo '
						<script type="text/javascript">
							alert("Anda belum memilih berkas untuk di-upload.");
							history.go(-1);
						</script>';
				}
            } else {
				echo '
					<script type="text/javascript">
						alert("Nama jenis surat sudah ada.");
						history.go(-1);
					</script>';
            }
        } else {
            if ($this->input->post('n_jnissurat_temp') == $this->input->post('n_jnissurat')) {
				$data = array(
					'id_surat' => $this->input->post('id_surat')
				);
				$this->tmjenissurat->update($id, $data);
				redirect(base_url().$this->page);
            } else {
                if ($this->tmjenissurat->select('n_jnissurat', $this->input->post('n_jnissurat'))->num_rows() == 0) {
					$data = array(
						'n_jnissurat' => $this->input->post('n_jnissurat'),
						'id_surat' => $this->input->post('id_surat')
					);
					$this->tmjenissurat->update($id, $data);
					redirect(base_url().$this->page);
                } else {
					echo '
						<script type="text/javascript">
							alert("Nama jenis surat sudah ada.");
							history.go(-1);
						</script>';
                }
            }
        }
    }

	function save_file($id)
	{
		if ($_FILES['userfile']['name'] != "") 
		{
			$n_jnissurat = $this->tmjenissurat->where("id = '".$id."'")->get()->n_jnissurat;
			$name = str_replace(" ", "_", $n_jnissurat);
			$explode = explode(".", str_replace(' ', '', $_FILES['userfile']['name']));
			$endof = '.'.end($explode);
			$exten = 'uploads/template/'.$name.$endof;
			$config['file_name'] = $name;
			$config['allowed_types'] = 'doc|docx';
			$config['upload_path'] = 'uploads/template/';
			$config['max_size']	= '30000';
			$config['overwrite'] = true;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload())
			{
				echo '
					<script type="text/javascript">
						alert("'.$this->upload->display_errors().'");
						history.go(-1);
					</script>';
			}
			else
			{
				$temp_file = $this->tmjenissurat->where("id = '$id'")->get()->file_surat;
				if(file_exists('./'.$temp_file)){
					unlink('./'.$temp_file);
				}
				$data = array(
					'file_surat' => $exten
				);
				$this->tmjenissurat->update($id, $data);
				redirect(base_url().$this->page);
			}
		}
		else 
		{
            echo '
			<script type="text/javascript">
				alert("File Surat Harus Diisi");
				history.go(-1);
			</script>';
        }
	}
	
    function delete($id) {
        $this->tmjenissurat->delete($id);
    }

    function data() {
        $this->load->library('datatables');
        echo $this->datatables
                ->select('
					tmjenis_surat.id,
					tmjenis_surat.n_jnissurat,
					tmjenis_surat.file_surat
				')
                ->from('tmjenis_surat')
				->edit_column('tmjenis_surat.file_surat', '<center>
					<a href="'.base_url().'$1"><i class="icon-file" title="File Template"></i></a>
				<center>','tmjenis_surat.file_surat')
                ->add_column('aksi', '<center>
					<a href="' . base_url() . $this->page . 'edit/$1"><i class="icon-pencil" title="Edit data"></i></a>
					<a id="delete" href="#" to="' . base_url() . $this->page . 'delete/$1"><i class="icon-remove" title="Hapus data"></i></a>
				</center>', 'tmjenis_surat.id')
                ->generate();
    }

}