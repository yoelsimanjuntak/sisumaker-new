<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#send').live('click', function(){
			parent.jQuery.fancybox.close();
			parent.location.href = base + 'main/inbox/setujui/<?php echo $id;?>';
		})
	});
</script>
<table class="table table-bordered table-invoice">
	<colgroup>
		<col class="con1" />
		<col class="con0" />
	</colgroup>
	<tbody>
		<tr>
			<td width="140px">TTD</td>
			<td>Dikirim Ke</td>
		</tr>
		<tr>
			<td>
				<?php if($ttd != ''){ ?>
				<img src="<?php echo base_url().$ttd;?>" alt=""/>
				<?php } ?>
			</td>
			<td>
				<?php if($result->num_rows() != 0){ foreach($result->result() as $row){ ?>
				<p><?php echo $row->n_jabatan.' '.$row->n_unitkerja.' - '.$row->n_pegawai;?></p>
				<?php }}else{ ?>
				<p class="text-error">
					Kami tidak menemukan ID TU untuk pengirim surat.<br/>
					Silahkan hubungi Admin SISUMAKER
				</p>
				<?php } ?>
			</td>
		</tr>
	</tbody>
</table>
<?php if($result->num_rows() != 0){ ?>
<a href="#" class="btn btn-primary pull-right" id="send"><i class="iconfa-check"></i> Setujui Surat</a>
<?php }else{ ?>
<a href="#" class="pull-right"><i class="iconfa-remove"></i> Error!</a>
<?php } ?>
<sup>*) Selanjutnya surat akan diberikan QRCode, Tanda Tangan, Stempel, dan dilakukan Penyekenan yang selanjutnya akan dikirim ketujuan.</sup>
<div style="clear:both"></div>