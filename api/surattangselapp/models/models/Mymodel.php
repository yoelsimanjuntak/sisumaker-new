<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mymodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function select($table, $key = false, $id = false)
    {
        if ($id == false) {
            return $this->db
                ->get($table);
        } else {
            return $this->db
                ->where($key, $id)
                ->get($table);
        }
    }

    public function surat_masuk($pegawai_id, $num, $ofs, $where = null)
    {
        if (!$where) {
            $where = '';
        }
        return $this->db
            ->query('
				SELECT
					tr_surat_penerima.id,
					tr_surat_penerima.d_entry,
					tr_surat_penerima.status,
					tmpegawai.n_pegawai,
					tr_surat_penerima.id_persuratan as id_persuratan,
					tmpersuratan.dari,
					tmpersuratan.tgl_surat,
					tmpersuratan.tgl_terima_surat,
					tmpersuratan.prihal,
					tmpersuratan.id_compose,
					tmpersuratan.id_sifatsurat,
                    tmsifat_surat.n_sifatsurat,
                    tmpersuratan.status_setujui,
					tmuser.photo
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_dari = tmpegawai.id
				JOIn tmuser ON tmpegawai.tmuser_id = tmuser.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
				WHERE
					tr_surat_penerima.id_pegawai_ke = ' . $pegawai_id . $where . '
				ORDER BY tr_surat_penerima.d_entry DESC
				LIMIT ' . $ofs . ', ' . $num);
    }

    public function surat_masukGroup($pegawai_id, $where = null)
    {
        if (!$where) {
            $where = '';
        }
        return $this->db
            ->query('
				SELECT
					COUNT(*) as jumlah
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_dari = tmpegawai.id
				JOIn tmuser ON tmpegawai.tmuser_id = tmuser.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
                WHERE
                tr_surat_penerima.status = 0 AND
					tr_surat_penerima.id_pegawai_ke = ' . $pegawai_id . $where . '
				');
    }

    public function surat_masukGroupTtd($pegawai_id, $where = null)
    {
        if (!$where) {
            $where = '';
        }
        return $this->db
            ->query('
				SELECT
					COUNT(*) as jumlah
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_dari = tmpegawai.id
				JOIn tmuser ON tmpegawai.tmuser_id = tmuser.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
                WHERE
                tmpersuratan.status_setujui = 0 AND
					tr_surat_penerima.id_pegawai_ke = ' . $pegawai_id . $where . '
				');
    }

    public function c_all_surat_masuk($pegawai_id)
    {
        $query = $this->db
            ->query("
				SELECT
					count(tr_surat_penerima.id) as jumlah
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_dari = tmpegawai.id
				JOIn tmuser ON tmpegawai.tmuser_id = tmuser.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
				WHERE
					tr_surat_penerima.id_pegawai_ke = '" . $pegawai_id . "'");
        return $query->row()->jumlah;
    }

    public function c_new_surat_masuk($pegawai_id)
    {
        $query = $this->db
            ->query("
				SELECT
					count(tr_surat_penerima.id) as jumlah
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_dari = tmpegawai.id
				JOIn tmuser ON tmpegawai.tmuser_id = tmuser.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
				WHERE
					tr_surat_penerima.id_pegawai_ke = '" . $pegawai_id . "'
					AND tr_surat_penerima.status = 0");
        return $query->row()->jumlah;
    }

    public function surat_keluar($pegawai_id, $num, $ofs, $where = null)
    {
        if (!$where) {
            $where = '';
        }
        return $this->db
            ->query('
				SELECT
					tr_surat_penerima.id,
					tr_surat_penerima.d_entry,
					tr_surat_penerima.status,
					tmpegawai.n_pegawai,
					tr_surat_penerima.id_persuratan as id_persuratan,
					tmpersuratan.dari,
					tmpersuratan.tgl_surat,
					tmpersuratan.tgl_terima_surat,
					tmpersuratan.prihal,
					tmpersuratan.id_compose,
					tmpersuratan.id_sifatsurat,
					tmsifat_surat.n_sifatsurat,
					tmuser.photo
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_ke = tmpegawai.id
				JOIn tmuser ON tmpegawai.tmuser_id = tmuser.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
				WHERE
					tr_surat_penerima.id_pegawai_dari = ' . $pegawai_id . $where . '
				ORDER BY tr_surat_penerima.d_entry DESC
				LIMIT ' . $ofs . ', ' . $num);
    }

    public function c_all_surat_keluar($pegawai_id)
    {
        $query = $this->db
            ->query("
				SELECT
					count(tr_surat_penerima.id) as jumlah
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_ke = tmpegawai.id
				JOIn tmuser ON tmpegawai.tmuser_id = tmuser.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
				WHERE
					tr_surat_penerima.id_pegawai_dari = '" . $pegawai_id . "'");
        return $query->row()->jumlah;
    }

    public function c_new_surat_keluar($pegawai_id)
    {
        $query = $this->db
            ->query("
				SELECT
					count(tr_surat_penerima.id) as jumlah
				FROM tr_surat_penerima
				JOIN tmpersuratan ON tr_surat_penerima.id_persuratan = tmpersuratan.id
				JOIN tmpegawai ON tr_surat_penerima.id_pegawai_ke = tmpegawai.id
				JOIn tmuser ON tmpegawai.tmuser_id = tmuser.id
				JOIN tmsifat_surat ON tmpersuratan.id_sifatsurat = tmsifat_surat.id
				WHERE
					tr_surat_penerima.id_pegawai_dari = '" . $pegawai_id . "'
					AND tr_surat_penerima.status = 0");
        return $query->row()->jumlah;
    }

    public function read($id_persuratan)
    {
        return $this->db
            ->select('
				tmpersuratan.id,
				tmpersuratan.no_surat,
				tmpersuratan.dari,
				tmpersuratan.tgl_terima_surat,
				tmpersuratan.tgl_surat,
				tmpersuratan.no_agenda,
				tmpersuratan.isi_surat,
				tmpersuratan.dasar,
				tmpersuratan.status_setujui,
				tmpersuratan.id_skpd_in,
				tmpersuratan.id_skpd_out,
				tmpersuratan.status,
				tmpersuratan.dasar,
				tmpersuratan.d_awal_kegiatan,
				tmpersuratan.d_akhir_kegiatan,
				tmpersuratan.v_kegiatan,
				tmpersuratan.id_compose,
				tmsifat_surat.n_sifatsurat,
				tmpersuratan.prihal,
				tmpersuratan.file,
				tmpersuratan.file_lampiran,
				tmpersuratan.c_lintas_skpd,
				tmjenis_surat.n_jnissurat,
				tmpersuratan.id_pegawai_setujui,
				tmpersuratan.id_pegawaittd
			')
            ->from('tmpersuratan')
            ->where('tmpersuratan.id', $id_persuratan)
            ->join('tmsifat_surat', 'tmpersuratan.id_sifatsurat = tmsifat_surat.id')
            ->join('tmjenis_surat', 'tmpersuratan.id_jnssurat = tmjenis_surat.id')
            ->get();
    }

    public function history_all($id_persuratan)
    {
        return $this->db
            ->select('
				tr_surat_penerima.id_pegawai_dari,
                tr_surat_penerima.id_pegawai_ke,
                tr_surat_penerima.file_coretan,
				tr_surat_penerima.file_surat_koreksi,
				tr_surat_penerima.lampiran_file_koreksi,
				tr_surat_penerima.catatan,
				tr_surat_penerima.d_entry,
				ke.n_pegawai as ke_pegawai,
				dari.n_pegawai as dari_pegawai,
				rake.photo as photo,
				(SELECT count(a.id) from tr_surat_penerima as a where a.d_entry = tr_surat_penerima.d_entry) as num
			')
            ->from('tr_surat_penerima')
            ->where('tr_surat_penerima.id_persuratan', $id_persuratan)
            ->join('tmpegawai as ke', 'tr_surat_penerima.id_pegawai_ke = ke.id')
            ->join('tmpegawai as dari', 'tr_surat_penerima.id_pegawai_dari = dari.id')
            ->join('tmuser as rake', 'dari.tmuser_id = rake.id')
            ->order_by('tr_surat_penerima.d_entry', 'asc')
            ->group_by('tr_surat_penerima.d_entry')
            ->get();
    }

    public function read_to($id_persuratan, $d_entry, $cc)
    {
        return $this->db
            ->select('
				ke.n_pegawai as ke_pegawai
			')
            ->from('tr_surat_penerima')
            ->where('tr_surat_penerima.id_persuratan', $id_persuratan)
            ->where('tr_surat_penerima.d_entry', $d_entry)
            ->where('tr_surat_penerima.cc', $cc)
            ->join('tmpegawai as ke', 'tr_surat_penerima.id_pegawai_ke = ke.id')
            ->get();
    }

    public function disposisi($id)
    {
        return $this->db
            ->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmpegawai.telp,
				tmskpd.id_kirim,
				tmjabatan.id as id_jabatan,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
            ->from('truser_kirim')
            ->where('truser_kirim.id_user ', $id)
            ->join('tmpegawai', 'truser_kirim.id_kirim = tmpegawai.tmuser_id')
            ->join('tmjabatan', 'tmpegawai.tmjabatan_id = tmjabatan.id')
            ->join('tmunitkerja', 'tmpegawai.tmunitkerja_id = tmunitkerja.id')
            ->join('tmskpd', 'tmpegawai.tmskpd_id = tmskpd.id')
            ->order_by('tmjabatan.id', 'asc')
            ->get();
    }

    public function penandatangan_sekda()
    {
        $jabatan_id = [1, 10, 2, 8];
        return $this->db
            ->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
            ->from('tmpegawai')
            ->where('tmpegawai.tmskpd_id', 17)
            ->where_in('tmpegawai.tmjabatan_id', $jabatan_id)
            ->join('tmjabatan', 'tmpegawai.tmjabatan_id = tmjabatan.id')
            ->join('tmunitkerja', 'tmpegawai.tmunitkerja_id = tmunitkerja.id')
            ->order_by('tmjabatan.no_urut', 'asc')
            ->get();
    }

    public function sifat_surat()
    {
        return $this->db
        ->select('
            id,
            n_sifatsurat
        ')
        ->from('tmsifat_surat')

        ->get();
    }

    public function penandatangan_skpd($skpd_id)
    {
        $jabatan_id = [3, 4, 5, 12, 13];
        return $this->db
            ->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
            ->from('tmpegawai')
            ->where('tmpegawai.tmskpd_id', $skpd_id)
            ->where_in('tmpegawai.tmjabatan_id', $jabatan_id)
            ->join('tmjabatan', 'tmpegawai.tmjabatan_id = tmjabatan.id')
            ->join('tmunitkerja', 'tmpegawai.tmunitkerja_id = tmunitkerja.id')
            ->order_by('tmjabatan.no_urut', 'asc')
            ->get();
    }

    public function id_tu($skpd_id)
    {
        return $this->db
            ->select('
				tmpegawai.id,
				tmpegawai.n_pegawai,
				tmpegawai.telp,
				tmjabatan.n_jabatan,
				tmunitkerja.n_unitkerja
			')
            ->where('tmpegawai.tmskpd_id', $skpd_id)
            ->where('tmuser_userauth.userauth_id', 5)
            ->from('tmpegawai')
            ->join('tmuser', 'tmpegawai.tmuser_id = tmuser.id')
            ->join('tmuser_userauth', 'tmuser.id = tmuser_userauth.tmuser_id')
            ->join('tmjabatan', 'tmpegawai.tmjabatan_id = tmjabatan.id')
            ->join('tmunitkerja', 'tmpegawai.tmunitkerja_id = tmunitkerja.id')
            ->limit(1)
            ->get();
    }

    public function nomor_save($id_skpd, $id_jenissurat)
    {
        $thn = date('y');
        $query = $this->db
            ->select('id, nomor')
            ->from('penomoran')
            ->where('id_jenissurat', $id_jenissurat)
            ->where('id_skpd', $id_skpd)
            ->where('tahun', $thn)
            ->get();

        if ($query->num_rows() != 0) {
            foreach ($query->result() as $row) {
                $nomor = $row->nomor + 1;
                $id = $row->id;
            }
            $form = ['nomor' => $nomor];
            $this->db
                ->where('id', $id)
                ->update('penomoran', $form);
            $return = $nomor;
        } else {
            $form = [
                'id_jenissurat' => $id_jenissurat,
                'id_skpd' => $id_skpd,
                'tahun' => $thn,
                'nomor' => 1
            ];
            $this->db->insert('penomoran', $form);
            $return = 1;
        }
        $a = strlen($return);
        for ($i = 4; $i > $a; $i--) {
            $return = '0' . $return;
        }
        return $return;
    }

    public function profile($auth)
    {
        return $this->db
        ->query(
            '
            SELECT
                tmuser.username,
                tmuser.realname,
                tmuser.photo,
                tmpegawai.nip,
                tmpegawai.telp
            FROM tmuser
            join tmpegawai on tmuser.id =  tmpegawai.tmuser_id
            WHERE
                tmuser.id = ' . $auth
           );
    }

    public function profile_edit($auth, $pegawai_id, $name, $nip, $telp, $username, $password = null)
    {
        $this->set_nip($nip, $auth, $pegawai_id);

        //tmuser
        $nameU = $this->db->set('realname', $name);
        $nameU->where('id', $auth);
        
        if ($nameU->update('tmuser') && $this->set_nama($name, $pegawai_id) && $this->set_telp($telp, $pegawai_id) && $this->set_username($auth, $username)) {
            if ($password != null) {
                $this->password_edit($auth, $password);
            }
            return true;
        } else {
            return false;
        }
    }

    public function set_nip($nip, $auth, $pegawai_id)
    {
        $query = $this->db
            ->select('nip')
            ->from('tmpegawai')
            ->where('tmuser.id', $auth)
            ->where('tmpegawai.nip', $nip)
            ->where('tmpegawai.id != '.$pegawai_id)
            ->join('tmuser', 'tmpegawai.tmuser_id = tmuser.id')
            ->get();

        if ($query->num_rows() != 0) {
            return false;
        } else {
            $nipU = $this->db->set('nip', $nip);
            $nipU->where('id', $pegawai_id);

            if ($nipU->update('tmpegawai')) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function set_telp($telp, $pegawai_id)
    {
        $telpU =$this->db->set('telp', $telp);
        $telpU->where('id', $pegawai_id);

        if ($telpU->update('tmpegawai')) {
            return true;
        } else {
            return false;
        }
    }

    public function set_nama($nama, $pegawai_id)
    {
        $q1 = $this->db->set('n_pegawai', $nama);
        $q1->where('id', $pegawai_id);

        if ($q1->update('tmpegawai')) {
            return true;
        } else {
            return false;
        }
    }

    public function password_edit($auth, $passwordb)
    {
        $pass = $this->db->set('password', md5($passwordb));
        $pass->where('id', $auth);

        if ($pass->update('tmuser')) {
            return true;
        } else {
            return false;
        }
    }

    public function set_username($auth, $username)
    {
        $usernameU = $this->db->set('username', $username);
        $usernameU->where('id', $auth);

        if ($usernameU->update('tmuser')) {
            return true;
        } else {
            return false;
        }
    }

    public function check_id($auth)
    {
        $query = $this->db
            ->select('tmpegawai.id')
            ->from('tmpegawai')
            ->where('tmuser.id', $auth)
            ->join('tmuser', 'tmpegawai.tmuser_id = tmuser.id')
            ->get()->row();
        
        return $query->id;
    }

    public function chck_pwd($auth, $passwordl)
    {
        $plama = $this->db
                ->query(
                    '
                SELECT
                    *
                FROM tmuser
                WHERE
                    tmuser.id = ' . $auth
            )->row()->password;
        if (md5($passwordl) != $plama) {
            return false;
        } else {
            return true;
        }
    }
}
