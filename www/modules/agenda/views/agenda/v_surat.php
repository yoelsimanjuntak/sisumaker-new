<div class="widgetbox box-inverse">	
	<h4 class="widgettitle nopadding">&nbsp;</h4>
	<div class="widgetcontent wc1">
		<?php if ($result->num_rows() != 0) { foreach ($result->result() as $row) { 
		$exp = explode(' ', $row->d_awal_kegiatan);
		$pecah1 = explode("-", $exp[0]);
		$date1 = $pecah1[2];
		$month1 = $pecah1[1];
		$year1 = $pecah1[0];
		$pecah2 = explode("-", date('Y-m-d'));
		$date2 = $pecah2[2];
		$month2 = $pecah2[1];
		$year2 =  $pecah2[0];
		$jd1 = GregorianToJD($month1, $date1, $year1);
		$jd2 = GregorianToJD($month2, $date2, $year2);
		$selisih = $jd2 - $jd1;
		
		if($selisih >= 0){
			if($row->c_absen == 0){ ?>	
			<a href="<?php echo base_url().$this->page.'absen_submit/'.$id;?>" class="btn btn-success"><i class="iconfa-check"></i> Absen hadir</a>
			<?php }else{ ?>
			<b>Anda telah absen pada tanggal <?php echo indo_date_time($row->d_absen);?>.</b>
			<?php }
		} ?>
		<div class="messageview">
			<div class="msgauthor">
				<div class="thumb"><img src="<?php echo base_url().'uploads/photo/'.$row->photo; ?>" alt="" /></div>
				<div class="authorinfo">
					<span class="date pull-right"><?php echo indonesian_date($row->d_entry);?></span>
					<h5><strong><?php echo $row->n_pegawai;?></strong></h5>
					<span class="to">to <?php echo $this->session->userdata('realname');?></span>
				</div>
			</div>
			<div class="msgbody">
				<table class="table table-bordered table-invoice table-invoice">
					<colgroup>
						<col class="con1" />
						<col class="con0" />
						<col class="con1" />
						<col class="con0" />
					</colgroup>
					<tbody>
						<tr>
							<td width="120px">Surat Dari</td>
							<td><?php echo $row->dari;?></td>
							<td width="120px">Tanggal Surat</td>
							<td width="320px"><?php echo indonesian_date($row->tgl_surat);?></td>
						</tr>
						<tr>
							<td>Sifat</td>
							<td colspan="3"><?php echo $row->n_sifatsurat;?></td>
						</tr>
						<tr>
							<td>Perihal</td>
							<td colspan="3"><?php echo $row->prihal;?></td>
						</tr>
					</tbody>
				</table>
				<?php if($row->d_awal_kegiatan != '' || $row->d_akhir_kegiatan != '' || $row->v_kegiatan != ''){ ?>
				<table class="table table-bordered pull-right" style="width:300px">
					<colgroup>
						<col class="con1" />
						<col class="con0" />
					</colgroup>
					<thead>
						<tr>
							<th colspan="2"><i class="iconfa-calendar"></i> &nbsp;Jadwal Kegiatan: </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="80px">Mulai</td>
							<td><?php echo indo_date_time($row->d_awal_kegiatan);?></td>
						</tr>
						<tr>
							<td>Akhir</td>
							<td><?php echo indo_date_time($row->d_akhir_kegiatan);?></td>
						</tr>
						<tr>
							<td>Keterangan</td>
							<td><?php echo $row->v_kegiatan;?></td>
						</tr>
					</tbody>
				</table>
				<?php }echo $row->isi_surat; ?>
				<b><a href="<?php echo base_url().$row->file;?>" class="btn"><i class="iconfa-paper-clip"></i> File Surat</a></b>
			</div>
		</div>
		<?php } } else { ?>
		<div class="messageview">
			<div class="row-fluid messagesearch">
				<div class="span9">
					<a href="<?php echo base_url().$this->page;?>" class="btn"><span class="iconfa-chevron-left"></span> Back</a>
				</div>
			</div>
			<div class="msgauthor">
				<p><b>Terjadi Kesalahan!</b> Data Tidak Ditemukan</p>
			</div>
		</div>
		<?php } ?>
		<div style="clear:both"></div>
	</div>
</div>